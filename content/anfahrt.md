---
title: "Anfahrt"
url: "/anfahrt"
ShowReadingTime: false
hidemeta: true
ShowBreadCrumbs: false
---

Schön, dass du mich besuchen willst. Aufgrund der *akuten* Unfähigkeit, aller Navigationshersteller, habe ich eine Seite gebaut, um die einwandfreie Anreise zu ermöglichen.

Genaue Adresse:

Niederösterreich-Ring 5/9/1  
3100 St. Pölten


## Links zu gängigen Navigations-Apps

Ich bekräftige nochmal, dass die Anreise mit Google/Apple Maps aufgrund der Ortsunkenntnis (kennt keine Busse & Radwege) nie vernünftig funktioniert.

**Update 23.09.2023**: Apple Maps kennt nun die Stadtbusse, manche Routen sind noch etwas verrückt, aber prinzipiell nicht mehr *komplett* unbrauchbar.

- [Google Maps](https://goo.gl/maps/U1SbxUxtawvxnN6DA)
- [Apple Maps](https://maps.apple.com/?ll=48.199005,15.640349&q=Stoal&t=m)
- [OpenStreetMap](https://www.openstreetmap.org/#map=19/48.19903/15.64051)

## Öffentliche Anreise

Ausgangspunkt ist hier immer St. Pölten Hbf, bis dorthin müsst ihr schon selbst finden.

Ab hier gilt die Qual der Wahl; bei nüchternen Zustand ist folgende Variante zu empfehlen:

### Nextbike

Sowohl beim Hauptbahnhof als auch bei meinem Wohnquartier existiert eine Nextbike-Station. Bis zu **30 Minuten Fahrtdauer** sind diese Leihräder in Niederösterreich gratis. Du kannst dir einfach eines beim Hbf ausborgen und dann bei der Station Mühlbach Ost zurückgeben.

- [Link zur iOS-App](https://itunes.apple.com/de/app/nextbike/id504288371?mt=8)
- [Link zur Android-App](https://play.google.com/store/apps/details?id=de.nextbike)

Zur Navigation am Rad kannst du dann wiederrum Google Maps/Apple Maps verwenden, siehe [hier](#links-zu-gängigen-navigations-apps)

### Bus

Da die Buspläne recht kompliziert sind und selbst für mich unverständlich, hier eine kleine Tabelle zur Orientierung:

#### Wochentags

Betriebszeiten 100: 05-17 Uhr  
Betriebszeiten 2: 05-22 Uhr  
Betriebszeiten 9: 05-20 Uhr

|Entfernung zu BST | Station                   | 100 -> Staudratgasse | 100 -> Landhaus | Bus 2 -> Harland | Bus 9 -> Rudolf-Tornar-Straße
|-----------|---------------------------|----------|----------|----------|----------|
|   1,8km   | St. Pölten Hbf (Nord)     | :18, :48 | :03, :34 |    -     |    -     |
|   **1,5km**   | **St. Pölten Hbf (Vorplatz)** | **:19, :49** | **:04, :35** | **:12, :42** | **:00, :30** |
|   600m    | Landhaus                  | :24, :54 | :09, :40 | :16, :46 | :04, :34 |
|   500m    | Staudratgasse             | :26, :56 | :15, :46 |    -     | :07, :37 |
|   150m    | Fachmarkt Salzer          |    -     |    -     | :18, :48 |    -     |
|   200m    | Arbeitergasse             |    -     |    -     |    -     | :09, :39 |

#### Samstags

normalerweise schneller via [Nextbike](#nextbike), aber der Vollständigkeit halber:

Betriebszeiten 100: - (Danke für nix, Udo Landbauer!)  
Betriebszeiten 2: 06-22 Uhr  
Betriebszeiten 4: 06-22 Uhr
Betriebszeiten 9: 06-21 Uhr  

|Entfernung zu BST | Station                   | Bus 2 -> Harland | Bus 4 -> Unterradlberg | Bus 6 -> Traisenpark | Bus 9 -> Rudolf-Tornar-Straße
|-----------|---------------------------|----------|----------|----------|----------|
|   1,8km   | St. Pölten Hbf (Nord)     | :07, :46 |    -     |    -     |    -     |
|   **1,5km**   | **St. Pölten Hbf (Vorplatz)** | **:12, :42** | **:25, :55** | **:10, :40** | **:00** |
|   600m    | Landhaus                  | :16, :46 | :28, :58 | :13, :43  | :04 |
|   620m    | Oberwagram Feuerwehr      |    -     | :30, :00 |    -     |    -     |
|   550m    | Purkersdorfer Straße/Mühlstraße |    -     |    -     | :15, :45 |    -     |
|   500m    | Staudratgasse             |    -     |    -     |    -     | :07 |
|   150m    | Fachmarkt Salzer          | :18, :48 |    -     |    -     |    -     |
|   200m    | Arbeitergasse             |    -     |    -     |    -     | :09 |

#### Sonntags

normalerweise schneller via [Nextbike](#nextbike), aber der Vollständigkeit halber:

Betriebszeiten 100: - (Danke für nix, Udo Landbauer!)  
Betriebszeiten 2: 07-20 Uhr  
Betriebszeiten 9: 07-20 Uhr  

|Entfernung | Station                   | Bus 2 -> Harland | Bus 6 -> Traisenpark | Bus 9 -> Rudolf-Tornar-Straße
|-----------|---------------------------|----------|----------|----------|
|   1,8km   | St. Pölten Hbf (Nord)     | :07 |    -     |
|   **1,5km**   | **St. Pölten Hbf (Vorplatz)** | **:12** | :40 | **:30** |
|   600m    | Landhaus                  | :16 | :43 | :34 |
|   550m    | Purkersdorfer Straße/Mühlstraße | - | :45 | - |
|   500m    | Staudratgasse             |    -     | - | :37 |
|   150m    | Fachmarkt Salzer          | :18 | - |    -     |
|   200m    | Arbeitergasse             |    -     | - | :39 |

### Zu Fuß/Fahrrad

Hier kannst du auch das Navi von Apple/Google verwenden. Über die Bruno-Kreisky-Straße ist man rund 5 Minuten schneller als wie über das Landhaus.

Am besten verlässt du den St. Pölten Hbf beim Ausgang Ost und folgst danach den Schildern in Richtung Landesregierung, dazu folgt man der Parkpromenade im Uhrzeigersinn und überquert den Neugebäudeplatz. Wenn du vor dem Landhaus stehst, gibt es nun die Variante über den Landhaussteg oder über die Straße. Am kürzesten ist die Variante über die Straße, dazu folgst du einfach der Wiener Straße stadtauswärts in Richtung Osten. Sobald du bei der gelben Tankstelle angelangt bist, biegst du Rechts ab und folgst der Bruno-Kreisky-Straße bis zu einem braunen Wohnbau. Im Mühlbachquartier bin ich bei Stiege 9 zu Hause.

## Mit dem Auto

Das ist eine schlechte Idee. Aufgrund dessen, dass wir in St. Pölten ressourcenschonend arbeiten, können wir nicht für jeden etwaigen Besucher einen Parkplatz hinbetonieren. Es stehen nur ein paar Besucherparkplätze zur Verfügung, deren Benutzung auf 2 Stunden limitiert ist. Außerdem, stell dir vor, du trinkst ein Bier mit mir..

Falls du es dir noch immer nicht anders überlegt hast, hier eine kleine Übersicht der Anfahrtsmöglichkeiten:
- S33 Abfahrt St. Pölten-Ost: Danach nicht Richtung Oberwagram einbiegen sondern auf der Umfahrung bleiben. Beim Kreisverkehr die erste Ausfahrt rechts nehmen und bei der nächsten Ampel links. Hier kannst du dir einen Parkplatz suchen, falls keiner vorhanden ist, bitte mich einfach anrufen.