---
title: "Impressum"
url: "/impressum"
ShowReadingTime: false
hidemeta: true
ShowBreadCrumbs: false
---

Servus, schön, dass du dir mein Impressum durchlesen willst oder musst, weil dir dein Arbeitgeber gesagt hat, du sollst eine Unterlasssungserklärung
oder sonst einen Mist vorbereiten. Dann hast du natürlich Pech und es ist nicht so schön. Wie heißt's so gerne: Augen auf bei der Berufs- und Partnerwahl.

## Adresse

Weil's sein muss aber ich sag's gleich, ich bin nicht oft daheim und die Briefbomben schickt bitte woanders hin.

Bernhard Steindl  
3100 St. Pölten

## Elektronischer Kontakt

[E-Mail](mailto:mail@bernhardsteindl.at); die Keys für Verschlüsselung dafür befinden sich am [Fileserver](https://cdn.bernhardsteindl.at/keys/bsteindl-public.asc) oder bei dem [Keyserver](https://keys.openpgp.org/vks/v1/by-fingerprint/BF864BE2913B04CD01B91F576DF568762B0DC626).

### Social Media

Auf der Startseite findet ihr alle relevanten Links dazu.

### Messenger

Signal Messenger: @bst.99  
WhatsApp, etc. auf Anfrage (per Mail bspw.)
