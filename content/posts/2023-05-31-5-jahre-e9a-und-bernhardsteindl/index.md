---
# Title
title: "5 Jahre e9a.at und bernhardsteindl.at - Ein Rückblick"
# Enter the author's name
author: Bernhard Steindl
# Customize URI; default: post/filename
url: /5-jahre-e9a-und-bernhardsteindl/
# Date
date: 2023-05-31T17:00:00+01:00
# Is this draft?
draft: false
# Show Table of Content?
showToc: true
# Available categories. Please comment out!
categories:
#- Admin-Tipps
- Allgemein
#- Arch Linux
#- Debian
#- Denkt's noch!
#- Grundlagen
#- Hardware
#- mariadb
#- News
#- nginx
#- Politik
#- Projekte
#- Proxmox
#- Rants
#- Satire
#- Synology
#- Tor
#- Ubiquiti Unifi
#- Windows
#- Wordpress
#- ZFS

---

# Jubiläum

{{< figure align=center src="images/e9alogo.png" caption="Das alte Logo des e9a-Blogs." >}}

Ich konnte es selbst nicht glauben, aber tatsächlich ist am heutigen Tag dieser Blog 5 Jahre alt! Auch wenn sich in dieser Zeit viel getan hat und sich auch weiterhin die Ausrichtung des Blogs laufend ändert, ist doch zumindest der (Haupt-)autor gleich geblieben.

## Mehrmaliger Domainwechsel

Dieser Blog und die jeweiligen Artikel haben bereits mehrere "Standorte" im WWW gefunden und bis dato habe ich es geschafft, dass jegliche Hyperlinks seit den letzten 5 Jahren trotz mehrmaliger Wechsel weiterhin funktionieren.

Eine kleine Auflistung:
- blog.e9a.at
- e9a.at
- bernhardsteindl.eu und schlussendlich
- bernhardsteindl.at

boten Platz für all die Inhalte, die es hier zu finden gibt.

## Technologiewechsel

Von Anbeginn war dieser Blog als Sprachrohr meiner Meinung und einiger technischer Artikel (unter anderem von Christoph Scheidl) gedacht. Als Techniker interessiert mich auch die technische Funktionalität des Blogs und daher wurde immer darauf geachtet, bestmögliche Performance abzuliefern.

Technisch gesehen wurde im Jänner 2022 von WordPress auf ein "cleanes" Hugo-Template gewechselt, mit der Mitnahme aller Artikel sowie der alten Hyperlinks. Technisch gesehen ist dieses Setup viel einfacher, es benötigt weder Datenbank noch dynamische Generierung von Inhalten, daher konnte auch das Hosting der Haupt-Blogseite auf Gitlab Pages ausgelagert werden. Dies machte zwar den theoretischen Performancezuwachs wieder zunichte, aber sorgt für ununterbrochene Verfügbarkeit an jeder Ecke der Erde.

# Best of e9a/bernhardsteindl.at

Auch wenn viele Artikel gut (und noch viel mehr schlecht) sind, gibt es doch ein paar Artikel, die mir in Erinnerung geblieben sind und weiterhin hohe Rankings bei Suchmaschinen aufweisen.

- [Überwachungsstaat Kasernstraße 6: Meine Erfahrungen im BSH Krems](/ueberwachungsstaat-kasernstrasse-6/)
- [Aus Liebe zum Menschen: 9 Monate sind genug](/aus-liebe-zum-menschen-9-monate-sind-genug/)
- [Die Betonierer am Werk](/die-betonierer-am-werk-entlastungsspange-b3/)
- [Typisch Krems: Sudern und Busse verunglimpfen](/typisch-krems-betonieren-sudern-und-busse-verunglimpfen/)
- [Die HEROLD.at SMS-API](/die-herold-at-sms-api/)

# Zukünftige Ausrichtung

Zwischen dem 26.08.2022 und dem 31.05.2023 wurden keine Artikel veröffentlicht. Dies ist die längste Zeitspanne ohne Artikel (278 Tage), die dieser Blog jemals gesehen hat. Der Grund dafür sind große Änderungen in meinem Leben, die fordernd genug waren, als über Artikel nachzudenken. 

Die Frequenz wird sich in Zukunft merkbar steigern, dafür sorgen mehrere Ideen:
- "Radfahrhauptstadt St. Pölten" - Ein schlechter Witz
- Warum es aktiven Klimaprotest braucht
- St. Pölten - Wo sogar die Donau einen Bogen herum macht?
- Politische Zukunft des Landes
- Der demografische Hintergrund hinter dem "Fachkräftemangel"
- 3 Jahre Ende des Zivildienst: Wie sieht es jetzt aus?

Ich hoffe, dass ich in Zukunft zumindest einen Artikel im Monat veröffentlichen kann und ich interessante Themen beleuchten kann, für die manchen der Mut oder die Zeit fehlt :-)