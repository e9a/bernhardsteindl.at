---
title: Yubikey für KDE-Lockscreen als U2F (2. Faktor) verwenden
author: Christoph Scheidl
type: post
date: 2021-04-10T18:37:19+00:00
url: /yubikey-fuer-linux-login-als-u2f-2-faktor-verwenden/
categories:
  - Admin-Tipps
  - Grundlagen
  - Hardware

---
Ich möchte an dieser Stelle einfach auf den Artikel von Yubico [hier](https://support.yubico.com/hc/en-us/articles/360013708900-Using-Your-U2F-YubiKey-with-Linux) verweisen, und folgenden Tipp für KDE-Nutzer hinzufügen:

Ich habe anstelle der einzelnen Dateien im `/etc/pam.d/` Ordner einfach den Befehl `auth required pam_u2f.so` in die `/etc/pam.d/common-auth` geschrieben. Damit habe ich auch am KDE-Lockscreen die 2FA-Authentication. Falls jemand von euch Tipps hat, wie man das selektiver lösen kann, gerne her mit Vorschlägen. Ich habe einfach die Datei für den `kscreenlocker` nicht gefunden.