---
# Title
title: "Green Future Krems: Is eh scho wuascht"
# Enter the author's name
author: Bernhard Steindl
# Customize URI; default: post/filename
#url: /yubikey-fuer-linux-login-als-u2f-2-faktor-verwenden/
# Date
date: 2022-08-26T00:00:00+01:00
# Is this draft?
draft: false
# Show Table of Content?
showToc: true
# Available categories. Please comment out!
categories:
#- Admin-Tipps
#- Allgemein
#- Arch Linux
#- Debian
#- Denkt's noch!
#- Grundlagen
#- Hardware
#- mariadb
#- News
#- nginx
- Politik
#- Projekte
#- Proxmox
#- Rants
#- Satire
#- Synology
#- Tor
#- Ubiquiti Unifi
#- Windows
#- Wordpress
#- ZFS

---

# Green Future Krems

*Dies ist ein Artikel der Serie [Pappkameraden in Krems](/posts/2022-08-25-die-kremser-pappkameraden/). Sämtliche Inhalte genügen keinen journalistischen Ansprüchen wie Ausgewogenheit oder DoubleChecks, falls Sie dennoch falsche Informationen finden oder Anmerkungen zu den Artikeln haben, würde ich mich über eine [Rückmeldung](/impressum) sehr freuen.*

Die Liste entstand als Abspaltung von den Grünen, da der Spitzenkandidat *angeblich von der Landespartei ausgewählt wurde und der bisherige Kandidat abgesetzt wurde*. Dieser gründete nun Green Future Krems.

Ich konnte weder Wahlprogramm noch sonst irgendwas (ja nicht einmal eine Facebook-Seite) finden. Daher als die Katze im Sack aus Prinzip unwählbar. Ich frage mich schon, warum die jemand wählen sollte, wenn sie nichtmal den Aufwand für eine Website stemmen. Offensichtlich herrscht hier die Devise: *Is eh scho wuascht - Wenn ma's schaffen sama drinnan, wenn ned hoid ned*

Falls jemand ein Programm oder sonstige Informationen findet, würde ich mich über eine [Zusendung](/impressum) sehr freuen!

Die einzige Information, die ich sonst noch finden konnte, war auf der Website der Stadt Krems, bei denen man die Wahlvorschläge einsehen kann. [Link](https://www.krems.at/fileadmin/Dateien/Wahlen/Liste_7_-_Green_Future_Krems__GREENK_.pdf)

*Lesen Sie weitere Artikel der Serie [Pappkameraden in Krems](/posts/2022-08-25-die-kremser-pappkameraden/)!*