---
# Title
title: "ThinkPad X230: Flash custom BIOS from skulls"
# Enter the author's name
author: Bernhard Steindl
# Customize URI; default: post/filename
# Date
date: 2022-03-05T15:00:00+01:00
# Is this draft?
draft: false
# Show Table of Content?
showToc: true
# Available categories. Please comment out!
categories:
#- Admin-Tipps
#- Allgemein
#- Arch Linux
#- Debian
#- Denkt's noch!
#- Grundlagen
- Hardware
#- mariadb
#- News
#- nginx
#- Politik
#- Projekte
#- Proxmox
#- Rants
#- Satire
#- Synology
#- Tor
#- Ubiquiti Unifi
#- Windows
#- Wordpress
#- ZFS

---

# Beginning

I've got a few X230 to modify them and give them to my family as cheap mobile notebooks. For testing, I've flashed Skulls (Ready2Use coreboot) on these but forgot to flash the custom EC to support the old X220's keyboard and support custom batteries (as the original got very expensive and some high-quality alternatives are available). This seems to be impossible due my tour through the web until you use a hardware flasher, which isn't cheap either and requires a few hours of reading and understanding the basics.

# The requirements

My X230 has the following parts:

- i5-3320M
- BIOS: Skulls flashed via 1vyrain
- EC v1.10 (anything beyond 1.14 is okay though)
- Linux with flashrom >= 1.0
- Boot to Linux with the kernel param "iomem=relaxed" to allow acces to protected spaces

# Building yourself

The next steps are for those, who want to build the flash file yourself and understand what they do. If you are ridiculous, you can use my prebuilt ones from here [cdn.bernhardsteindl.at](https://cdn.bernhardsteindl.at/X230/READY2FLASH_x230_v2-60.tar.xz) and flash them with (remember to use kernel param iomem=relaxed for this boot)
```
sudo flashrom -p internal -l ./X230_v2_60.layout -i bios -w flashrom_bios.rom --noverify-all
```
After a reboot, Lenovo bios will complain about a few things (False time, some corrupted EFI things, ...) which can be fixed like stated [below]().

## Building a valid BIOS

flashrom requires a full 12MB flash file, but the original BIOS only has 4MB. This is due the layout of the X230 where a 4MB chip containes the BIOS and another 8MB chip contains the rest. This is called the *top* and the *bottom* chip in all manuals. Flashing of the **BOTTOM**-Chip is not allowed by 1vyrain-flashed BIOS due restrictions of the Intel ME. To eliminate ME and getting a *fully* freed X230 you will need a hardware flasher.

Indeed, with the explanation above, you can see that this is far more complicated as I though so you should think twice before you do anything. Sadly, I cannot gurantee that this work for your Notebook, but it should for all X30-Lenovo notebooks which are supported by 1vyrain.

Thankfully, the famous modder XY TECH has dumped out the BIOS completely and we will use his files here. First, choose a BIOS-Version you want to use:

- **v2.60: Compatible with 1vyrain, highly recommended (SHA256)**
- v2.72: Last version before spectre and meltdown (SHA256)
- v2.77: Latest X230 BIOS version (SHA256)
- you can use any BIOS you want, just use the .FL1 from the [Lenovo BIOS Update-CD](https://pcsupport.lenovo.com/us/en/products/laptops-and-netbooks/thinkpad-x-series-laptops/thinkpad-x230/downloads/ds029187) instead of my BIN files

It is recommended to use the v2.60 as it is fully compatible with the EC patches and 1vyrain. Download the valid BIOS-File (from [XYTECH](https://github.com/xy-tech/x330-bios/tree/main/stock/original) or from my [mirror](https://cdn.bernhardsteindl.at/X230/v2.60.bin)) and combine it with the valid bottom dump ([XYTECH](https://github.com/xy-tech/x330-bios/tree/main/stock/original), [mirror](https://cdn.bernhardsteindl.at/X230/bottom_stock.bin)).

*Note: If you don't have a modified X230 with a FHD mod or a X330 built by XYTECH, don't use his patched BIOS files unless you know what you are doing!*

For using flashrom we combine these with the command:

```
cat bottom_stock.bin v2.60.bin > flashrom_bios.rom
```

This file should have exact 12MB to be recognized for flashrom.

## Building a layout file

To flash only the top BIOS region and not trying to flash anything else, you need to get the right regions to flash. This is achieved with the [ifdtool](https://www.coreboot.org/Intel_Sandybridge_Build_Tutorial#Get_the_flash_layout)

```
git clone https://review.coreboot.org/coreboot 
cd coreboot
cd util/ifdtool
make

./ifdtool -f X230_v2_60.layout v2.60.bin
```
*Note: As we do only flash the BIOS regions, reading out anything else from the full 12MB file is not required*

Open this file and it should look like mine:

```
00000000:00000fff fd
00500000:00bfffff bios
00003000:004fffff me
00001000:00002fff gbe

```

If there are more regions that above, remove this lines!

## FLASHING [CAUTION!]

Now copy your flashrom_bios.rom and your layout file into the same directory. Verify, that you booted Linux with the kernel param "iomem=relaxed". Plug in your charger and your battery and execute:

```
sudo flashrom -p internal -l ./X230_v2_60.layout -i bios -w flashrom_bios.rom --noverify-all
```

This could take a while!

# Flashing BIOS again for correct EC

Most BIOS-Versions from Lenovo have a exact EC as they are compatible with. As we are only touching the BIOS region, EC got untouched and could be too new or to old for your BIOS-Version so it is recommended to flash the right BIOS again after our "Recovery"-Process.

For Linux, you have to use geteltorito to build a IMG for a Pendrive. Download a [Lenovo BIOS-ISO-File](https://pcsupport.lenovo.com/us/en/products/laptops-and-netbooks/thinkpad-x-series-laptops/thinkpad-x230/downloads/ds029187) and convert it:

```
geteltorito.pl -o penstick.img input-iso.iso 
```
Now use dd to give this on a drive: (Look with lsblk for your Drive before destroying anything!)

```
dd if=penstick.img of=/dev/sdX status=progress
```

Boot from the Stick and flash it again! You can use this step to rollback all your 1vyrain-related changes and sell this thing or whatever.

**FOR USING 1VYRAIN OR THE EC FLASH AGAIN STAY BEYOND BIOS 1.61 AND EC 1.15!**

#### Sources

- [Reddit](https://www.reddit.com/r/coreboot/comments/sv0eyg/restore_stock_bios_after_1vyrain/hy1sqv5/)
- [XYTECH](https://www.xyte.ch/support/x330-support/x330-bios/)