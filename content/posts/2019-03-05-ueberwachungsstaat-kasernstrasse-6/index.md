---
title: 'Überwachungsstaat Kasernstraße 6: Meine Erfahrungen im Bundesschülerheim Krems'
author: Bernhard Steindl
type: post
date: 2019-03-05T14:07:39+00:00
excerpt: 'Ein Erfahrungsbericht eines Internatsschülers im BSH Krems - die erschreckende Wahrheit über den Zustand'
url: /ueberwachungsstaat-kasernstrasse-6/

categories:
  - Politik
tags:
  - bsh krems
  - bundesschülerheim
  - erfahrungen
  - krems
  - maden
showToc: true
cover:
    image: "images/IMG_20180621_201704_996.jpg"
    # can also paste direct link from external site
    # ex. https://i.ibb.co/K0HVPBd/paper-mod-profilemode.png
    alt: "<alt text>"
    caption: "Eingang in das Bundesschülerheim"
    relative: false # To use relative path for cover image, used in hugo Page-bundles

---

{{<figure src=images/signal-attachment-2019-03-05-161931.jpeg >}}

**Es gibt zu diesem Artikel bereits ein [Update][1]. Ich unterlasse grobe Änderungen, da dadurch der Stil des Artikels verlorenginge.**

In den nächsten Punkten erzähle ich von Erfahrungen, Ereignissen und aktuellen Begebenheiten aus dem „Mädchengefängnis“. Vorab: Es ist nichts erfunden, trotzdem können einige Begebenheiten von der Realität abweichen, da die Betrachtungsweise eines jeden Menschen unterschiedlich ist. Ich erzähle aus der Sicht eines ehemaligen Mitglieds und aus Berichten von Freunden und Kollegen. **Ich betone ausdrücklich, dass all diese Dinge in meiner Zeit von September 2014 und Juni 2018 passiert sind.**

Dieser Artikel ist das Ergebnis von harter und zeitintensiver Recherchearbeit, es gibt unglaublich viele Geschichten und Vorfälle: Hier ein kleiner Auszug davon, was veröffentlicht werden darf

Falls dir auch etwas einfällt oder du selbst auch mal Schüler des BSHs gewesen bist, kannst du ganz unten, auch ohne Registrierung, deinen Senf dazu abgeben.

Ich bitte um eine ausdrückliche Trennung dieser Ereignisse von der anschließenden HTL und HLW. Diese haben mit diesen Vorfällen absolut nichts zu tun.

# 1. Das Mädchengefängnis

Jedem ist bekannt, das im BSH Mädchen und Burschen unterschiedlich behandelt werden. Während uns Burschen relativ großzügige Freiheiten gewährt wurden, welche auch an unsere tollen Erzieher geknüpft waren, werden Mädchen praktisch eingesperrt. Hier ein paar Einzelheiten:

UPDATE: Wie in der neueren Stellungnahme beschrieben, hat sich hier einiges getan. Man ist hier durchaus bereit, den Schülern mehr Freiraum zu bieten.

## 1.1 Wie im „Häfn“

Mädchen haben extrem beschränkten „Freigang“. Fortgehen ist einmal die Woche von _20 bis 21:30 Uhr_ [sic!] erlaubt.

In der täglichen Lernstunde (die für uns Burschen tatsächlich nie existierte) dürfen die Mädchen nicht in ihrem Bett lernen, sondern müssen sich am Schreibtisch befinden (selbst wenn sie nichts zu tun haben). Elektronische Geräte sind nicht erlaubt, auch wenn diese durchaus zu Lernzwecken verwendet werden können.

Selbst beim Nachmittagsausgang gibt es **extreme** Einschränkungen:

  * Wann?
  * **Wohin**?!
  * Rückkehrzeitpunkt

Falls dieser überhaupt genehmigt wird, gibt es schon bei der geringsten Verspätung extreme Konsequenzen.

## 1.2 Willkommen in der Justizanstalt

Im Gegensatz zur Justizanstalt Stein ist der Service, den die Schülerinnen hier bekommen, kostenpflichtig.

Ich bin kein Anwalt, aber diese Maßnahmen, die auch für volljährige Mädchen gelten, sind sehr suspekt. [Es gibt bei der Freiheitsentziehung Ausnahmen für Erziehung][2], aber ob diese Maßnahmen einer Gerichtsverhandlung standhalten?

Die Türen zum Mädchentrakt sind ab 22:00 Uhr nach außen zugesperrt, von innen kann man sie (gezwungenermaßen durch den Brandschutz) öffnen, doch leider wurde diese „Lücke“ ausgenutzt: Die Mädchen konnten sich ja tatsächlich frei bewegen. 

Den neuesten Gerüchten zufolge kam es zu einem Vorfall, in dem ein Bursche bei einem Mädchen in der Nacht schlief. In Folge dessen wurde der ganze Mädchentrakt mit Loxone-Türsensoren ausgestattet.

{{<figure src=images/signal-attachment-2019-03-05-093945.jpeg caption="Dieser Sensor piepst bei einer Erzieherin, wenn die Tür durchquert wird" align=center >}}

Sobald ein Mädchen nach 22:00 Uhr eine Trakttür (welche ungefähr alle 5 Zimmer platziert sind) durchbricht, piepst es bei einer Erzieherin. Nach der Alarmierung eilen diese sofort zum Verbrechensort und versuchen, diesen rigorosen Rechtsbruch aufzuklären.

Das ist eine unglaubliche Entgleisung der Freiheit, was rechtfertigt diese Maßnahme?

Noch nicht genug: Manche Zimmer liegen unglücklicherweise an einem Vordach, theoretisch kann man hier also den Zwängen des BSH entfliehen. Das ist angeblich auch schon mal passiert, doch rechtfertigt dies nicht eine großzügige Vergitterung dieser Fenster.

Es gab Erzieherinnen, die überprüft haben, ob zur Nachtruhe um 22:00 Uhr wirklich jeder schläft. Weiters brachen Sie einfach ins Zimmer oder gar in das Bad ein. Die Privatssphäre ist im Internat aus Prinzip schon recht eingeschränkt, aber diese Übertretungen sind wahrlich nicht zu dulden.

Ein weiteres Problem sind die Kastenkontrollen: Diese sind das privateste, das ein Internatsschüler besitzt. Ohne Anwesenheit dessen darf der Kasten nicht geöffnet werden. Diese Regelung wurde des öfteren gebrochen.

Mir berichten Mädchen, dass bei Unordnung ihre gesamten Hygieneartikel in den Mistkübel landeten &#8211; ein Unding, doch im BSH Krems für das Personal kein Problem.

## 1.3 Das Strafsystem

Mädchen, die sich weigern, ihr komplettes Leben in den 4 Wänden der Diktatur zu verbringen oder gegen sonstige „Gesetzgebungen“ verstoßen, werden oft verwarnt und landen zufällig auf der „Warteliste“. Diese Personen haben im nächsten Jahr wahrscheinlich keinen Heimplatz mehr. Das wird den Betroffenen möglicherweise erst im Juli mitgeteilt, was natürlich auch toll ist, wenn im September Schulbeginn ist (und eine etwaige Wohnungssuche zu diesem Zeitpunkt bereits recht schwierig wird).

## 1.4 Besondere Ereignisse

Man hat im BSH die Möglichkeit, sich für gewisse Tage (und Nächte) abzumelden. Sobald die Volljährigkeit erreicht ist (in Österreich dzt. [18 Jahre][3]), kann man sich selbstständig abmelden, vorher mit Einwilligung der Eltern.

{{<figure src=images/signal-attachment-2019-03-05-160823.jpeg caption="Faschingsdienstag, wenn man im BSH festsitzt" align=center >}}

Doch dies wird unter Umständen bei den Mädchen zum Beispiel am Faschingsdienstag (größte Faschingsparty in NÖ) auch bei Volljährigkeit abgelehnt. Volljährige Menschen einzusperren ist rechtswidrig. Das Recht folgt dem BSH!

Kleine Zusammenfassung: Als Mädchen darf man im Internat prinzipiell nicht viel. Die meisten Mädchen (wenn sie die finanziellen Mittel haben) flüchten mit spätestens 18 Jahren in eine eigene Wohnung oder pendeln wieder.

# 2.0 Infrastrukturapokalypse

Mir ist bewusst, dass das BSH nicht unendlichem Geldfluss unterliegt und auch gewisse Maßnahmen (Budget, Staatseinfluss, ...) nicht von der Anstalt selbst getragen werden. Also ist dies nicht wirklich als Kritik am Heim, sondern eher an den Sparmaßnahmen des Bundes und des Landes NÖ zu sehen. 

Doch macht es mich stutzig, dass Sensoren, die für nichts anderes als das Einsperren der Schüler vorhanden sind, anscheinend mit Leichtigkeit finanziert werden können

## 2.1 Schimmelprobleme

Das Bundesschülerheim ist in den Immobilien der BIG eingemietet, eine Sammlung von Staatlichen Immobilien die unter Schwarz-Blau I ausgelagert wurden. Dadurch wird hier gespart und optimiert was das Zeug hält.

In Teilen des BSH gab es immer wieder Schimmelvorfälle, ein Bekannter von mir lebte in solch einem Zimmer, er hatte zwar die Möglichkeit Zimmer zu wechseln, doch ist dies unter dem Jahr nervauftreibend und außerdem hätte er meine Nachbarschaft verloren. Die Opfer für das Verbleiben waren hoch, er hatte monatelang kein fließendes Wasser und musste bei mir duschen. Es dauerte Jahre, bis das Schimmelproblem identifiziert und behoben wurde.

Hier sollte sich die BIG schämen, wie sie das denkmalgeschützte Gebäude vernachlässigt. Undichte Fenster, schimmelnde Trakte, dicke Risse durch das ganze Haus, das ist in einem der reichsten Länder der Welt nicht notwendig. Vielleicht sollte man eine Immobiliengesellschaft des Staates nicht kaputtsparen.

## 2.2 Heizung

Das Haus ist sehr alt, die Fenster alte Doppelgitterfenster. Im Winter ist es kalt und es zieht. In den 4 Jahren, in denen ich dort wohnte, sind einige Decken, Kuschelsocken und Nerven notwendig gewesen. Um Punkt 22:00 wurde die Heizung abgeschaltet, egal wie kalt oder warm es draußen war. Genauso mit dem Warmwasser, besonders Donnerstagabend: Wenn man Pech hatte konnte man nicht mehr warm duschen.

Am Wochenende läuft natürlich auch keine Heizung, falls man schulisch gezwungen ist, im Internat zu bleiben (Tag der offenen Tür), darf man kalt duschen und sich den Rausch ausfrieren 😉

## 2.3 Wasser

{{<figure src=images/429715968_250467.jpg caption="Foto eines frustrierten Schülers, der seine Trinkflasche nicht auffüllen konnte" align=center >}}

Nach den Ferien sind im Warmwasser manchmal „Rostrückstände“ vorhanden, auch hier sollte man besser nicht duschen oder dieses Wasser gar trinken. Immer wieder spritzen auch während des Schuljahrs „rote Fontänen“ aus den Duschköpfen raus.

**UPDATE**: Dieses Problem entstand in den Kremser Wasserwerken, nicht im BSH.

## 2.4 Internet

Gleich zu Beginn meiner Schullaufbahn an der HTL Krems hatte ich von September bis November kein Internet. Wir benutzen das Internet nicht nur zum Streamen von Filmen und Musik, sondern auch für Hausübungen und Lernzwecke. Auch die Jahre danach zwang uns die furchtbare Performance zum Ausweichen auf LTE, natürlich auf unsere Kosten.

Auch hier versprach der Direktor lange Verbesserungen, die in meiner Zeit nie vollzogen wurden.

Ich hatte mit ein paar Freunden im Internat ein Konzept erarbeitet, welches eine komplette Neuverkabelung und Neuaufbau der Netzwerkinfrastruktur beinhaltete. Wir hatten ein Meeting, in dem uns viel versprochen wurde. Wir hätten dieses Projekt kostenlos in unserer Freizeit durchgeführt, trotzdem wurden wir schlussendlich abgelehnt.

Folgende Probleme hatte das Netzwerk:

  * Authentifizierung mit Sozialversicherungsnummer und Namen -> Nicht änderbar
  * Uralte Software, hunderte Sicherheitslücken
  * Abstürze, Performanceeinbrüche
  * Alte Hardware

Der bisherige Dienstleister wurde beibehalten, bis es im September 2018 zu einem Crash kam. Über 3 Monate hatten die Schüler kein Internet.

Hier gibt es einiges an Chaos in den staatlichen Gebäuden: Während die HTL und angeblich auch die HLW/HLM (im selben Gebäude!) einen Glasfaseranschluss haben, war dies für das BSH nicht möglich. Klares Versagen von Vater Staat, der oft von Digitalisierung spricht, aber in Wahrheit nicht einmal schafft, die eigenen Anstalten gscheit anzubinden.

Mittlerweile wurde der Dienstleister gewechselt und die Performance soll sich gebessert haben.

## 2.5 Lärmschutz, Türalarme, &#8230;

Noch bevor es die Alarme im Mädchentrakt gab, gab es diese bei den Übergängen in die HTL und bei den Notausgängen. Bei einem Durchgang außerhalb der Öffnungszeiten ertönt ein ohrenbetäubendes Piepsen, welches erst durch Eingreifen eines Erziehers unterbrochen werden kann.

Diese vollkommene sinnlose Maßnahme ärgert nur die Schüler, die neben diesen Türen wohnen und verhindert keinen einzigen Ein/Ausbruch. Besonders nervenaufreibend ist dies vor allem in der Nacht &#8211; die anwohnenden Schüler werden durch diese Lärmbelästigung geweckt und müssen diese dann oft über mehr als 5 Minuten hinweg ertragen &#8211; bis endlich ein Erzieher aufsteht und den Alarm wieder deaktiviert (Diejenigen, die ihn ausgelöst haben, sind bis zu diesem Zeitpunkt bereits über alle Berge ...). Wobei es hier auch stellenweise notwendig war, den Vorfall erst einmal mühsam bei einem Erzieher zu melden, damit überhaupt jemand tätig wird. **In Folge der Beschwerden wurden dann die Notausgänge zugesperrt**. Stell ich mir bei einem Feuer nicht so toll vor.

Im Laufe der Zeit wurden Lärmschutzwände im Speisesaal montiert, die Aussahen wie schwebende Ufos und laut Direktor Böhm sehr teuer waren. Niemand hatte nach diesen gefragt, noch war die Lautstärke einem Schüler zu hoch.

# 3.0 Das leidige Essensthema

In meiner Laufzeit im BSH wurden über 3 Essensumfragen durchgeführt. Gebessert hat sich bis heute nicht viel, viele Schüler sind vom örtlichen Pizzaservice oder vom Billa in der Utzstraße abhängig. Und nein, das liegt nicht an meinem Geschmack, manche „Gerichte“ waren und sind _ungenießbar_.

{{<figure src="images/Screenshot_20161003-164742.png" caption="Abwechslungsreiche Küche im BSH" align=center >}}

Die Küche ist ziemlich kreativ in der Namensgebung: Egal ob _Kalte Platte_, _X&#8217;unde[sic!] Jausn_, _Wurstaufschnitt_, _Kalter Schweinsbraten_ und _Käseplatte_ oder _Hascheefleckerl_, _Schinkenfleckerl_, _Fleischfleckerl_ und _Nudeln mit Schinken_, diese Gerichte unterscheiden sich nur im Namen.

Es gab auch immer wieder Gerichte mit dem Namen „Tiroler mit Butter“. Also ich weiß nicht ob ein Tiroler besser schmeckt als ein Steirer, aber generell bin ich dagegen, Menschen zu essen.

{{<figure src="images/image.png" align=center >}}

Wie überall im Leben gab es Höhen und Tiefen, aber durchwegs als „Gut“ konnte man die Küche nicht bezeichnen. Besonders ab April 2018 verschlechterte sich der Zustand zunehmend und ich war nicht mehr bereit, 380€ im Monat für diese Leistung auszugeben.

## 3.1 Bear Grylls für Arme: Vorfälle im BSH

Vorweg, es gibt gewisse Gerüchte über Kleintiere und andere Zwischenfälle. Ob diese nun passiert sind, vom Schüler verursacht wurden oder wie auch immer: Wir geben hier nicht dem BSH eine kollektive Schuld. Fehler passieren und diese Fälle sind sehr selten. Trotzdem sind diese dokumentiert und nicht ganz so leiwaund&#8230;

{{<figure src="images/photo_2016-04-05_19-26-31.jpg" align=center >}}
{{<figure src="images/photo_2016-04-05_19-26-32.jpg" align=center >}}
Auch weitere Kleintiere hatten es sich im BSH gemütlich gemacht, immerhin spricht nichts gegen zusätzliche Proteine:<figure class="wp-block-image">

{{<figure src="images/IMG_20161024_171719.jpg" align=center caption="Auch Ameisen verirren sich manchmal in die Küche" >}}

{{<rawhtml>}}
<video controls src="https://share.dafnik.me/do_not_delete/VID-20150325-WA0000.mp4"></video>
{{</rawhtml>}}


# 4.0 Meine Zeit als Internatssprecher

In meiner Zeit als Internatssprecher(-stellvertreter) lernte ich das Bundesschülerheim von innen kennen. Es gab in meiner Zeit nur zwei öffentliche Sitzungen, die Protokolle verfasste ich. Folgende Zitate fand ich besonders amüsant:


>_Wir haben Außensensoren, wir haben Innensensoren und noch denen wiad ghazt!_  
**Mag. Johann Böhm - In den Zimmern ist es eiskalt**

>_Wenn i siach wia es Burschen do mit kurze Leibaln do sitzen, brauch i ned ehazn!_  
**Mag. Johann Böhm - Im Seminarraum (wärmster Raum im BSH wegen dem darunterliegenden Heizraum) ist es zugegeben warm, daher ziehen wir unsere Pullis aus**

>_I hob letztens zwa dawischt, wias hamkema san, schauts i hob a Foto gmocht._  
**Mag. Johann Böhm - Erkennt zwei Burschen wieder, die nach einer langen Nacht wieder ins BSH „einbrechen“ wollten**

>_Schüler: Meiner Meinung nach ..._  
>_Böhm: Dei Meinung is mia wuascht!_  
**Mag. Johann Böhm &#8211; Auf Kritik wird angemessen reagiert**

Der Direktor hat allgemeinen Zugriff auf alle Internatszimmer und schaut hin und wieder, ob niemand einen Kühlschrank, Heizstrahler oder (Gott bewahre!) einen WLAN-Router angesteckt hat. Ob das tatsächlich er war, lässt sich nicht belegen.

Tatsächlich hatte ich einmal die Zimmertüre offen gelassen und er kassierte schlussendlich mein Handy ein. Ich durfte es mir in der Direktion abholen mit einem Vortrag über Diebstähle im BSH.

Die Angst gegen Diebstahl ist natürlich gerechtfertigt, tatsächlich ist in den vier Jahren bei offener Tür nichts gestohlen worden.

Die letzten zwei Internatssprecher sind mit viel Elan in das Amt getreten, haben aber bereits während des Jahres aufgegeben. Kein Internatssprecher konnte irgendetwas verändern, man traf immer auf härtesten Widerstand.

Der größte Erfolg meines Jahres war die Erlaubnis von Kaffeemaschinen und Wasserkochern in den Zimmern. Das ist für die vielen investierten Stunden einfach zu wenig.

#### 4.1 Führungskultur und Auffälligkeiten

Auch in der Belegschaft ist Böhm nicht besonders beliebt. Gewisse Erzieher arbeiten hart daran, die Restriktionen von Herrn Böhm zu umgehen und zumindest ihren Schützlingen ein _halbwegs_ angenehmes Leben zu ermöglichen.

Über andere Meinungen wird tendenziell eher „drübergfoan“, anstatt miteinander zu diskutieren, die Zustände bei den Mädchen wird auf den ErzieherInnen abgewälzt, während diese die Schuld auf die Leitung schieben.

Ich bin ich eher der Meinung, dass einzelne ErzieherInnen für den Zustand verantwortlich sind, da sie ihre Mädchen „beschützen“ wollen. Ich stelle mir da immer die Frage:

Meine Damen und Herren, wollen Sie volljährige Mädchen beschützen? Vor was? Außenwelt? Bösen Penissen?

#### 4.2 Weitere Einzelheiten

Beim HTL-Ball darf man für 25€ im Bundesschülerheim übernachten. Inkludierte Leistungen:

  * Kaltes Wasser
  * Keine Heizung
  * Altes Brot, vielleicht ein paar Semmeln der Vorwoche, Schinken und Käse sowie 2017 auch kalte Fleischlaibchen

**UPDATE:** Mittlerweile hat sich in dieser Hinsicht einiges getan, das ist der aktuelle Wisch, den einem vorgelegt wird, wenn man in der Ballnacht im BSH übernachten will:

{{<figure src="images/photo5764894199672582605.jpg" align=center >}}

Anscheinend wird Kritik doch nicht völlig ignoriert: Nun darf man kostenlos die Ballnacht im Bundesschülerheim verbringen.

Leider steht wie jedes Jahr wieder einiges in der Vereinbarung, das man ignorieren kann:

>auch bei Volljährigkeit des Schülers vom Erziehungsberechtigten unterzeichnen  
**Es gibt ab 18 Jahren keine Erziehungsberechtigte mehr. Mein Vater muss beim Abschluss eines Handyvertrags auch nicht meinen Opa fragen.**

> Können die Verursacher nicht gefunden werden, werden alle Anwesenden in der Ballnacht zur Verantwortung gezogen  
**[Kollektivbestrafung](https://de.wikipedia.org/wiki/Kollektivhaftung) bringt ganz sicher was!**

>Schüler, die alkoholisiert ins Bundesschülerheim zurückkehren...  
**Ich kenne niemanden, der beim HTL Ball etwas getrunken hat, deswegen ist diese Regelung auch echt sinnvoll.**

# 5.0 Fazit

>I wü nimmer hin.
**&#8211; Bernhard Steindl**

_Was darf ein_ **_Bundes_**_schülerheim?_ Nicht jeder hat die finanzielle Unterstützung, um vom Internat zu fliehen, falls einem die Regeln nicht passen. Es ist eine Bundeseinrichtung und demnach ist es nicht dazu da, unnötigerweise die Freiheit einzuschränken und volljährige Personen zu „frotzeln“.

Auch wenn das Internat eine Erfahrung war, die ich nicht mehr missen will, möchte ich nicht mehr zurück. Mit über 18 Jahren sehnt man sich nach Freiheit, die es zumindest in diesem Internat nicht gibt.

Ja, es gibt eine Hausordnung und die meisten Punkte darin sind sinnvoll. Doch das hier ist ein **Bundes**schülerheim und es stellt den Anspruch, Personen die nicht in der Umgebung wohnen, kostengünstig und bestmöglich unterzubringen, da hier die Wirtschaft keine Alternative bietet. Niemand redet hier von Überwachung, Ignoranz und Inkompetenz in vielen Bereichen.

Es gibt für viele Personen **keine** Alternative zum Bundesschülerheim. Ein _paar_ Anpassungen, die **vollkommen** **kostenlos** sind, würden das Leben für viele Menschen, die monatlich viel Geld dafür zahlen (~380€ + Taschengeld), sehr erleichtern:

Allgemeine Maßnahmen:

  * Abklären von Freiheiten mit Eltern bei der Anmeldung
  * Nachmittagsausgang: Hier gibt es keine Diskussion darüber, wie lange ein Jugendlicher raus darf. **Hier gilt absolute Freiheit!**
  * Solange die Noten passen, keine verpflichtenden Studierstunden
  * Die Absperrungen im Internat von gewissen Bereichen und Überwachungsmaßnahmen unterlassen
  * Küchenkonzept überdenken: Warum darf man sich nicht vom Essen abmelden? (Auch wenn dies rechtlich dzt. nicht möglich ist, ist es nicht unmöglich)
  * Kühlschränke erlauben
  * Hausordnung anpassen (z.b. Kein Verbot von WLAN-Router, &#8230;)
  * Internat nicht wie ein Gefängnis führen: Mädchen/Burschen müssen nicht getrennt werden etc.
  * Mädchen nicht vor „Gefahren“ schützen, die nicht da sind

Für Personen über 18:

  * Keine Frotzeleien mehr für Eigenberechtigte: Es sollte klare Regeln geben, die Verantwortung obliegt dann beim Eigenberechtigten

Diese Maßnahmen basieren auf klarem Menschenverstand, werden aber von allen Beteiligten getrost ignoriert. Wenn man ein Internat absolutistisch führt und über die Meinung der Schüler „drüberfoat“, braucht man sich nicht wundern, wenn sich Menschen dagegen auflehnen. Dieser Artikel ist dazu da, die Zustände aus der Sicht eines (ehemals) Betroffenen sachlich zu kritisieren und aufzuzeigen.

Ich weise nochmals ausdrücklich [auf den neuen Artikel hin][1], der die Veränderungen seit diesem Artikel beschreibt.

 [1]: https://blog.e9a.at/frischer-wind-im-alten-haus/
 [2]: https://de.wikipedia.org/wiki/Freiheitsentziehung#%C3%96sterreich
 [3]: https://de.wikipedia.org/wiki/Vollj%C3%A4hrigkeit#%C3%96sterreich