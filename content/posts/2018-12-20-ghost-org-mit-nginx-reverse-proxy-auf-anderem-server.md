---
title: ghost.org mit Nginx-Reverse-Proxy auf anderem Server
author: Christoph Scheidl
type: post
date: 2018-12-20T10:41:41+00:00
url: /ghost-org-mit-nginx-reverse-proxy-auf-anderem-server/
categories:
  - nginx

---
Heute habe ich das erste Mal eine Ghost-Instanz zuhause aufgesetzt. Der Reverse-Proxy hat mir dabei ein wenig Kopfschmerzen bereitet. Schlussendlich bin ich bei folgender Lösung gelandet:

Diese Anleitung bis zu &#8222;ghost install&#8220; befolgen: <https://docs.ghost.org/install/ubuntu/>

Beim Hostnamen geben wir unsere URL mit https an  


Alle Parameter so weit eingeben, bis die Frage kommt: Nginx installieren? Wir wollen Nginx nicht auf dieser Instanz, deshalb hier mit Nein beantworten.

Auch SSL wollen wir nicht hier haben, sondern am Reverse-Proxy, deshalb ebenfalls mit Nein

Nun mittels folgendem Befehl die Ghost-Instanz im ganzen LAN erreichbar machen:

<pre class="wp-block-code"><code>ghost config set ip &lt;DEINE IP-ADRESSE></code></pre>

Nun können wir folgende Nginx-Konfiguration verwenden:

<pre class="wp-block-code"><code>server {
    listen 80;
    # Unter diesem Namen wird Ghost erreichbar sein
    server_name &lt;DEIN DOMAINNAME>;

    # Umleiten auf SSL
    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl;

    server_name &lt;DEIN DOMAINNAME>;

    ssl_certificate /etc/letsencrypt/live/&lt;DEIN DOMAINNAME>/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/&lt;DEIN DOMAINNAME>/privkey.pem;

    location / {
        client_max_body_size 10G;

       
        proxy_pass http://&lt;DEINE IP-ADRESSE>:2368;
        proxy_redirect off;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-Host &lt;DEIN DOMAINNAME>;
    }
}</code></pre>

Ich bin mit Nginx kein Profi, Verbesserungsvorschläge sind gerne willkommen, aber so funktioniert es bei mir.

###### Links und Credit

<p style="font-size:12px">
  <a href="https://docs.ghost.org/install/ubuntu/">https://docs.ghost.org/install/ubuntu/</a><br />
</p>