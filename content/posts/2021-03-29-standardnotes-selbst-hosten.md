---
title: standardnotes selbst hosten
author: Christoph Scheidl
type: post
date: 2021-03-29T06:42:04+00:00
url: /standardnotes-selbst-hosten/
classic-editor-remember:
  - block-editor
categories:
  - Admin-Tipps
  - nginx

---
Ich habe mir eine Standard-Notes-Instanz zuhause selbst aufgesetzt, so einfach wie das ist nur selten etwas: Einfach die Anleitungen (siehe unten) befolgen, einmal eine nginx-Instanz auf der Docker-VM, einmal eine nginx-Instanz auf dem Reverse-Proxy (falls ihr einen eigenen habt), und die Sache läuft.

###### Links und Credit

<https://docs.standardnotes.org/self-hosting/docker>  
<https://docs.standardnotes.org/self-hosting/https-support>