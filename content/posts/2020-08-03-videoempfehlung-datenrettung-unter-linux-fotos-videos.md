---
title: 'Videoempfehlung: Datenrettung unter Linux (Fotos, Videos)'
author: Christoph Scheidl
type: post
date: 2020-08-03T06:31:36+00:00
url: /videoempfehlung-datenrettung-unter-linux-fotos-videos/
classic-editor-remember:
  - block-editor
categories:
  - Allgemein

---
Ich habe letztens ein Video vom guten [BitBastelei][1] gesehen, bei dem er sehr gut und ausführlich beschreibt, wie man von einer kaputten SD-Karte noch etwas holen kann:

<https://youtu.be/IpRYVSf89Nw>

 [1]: https://www.youtube.com/channel/UCz0-R_txYGlU1cFBPtUXGSg