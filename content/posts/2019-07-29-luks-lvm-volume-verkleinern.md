---
title: LUKS (LVM-Volume) verkleinern
author: Christoph Scheidl
type: post
date: 2019-07-29T12:50:45+00:00
url: /luks-lvm-volume-verkleinern/
categories:
  - Admin-Tipps
  - Debian
  - Grundlagen

---
Ich habe mir einen neuen Laptop gekauft, ein ThinkPad T495s. Da ich gerne Dual-Boot Kubuntu 19.04 und Windows 10 fahren will, aber auf eine voll-verschlüsselte Festplatte nicht verzichten will, habe ich den harten Weg auf mich genommen, und die LUKS-Partition, die der Ubuntu-Installer nur über die ganze Festplatte machen kann, manuell verkleinert. Leider liegt das ganze schon eine Zeit lang zurück, und ich habe mir nur unterstehende Links als nützlich markiert. Aber soviel sei gesagt, es ist nicht trivial, und erfordert einiges an umrechnen von verschiedenen Speichergrößen. Ganz wichtig dabei zu beachten:  
Alle diese Tools verwenden keine echten zB **Gigabyte**, sondern **Gibibyte**, das bedeutet, keine 1000, sondern 1024 als Umrechnungsfaktor.

### Eine kurze Auflistung/Erklärung:

1 Tebibyte = 1024 Gibibyte  
1 Gibibyte = 1024 Mebibyte  
1 Mebibyte = 1024 Kibibyte  
1 Kibibyte = 1024 Byte  
1 Byte = 8 Bit

Als ganzes:

1 Tebibyte = 1024 Gibibyte = 1048576 Mebibyte = 1073741824 Kibibyte = 1099511627776 Byte = 8796093022208 Bit

Würde man das jetzt mit 1000 als Umrechnungsfaktor berechnen würde (Kilobyte, Megabyte, Gigabyte, Terabyte), kämen wir auf folgende Ergebnisse:  
  
8796093022208 Bit = 1099511627776 Byte = 1099511627,776 Kilobyte = 1099511,627776 Megabyte = 1099,511627776 Gigabyte = 1,099511627776 Terabyte

Oder gerundet auf das Komma:

8796093022208 Bit = 1099511627776 Byte = 1099511628 Kilobyte = 1099512 Megabyte = 1100 Gigabyte = 1 Terabyte

Das ist auch der Grund, warum im System meist weniger Speicher angezeigt wird, als auf der Verpackung steht:  
  
1 Terabyte = 0,9 Tebibyte = 931 Gibibyte

Und die Systeme arbeiten IMMER mit 1024 als Umrechnungsfaktor, weil das eine 2er Potenz ist (2^10, sprich &#8222;2 hoch 10&#8220;)

### Man merke sich also:

1 Byte = 8 Bit (dividiert durch 8)  
1 Kibibyte = 1024 Byte (dividiert durch 1024)  
ABER:  
1 Kilobyte = 1000 Byte (dividiert durch 1000)

###### Links Und Credit

<p style="font-size:12px">
  <a href="https://superuser.com/questions/1296301/lvm-complaining-about-device-smaller-than-logical-volume">https://superuser.com/questions/1296301/lvm-complaining-about-device-smaller-than-logical-volume</a> <br /> <a href="https://help.ubuntu.com/community/ResizeEncryptedPartitions">https://help.ubuntu.com/community/ResizeEncryptedPartitions</a> <br /> <a href="https://wiki.archlinux.org/index.php/Resizing_LVM-on-LUKS">https://wiki.archlinux.org/index.php/Resizing_LVM-on-LUKS</a> <br /> <a href=https://www.christitus.com/lvm-guide/>https://www.christitus.com/lvm-guide/</a>
</p>