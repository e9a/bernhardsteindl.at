---
title: DDNS auf Synology DiskStation
author: Christoph Scheidl
type: post
date: 2019-12-06T10:16:35+00:00
url: /ddns-auf-synology-diskstation/
categories:
  - Admin-Tipps
  - Synology

---
Meinen Heimserver muss ich natürlich auch irgendwo sichern, Off-Site, versteht sich. Weil ich eine alte Synology DiskStation mit nur einem Festplattenschacht übrig hatte, und diese zu sonst nichts wirklich gut zu gebrauchen ist, nutze ich diese in einem anderen Haushalt als Backup-Endpunkt. Leider ist DSM, das Synology-OS, etwas eigenwillig, und deshalb ist nicht alles so trivial wie unter Debian. Um meinem DDNS-Provider [nsupdate.info][1] nutzen zu können, musste ich ein kleines Script im Internet suchen und modifizieren, um das ganze lauffähig zu bekommen.

Dieses Script führt man dann als root über den integrierten &#8222;Task-Scheduler&#8220; (zu finden im Control-Panel) einfach z.B. alle 10 Minuten aus, und schon weiß man zu jeder Zeit seine IP-Adresse.

Kleine Anmerkung: Da ich leider kein IPv6 dort habe, wo ich diese DiskStation einsetze, musste ich die entsprechenden Code-Zeilen auskommentieren, ansonsten würde ich von nsupdate gebannt werden, weil die Werte nicht zusammen-stimmen.

Als Backup-Endpunkt nutze ich übrigens im Moment &#8222;borgbackup&#8220;, gedenke allerdings, mir einmal &#8222;restic&#8220; genauer anzusehen. Vielleicht folgt zu diesem Thema einmal ein Blog-Artikel.

###### Links und Credit

<p style="font-size:12px">
  Credit geht an <a href="https://gist.github.com/raphiz">raphiz</a> und <a href="https://gist.github.com/rosch100">rosch100</a><br /><a href="https://gist.github.com/raphiz/837453f189dca966a69c">https://gist.github.com/raphiz/837453f189dca966a69c</a><br />
</p>

 [1]: https://www.nsupdate.info/