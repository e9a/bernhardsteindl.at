---
title: Proxmox-Container (LXC) lässt sich nach Proxmox-Upgrade nicht mehr starten
author: Christoph Scheidl
type: post
date: 2019-12-18T11:03:45+00:00
url: /proxmox-container-laesst-sich-nach-proxmox-upgrade-nicht-mehr-starten/
classic-editor-remember:
  - block-editor
categories:
  - Debian
  - Proxmox
  - ZFS

---
Nach dem Proxmox-Upgrade wollte bei mir ein Proxmox-Container nicht mehr booten, die Fehlermeldung war leider nicht sehr aussagekräftig.

Nach kurzem googlen dann die Lösung: das ZFS-Subvol wollte sich nicht mounten lassen, weil das Verzeichnis nicht leer war. Hier war ein leeres `dev/` Verzeichnis Schuld an dem Unglück. Nach dem Löschen des `dev/` Verzeichnisses kann man das Image selbst mit `zfs mount` einbinden.

Kurzes Beispiel:

Euer Container mit der ID 100 wurde auf ein ZFS-Storage installiert. Nehmen wir an, das war direkt auf dem lokalen ZFS-Pool von Proxmox, dem `rpool/data/`. Nun wollt ihr den Container starten, doch das funktioniert nicht. Also macht ihr `ls -alh /rpool/data/subvol-100-disk1/` und seht dort ein Verzeichnis `dev/`, das leer ist.

Wenn ansonsten nichts zu sehen ist außer dieses leere `dev/` Verzeichnis, dann könnt ihr mit `rm -r /rpool/data/subvol-100-disk1/dev` dieses löschen. Anschließend macht ihr `zfs mount rpool/data/subvol-100-disk1`, und wenn das ohne Probleme und Fehlermeldung funktioniert, sollte der Container wieder starten.

Ich habe heute das Problem nicht wegbekommen, und nach einem Neustart war es immer und immer wieder soweit, dass die Container nicht starten wollten. Dabei war die Lösung doch recht einfach: Anstatt nur den `dev`-Ordner zu löschen, müssen alle Datasets des betroffenen Pools unmountet werden, um danach den Ordner auf root-Ebene, also zB `rm -rf /data_redundant`, zu löschen. Danach einfach mittels Befehl `systemctl restart zfs-mount.service` den Dienst neu starten. Gelingt das ohne Fehlermeldung, sollte auch nach einem Reboot alles in Ordnung sein.

Was außerdem noch seitens Proxmox empfohlen wird, ist das Cachefile des zpools neu zu setzen. Ich hab das Anfangs ohne Erfolg versucht, aber vielleicht hilft es ja, solchen neuen Fehlern vorzubeugen, ich hatte nämlich seither keine Probleme mehr. Nachdems nicht geschadet hat, einfach mal zustätzlich ins Terminal reinklopfen:

<pre class="wp-block-code"><code>zpool set cachefile=/etc/zfs/zpool.cache POOLNAME
update-initramfs -u -k all</code></pre>

###### Links und Credit

<p style="font-size:12px">
  <a href="https://forum.proxmox.com/threads/lxc-container-cant-start.53431/">https://forum.proxmox.com/threads/lxc-container-cant-start.53431/</a><br /><a href="https://forum.proxmox.com/threads/update-broke-lxc.59776/#post-277303">https://forum.proxmox.com/threads/update-broke-lxc.59776/#post-277303</a><br /><a href="https://forum.proxmox.com/threads/update-broke-lxc.59776/post-329812">https://forum.proxmox.com/threads/update-broke-lxc.59776/post-329812</a><br /><a href="https://forum.proxmox.com/threads/lxc-not-starting-after-v5-to-v6-upgrade-using-zfs-for-storage.57698/post-269706">https://forum.proxmox.com/threads/lxc-not-starting-after-v5-to-v6-upgrade-using-zfs-for-storage.57698/post-269706</a><br /><a href="https://proxmox-openvz.blogspot.com/2019/05/debugging-proxmox-lxc-container-that.html">https://proxmox-openvz.blogspot.com/2019/05/debugging-proxmox-lxc-container-that.html</a><br />
</p>