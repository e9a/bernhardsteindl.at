---
title: m3u8-Streams einfach mit ffmpeg herunterladen
author: Christoph Scheidl
type: post
date: 2020-04-08T07:43:12+00:00
url: /m3u8-streams-einfach-mit-ffmpeg-herunterladen/
categories:
  - Admin-Tipps
  - Grundlagen

---
Im Internet läuft mittlerweile immer mehr über HLS-Streams (m3u8). Da dies nicht so einfach herunterzuladen ist wie zB eine mp4, hier eine kurze Anleitung:

<pre class="wp-block-code"><code>ffmpeg -user_agent "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/601.7.8 (KHTML, like Gecko) Version/9.1.3 Safari/537.86.7" -i URL_TO_m3u8 -c copy output.mkv</code></pre>

Der User-Agent wird deshalb verwendet, da bei manchen Seiten der Download sonst nicht funktioniert.

Eine kurze Erklärung zu m3u8: Dabei handelt es sich eigentlich um eine Playlist, die der Browser lädt. Das heißt, es findet sich nicht ein gesamtes Video in einer Datei, sondern es sind viele kleine Videos, die der Browser zusammen stückelt.