---
# Title
title: "Kremser Wahl 2022: Die Kremser Pappkameraden"
# Enter the author's name
author: Bernhard Steindl
# Customize URI; default: post/filename
#url: /yubikey-fuer-linux-login-als-u2f-2-faktor-verwenden/
# Date
date: 2022-08-25T10:00:00+01:00
# Is this draft?
draft: false
# Show Table of Content?
showToc: true
# Available categories. Please comment out!
categories:
#- Admin-Tipps
#- Allgemein
#- Arch Linux
#- Debian
#- Denkt's noch!
#- Grundlagen
#- Hardware
#- mariadb
#- News
#- nginx
- Politik
#- Projekte
#- Proxmox
- Rants
#- Satire
#- Synology
#- Tor
#- Ubiquiti Unifi
#- Windows
#- Wordpress
#- ZFS

#weight: 5

cover:
    image: "images/cover.jpeg"
    # can also paste direct link from external site
    # ex. https://i.ibb.co/K0HVPBd/paper-mod-profilemode.png
    alt: "Die ÖVP ist im Urlaub"
    caption: "Die ÖVP ist im Urlaub"
    relative: false # To use relative path for cover image, used in hugo Page-bundles
---

Ich lebe seit 2014 in Krems, durfte aber bis dato bei keiner Kommunalwahl teilnehmen. Wie das geht? 2017 war ich noch im Internat und hatte keinen Zweitwohnsitz angemeldet. Da das Zweitwohnsitz-Wahlrecht von einer großen, mehrmals korruptionsanfälligen Partei in Österreich [hin](https://www.meinbezirk.at/amstetten/c-politik/zweitwohnsitzer-unter-lupe_a147579) und [wieder](https://noe.orf.at/v2/news/stories/2524582/) ein bisschen für eigene Zwecke genutzt wurde, ist es mittlerweile abgeschafft.

Nun schlage ich also die erste Kommunalwahl in Krems und die Situation ist maximal unübersichtlich, alle Straßen mit schiachen Gsichtern vollgepflanzt und man fragt sich, wen man von den Pappkameraden eigentlich wählen soll. Nun habe ich leider das Problem, dass ich gerne abschweife und in den eigentlichen Parteithemen Österreichische Realpolitik beimische. Ich empfehle daher **dringendst**, [alle neun Artikel (1 Einleitungsartikel + 8 Artikel über alle antretenden Listen) zu lesen!](#die-blogposts-zu-den-jeweiligen-einzelparteien)

*Eine kleine Orientierungshilfe der Kommunalpolitik in den letzten Jahren*

## Zusammenfassung der letzten Jahre in Krems (SPÖ-Regierung)

Seit mittlerweile 10 Jahren ist Dr. Resch Bürgermeister der Stadt und macht das, was in Österreich – oder genauer gesagt in Krems – am populärsten ist: Stillstand gepaart mit Schuldenabbau und Aktionismus.

Das ist auch nicht unbedingt schlecht und zumindest bislang der Wählerwille gewesen.
Spin der SPÖ Krems war/ist ja, dass nach 60 Jahren ÖVP-Bürgermeister die Stadt finanziell am Abrund stand und selbst das Land NÖ keine weiteren Darlehen ohne Auflagen mehr genehmigen wollte.

### Einschub: Wahlergebnisse in Krems seit dem 2. Weltkrieg

Was hier aber vergessen wird: Seit der ersten Wahl seit dem 2. Weltkrieg am 07.05.1950 bis zur *entscheidenden Wahl* 2012 hatten ÖVP (immer in Führung) und SPÖ stets gemeinsam eine Mehrheit zwischen 70 und 80% im Gemeinderat. Von den 15 Perioden im Gemeinderat regierte die ÖVP nur 5 in absoluter Mehrheit, das letzte Mal zwischen 1977 und 1982. ([Link zu den Ergebnissen](https://www.krems.at/rathaus/wahlen/allgemeine-information#c3023))

Die These, dass die SPÖ unbeteiligt am finanziellen Fiasko in 30 Jahren Stimmhilfe (1982-2012) der Stadt sein soll, ist somit durchaus interessant. Aus zeitlichen Gründen konnte ich leider nicht recherchieren, wer die Budgets in den 30 Jahren mitabgesegnet hat – eine gewisse Mitschuld ist aber auf jeden Fall nicht zu verneinen.

### Einschub Ende

## Finanz & Investitionspolitik

Das offensichtlichste, das einem Auswärtigen in Krems auffällt, ist die marode Infrastruktur der Stadt.
Während die Stadtränder mit fürchterlicher Architektur zugekleistert werden (bspw. *WTC Krems*, *Gedesag-Bau Weinzierl XIII*), verfallen die stadteigenen Straßen und wichtige Infrastruktur wie das Hallenbad offenen Auges.

Hier liegt ein großer Denkfehler der Politik vor:
Eine Stadt/Land/Staat kann nicht wie eine Hausfrau wirtschaften!
Was im Kleinen (also im Privaten) Sinn macht – nämlich dafür zu sorgen, dass der Schuldenstand gering ist –, macht bei großen Unternehmen und aus volkswirtschaftlicher Sicht keinen Sinn.
Während eine Familie zumindest hier in Österreich meistens einmal im Leben einen großen Kredit aufnimmt und danach gut bedient ist, muss eine Kommune **gezwungenermaßen ständig investieren**.
Wenn man beispielsweise eine Komplettsanierung aller Straßen starten würde und sie in etwa 20 Jahren (optimistisch!) fertig wäre, müsste man in der anderen Ecke wieder anfangen.
Nicht getätigte Investitionen führen dazu, dass die Jüngeren ein paar Jahre später viel mehr Geld in die Hand nehmen müssen, denn umso schlechter diese beinander ist, umso eher muss komplett neu gebaut werden und das ist bekanntlich teurer als zu renovieren.

Alle Investitionen einer Stadt ohne neue Kredite müssen Jahre – wenn nicht Jahrzehnte – warten, was zu Situationen wie das neue Hallenbad in Krems führt:

- Hätte man das Bad bereits 2012 gebaut, wäre die ganze Stadt bereits mehr als 10 Jahre früher in den Genuss eines neuen Bades gekommen.
- Die aufgenommene Kreditsumme in der Höhe von ca. 20 Millionen Euro wär bereits jetzt – durch die Inflation – mehr als 20% weniger wert, zur Hälte vermutlich auch schon abbezahlt.
- Derzeitige Kostenschätzungen tendieren zwischen 30 und 35 Millionen Euro, die 25 Millionen Stand 2019 sind unmöglich haltbar.
  Das Bad kommt der Stadt insgesamt viel teurer als wenn man es bereits vor Jahren kreditfinanziert gebaut hätte.
- Man hätte die letzten 10 Jahre (bekanntlich eine Niedrigzinsphase) – wie andere Städte – dazu nutzen können, die Infrastruktur auf Vordermann zu bringen.

Nun stehen einige Investitionen an und für diese wird man auch Schulden aufnehmen müssen.
Natürlich wird man den Höchststand von 2012 nicht mehr so schnell erreichen, aber auch durch weiteres Investieren in die Stadt und durch die Inflation ("Geldentwertung") wären die alten Schulden automatisch weniger geworden.

**Durch eine attraktive Infrastruktur hält man Bewohner*innen und Unternehmen in die Stadt.**
Die gesamte Innenstadt gehört dringend verkehrsberuhigt und umgestaltet, damit sie wieder an Attraktivität gewinnt.
Der gesamte Investitionsstau wird sich in den nächsten Jahren als bedrohlich darstellen und das "Sparen" der letzten 10 Jahre als groben Fehler.

Diese Politik nennt man *Austeritätspolitik* und ist [hochumstritten](https://de.wikipedia.org/wiki/Austerit%C3%A4t).
Zumindest am Anfang der 2010er Jahre hätte – anstatt zu sparen – investiert werden müssen.
Jetzt angesichts stark steigender Preise und vermutlich steigender Zinsen ist investieren teurer denn je!

### Verkehrs & Umweltpolitik

Ich würde die Verkehrspolitik als „gemäßigte Autofahrerpolitik“ bezeichnen.
Während man aus fachlicher Sicht seit fast 50 Jahren weiß, dass neue Straßen nur mehr Verkehr generieren und das nur Verkehrsberuhigung wieder Leben in die Stadt zurückbringt, muss man der (oftmals verwöhnten und faulen) Bevölkerung langsam aber sicher näherbringen, nicht mehr jeden Meter mit dem Auto zu fahren.
Natürlich will man dies eher im Schneckentempo angehen, um ja niemanden zu verärgern.
Aber wie unheimlich langsam Radwege in Krems gebaut werden, bringt mich regelmäßig zur Weißglut.

Aber zumindest kleine Schritte gegen die heilige Kuh Auto sind in Sichtweite, auch wenn ein [„alter Depp“](https://www.noen.at/krems/fleischerei-graf-kampf-um-zwei-parkplaetze-in-krems-krems-kremser-innenstadt-parkplaetze-fleischerei-graf-print-287304232) wegen 2 (in Worten: **ZWEI**) Parkplätzen [eine Petition startet](https://kurier.at/chronik/niederoesterreich/krems/gegen-parkverbot-kremser-fleischer-sammelt-unterschriften/401487606). Dass der *Umsatzverlust von 20%* vielleicht der Tatsache geschuldet ist, dass er sich im Fernsehen zum Affen macht und **ich dort sicher nicht mehr hingehe**, hat der Fleischer wohl nicht bedacht.
Natürlich ist sich die ÖVP nicht zu schade, sich hier blöd hinzustellen, obwohl ich schon vor über einen Jahr bewiesen habe, dass das [Parken an dieser Stelle seit jeher **illegal** war](/typisch-krems-betonieren-sudern-und-busse-verunglimpfen/).

Ein paar Artikel zur SPÖ-Verkehrspolitik in Krems:
- [Typisch Krems: Betonieren, Sudern und Busse verunglimpfen](https://bernhardsteindl.at/typisch-krems-betonieren-sudern-und-busse-verunglimpfen/)
- [Die Betonierer am Werk: "Entlastungsspange B3"](https://bernhardsteindl.at/die-betonierer-am-werk-entlastungsspange-b3/)

Falls es jemanden interessiert, Hermann Knoflacher hat bereits vor fast 50 Jahren nachgewiesen,
dass das Auto keine Lösung für Verkehrsprobleme ist, sondern lediglich Probleme verursacht: Von Bewegungsmangel über Unfallabteilungen in Krankenhäusern, die nur mehr für Verkehrsunfälle da sind, über die nachhaltige Zerstörung der Städte.
- [Hermann Knoflacher](https://de.wikipedia.org/wiki/Hermann_Knoflacher)
- [Verkehrswende](https://de.wikipedia.org/wiki/Verkehrswende)

## Gemeinderatspolitik

Positiv ist in jedem Fall, dass man sich als interessierter Bürger jede einzelne Gemeinderatssitzung **online ansehen** kann.
Natürlich erfährt man nicht, was in den jeweiligen Ausschüssen passiert und teilweise kann man den Vorgang sicher auch als „Polit-Theater“ bezeichnen.
Einmal im Jahr lohnt sich die Sitzung aber für jeden Bürger: Das ist die ausführliche und genaue Erklärung der Finanzen durch KLS-Gemeinderat Wolfgang Mahrer.

## Bildungspolitik

Im Prinzip kann eine Stadt, die nicht ein Land wie Wien ist, relativ wenig machen;
sind doch sowohl Mittelschulen als auch Gymnasien sowie alle Höheren Schulen entweder in Landes- oder Bundeshand.
Somit ist die Zuständigkeit maximal bei Kindergärten und Volksschulen gegeben.
Da ich keine Kinder habe, kann ich relativ wenig dazu sagen, außer dass die Helikopter-Muttis, die ihre Gfraster jeden Zentimeter mit dem Auto herumchauffieren, eine wahre Zumutung für alle Unbeteiligten sind.


# Die Blogposts zu den jeweiligen Einzelparteien

*Diese werden erst im Laufe der Woche veröffentlicht*

- [SPÖ: Von Pensionisten für Pensionisten](/posts/2022-08-26-die-spoe-krems-von-pensionisten-fuer-pensionisten/)
- [FPÖ: Hundezone und Fairness für alle!](/posts/2022-08-26-fpoe-krems-hundezone-und-fairness-fuer-alle/)
- [KLS: Wir sind keine Kommunisten](/posts/2022-08-26-kls-wir-sind-keine-kommunisten/)
- [NEOS: Alter Wein in neuen Schläuchen](/posts/2022-08-26-neos-alter-wein-in-neuen-schlaeuchen/)
- [GRÜNE: Ohne Wahlprogramm keine gute Wahl](/posts/2022-08-26-gruene-ohne-wahlprogramm-keine-gute-wahl/)
- [Green Future Krems: Is eh scho wuascht](/posts/2022-08-26-green-future-krems-is-eh-scho-wuascht/)
- [MFG: Mit Furchtbaren Gsichtern](/posts/2022-08-26-mfg-mit-furchtbaren-gsichtern/)


# Conclusio

Irgendwie war jetzt niemand hier besonders überzeugend. Manche glänzen mehr, manche weniger, nur allein die ÖVP fällt (mal wieder…) ungut auf. Für alle unentschlossenen, die etwas Zeit haben, empfehle ich die Nachsicht einiger Gemeinderatssitzungen, welche [alle aufgezeichnet und auf YouTube](https://www.youtube.com/channel/UCL6vtYKsiryjNN7Ma9pA4tQ/) veröffentlicht werden.

Eins ist klar: Seit dem Ende der ÖVP-geführten Regierung vor 10 Jahren hat sich einiges zum Besseren gewendet. Wie es nun weitergehen sollen, werden die Wählerinnen und Wähler selbst entscheiden müssen. Wichtig wäre nur, dass man sich umfassend informiert und sich nicht von irgendwelchen Versprechen blenden lässt.

## Persönliche Meinung Steindl

Am Ende des Tages bleibt noch folgende Überlegung zur Wahlentscheidung:

Ziel sollte es immer sein, eine Mehrheit von FPÖ und ÖVP zu verhindern. Also hier eher nicht das X machen. Dann wären noch die NEOS, welche aber in Wien auch eher das [Beiwagerl spielen](https://www.derstandard.at/story/2000138676060/erstes-darlehen-der-stadt-fuer-wien-energie-schon-im-juli) sowie [dumme Prestigeprojekte planen](/posts/2022-08-26-neos-alter-wein-in-neuen-schlaeuchen/).

Dann bleiben noch 4 über: SPÖ, KLS, GRÜNE und Green Future. Die ersten beiden sind linkssozial angesiedelt, haben aber weitgehende Übereinstimmungen im Programm stehen, in der Realpolitik sind aber weitreichende Unterschiede zu erkennen. Die beiden letzteren haben kein Wahlprogramm, da müsst ihr euch schon selbst bei den Mandataren informieren, was eigentlich ihr Ziel ist. Schlussendlich gebe ich keine Wahlempfehlung außer **verdammt nochmal keine (a-)soziale Nationalpartei und nicht schon wieder die "neue" ÖVP zu wählen**. 

Das ist aber meine Privatmeinung, welche nicht unbedingt auf Fakten sondern auch auf eigener Meinung und Erfahrungen mit Korruption und Misswirtschaft zu tun hat.