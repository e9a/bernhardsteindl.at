---
# Title
title: "SPÖ Krems: Von Pensionisten für Pensionisten"
# Enter the author's name
author: Bernhard Steindl
# Customize URI; default: post/filename
#url: /yubikey-fuer-linux-login-als-u2f-2-faktor-verwenden/
# Date
date: 2022-08-31T18:00:00+01:00
# Is this draft?
draft: false
# Show Table of Content?
showToc: true
# Available categories. Please comment out!
categories:
#- Admin-Tipps
#- Allgemein
#- Arch Linux
#- Debian
#- Denkt's noch!
#- Grundlagen
#- Hardware
#- mariadb
#- News
#- nginx
- Politik
#- Projekte
#- Proxmox
#- Rants
#- Satire
#- Synology
#- Tor
#- Ubiquiti Unifi
#- Windows
#- Wordpress
#- ZFS

---

# SPÖ

*Dies ist ein Artikel der Serie [Pappkameraden in Krems](/posts/2022-08-25-die-kremser-pappkameraden/). Sämtliche Inhalte genügen keinen journalistischen Ansprüchen wie Ausgewogenheit oder DoubleChecks, falls Sie dennoch falsche Informationen finden oder Anmerkungen zu den Artikeln haben, würde ich mich über eine [Rückmeldung](/impressum) sehr freuen.*

Irgendwie wirkt die SPÖ ja generell aus der Zeit gefallen. In Wien betoniert ein Boomer-Bürgermeister eine neue Stadtautobahn, der [Schilfkickl](https://www.nachrichten.at/meinung/leserbriefe/der-schilf-kickl;art11086,3426655) will Donauwasser in den Neusiedlersee leiten, damit die Touristen nicht im Gatsch picken und schlussendlich will der NÖ-SPÖ-Hansl die OMV ([Ja, die Firma in Staatsbesitz, welche Klimaaktivisten ausspioniert](https://www.trendingtopics.eu/spionage-vorwurf-omv-soll-klimaaktivistinnen-ueberwacht-haben-lassen/)) verstaatlichen, nachdem seine Partei sie damals teilprivatisiert hat.

Wieder retour nach Krems. Nachdem die SPÖ seit 10 Jahren den Bürgermeister stellt, ist ein Großteil der geschaffenen Fakten und der gelebten Politik bereits im Artikel [Pappkameraden in Krems](/posts/2022-08-25-die-kremser-pappkameraden/) thematisiert. Die restlichen Dinge, die es nicht in den Hauptartikel geschafft haben und die Plakate finden hier ihren Platz.

{{< figure src="images/resch1.jpeg" caption="Ich glaube die *zahlreichen* Ansiedelungen sind nicht primär das Ergebnis der Politik, denn *was genau* hätte denn zu mehr Ansiedelungen führen sollen?" >}}
{{< figure src="images/resch2.jpeg" caption="Damit hat er aus fachlicher Sicht recht, aber am Ende des Tages hätte das jede Partei tun müssen." >}}
{{< figure src="images/resch3.jpeg" caption="Das stimmt einfach nicht. So wirklich belebt ist sie erst, wenn die ganzen Nebengassen auch nicht mehr leer stehen." >}}
{{< figure src="images/resch4.jpeg" caption="Und wer zahlt die teuren Hütten? Die zahlen wir uns doch eh selbst! Fremdes Geld auszugeben ist keine Leistung." >}}
{{< figure src="images/resch5.jpeg" caption="Werden damit Fitnessstudios gefördert oder ist das eh nur die klassische Vereinsförderung? Versteh ich nicht…" >}}
{{< figure src="images/resch6.jpeg" caption="Wo sind diese *hunderte Bäume?* In der Dinstlstraße oder neben der neu erreichteten Straße in Landersdorf (beim Billa Plus) jedenfalls nicht. Da ist „brav“ alles zubetoniert." >}}

Interessant ist jedenfalls die Mitglieder der Liste RESCH: Nur 4 von 79 Personen auf der Liste sind unter 25 Jahre alt, der SPÖ-Gemeinderat in der neuen Periode ist im Schnitt 53,9 jahre alt und damit nochmal 9 Jahre älter als der [Bevölkerungsschnitt der Bevölkerung](https://ugeo.urbistat.com/AdminStat/de/at/demografia/dati-sintesi/krems-an-der-donau--stadt-/301/3).

Nur zum Vergleich der Altersschnitt der Listen der anderen Parteien:
1. FPÖ Krems: 54,4 (Gut, wer 89-Jährige Personen ernsthaft in eine Gemeinderatsliste setzt ...)
2. SPÖ Krems: 53,9
3. KLS: 52,8
4. MFG: 51,3
5. ÖVP: 49,8 (Das frische Team riecht aber auch schon ein bisserl nach Opa)
6. NEOS: 47,8 (Für eine neue Politik genug [Boomer](https://de.wikipedia.org/wiki/OK_Boomer))
7. Green Future: 47,5 (Statistisch schwierig, da nur 2 Personen)
8. Grüne: 31

Somit liegen zwischen dem *höchsten* und der *niedrigsten* Schnitt satte 23,4 Jahre Unterschied. Natürlich sagt das nicht besonders viel aus, denn es gibt noch immer genug ewiggestrige Politik (*FPÖ Deutschnationalismus hust*), welche von Jüngeren mitgetragen werden. Nichtsdestotrotz beschäftigt sich die Kommunalpolitik fast ausschließlich nur mit der Zukunft und da sind Fast-Pensionisten in Überzahl nicht die Lösung.

## Die Suche nach dem Wahlprogramm

Gut, ich besuche erst mal [spoekrems.at](https://spoekrems.at/).
Egal wo man hinklickt wird man auf [reinhardresch.at](https://reinhardresch.at) weitergeleitet und sie wollen mir „gschissene“ Cookies reindrücken.
Besonders sozial ist das mal nicht, aber den Umstand ignorieren wir mal hartnäckig.

Der Versuch, der Website ein Wahlprogramm zu entlocken, ist mir nicht gelungen.
Es ist nur eine Sammlung belangloser Blogposts; die Zielgruppe dürfte nicht besonders IT-affin sein.
Um zumindest irgendwelche Infos herauszufinden, habe ich die Suchmaschine angeworfen und folgende Zusammenfassung von [ORF.at](#_FEHLT_#) gefunden:

> „Wir werden eine Verwaltungsreform angehen. Dann gibt es große Investitionen wie die Badearena neu und eine Infrastrukturoffensive. Es ist einiges aufzuholen, nachzuholen und weiterzuentwickeln“, so Resch. Bei der Wahl vor fünf Jahren hatte die SPÖ deutlich zugelegt. „Das Wahlziel ist einfach: Es muss vor der Prozentzahl ein Plus stehen“, so der Bürgermeister.

Was für eine Verwaltungsreform will er angehen? Ich konnte dazu nichts finden. Man könnte mal im Magistrat mit einer Reform im Sinne einer Grundeinschulung über die Zuständigkeit eines kommunalen Amts anfangen. Wie ich das letzte Mal dort war, wollte mir eine Mitarbeiterin des **Meldeamts** erklären, dass sie sich nicht sicher sei, ob man hier einen Hauptwohnsitz melden kann. 8 Jahre Magistrat Krems werde ich bei Zeiten auch mal in einem Artikel zusammenfassen, sobald sie nicht mehr für mich zuständig sind.

Nun führte ich meine Recherche weiter und fand den *Kremser Stadtkurier*, eine SPÖ-Parteizeitung.
Alle Informationen für die nächsten Punkte habe ich aus dieser entnommen; ein offizielles Parteiprogramm gibt es offenkundig **nicht**.

[Stadtkurier 08/22 von bernhardsteindl.at](images/KSK_Aug_22_web.pdf) oder direkt von der [SPÖ Krems](https://spoekrems.at/fileadmin/user_upload/KSK_Aug_22_web.pdf)

*Nachtrag: Am 17.08. bekam ich den "Stadtkurier" ungefragt per Postwurfsendung an meine Privatadresse in Krems.*

### Bildungspolitik

Die Möglichkeiten der SPÖ im Bildungswesen sind ziemlich eingeschränkt. Nehmen wir mal das Ziel der Gesamtschule (also statt Gymnasium & Mittelschule nur noch ein mittlerer Schultypos), da kann ein Bürgermeister der Stadt genau nix bewirken. Anders sieht es bei Neubauten & Renovierungen bestehender Schulen aus, im Wahlprogramm wird ein neuer Kindergarten in Gneixendorf und die Renovierung einer Schule in Rehberg versprochen. Aber sind wir uns ehrlich: Die meisten dieser Vorhaben wurden einstimmig im Gemeinderat beschlossen, kein vernünftiger Mensch hat irgendetwas gegen Renovierungen.

Anders sehe ich die Sache bei den Privatuniversitäten, aber auch hier kann der Bürgermeister nicht wirklich eingreifen. Meiner bescheidenen Meinung nach sind Privatuniversitäten überflüssig und nur der Elitenförderung verschrieben. Oder kann mir jemand den Mehrwert der [Danube Private University](https://www.noen.at/krems/vergleich-so-teuer-ist-studieren-in-krems-krems-marga-wagner-pischel-dpu-danube-private-university-studieren-imc-fh-krems-155718547) erklären? Braucht viel Platz, die Studenten treiben die Wohnungspreise in die Höhe, die teuren Autos sieht man in der ganzen Stadt und schlussendlich werden diese wahrscheinlich eh Privatärzte (in Deutschland). Mich würde interessieren, wie die gesellschaftliche Bilanz aussehen würde.

Die Karl-Landsteiner-Uni betoniert eines der letzten freien zentralen Grundstücke in Krems zu (das ist sowieso pervers, über Tochtergeselltschaften des Landes und über Umwegen gehört die KL sowieso zu 100% der Republik, da muss auch ein eigener Artikel her), währenddessen es überall an *leistbaren* Wohnen fehlt. Das Privatunis insgesamt zuwenig kontrolliert werden, [ist auch nichts neues](https://www.diepresse.com/595608/doktorat-verboten-tiroler-privat-uni-in-turbulenzen)

### Gemeinderatspolitik

Wenn eine Partei oder eine Person länger an der Macht ist, wird sie meistens skrupellos bezüglich der Trennung zwischen Partei und Amt. Aktuelles Beispiel dafür wären die *Spaziergänge* von Dr. Resch, welche nichts als wie Wahlkampf ist. Veröffentlicht im [YouTube-Channel](https://www.youtube.com/channel/UCL6vtYKsiryjNN7Ma9pA4tQ/) der Stadt, als wäre das normal.

Gemeinsam mit der ÖVP verhindert man seit Jahren Transparenz bei den gemeindeeigenen Unternehmen (GEDESAG, Kremser Immobilien, ...) aus *geschäftlichen* Gründen, der Rechnungshof selbst sieht kurz zusammengefasst einen [undurchsichtigen Haufen Beteiligungen](https://www.rechnungshof.gv.at/rh/home/home/Beteiligungen_Stadt_Krems_an_der_Donau_und_der_Stadtgemeinde.pdf), die teilweise aus steuerschonenden Gründen angelegt wurden, dessen Effektivität aber nie überprüft wurden.

Weiters ist das Kontrollamt der Stadt Krems nur für die Stadt und deren Eigenbetriebe zuständig, die stadteigenen Gesellschaften werden **nicht** überprüft. Insbesondere bei Immobilien-*Giganten* wie der GEDESAG könnte dies Sprengkraft entfalten, wenn die Gesellschaft jemals in Schwierigkeiten kommen sollte. Immerhin hat die GEDESAG Verbindlichkeiten in Höhe von 832,64 Millionen Euro. Den gesamten Bericht kann man [hier](https://www.rechnungshof.gv.at/rh/home/home/Beteiligungen_Stadt_Krems_an_der_Donau_und_der_Stadtgemeinde.pdf) nachlesen. Zu viele Verbindlichkeiten tun nicht gut, siehe Wien-Energie-Skandal.


### Verkehrs & Umweltpolitik

Mir noch nicht bekannte Punkte waren:
- Zweigleisiger Ausbau & Elektrifizierung St. Pölten - Krems
- Begegnungszone Hoher Markt/Untere-Landstraße
- Tempo 100 auf B37a & ~~Volle~~ [#_unleserlich_#] ASt Hollenburg
- 30km/h in Stein (ausgenommen B3)
- Sanierung der Austraße
- ...

Papier ist bekanntlich geduldig und so schlimm wie in [Wien] ist die SPÖ bei uns nicht, aber nur deshalb, da ihre Macht begrenzt ist und noch nicht so verfestigt und allumfassend wie in Wien (seit 77 Jahren) ist. 

### Finanz & Investitionspolitik

Wie ich bereits im Hauptartikel erwähnte, finde ich den Sparzwang lächerlich. Um irgendwie ein gerades Budget halten zu können, werden Projekte gestrichen oder hinausgezögert. Schlussendlich muss aber irgendwann investiert werden.

# Conclusio

Modern ist die SPÖ Krems nicht. Mit einem Durchschnittsalter 10 Jahre vor der Pension in der eigenen Liste und den ganzen alten Ideen ist man auch nicht gewillt, Veränderung zu zeigen. Ist diese notwendig? Meiner Meinung nach Ja, wobei in Krems die SPÖ von den Großparteien angesichts der ÖVP und FPÖ ein geringeres Übel scheint. Trotzdem - die Zeit des Bürgermeisters läuft langsam ab, niemand rechnet damit, dass er die nächsten 5 Jahre noch weitermacht. Auch seine Wähler werden von der biologischen Wahrheit nicht verschont und werden wie die zahlreichen Radio-Niederösterreich-Hörer langsam aber sicher unter der Erde landen.

*Lesen Sie weitere Artikel der Serie [Pappkameraden in Krems](/posts/2022-08-25-die-kremser-pappkameraden/)!*