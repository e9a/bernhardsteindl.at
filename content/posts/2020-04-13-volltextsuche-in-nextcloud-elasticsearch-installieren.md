---
title: Volltextsuche in Nextcloud – Elasticsearch installieren
author: Christoph Scheidl
type: post
date: 2020-04-13T08:51:03+00:00
url: /volltextsuche-in-nextcloud-elasticsearch-installieren/
classic-editor-remember:
  - block-editor
categories:
  - Allgemein

---
Es ist ja kein Geheimnis, dass wir die Nextcloud einsetzen. Da sich diese in den letzten Jahren immer weiter füllt, wird das Suchen und Finden von Dateien immer schwieriger. Hier gibt es eine tolle Möglichkeit, auch in den Dateien zu suchen, nämlich Elasticsearch. Dies ist auch schnell installiert, wie man in diesem [Video][1] und in diesem [Guide][2] sehen kann.

###### Links und Credit

<p style="font-size:12px">
  <a href="https://youtu.be/lAmBlfB9S0k">https://youtu.be/lAmBlfB9S0k</a><br /><a href="https://decatec.de/home-server/volltextsuche-in-nextcloud-mit-ocr/">https://decatec.de/home-server/volltextsuche-in-nextcloud-mit-ocr/</a>
</p>

 [1]: https://youtu.be/lAmBlfB9S0k
 [2]: https://decatec.de/home-server/volltextsuche-in-nextcloud-mit-ocr/