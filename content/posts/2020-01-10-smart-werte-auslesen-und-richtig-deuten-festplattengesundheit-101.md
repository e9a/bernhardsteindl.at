---
title: SMART-Werte auslesen und richtig deuten – Festplattengesundheit 101
author: Christoph Scheidl
type: post
date: 2020-01-10T09:30:51+00:00
url: /smart-werte-auslesen-und-richtig-deuten-festplattengesundheit-101/
categories:
  - Admin-Tipps
  - Debian
  - Grundlagen

---
In der IT gibt es immer wieder ein großes Thema: DATENVERLUST. Das Grauen jedes Nutzers. Wir haben schon öfter gesagt, das man sich nie auf einzelne Komponenten verlassen sollte, wenn es um Daten geht. Deshalb empfehlen wir seit jeher, mindestens die 3-2-1-Regel zu befolgen: 3 Datenbestände auf mindestens 2 Medien und eine Außer-Haus-Kopie. Vielleicht kommt noch einmal ein umfassender Artikel zu dem Thema, wie sich das möglichst schmerzfrei und einfach umsetzen lässt, aber heute soll es um etwas anderes gehen.

Nehmen wir an, ihr findet ein paar alte Festplatten zuhause, und denkt euch, super, die kann ich gleich als Datenspeicher verwenden. Bevor ihr jedoch eure Daten daraufpackt und vielleicht sogar von der Quelle löscht, solltet ihr euch ansehen, in welchem Zustand diese Festplatten sind. Dafür gibt es die sogenannten &#8222;smartmontools&#8220;. Diese können die Festplattenparameter auslesen, die sich dann interpretieren lassen.

Leider gibt es hier keinen 100%-igen Standard, wie so etwas aussieht bzw. wie so etwas zu deuten ist. Jeder Hersteller kocht hier ein wenig seine eigene Suppe. Grundsätzlich lassen sich folgende Annahmen treffen:

  * Hat eine Enterprise-Festplatte (also zB WD-RED, Seagate Ironwolf, alles was eben für den Dauerbetrieb konzipiert ist) mehr als `40.000` Stunden, sollte man sich um die Daten darauf bereits sorgen. Meistens geben die Hersteller sogar empfohlene maximale Betriebsstunden an.
  * Ist die **`Reallocated Sectors Count`**-Anzahl größer als ``, würde ich mir auch schön langsam Sorgen machen. Das bedeutet nämlich, das es bereits kaputte Sektoren auf der Platte gibt, und die Platte auf andere Sektoren ausweichen musste.
  * Auch auf die **`(Raw) Read Error Rate`** sollte man ein Auge werfen. Auch diese sollte nicht größer als `` sein.

Dies sind jetzt natürlich nur grobe Richtwerte, auf die man sich nicht festnageln darf. Sie sollen nur einen Anhaltspunkt geben. Alle Parameter mit Erklärung findet man [hier][1].

Man muss bei den Ergebnissen zwischen dem `Value`&#8211; und dem `RAW`-Wert unterscheiden. Beim `Value`-Wert handelt es sich um einen normalisieren Wert, der quasi über einem gewissen `Threshold` als OK gilt und darunter als schlecht. Ich bevorzuge allerdings den `RAW`-Wert, der den echten Messwert anzeigt. Bei den oben genannten Werten sind auch die `RAW`-Werte gemeint, wenn es heißt sie sollen nicht mehr als `40.000` Stunden haben oder den Wert `` nicht überschreiten.

Um nun einen SMART-Test auszuführen, muss man zuerst die `smartmontools` installieren, unter Debian zB:

<pre class="wp-block-code"><code>apt install smartmontools</code></pre>

Nun lässt sich der SMART-Test mit folgendem Befehl starten (root-Rechte erforderlich):

<pre class="wp-block-code"><code>smartctl -t short /dev/sdc</code></pre>

Dies startet den Schnelltest (Dauer ungefähr 2 Minuten) auf der Festplatte `/dev/sdc`. Es gibt außerdem die Optionen `long`, `conveyance` oder `select`. Mehr Infos dazu findet man bei [Thomas-Krenn][2].

Nachdem die 2 Minuten um sind, kann man die Ergebnisse nun mittels diesem Befehl auslesen:

<pre class="wp-block-code"><code>smartctl -a /dev/sdc</code></pre>

Solltet ihr noch Tipps haben oder wollt eure Erfahrungen teilen, freue ich mich auf eine Diskussion in den Kommentaren!

###### Links und Credit

<p style="font-size:12px">
  <a href="https://www.thomas-krenn.com/de/wiki/SMART_Tests_mit_smartctl">https://www.thomas-krenn.com/de/wiki/SMART_Tests_mit_smartctl</a><br /><a href="https://de.wikipedia.org/wiki/Self-Monitoring,_Analysis_and_Reporting_Technology#%C3%9Cbliche_Parameter">https://de.wikipedia.org/wiki/Self-Monitoring,_Analysis_and_Reporting_Technology#%C3%9Cbliche_Parameter</a><br /><a href="https://www.thomas-krenn.com/de/wiki/Kategorie:Smartmontools">https://www.thomas-krenn.com/de/wiki/Kategorie:Smartmontools</a>
</p>

 [1]: https://de.wikipedia.org/wiki/Self-Monitoring,_Analysis_and_Reporting_Technology#%C3%9Cbliche_Parameter
 [2]: https://www.thomas-krenn.com/de/wiki/SMART_Tests_mit_smartctl