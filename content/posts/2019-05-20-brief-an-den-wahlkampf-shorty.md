---
title: Brief an den Wahlkampf-Shorty
author: Bernhard Steindl
type: post
date: 2019-05-20T19:08:50+00:00
url: /brief-an-den-wahlkampf-shorty/
categories:
  - Politik
  - Rants

---
Lieber Shorty,

da du nach der nächsten Wahl aufgrund der Dekadenz der Bevölkerung wieder unser Kanzler sein wirst, wünsche ich mir von dir folgendes:

  1. Es wäre ganz nett, wenn du das Österreichische Parlament ernst nehmen würdest, anstatt im Eilverfahren illegale Gesetze durchzupeitschen.
  2. Harald Mahrer kann nicht alles machen. Und bitte setz endlich eine Umweltministerin ein, die mehr als blöd dasitzen und Standortverlegungen durchführen kann. Das Thema ist ernst.
  3. Mir wäre es lieber, wenn du mit keinen rechtsextremen Nasnbohrern koalieren würdest, auch wenn sie dir ideologisch am Nächsten stehen.
  4. Nur weil du 20 Kilo Haargel am Kopf picken hast, musst du nicht immer den gscheitesten spielen. Irren ist menschlich.
  5. Spreng nicht immer die Regierung: 2 Regierungen in 2 Jahren, wir Österreicher haben neben wählen auch ein echtes Leben.
  6. Erklär einmal, was der neue Stil eigentlich ist. Niemand weiß das, vielleicht weißt du das?
  7. Respektiere die Presse. Messetsch-Control ist eine Verarsche jedes denkenden Bürgers. Diskussionen sind öffentlich im Parlament zu führen.
  8. Lass die Finger von Themen, von denen du keine Ahnung hast. Stichwort ORF, Bildung und Politik
  9. Erklär mal, woher das ganze Geld für den ÖVP-Wahlkampf 2017 kam.
 10. Du wirkst immer so, als hättest du jeden Satz vorher auswendig gelernt. Spontanität erwarte ich von einem Staatsmann.

Ich weiß, dass du das nie lesen wirst, weil du deine Zeit lieber mit dem Spiegel verbringst. Ich auch, wir sind beide schon recht hübsch.

Bussi,

dein Stoal