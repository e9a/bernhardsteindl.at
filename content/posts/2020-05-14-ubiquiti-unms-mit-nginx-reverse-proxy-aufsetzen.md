---
title: Ubiquiti UNMS mit nginx Reverse-Proxy aufsetzen
author: Christoph Scheidl
type: post
date: 2020-05-14T09:01:31+00:00
url: /ubiquiti-unms-mit-nginx-reverse-proxy-aufsetzen/
categories:
  - Allgemein

---
Da ich nun für den eigenen Gebrauch eine Funkstrecke auf Basis der Nanostation AC loco installiert habe, wollte ich mich einmal mit dem (nicht zwingend erforderlichen) Controller für diese Geräte beschäftigen, UNMS. Da ich das ganze natürlich, wenn es schon möglich ist, selber hosten möchte, habe ich es kurzerhand bei mir aufgesetzt. Warnung: Das ganze basiert auf Docker und läuft deshalb nicht auf LXC.

Aufgesetzt ist das Ganze echt schnell, bei mir auf einem blanken Debian 10 einfach folgenden Befehl eingeben:

<pre class="wp-block-preformatted">curl -fsSL https://unms.com/v1/install > /tmp/unms_inst.sh && sudo bash /tmp/unms_inst.sh --behind-reverse-proxy --public-https-port 443 --http-port 9080 --https-port 9443</pre>

Das ganze einfach schön durchlaufen lassen, falls er nach Lets-Encrypt fragt, das machen wir hier nicht, das wir den Reverse-Proxy auf einer eigenen Instanz laufen lassen.

Beim nginx Reverse-Proxy müssen wir dann einfach folgende Config eintragen:

<pre class="wp-block-code"><code>map $http_upgrade $connection_upgrade {
  default upgrade;
  ''      close;
}

server {
  listen 80;
  server_name YOUR.DOMAIN;

  client_max_body_size 4G;

  location / {
    proxy_redirect off;
    proxy_set_header Host $host;
    proxy_pass http://YOUR.IP:9080/;
  }
}

server {
  listen 443 ssl http2;
  server_name YOUR.DOMAIN;

  ssl_certificate     /etc/letsencrypt/live/YOUR.DOMAIN/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/YOUR.DOMAIN/privkey.pem;

  ssl on;

  set $upstream YOUR.IP:9443;

  location / {
    proxy_pass     https://$upstream;
    proxy_redirect https://$upstream https://$server_name;

    proxy_cache off;
    proxy_store off;
    proxy_buffering off;
    proxy_http_version 1.1;
    proxy_read_timeout 36000s;

    proxy_set_header Host $http_host;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $connection_upgrade;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header Referer "";

    client_max_body_size 0;
  }
}</code></pre>

Bitte tippt das aber jetzt nicht einfach ab. Ubiquiti ist nämlich eines der wenigen genialen Unternehmen, die das sogar auf ihrer Seite stehen haben, und immer aktuell halten. Deshalb werft einen Blick bei den Links und Credits rein!

###### Links und Credits

<p style="font-size:12px">
  <a href="https://help.ui.com/hc/en-us/articles/115012196527">https://help.ui.com/hc/en-us/articles/115012196527</a><br /><a href="https://community.ui.com/releases/">https://community.ui.com/releases/</a><br /><a href="https://help.ui.com/hc/en-us/articles/115015690207-UNMS-Reverse-Proxy">https://help.ui.com/hc/en-us/articles/115015690207-UNMS-Reverse-Proxy</a>
</p>