---
title: Unter Linux alle Festplatten mit allen Identifiern auslesen
author: Christoph Scheidl
type: post
date: 2019-12-21T13:19:05+00:00
url: /unter-linux-alle-festplatten-mit-allen-identifiern-auslesen/
categories:
  - Admin-Tipps
  - Debian
  - Grundlagen
  - Proxmox

---
Manchmal kommt man in die Situation, dass man wissen möchte oder muss, wie welche Hard-Drive unter Linux jetzt heißt. Mit dem `udev`-System gibt es hier verschiedene Bezeichnungen, die alle unterschiedliche Formate und Aufgaben haben. So ist es zum Beispiel empfehlenswert, bei einem ZFS-Pool die `wnn` Bezeichnung zu verwenden, damit beim Austauschen der Disks noch immer alles Paletti ist. Um nun von jeder Festplatte alle Bezeichner zu bekommen, empfiehlt sich das Tool `hwinfo`.

Verwendung:

  * `hwinfo --disk`: Zeigt alle Festplatten mit allen Infos und Mountpoints an.
  * `hwinfo --partition`: Zeigt alle Partitionen mit allen Infos und Mountpoints an.

Bei beiden Befehlen ist es möglich, zusätzlich `--short` hinten anzuhängen, um nur eine kurze List zu erhalten, welche Disks bzw. Partitionen es gibt.

###### Links und Credit

<p style="font-size:12px">
  <a href="https://askubuntu.com/questions/27997/which-hard-disk-drive-is-which">https://askubuntu.com/questions/27997/which-hard-disk-drive-is-which</a><br />
</p>