---
# Title
title: "NiK - NEOS in Krems: Alter Wein in neuen Schläuchen"
# Enter the author's name
author: Bernhard Steindl
# Customize URI; default: post/filename
#url: /yubikey-fuer-linux-login-als-u2f-2-faktor-verwenden/
# Date
date: 2022-08-31T19:00:00+01:00
# Is this draft?
draft: false
# Show Table of Content?
showToc: true
# Available categories. Please comment out!
categories:
#- Admin-Tipps
#- Allgemein
#- Arch Linux
#- Debian
#- Denkt's noch!
#- Grundlagen
#- Hardware
#- mariadb
#- News
#- nginx
- Politik
#- Projekte
#- Proxmox
#- Rants
#- Satire
#- Synology
#- Tor
#- Ubiquiti Unifi
#- Windows
#- Wordpress
#- ZFS

---

# NiK - NEOS in Krems

*Dies ist ein Artikel der Serie [Pappkameraden in Krems](/posts/2022-08-25-die-kremser-pappkameraden/). Sämtliche Inhalte genügen keinen journalistischen Ansprüchen wie Ausgewogenheit oder DoubleChecks, falls Sie dennoch falsche Informationen finden oder Anmerkungen zu den Artikeln haben, würde ich mich über eine [Rückmeldung](/impressum) sehr freuen.*

*Gehst du mit der Jugend saufen, kannst du billig Stimmen kaufen* - [Link](https://www.youtube.com/watch?v=qaVJlWj7U0A) - das war das erste was man von dieser "Partei" [hören oder lesen konnte](https://www.noen.at/krems/aufreger-vor-wahl-neos-zum-schulschluss-mit-spritzer-trichter-im-kremser-stadtpark-krems-stadtpark-krems-schulschlussfest-schulschluss-neos-gemeinderatswahl-gemeinderatswahl-2022-manuela-leoni-nikolaus-lackner-dominic-heinz-print-328141870). Da hat wohl wer vom Häfn/Ibiza-HC gelernt.

Man besucht [neos.eu/nik](https://neos.eu/nik), weil man über die Website selbst überhaupt nicht zur Kremser „Ortsgruppe“ kommt. Der Webdesigner ist wohl frei nach neoliberalen Grundsätzen schlecht bezahlt worden, sonst hätte er bemerken müssen, dass auf einem Widescreen-Monitor alle Köpfe abgeschnitten sind.

Schauen wir uns erstmal die Wahlplakate an:

{{< figure src="images/neos1.jpeg" caption="Ein komplett nichtssagender Slogan. Wenn ich ein Nazi bin, sind dann die Neos auch Nazis?" >}}
{{< figure src="images/neos2.jpeg" caption="Parkplätze bauen kann der Resch auch, also was ist an den NEOS jetzt neu? *Neue Volkspartei hust*" >}}
{{< figure src="images/neos3.jpeg" caption="Keine Freunderlwirtschaft mehr mit ÖVP-Freunderl… oder so…" >}}
{{< figure src="images/neos4.jpeg" caption="Kein Kommentar." >}}
{{< figure src="images/neos5.jpeg" caption="Hey, den kenn ich! War der nicht mal Lehrer an der HTL Krems?" >}}

Wissenswert ist auch, das die Neos in Krems ein Zusammenschluss der Liste ProKrems (GRW 2017: 2,34%) und den neu kandidierenden NEOS sind. Vielleicht erhofft man sich mit dem Namen NEOS mehr Prozente - denn das Programm ist für NEOS-Verhältnisse ausgenommen konservativ, von der Bildungspolitik abgesehen.

## NEOS Wahlprogramm

[von bernhardsteindl.at](images/NEOS_Wahlprogramm.pdf), von [NEOS.eu](https://www.neos.eu/_Resources/Persistent/bf04d011d2b569a0b0e0e252370c2a4666f24da8/Unser%20Programm%20f%C3%BCr%20Krems.pdf)

### Bildungspolitik

Gleich am Anfang wird Krems als Universitätsstadt mit 5 Universitäten gelobt – dabei wird aber vergessen dass es nur 3 *richtige* Universitäten in Krems gibt. Und davon sind 2 nicht öffentlich und damit für jeden Normalverdiener prinzipiell unleistbar. Dann bleibt eigentlich nur mehr das IMC, denn eigentlich ist die Kirchlich Pädagogische Hochschule weder eine Uni, noch in Krems beheimatet. Ich lehne Privatuniversitäten prinzipiell ab, dazu sollte ich irgendwann einen eigenen Artikel dazu schreiben, Stichwort *Piefke- und Gstopften-Unis*.

Bildungspolitik ist eins der Steckenpferde der NEOS, zumindest im Wahlkampf, denn in der Realpolitik sieht das [ganz anders](https://www.derstandard.at/story/2000127659550/wiederkehr-zur-bildungsreform-gleich-viele-gewinner-wie-verlierer) aus. Hier wird (wie im Gemeinderatswahlkampf immer!) die Handlungsfähigkeit der Stadt überschätzt. Die Stadt ist im hohen Maße sowohl gesetzgeberisch als auch finanziell von der Landesregierung abhängig. SPÖ-regierte Gemeinden und Städte bekommen [deutlich weniger](https://www.addendum.org/niederoesterreich/bedarfszuweisungen/) Geld vom absolutistisch ÖVP-geführtem Land als ÖVP-regierte Kommunen. Nur als Hinweis nebenbei, wen man bei der Landtagswahl nächstes Jahr eher nicht wählen sollte, wenn man nicht am Stockholm-Syndrom leidet.

Abgesehen davon befinden sich im Parteiprogramm nur Kleinigkeiten wie schwer realisierbare 45-Stunden-Kindergärten (Damit die Eltern mehr arbeiten können um sich das Studium an der Privatuni dann leisten zu können oder warum auch immer…). Aber gut, Kindergarten am Samstag macht schon Sinn, dann kann man die Gschroppen wenigstens wohin schieben, wenn man in Ruhe shoppen oder ins Bordell gehen will.

### Gemeinderatspolitik

Die NEOS plakatieren immer Transparenz, im Wahlprogramm selbst kommt dieser Punkt **kein einziges Mal vor**. Weiters warb die NEOS in der Vergangenheit damit, dass sämtliche Wahleingänge und Ausgänge veröffentlicht werden – ich bin gespannt ob das nach der Wahl passiert!

Da die NEOS auch immer von anderen fordern, keine Postenschacher durchzuführen, ist natürlich fraglich, warum ihr Großspender Hans Peter Haselsteiner (*„Der Haselsteiner kriegt keine Aufträge mehr!“*) in den ORF-Stiftungsrat entsendet wurde – gleich als erste Amtshandlung nach ihrem Einzug in das Parlament? Er ist zwar [freiwillig gegangen](https://www.diepresse.com/5900715/keine-entpolitisierung-des-orf-stiftungsrat-haselsteiner-geht), aber *neue Politik* ist das nicht. Natürlich darf davon ausgegangen werden, dass nicht extra ein [ORF Bezirksstudio Krems](Link) gegründet wird, damit man wieder neue Posten zu besetzen hat, aber insgesamt macht das keinen guten Eindruck.

### Verkehrs & Umweltpolitik

Klassische ÖVP-Kopie: Öffis predigen, Parkplätze bauen! Eigentlich wollte ich schon bei der Überschrift **„Das gelbe Monster - der Stadtbus von Krems“** mit dem Lesen aufhören, aber weiterlesen lohnt sich: Kapitel „Sky City“ – Die NiK wollen die B3 zwischen Krems-Mitte und dem Museumsplatz überplatten (an und für sich nicht so blöd) und einen gesamten Stadtteil darauf bauen. Ich hab mir mal die Mühe gemacht und das ausgemessen. Die, aufgrund der schwierigen Topographie, maximal nutzbare Fläche (Dabei müsste man aber schon einen Park abreissen, die Wachaubahn „weg-ignorieren“!) sind 9 Hektar. Wenn wir jetzt Turbokapitalismus spielen und die zwei Teiche neben der Eisenbahnbrücke auch *entwickeln* wollen, kommen wir dann schon auf 20 Hektar. Da müsste dann aber auch das Hallenbad weichen, aber das braucht ja bekanntlich „eh niemand“ und die Sporthalle ist „eh überbewertet“.

Stattdessen soll die sogenannte „Sky City“ Platz für „Veranstaltungen“, Gratis-Parkplätze (\[sic!\] Die wollen 2022 noch neue Parkplätze bauen und damit offensichtlich einen Park zubetonieren), junges Wohnen, Freizeiteinrichtungen und auch Gastronomie bieten. Auch für Kongresse, Kabarett und Konzerte soll Platz sein – und das alles auf 9 (oder 20) Hektar! Zum Vergleich: Die gesamte Altstadt hat 30 Hektar Fläche; der gesamte Gewerbepark ist übrigens genauso groß – autogerecht gestaltet geht sich das auf dieser Fläche nie aus! Selbst wenn man einen Wohnblock wie den Karl-Marx-Hof in Wien Heiligenstadt hinstellt, bleiben hier ein paar kleine „Problemchen“ wie etwa die Finanzierung, oder auch wie das mit dem Weltkulturerbe funktionieren soll. Aber das privatisieren wir eh alles weg.

Wenigstens wissen die NEOS selbst, dass das ein zum Himmel schreiender (Sky City)-Blödsinn ist und schreiben am Ende hin: *Dies klingt nach einem schönen Traum? Stimmt – es wird auch einer bleiben.* - Na hoffentlich!

{{< figure src="images/neos_sky_bullshit.png" caption="Es ist jetzt nicht auf dem Millimeter genau aber in Grunde genommen ist das nicht soviel Platz." >}}

Abgesehen vom üblichen Stadtbus-Bashing und der tollen Stadtbahn, die alle Probleme lösen soll, sowie den unrealistischen Parkkonzepten (Die Gleichstellung von Kurz- und Dauerparkzone geht rechtlich nicht!) nichts Auffälliges. Keinerlei Zustimmung zur notwendigen Verkehrswende – hier denken die Verantwortlichen offensichtlich immer noch in Parkplätzen, wie alle anderen Altparteien auch.

### Finanz & Investitionspolitik

Wie es finanziell mit der Stadt weitergehen soll, habe ich nicht gefunden. Genausowenig wie die vielen Vorhaben finanziert werden sollen. 20 Hektar abzureissen und neu zu erschließen würde das Budget der Stadt in jedem Fall sprengen – das riecht dann wieder nach Public-Private-Partnership und das bedeutet wiederrum [Gewinne privatisieren, Verluste verstaatlichen](https://de.wikipedia.org/wiki/%C3%96ffentlich-private_Partnerschaft).

# Zusammenfassung

Alter Wein in neuen Schläuchen: Wenn man versucht, die ÖVP zu modernisieren, kommen die Neos dabei heraus. Leider riecht das Programm noch immer an jeder Ecke nach ÖVP - sogar die nicht einlösbaren Versprechen sind wieder dabei. Wieviele Kremserinnen und Kremser sich von der Verpackung verführen lassen werden - das wissen wir erst am 04.09.

*Lesen Sie weitere Artikel der Serie [Pappkameraden in Krems](/posts/2022-08-25-die-kremser-pappkameraden/)!*