---
title: Neues Arch und bestehende Windows-Partition Dual-Boot
author: Christoph Scheidl
type: post
date: 2018-12-29T12:38:13+00:00
url: /arch-und-windows-dual-boot/
categories:
  - Arch Linux

---
Ich habe im Sommer von KDE neon auf Arch-Linux gewechselt, da es sich mit meinem Thinkpad T480 besser verträgt. Ich habe damals einfach über mein bestehendes KDE-neon und Windows 10 Dual-Boot-Gespann auf die Partition von KDE neon drüberinstalliert. Dadurch hat aber GRUB meinen Windows-Eintrag und die Windows-EFI-Datein geschossen. Bis jetzt hat mich das nicht gestört, aber da ich nun Windows 10 wieder benötige, musste ich eine Lösung suchen. Dabei bin ich auf diesen Link gestoßen: [https://www.reddit.com/r/archlinux/comments/4mewqa/archwindows\_dualboot\_repair\_missing\_bootmgfwefi/][1]  
Gelesen, getan wie beschrieben, mit einem Windows 10 USB-Stick gestartet, die EFI-Partition repariert, dann noch im BIOS den Arch-Grub Eintrag nach ganz oben geschoben. Nun im Arch-Linux die EFI-Partition gemountet, mit den Befehlen

<pre class="wp-block-code"><code>sudo os-prober
sudo grub-mkconfig -o /boot/grub/grub.cfg</code></pre>

die GRUB-Config neu initalisiert und siehe da, alles funktioniert wie es soll. Solltet ihr diese Tools nicht installiert habt, einfach mittels pacman nachinstallieren.

###### Links und Credit

<p style="font-size:12px">
  <a href="https://www.reddit.com/r/archlinux/comments/4mewqa/archwindows_dualboot_repair_missing_bootmgfwefi/">https://www.reddit.com/r/archlinux/comments/4mewqa/archwindows_dualboot_repair_missing_bootmgfwefi/</a>
</p>

 [1]: https://www.reddit.com/r/archlinux/comments/4mewqa/archwindows_dualboot_repair_missing_bootmgfwefi/