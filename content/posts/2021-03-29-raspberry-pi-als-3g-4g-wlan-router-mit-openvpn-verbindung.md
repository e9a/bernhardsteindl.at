---
title: Raspberry Pi als 3G/4G WLAN-Router mit OpenVPN-Verbindung
author: Christoph Scheidl
type: post
date: 2021-03-29T06:49:43+00:00
url: /raspberry-pi-als-3g-4g-wlan-router-mit-openvpn-verbindung/
classic-editor-remember:
  - block-editor
categories:
  - Admin-Tipps
  - Debian
  - Hardware

---
Vor einiger Zeit habe ich einmal versucht, einen Raspberry Pi 3 als 3G/4G WLAN-Router mit OpenVPN zu konfigurieren. Da ich damals keinen Artikel ausformuliert habe, hier meine nützlichen Links und mein damaliges Resümee:

  * Default-Routen können die Geduld auf die Probe stellen
  * wvdial funktioniert mit einem ZTE MF180 nicht so wirklich, NetworkManager funktioniert hier besser
  * NetworkManager kann auch ohne GUI konfiguriert werden, das funktioniert dann auch
  * Wenn man die Ethernet-Schnittstelle nicht verwenden möchte, sollte man sie auch nicht konfiguieren, (Default Routen sind eine Geduldsprobe).
  * Ein OpenVPN-Client mit der Option `redirect-gateway def1` führt dazu, dass der gesamte Traffic über das VPN läuft.

###### Links und Credit

<https://vpn-anbieter-vergleich-test.de/anleitung-raspberrypi-vpn-router/>  
<https://hamy.io/post/0003/optimizing-openvpn-throughput/>  
<https://thepi.io/how-to-use-your-raspberry-pi-as-a-vpn-router/>  
<https://gist.github.com/superjamie/ac55b6d2c080582a3e64>  
<https://stackoverflow.com/questions/43001223/how-to-ensure-that-there-is-a-delay-before-a-service-is-started-in-systemd>  
<https://raspberrypi.stackexchange.com/questions/69866/wlan0-could-not-connect-to-kernel-driver>  
<https://www.raspberrypi.org/documentation/configuration/wireless/access-point.md>  
<https://www.raspberrypi.org/forums/viewtopic.php?t=247225>  
<https://filippobuletto.github.io/home-router-lte/#internet-connection>  
<https://unix.stackexchange.com/questions/113975/configure-gsm-connection-using-nmcli/114006#114006>  
<https://fedoraproject.org/wiki/Networking/CLI>  
<https://askubuntu.com/questions/537406/configure-gsm-connection-using-shell-nmcli>  
<https://www.tecmint.com/setup-linux-as-router/>  
<https://unix.stackexchange.com/questions/365380/how-to-persist-ip-rule-and-route-whenever-server-rebooted>  
<https://www.cyberciti.biz/faq/howto-linux-configuring-default-route-with-ipcommand/>  
[https://www.digitalocean.com/community/tutorials/how-to-use-iproute2-tools-to-manage-network-configuration-on-a-linux-vpshttps://www.digitalocean.com/community/tutorials/how-to-use-iproute2-tools-to-manage-network-configuration-on-a-linux-vps][1]  
<https://wiki.linuxfoundation.org/networking/iproute2>  
<https://unix.stackexchange.com/questions/198554/3g-connection-with-wvdial-gets-no-ip-exits-with-code-16>  
<https://www.raspberry-pi-geek.de/ausgaben/rpg/2015/06/der-raspberry-pi-als-3g-hotspot/2/>

 [1]: https://www.digitalocean.com/community/tutorials/how-to-use-iproute2-tools-to-manage-network-configuration-on-a-linux-vps