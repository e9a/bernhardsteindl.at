---
title: Debloat Android – Huawei-Gerät ohne Root bestmöglich bereinigen
author: Christoph Scheidl
type: post
date: 2020-07-06T07:07:15+00:00
url: /debloat-android-huawei-geraet-ohne-root-bestmoeglich-bereinigen/
categories:
  - Allgemein

---
Da mir von meinem Arbeitgeber ein Huawei P20 als Diensthandy zur Verfügung gestellt wird, kann ich mir weder das Gerät aussuchen, noch kann ich mir eine Custom-ROM installieren. Es gibt aber trotzdem eine Möglichkeit, das Gerät etwas zu bereinigen: Das Deaktivieren von Systemapps über ADB.

Disclaimer: Diese Anleitung gibt nur eine Richtung vor. Es besteht natürlich wie immer kein Anspruch darauf, dass das was ich hier zeige bei euch das gewünschte Ergebnis bringt. Natürlich kann es auch sein, dass euer Gerät beim Deaktivieren der falschen Pakete nicht mehr richtig funktioniert. Deshalb, macht vorher ein BACKUP!

Ich benutze auf meinem Rechner Ubuntu, da hab ich mir einfach die adbtools installiert. Anleitungen dafür fliegen ja zu Hauf herum. Auch wie man ADB auf dem Smartphone aktiviert, findet man haufenweise und sollte kein Problem darstellen.

Wenn man nun eine ADB-Shell startet, kann mittels dem Befehl

<pre class="wp-block-code"><code>pm list packages | sort</code></pre>

alle auf dem Gerät installierten Pakete sehen. Um ein Paket zu deaktiveren, benützt man folgenden Befehl:

<pre class="wp-block-code"><code>pm disable-user --user 0 &lt;Package Name></code></pre>

Will man ein Paket wieder aktiveren, braucht man diesen:

<pre class="wp-block-code"><code>pm enable --user 0 &lt;Package Name></code></pre>

Hilfreich ist für diese Aufgabe einerseits die App &#8222;Package Name Viewer 2.0&#8220;, die einem zu jeder App den Paket-Namen zeigt, und die unten stehenden Links geben auch eine gute Übersicht welche Apps vielleicht unnötig sein könnten. Wie bereits erwähnt, das kann ins Auge gehen bei gewissen Paketen, deshalb lieber etwas zu Vorsichtig sein als einen Schnellschuss zu wagen.

###### Links und Credit

<p style="font-size:12px">
  <a href="https://forum.xda-developers.com/honor-6x/how-to/guide-list-bloat-software-emui-safe-to-t3700814">https://forum.xda-developers.com/honor-6x/how-to/guide-list-bloat-software-emui-safe-to-t3700814</a><br /><a href="https://www.themefoxx.com/debloat-huawei-honor-uninstalling-system-apps/">https://www.themefoxx.com/debloat-huawei-honor-uninstalling-system-apps/</a><br /><a href="https://www.xda-developers.com/disable-system-app-bloatware-android/">https://www.xda-developers.com/disable-system-app-bloatware-android/</a><br /><a href="https://www.ytechb.com/how-to-debloat-huawei-phones/">https://www.ytechb.com/how-to-debloat-huawei-phones/</a><br /><a href="https://stackpointer.io/mobile/android-adb-list-installed-package-names/416/">https://stackpointer.io/mobile/android-adb-list-installed-package-names/416/</a>
</p>