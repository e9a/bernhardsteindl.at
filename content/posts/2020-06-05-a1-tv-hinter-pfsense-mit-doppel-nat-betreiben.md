---
title: A1-TV hinter pfSense mit Doppel-NAT betreiben
author: Christoph Scheidl
type: post
date: 2020-06-05T04:42:33+00:00
url: /a1-tv-hinter-pfsense-mit-doppel-nat-betreiben/
categories:
  - Allgemein

---
Da ich bei mir einen A1-Anschluss mit A1TV habe, und ich über m3u8-Playlists gerne auch TV zB auf meinem iPad sehen möchte, habe ich nach einer Lösung gesucht, per IGMP-Proxy die Multicast-Streams auch in meinem lokalen Netzwerk hinter einer pfSense-Firewall erreichbar zu machen. Weil Mulitcast über WLAN zB nicht wirklich gut funktioniert, greife ich noch auf ein kleines Programm namens udpxy zurück, dass mir die Multicast-Streams zu HTTP-Streams umwandelt.

Wie genau man das ganze einrichtet, könnt ihr gerne in meinem [Forum-Eintrag bei dieschmids.at][1] lesen!

###### Links und Credit

[https://www.dieschmids.at/forum/14-netzwerk-und-wlan/53167-a1tv-hinter-doppelnat-mit-pfsense-will-nicht-laufen?start=6#78349](https://www.dieschmids.at/forum/14-netzwerk-und-wlan/53167-a1tv-hinter-doppelnat-mit-pfsense-will-nicht-laufen?start=6#78349)

 [1]: https://www.dieschmids.at/forum/14-netzwerk-und-wlan/53167-a1tv-hinter-doppelnat-mit-pfsense-will-nicht-laufen?start=6#78349