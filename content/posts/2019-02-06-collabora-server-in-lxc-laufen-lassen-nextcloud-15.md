---
title: Collabora-Server in LXC laufen lassen – Nextcloud 15
author: Christoph Scheidl
type: post
date: 2019-02-06T11:53:42+00:00
url: /collabora-server-in-lxc-laufen-lassen-nextcloud-15/
categories:
  - Debian
  - nginx
  - Proxmox

---
UPDATE: sollte sich nach einem Kernel-Update der Docker-Dämon nicht mehr starten lassen, dann folgenden Befehl probieren:

<pre class="wp-block-code"><code>rm /var/lib/docker/aufs -rf</code></pre>

Danach den Dämon neu starten, dann sollte ein Neustart des Containers wieder funktionieren! UPDATE ENDE

Mit Nextcloud Version 15 gibt es nun eine neue und sehr interessante Funktion, mit der sich direkt im Webinterface der Self-Hosting-Cloud, und auch der dazugehörigen App, Dokumente, Tabellenkalkulationen und Präsentationen in unterschiedlichsten Dateiformaten ansehen und bearbeiten lassen. Dazu reicht es allerding nicht, die Funktion in der Nextcloud zu aktivieren, sondern es wird ein weiterer Dienst benötigt, der wie folgt mit Docker aufzusetzen ist:

#### Installation von Docker

Abhängig von eurer Betriebssystemwahl gibt es hier unterschiedliche Methoden, in diesem Beispiel verwendet wird wie immer Debian, allerding in einem LXC. Hier der Link des Herstellers zur Installationsanleitung:  
<https://docs.docker.com/install/linux/docker-ce/debian/>

Da wir das ganze in einem LXC installieren möchten, gibt es vor dem ersten Start eines Containers jedoch eine zusätzliche Maßnahme zu treffen. Dazu einfach in die Konfigurationsdatei des LXC (z.B.: 100.conf) folgende Zeilen hinzufügen (nachzulesen unter  
[https://www.solaris-cookbook.eu/virtualisation/proxmox/proxmox-lxc-running-docker-inside-container)][1]:

<pre class="wp-block-code"><code># insert docker part below
lxc.apparmor.profile: unconfined
lxc.cgroup.devices.allow: a
lxc.cap.drop:</code></pre>

Nun können wir den Docker-Container mittels folgenden Befehl starten (auf der Nextcloud-Seite wird vorher der Container heruntergeladen, Docker lädt allerdings bei fehlenden lokalen Datein selbständig nach, sodass man sich das hier sparen kann:

<pre class="wp-block-code"><code>docker run -t -d -p 9980:9980 -e 'domain=domain\\.der\\.nextcloud' -e 'dictionaries=de en es ..' --restart always --cap-add MKNOD collabora/code</code></pre>

Im Unterschied zu dem Befehl auf der Nextcloud-Seite habe ich das 127.0.0.1 vor den Ports weggelassen, weil ich später mit einem Reverse-Proxy von einem anderen Container auf den Dienst zugreifen möchte. WICHTIG ist auch zu beachten, dass die Domain, die im Parameter -e angeben wird, NICHT die der Collabora-Instanz, SONDERN DIE DER NEXTCLOUD IST, DIE NACHHER VERWENDET WIRD! (Man kann vielleicht erkennen, das mich dieser Umstand ein wenig verärgert hat, da hat wieder einige Zeit daran geglaubt, da das nicht so genau auf der Nextcloud-Website beschrieben ist).

Nun müssen wir am Reverse-Proxy noch folgende Konfiguration einspielen (Danke an  
<https://icewind.nl/entry/collabora-online/>):

<pre class="wp-block-code"><code>server {
    listen       443 ssl;
    server_name  office.example.com;

    ssl_certificate /path/to/certficate;
    ssl_certificate_key /path/to/key;
    
    # static files
    location ^~ /loleaflet {
        proxy_pass https://192.168.1.2:9980;
        proxy_set_header Host $http_host;
    }

    # WOPI discovery URL
    location ^~ /hosting/discovery {
        proxy_pass https://192.168.1.2:9980;
        proxy_set_header Host $http_host;
    }

   # main websocket
   location ~ ^/lool/(.*)/ws$ {
       proxy_pass https://192.168.1.2:9980;
       proxy_set_header Upgrade $http_upgrade;
       proxy_set_header Connection "Upgrade";
       proxy_set_header Host $http_host;
       proxy_read_timeout 36000s;
   }
   
   # download, presentation and image upload
   location ~ ^/lool {
       proxy_pass https://192.168.1.2:9980;
       proxy_set_header Host $http_host;
   }
   
   # Admin Console websocket
   location ^~ /lool/adminws {
       proxy_pass https://192.168.1.2:9980;
       proxy_set_header Upgrade $http_upgrade;
       proxy_set_header Connection "Upgrade";
       proxy_set_header Host $http_host;
       proxy_read_timeout 36000s;
   }
   location ~ ^/hosting/capabilities {
       proxy_pass https://192.168.1.2:9980;
       proxy_set_header Host $http_host;
   }
}</code></pre>

###### Links und Credit

<p style="font-size:12px">
  <a href="https://nextcloud.com/collaboraonline/">https://nextcloud.com/collaboraonline/</a><br /><a href="https://docs.docker.com/install/linux/docker-ce/debian/">https://docs.docker.com/install/linux/docker-ce/debian/</a> <br /><a href="https://www.solaris-cookbook.eu/virtualisation/proxmox/proxmox-lxc-running-docker-inside-container/">https://www.solaris-cookbook.eu/virtualisation/proxmox/proxmox-lxc-running-docker-inside-container</a><br /><a href="https://icewind.nl/entry/collabora-online/">https://icewind.nl/entry/collabora-online/</a>
</p>

 [1]: https://www.solaris-cookbook.eu/virtualisation/proxmox/proxmox-lxc-running-docker-inside-container/