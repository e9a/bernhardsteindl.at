---
title: 'Social Media neu gedacht: Willkommen bei voi.social'
author: Bernhard Steindl
type: post
date: 2020-03-27T22:39:58+00:00
url: /social-media-neu-gedacht-willkommen-bei-voi-social/
categories:
  - Allgemein

---
Am 26. März startete auf der Infrastruktur von IT Kaufmann ein neuer Node des sozialen Netzwerks Mastodon. Was Mastodon ist und was mein Ziel ist, wird in diesem Beitrag erklärt.

Anfangs mal ein Video von den Machern des Sozialen Netzwerks:

Ich trage nun meinen Beitrag dazu bei und habe mit Dominik Dafert ([Website][1]) und ein paar weiteren Kollegen die Domain voi.social erworben und die Instanz aufgesetzt.

Mastodon ist vergleichbar mit Twitter, die User-IDs bestehen aus @username@instanz-domain.org, beispielsweise bei mir @bsteindl@voi.social. Als zusätzliches extra gibt es auch noch bei voi.social die Verlinkung von <username>.is.voi.social. Ja, nichts weltbewegendes, aber immerhin.

Das Netzwerk ist Föderal, das heißt, ich kann auch mit allen anderen Instanzen kommunizieren. Dann ist das Netzwerk gar nicht mehr so klein und man kann mit vielen (wenn auch meistens IT-affinen) Personen interagieren.

Daher mein Aufruf: Erstellt euch einen Account, ladet euch eine von den [Apps][2] herunter und schaffen wir eine freie Alternative zu Facebook, Instagram, Twitter und co.

## Registrierungsanleitung

### 1. voi.social besuchen

<div class="wp-block-group">
  <div class="wp-block-group__inner-container">
    <div class="wp-block-group">
      <div class="wp-block-group__inner-container">
        <p>
          <a href="https://voi.social/about">Hier klicken</a> und registrieren
        </p>
      </div>
    </div>
    
    <h4>
      2. E-Mail bestätigen
    </h4>
    
    <p>
      Öffne dein E-Mail-Programm und bestätige deine E-Mail-Adresse
    </p>
    
    <h4>
      3. Fertig!
    </h4>
    
    <p>
      Nun kannst du dein Profilbild festlegen und Personen suchen, denen du folgen kannst!
    </p>
    
    <h2>
      FAQ
    </h2>
  </div>
</div>

### Du machst sicher voll viel Spionage!

Nein, mache ich nicht. Ich versuche gerade, Mastodon zu trainieren, noch weniger Daten zu sammeln. Leider gibt es keine bessere Variante, als IP-Adressen zumindest für eine kurze Zeit zu sammeln, damit man Spam bekämpfen kann. Wenn das ein Problem ist, empfehle ich, die Instanz mit dem [Tor-Browser][3] mit dem .[onion-Lin][4]k aufzurufen.

### Klarnamenpflicht? Regeln?

Findest du [hier][5].

### Warum soll ich einen Account erstellen? Das nutzt ja keiner!

Ich nenne dieses Problem Henne-Ei-Problem: Ohne Benutzer keine Inhalte, ohne Inhalte keine Benutzer. Irgendwo muss man diese Kette aufbrechen, dasselbe haben wir bei Messenger, Betriebssystemen, etc.

[Dann nichts wie los!][6]

 [1]: https://dafnik.me
 [2]: https://joinmastodon.org/apps
 [3]: https://torproject.org
 [4]: http://rh7u4ia5hsy3epzvcuqvx6kqhs7m4puqicke5j33c6w3ksq7gianrlyd.onion/
 [5]: https://voi.social/terms
 [6]: https://voi.social