---
title: Bootbaren Windows-Stick auf Linux erstellen
author: Christoph Scheidl
type: post
date: 2021-04-10T18:30:39+00:00
url: /bootbaren-windows-stick-auf-linux-erstellen/
classic-editor-remember:
  - block-editor
categories:
  - Admin-Tipps
  - Debian
  - Grundlagen
  - Windows

---
Als Linux-User ist man normalerweise verwöhnt, dass man Images, die man auf einem USB-Stick bootbar aufspielen will, einfach mit `dd` raufschreibt. Leider funktioniert das bei Windows meist nicht so einfach, weil man einen Master Boot Record braucht. Hier nun eine mögliche Lösung, wie man es trotzdem schaffen kann.

Hinweis: solltet ihr leicht wo Zugriff auf einen Windows-Rechner haben, nehmt diesen und installiert euch einfach Rufus. Geht meistens schneller.

Ansonsten hier die Anleitung unter Linux:

Zuerst lädt man sich ms-sys von Sourceforge herunter und kompiliert sich dieses (notwendig dafür sind die build-essential). Das Kompilieren selbst geht ganz einfach mit `make && sudo make install`.

Danach legt man auf dem USB-Stick eine NTFS-Partition an, z.B. mit fdisk:

<pre class="wp-block-code"><code>sudo fdisk /dev/sdb
n
p
ENTER
ENTER
ENTER
t
7
w
sudo mkfs.ntfs -f /dev/sdb1</code></pre>

Wenn das erledigt ist mountet man sich das ISO-File und die neue Partition und kopiert den Inhalt der ISO-Datei auf die Platte:

<pre class="wp-block-code"><code>sudo mount -t udf -o loop,ro,unhide WIN10.iso
sudo copy -avr /ISO_MOUNTPOINT /USB_MOUNTPOINT</code></pre>

Wenn das erledigt ist, kann man mit folgendem Befehl und dem Tool ms-sys den Master Boot Record anlegen:

<pre class="wp-block-code"><code>sudo ms-sys -7 /dev/sdb
sudo sync
sudo unmount /USB_MOUNTPOINT</code></pre>

Jetzt kann man auch schon von dem Stick booten.

###### Links Und CreDit

<p style="font-size:10px">
  <a href="https://www.cyberciti.biz/faq/create-a-bootable-windows-10-usb-in-linux/">https://www.cyberciti.biz/faq/create-a-bootable-windows-10-usb-in-linux/</a><br /><a href="https://sourceforge.net/projects/ms-sys/">https://sourceforge.net/projects/ms-sys/</a><br />
</p>