---
title: ThinkPad T495s – Probleme mit Linux Kernel 5.3
author: Christoph Scheidl
type: post
date: 2019-12-16T19:34:45+00:00
url: /thinkpad-t495s-probleme-mit-linux-kernel-5-3/
categories:
  - Hardware

---
Vor kurzem habe ich ein System-Upgrade von Kubuntu 19.04 auf 19.10 gemacht. Danach gingen vor allem 2 Dinge nicht mehr: DNS und Standby bzw. Aufwachen vom Standby.

Für das DNS Problem gab es schnellen Fix, einfach den Dienst `systemd-resolved` neu gestartet bzw. wieder enabled, also:

<pre class="wp-block-code"><code>sudo systemctl restart systemd-resolved
sudo systemctl enable systemd-resolved</code></pre>

Das 2. Problem war etwas schwieriger zu lösen. Viele Leute mit Lenovo Rechnern mit AMD-CPUs haben dieses Problem seit dem Kernel 5.1.16.

Was bei meinem ThinkPad T495s die Lösung brachte war die Kernel-Option `amd_iommu=off`, siehe [hier][1]

Kurz beschrieben:

<pre class="wp-block-code"><code>sudo nano /etc/default/grub

GRUB_CMDLINE_LINUX_DEFAULT= &#91;BESTEHENDE PARAMETER] amd_iommu=off</code></pre>

 [1]: https://www.reddit.com/r/thinkpad/comments/djihpo/amd_thinkpads_keep_getting_better_53_kernel/fb5ba06?utm_source=share&utm_medium=web2x