---
title: mariadb in unprivileged LXC-Container nach Debian 10 (buster) Upgrade wieder lauffähig bekommen
author: Christoph Scheidl
type: post
date: 2019-08-27T06:00:24+00:00
url: /mariadb-in-unprivileged-lxc-container-nach-debian-10-buster-upgrade-wieder-lauffaehig-bekommen/
categories:
  - Admin-Tipps
  - Proxmox

---
Gestern habe ich testweise eine Test-Nextcloud in einem unprivileged LXC-Container auf Debian 10 upgegraded. Leider wollte danach die Cloud nur mehr einen weißen Bildschirm zeigen. Die Lösung: Die mariadb-server Instanz wollte nicht mehr starten. Hier nun ein Workarround, wie das ganze fürs erste wieder lauffähig wird. Leider handelt es sich nämlich um einen Bug in LXC oder apparmor mit Debian 10, siehe [hier][1].

Um den Fehler zu beheben, entfernt man folgende Zeilen:

<pre class="wp-block-preformatted">ProtectSystem=full
PrivateDevices=true
ProtectHome=true</pre>

In folgender Datei:

<pre class="wp-block-preformatted">/lib/systemd/system/mariadb.service</pre>

Danach noch den Befehl

<pre class="wp-block-code"><code>systemctl daemon-reload</code></pre>

ausführen, und den Service neustarten, damit funktioniert das ganze wieder.

###### Links und Credit

<p style="font-size:12px">
  <a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=920643">https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=920643</a>
</p>

 [1]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=920643