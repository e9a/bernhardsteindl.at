---
title: "Shorty is weg?!"
date: 2021-12-02T22:20:58+01:00
draft: false
---


# Was jeder wusste

Statt zur Seite treten wurde es Ernst. Basti ist Geschichte. Nicht dass damit irgendein Problem
in diesem Land gelöst wurde, nein, aber nun könnte man echt einen Neubeginn starten.

## Seit 2017 regiert das Chaos

Seit Sebastian Kurz 2017 die Partei übernommen hatte, wurde 2 Mal vorzeitig gewählt, Kickl kaufte Polizeipferde, Ibiza flog auf und die Pandemie begann. Politische Gegner wurden egal auf welcher Seite (Blau oder Grün) an der Wand zermahlen. Bis die WKSTA kam.

## Fragwürdige Umfragen

Es gilt die Unschuldsvermutung. Eh kloa! Es gilt aber auch die Ungustl-Vermutung. Denn wer Umfragen bei Ö24 (*speib*) kauft, den kann man eh vergessen. Abgesehen davon, dass die garnicht notwendig gewesen wären, 2017 haben ihn sogar noch die vernünftigen ÖVPler gewählt.

## 2019: Here we go again

Nachdem sensationellen Ibiza-Video durften wir wieder zur Wahlurne schreiten -> diesmal verloren wir nicht nur unseren heißgeliebten Freund St. Rache sondern auch meine geliebte Schwarz-Blaue Regierung, welche nur durch ihre Unfähigkeit zusammengehalten wurden. Gleichzeitig darf man nicht vergessen, dass das bereits das letzte Jahr ohne Pandemie war. Unglaublich, wie schnell die Zeit vergeht.

## 2020: Pandemie

Als wir dann endlich eine neue Regierung hatten, war die Freude auch gleich mal verflogen, nachdem eine Pandemie zu managen war (oder ist?). Hier wurde immer wieder das Licht am Ende des Tunnels versprochen sowie ein Gesundheitsminister vernichtet. Leider wieder kein Problem gelöst, nur Milliarden verschmissen.

## Sebastians Glück und Ende

Ein Glück das der Sebastian Papa wird und man so ein unrühmliches Ende verhindern konnte. Möge ihn die Raiffeisen wie alle anderen gescheiterten ÖVPler einen schönen Versorgungsposten geben, damit auch er einmal einen Beruf lernen darf.

Ende.
