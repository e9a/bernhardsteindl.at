---
# Title
title: "Hilfe ist gefragt - OpenSSL Geschwindigkeitsvergleich"
# Enter the author's name
author: Bernhard Steindl
# Customize URI; default: post/filename
#url: /yubikey-fuer-linux-login-als-u2f-2-faktor-verwenden/
# Date
date: 2023-07-27T13:00:00+02:00
# Is this draft?
draft: false
# Show Table of Content?
showToc: false
# Available categories. Please comment out!
categories:
#- Admin-Tipps
#- Allgemein
#- Arch Linux
#- Debian
#- Denkt's noch!
#- Grundlagen
#- Hardware
#- mariadb
#- News
#- nginx
#- Politik
#- Projekte
#- Proxmox
#- Rants
#- Satire
#- Synology
#- Tor
#- Ubiquiti Unifi
#- Windows
#- Wordpress
#- ZFS

---

Ein Post in eigener Sache - ich brauche für eine Arbeit meinerseits möglichst viele Benchmark-Ergebnisse der Crypto-Suite OpenSSL, um zu beweisen, dass Elliptic Curve-Verfahren wesentlich schneller und effizienter arbeiten - insbesonderer auf **alter** und **neuer, besonders schneller** Hardware.

Da ich selbst leider kaum alte Hardware besitze, bitte ich euch, folgende Kommandos auf irgendwelchen alten Geräten (bevorzugt mit einem Unix-artigen OS) auszuführen und mir die Ergebnisse zukommen zu lassen.

```
openssl speed

openssl speed dsa2048 ecdsap256 ecdsap521 ed25519

openssl speed ffdh2048 ffdh4096 ecdhp256 ecdhp521 ecdhx25519 ecdhx448

openssl speed rsa1024 rsa2048 rsa4096 rsa7680 ecdsap256 ed25519

openssl speed -evp chacha20-poly1305 aes-256-cbc aes-128-cbc

openssl version
```

Falls ihr eine Fehlermeldung bekommt, bitte ich euch, auf ein aktuelleres OS umzusteigen (es kann sein, dass ein paar meiner explizit genannten Algorithmen), ich selbst testete alles unter einem OpenSSL 3.1.1.

Falls ihr noch kein OpenSSL installiert habt, unter gängigen Linux-Distributionen ist dies mit `apt/dnf/pacman install openssl` schnell erledigt, auch unter macOS mit `brew install openssl` sowie unter Windows finden sich [hier](https://wiki.openssl.org/index.php/Binaries) fertige Binaries.

Die gesamte Prozedur dauerte etwa 15 Minuten bei meiner aktuellen Hardware, ich gehe davon aus, dass bei entsprechend antiker Hardware durchaus auch Stunden dauern kann.

Folgende Hardware wäre noch super interessant:
- Hardware Pre-2010;
- Generell die langsamsten Geräte, die ihr finden könnt
- Raspberry Pi 1. Generation
- Alte Server-Hardware
- Sehr schnelle, moderne Hardware wie Server, AMD Ryzen 8 oder 12-Kerner sowie leistungsstarke Notebooks im Vergleich zum MacBookPro 2022

Wie gesagt bitte ich euch, die Ergebnisse mir zukommen zu lassen. Bei Gelegenheit lasse ich auch ein Bier springen.