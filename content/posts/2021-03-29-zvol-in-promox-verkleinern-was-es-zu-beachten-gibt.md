---
title: ZVOL in Promox verkleinern – Was es zu beachten gibt
author: Christoph Scheidl
type: post
date: 2021-03-29T06:50:58+00:00
url: /zvol-in-promox-verkleinern-was-es-zu-beachten-gibt/
classic-editor-remember:
  - block-editor
categories:
  - Proxmox
  - ZFS

---
Hier ein paar kurze Eckpunkte, was es zu beachten gibt, wenn man ein ZVOL in Proxmox verkleinern will (in meinem Fall eine Windows-Partition):

  * NTFS verkleinern -> am besten mit EaseUS Partition Master Free
  * Alle Partitionen verschieben, sodass der freie Platz ganz hinten ist.
  * zfs set volsize=200G rpool/vm-100-disk-0 (ACHTUNG, 3x kontrollieren ob das auch stimmt!)(Kann sehr lange dauern)