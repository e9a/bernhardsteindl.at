---
title: 'Paris: Deppate Magnetstreifen und Interrail-Abzocke'
author: Bernhard Steindl
type: post
date: 2019-07-07T12:58:31+00:00
url: /paris-deppate-magnetstreifen-und-interrail-abzocke/
categories:
  - Allgemein

---
Ich wollte eigentlich während meiner einmonatigen Reise durch Europa Ruhe bewahren und mich nicht melden. Leider veranlasst mich das Verkehrssystem in meinem zweiten Ziel Paris dazu, mich hier ein wenig auszuheulen.

## Das Zugangssystem

Das Pariser Öffisystem ist relativ dicht und gut ausgebaut, mit über 15 Metrolinien, dazu einige S-Bahn und Vorortsysteme gibt es an der Erreichbarkeit jeglicher Ziele in Paris und Umgebung nichts zu meckern.

Leider gibt es im Gegensatz zu Wien ein geschlossenes Zugangssystem, welches den Zugang entweder mit Navigo-Karten (einem NFC-artigen System, für Dauerkarten gedacht) und Magnetstreifen. Diese werden für einzelne Fahrten und Tagestickets genutzt. Jede einzelne Station im kompletten Netz in der Stadt hat eine strenge Zutrittskontrolle, ohne den Magnetkarten kommt man weder rein noch raus.

Nun, man denkt sich nichts dabei, kauft sich ein Ticket für eine Fahrt und legt los. Wir lebten im Pariser Vorort Bois-Colombes, indem man den Pariser Bahnhof Saint-Lazare in Zehn Minuten erreichen konnte.

Wir stiegen in Saint Lazare aus und beim Hinausgehen funktionierte mein Ticket nicht mehr. Ich versuchte es nochmal, das Ticket funktionierte nicht mehr.

Das war aber kein Einzelfall. Am nächsten Tag kaufte ich mir ein Tagesticket, welches den ganzen Tag unbegrenzte Fahrten erlaubte. Ich brauchte an dem Tag 3 Magnetstreifentickets! Jedes mal, wenn eines versagte, meldete ich mich beim Schalter, durfte dort darüber diskutieren, das ich nichts falsch gemacht habe (was kann man bei einem unbegrenzten Ticket falsch machen?) und das sie bitte nicht mir die Schuld für ihr System geben sollen. Am Schluss fand ich mich darüber ab, einfach über die Zugangskontrollen hinweg zu setzen, ich habe ja ein gültiges gekauftes Ticket, ich hab ja was besseres auch zu tun!

## Die französische Eisenbahn

Bezüglich Pünktlichkeit gibt es hier absolut nichts zu meckern. Das System funktioniert gut und daran zweifle ich nicht. Aber die Art und Weise, wie hier mit Interrail-Passbesitzern umgegangen wird, ist absolut nicht in Ordung.

Ich habe einen Interrail-Global Pass für den gesamten Monat Juli. Was ist nicht inklusive? Etwaige Reservierungen. Nun, in Österreich, Deutschland und der Schweiz kein Problem, entweder man fährt auf Gut Glück und bekommt vielleicht keinen Platz oder man reserviert sich um 3€ einen Platz. Das ist in Ordnung.

Aber die französische Eisenbahn hatte hier eine geniale Idee! Die Hochgeschwindigkeitszüge TGV, Thalys und Eurostar benötigen zwingend eine Reservierung. Die Reservierung ist bei gekauften Tickets automatisch mit dabei und der normale Fahrgast muss sich auch nicht darum kümmern. Doch welche Fahrgäste müssen sich dann Reservierungen kaufen? Genau, Interrail-Besitzer. Eine kurze Auflistung an Beispielen, wieviel mir die Reservierung gekostet hätte:

  * TGV Lyris (Zürich-Paris): 41€ pro Person
  * Thalys (Paris-Brüssel): 55€ pro Person
  * Eurostar (Brüssel-London): 30€ pro Person

Ein fettes &#8222;Gehts scheißen!&#8220; meinerseits. Die SBB-Schalter in Zürich halfen mir gutmöglichst, diese Schikanen zu umgehen. Im Beispiel Zürich-Paris fuhr ich statt mit dem direkten TGV mit der Schweizer Bahn bis nach Straßburg und von da weg mit dem TGV. Kostenpunkt der Fahrkarte: 12€/Person. Fahrzeit: Eine Stunde länger, die wir in Straßburg warten mussten.

Paris-Brüssel (dem Zug, indem ich gerade Sitze) ist hier weit interessanter. Nachdem eine Reservierung für meine Freundin und mich 110€ gekostet hätte, versuchte ich online bei der belgischen Eisenbahn ein Ticket zu ergattern. Tara! Ein Erste-Klasse-Ticket kostet online nur 25€ Ich buchte zwei Tickets und nun sitze ich für den halben Preis einer Sitzplatzreservierung in der ersten Klasse.

Also wenn ihr mich fragt, ist das beinharter Beschiss!

Abgesehen davon ist das Schnellzugsystem in Frankreich spitze. Ich düse mit 320 km/h durch die Pampa und bin in einer Stunde in Brüssel. Ciao!