---
title: ddclient als Daemon für DynDNS verwenden
author: Christoph Scheidl
type: post
date: 2020-12-29T14:57:00+00:00
url: /ddclient-als-daemon-fuer-dyndns-verwenden/
classic-editor-remember:
  - block-editor
categories:
  - Admin-Tipps
  - Debian

---
Ich betreibe privat Server hinter dynamischen IP-Adressen. Mein Domain-Provider [core-networks.de][1] (kann ich sehr empfehlen), bietet für meine Domains auch einen DynDNS-Dienst an. Den nutze ich auf einer Debian-Maschine mit dem Tool ddclient.

Dieses Tool installiere und konfiguriere ich wie folgt:

  1. Installation mit dem Befehl `apt update && apt install ddclient`
  2. In der Datei `/etc/ddclient.conf` werden die Einstellungen für den DynDNS-Provider festgelegt, [core-networks.de hat hier][2] seine Config im FAQ stehen.
  3. In der Datei `/etc/default/ddclient` stelle ich folgende Optionen um:
      1. `run_ipup` auf `false`
      2. `run_daemon` auf `true`
  4. Mit dem Befehl `systemctl start ddclient` starte ich den ddclient als systemd-Dienst.
  5. Damit das ganze auch beim Neustart noch funktioniert, mache ich noch ein `systemctl enable ddclient`

Damit ist das ganze schon wieder erledigt, und meine IP-Adresse wird zuverlässig bei core-networks.de aktualisiert, sobald sie sich ändert.

 [1]: https://www.core-networks.de/
 [2]: https://www.core-networks.de/faq/225.html