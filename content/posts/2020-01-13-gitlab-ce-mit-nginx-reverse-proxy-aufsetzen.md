---
title: Gitlab CE mit nginx-Reverse-Proxy aufsetzen
author: Christoph Scheidl
type: post
date: 2020-01-13T11:09:26+00:00
url: /gitlab-ce-mit-nginx-reverse-proxy-aufsetzen/
classic-editor-remember:
  - block-editor
categories:
  - Debian
  - nginx

---
Hier die nginx Reverse-Proxy-Config für gitlab-ce

<pre class="wp-block-code"><code>server {
        listen 80;
        listen &#91;::]:80;
        server_name DEINE_URL;
        return 301 https://$server_name$request_uri;}

server {
    listen 443 ssl http2; 
    listen &#91;::]:443 ssl http2; # OCSP Stapling fetch OCSP records from URL in ssl_certificate and cache them
    server_name DEINE_URL; 

    ssl_stapling on;
    ssl_stapling_verify on;
    ssl_certificate /etc/letsencrypt/live/git.itkfm.at/fullchain.pem; 
    ssl_certificate_key /etc/letsencrypt/live/git.itkfm.at/privkey.pem; 

    resolver 9.9.9.9;
    ssl_ecdh_curve prime256v1;
    add_header Strict-Transport-Security "max-age=15768000; includeSubDomains; preload;";
        # Server should determine the ciphers, not the client
        ssl_prefer_server_ciphers on;

        # SSL session handling
        ssl_session_timeout 24h;
        ssl_session_cache shared:SSL:50m;
        
        add_header X-Content-Type-Options nosniff;
        add_header X-XSS-Protection "1; mode=block";
        add_header X-Robots-Tag none;
        add_header X-Download-Options noopen;
        add_header X-Permitted-Cross-Domain-Policies none;
        
        location / {
            proxy_pass https://DEINE_IP:443;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
    }
}</code></pre>