---
title: apache2 in LXC lauffähig bekommen
author: Christoph Scheidl
type: post
date: 2019-12-20T10:25:34+00:00
url: /apache2-in-lxc-lauffaehig-bekommen/
categories:
  - Admin-Tipps
  - Proxmox

---
Aktuell ist in Proxmox bei den LXCs ein Bug drinnen, der verhindert, das Apache2 richtig startet (Fehlercode: `Failed to set up mount namespacing: permission denied`). Um das Verhalten zu fixen, muss man in den Optionen des LXCs bei Features `nesting` aktivieren. Danach den LXC stoppen, starten, und es sollte funktionieren.

###### Links und Credit

<p style="font-size:12px">
  <a href="https://forum.proxmox.com/threads/apache2-service-failed-to-set-up-mount-namespacing-permission-denied.56871/">https://forum.proxmox.com/threads/apache2-service-failed-to-set-up-mount-namespacing-permission-denied.56871/</a>
</p>