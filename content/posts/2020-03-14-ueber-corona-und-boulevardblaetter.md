---
title: Über Corona und Boulevardblätter
author: Bernhard Steindl
type: post
date: 2020-03-14T17:53:31+00:00
url: /ueber-corona-und-boulevardblaetter/
categories:
  - Allgemein

---
Mein lieber Herr Fellner, liebe Krone-Redaktion, liebe Hamster-Käufer,

hört endlich auf, die Bevölkerung zu verunsichern. Als Sanitäter hat man Kontakt mit vielen verschiedenen, aber hauptsächlich alten Menschen und es ist unglaublich, welche Lügen und Falschmeldungen hier kursieren.

Die Leute decken sich unnötigerweise mit allerlei Zeug ein, da viele noch immer glauben, dass nächste Woche alles zu ist. Ich habe soviele absurde Dinge gehört, welche ich mir nur aus einer skurrilen Mischung aus Social Media und Boulevardblätter erklären kann. Ich habe mir deshalb gedacht, ich bastle schnell ein kleines Krisen-FAQ, um hier endlich mal mit Fakten zu arbeiten.

Meiner Meinung nach gehört der gesamte Boulevard (Österreich/Ö24, Heute, Krone) sowieso verboten, diese drei Medien verursachen journalistischen und realen Müll, der jeden Tag auf den Straßen in der Stadt herumliegt. Ganz ehrlich, schleichts eich!

## Ich hab gelesen, dass ab Montag alles zu hat!

Vergesst die sozialen Medien in Krisen. Verlässt euch auf offizielle Informationen. Weiter unten steht, wo ihr die bekommt.

Haltet euch auch von Ö24/Heute/Krone fern, sie [verbreiten offensichtlich Fake News][1].

Lebt euer Leben mit den paar Einschränkungen und Empfehlungen, die die Bundesregierung angekündigt hat (ganz unten).

## Warum gibts eigentlich ka Häuslpapier mehr?

Das liegt daran, dass gewisse Medien (hust) verbreiten, dass Klopapier das neue weiße Gold ist. Naja, jetzt streiten sich die Leute drum und es ist fast nirgendwo mehr zu kriegen, auch wenn mans wirklich braucht.

Ich weiß nicht, warum die Leute so dumm sind und das glauben. Übrigens gibt es auf willhaben gerade ein Spezial-Angebot!<figure class="wp-block-image size-large">

<img loading="lazy" width="586" height="158" src="https://blog.e9a.at/wp-content/uploads/2020/03/image.png" alt="" class="wp-image-693" srcset="https://blog.e9a.at/wp-content/uploads/2020/03/image.png 586w, https://blog.e9a.at/wp-content/uploads/2020/03/image-300x81.png 300w" sizes="(max-width: 586px) 100vw, 586px" /> <figcaption>Quelle: willhaben.at, 14.03.2020</figcaption></figure> 

## Warum sind die Regale leer?

Das liegt nicht daran, dass die Nahrungsmittellieferungen nicht kommen, sondern weil die Leute derzeit extrem viel einkaufen. Es gibt keine Gefahr, dass hier zu Engpässen kommt. Geht einfach normal einkaufen!

## Unternimmt das Rote Kreuz etwas dagegen?

Ja, es wurden einige Hygienemaßnahmen gesetzt und das ist meiner Meinung nach ausreichend. Es gibt besteht keine Gefahr, wenn man Hilfe benötigt. Einfach wie immer bei 144 anrufen, wenn ein Notfall besteht.

## Bleibt die Versorgung aufrecht?

Natürlich bleibt die Versorgung aufrecht, es gibt keine Engpässe, die nicht von den Menschen selbst verschuldet sind. Die Supermärkte kommen mit den Lieferungen nicht nach, da die Nachfrage so hoch ist, nicht die Lieferungen so gering!

## Du hast ja keine Ahnung, du bist ja Serveradmin

Ja, das ich kein Virologe bin, gebe ich gerne zu. Aber ich sammle hier Informationen, damit keiner Panik schürt.

## Wenn die alle Lügen, wer lügt dann nicht?

Die Gratiszeitungen leben davon, &#8222;Die Welt geht unter&#8220; als Headline zu titulieren. Wenn sich dadurch die Auflage erhöht, erhöhen sich die Werbeeinnahmen. Diese &#8222;Blattln&#8220; solltet ihr sowieso immer meiden, da sie immer Panik und Stimmung verbreiten, egal vor was.

Ich empfehle jetzt an dieser Stelle keine Medien, aber es gilt folgender Grundsatz: &#8222;Eine gute Zeitung erkennt man daran, dass sie kein Horoskop beinhaltet&#8220;

## Wo kann ich mich informieren?

Ich empfehle den ORF und die Informationsseiten der AGES und der Gesundheitsregierung. Auch der NDR macht gute Arbeit.

[NDR &#8211; Podcast mit dem Virologen Dr. Drosten][2]

[AGES &#8211; Informationen zum Coronavirus][3]

 [1]: https://www.reddit.com/r/Austria/comments/fi2w5s/oe24_sollte_verboten_werden_nur_meine_meinung/
 [2]: https://www.ndr.de/nachrichten/info/podcast4684.html
 [3]: https://www.ages.at/themen/krankheitserreger/coronavirus/