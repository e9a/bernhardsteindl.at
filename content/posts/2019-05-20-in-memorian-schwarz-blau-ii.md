---
title: 'In memoriam: Schwarz-Blau II'
author: Bernhard Steindl
type: post
date: 2019-05-20T18:55:16+00:00
url: /in-memorian-schwarz-blau-ii/
categories:
  - Politik

---
Ein guter Tag beginnt mit einem Rücktritt eines FPÖ-Politikers. Ja, es gab viele gute Tage seit Freitag und heute ist beinahe der beste.

Mich haben bereits ein paar Personen gefragt, wo mein Blogeintrag zur aktuellen Implodierung der FPÖ bleibt und ich antwortete stets mit: Hier fehlt noch eine Person, die gehen muss.

Ich spreche von Herbert Kickl, dem &#8222;Daham statt Islam&#8220;-Haus- und Hofreimer der Hirnbe**F**reiten **P**artei **Ö**sterreichs. Ich zitiere hier bewusst Reden und Statements von Politikern und Nationalratsabgeordneten, um aufzuzeigen, was hier in den letzten Monaten passiert ist. Für mich ist das Parlament eine Abbildung der Bevölkerung und deshalb finde ich, dass es nicht zu vernachlässigen ist. Ich möchte nicht, dass man vergisst, was diese Regierung seit der Wahl mit unserem Land angerichtet hat.

## 1. Das Ausreisezentrum<figure class="wp-block-embed-twitter aligncenter wp-block-embed is-type-rich is-provider-twitter">

<div class="wp-block-embed__wrapper">
  <blockquote class="twitter-tweet" data-width="550" data-dnt="true">
    <p lang="de" dir="ltr">
      "Welche Stimmung Sie hier erzeugen, nur damit Sie Stimmen bekommen, welcher Schaden das für die Gesellschaft ist. Und diesen Preis, den zahlen wir alle."<br /><br />Ganze Rede: <a href="https://t.co/X6K2VNtSOn">https://t.co/X6K2VNtSOn</a> <a href="https://t.co/s8KSYXSC8k">pic.twitter.com/s8KSYXSC8k</a>
    </p>&mdash; Irmgard Griss (@IrmgardGriss) 
    
    <a href="https://twitter.com/IrmgardGriss/status/1122042079100702720?ref_src=twsrc%5Etfw">April 27, 2019</a>
  </blockquote>
</div><figcaption>Dieser tollen Rede der ehemaligen Präsidentschaftskandidation Irmgard Griss (NEOS), ist absolut nichts hinzuzufügen.</figcaption></figure> 

## 2. Wer schafft die Arbeit?

Toll ist auch das Bemühen der Regierung, mit allen Mitteln einen weiteren Feiertag zu verhindern. Neben anderen &#8222;deppaten&#8220; Äußerungen, wie das [Auskommen mit 150€][1] oder dem rassistischen Ali-Video von FPÖ-TV hat sie bereits hinglänglich ihre absolute Unfähigkeit bewiesen und hätte bereits vor Monaten wegmüssen.

Ein paar Schmankerl:<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">

<div class="wp-block-embed__wrapper">
</div><figcaption>Man beachte das &#8222;Muhahahaha&#8220; am Anfang des Videos</figcaption></figure> <figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">

<div class="wp-block-embed__wrapper">
</div><figcaption>von FPÖ-TV veröffentlicht</figcaption></figure> 

## 3. Rauchverbot

Für die Inkompetenz von unserer ehemaligen Gesundheitsministerin reicht ein Punkt nicht aus, auch das Aufheben des bereits beschlossenen Rauchverbots in der Gastronomie war ein besonders furchtbarer Punkt. Nur in Österreich geht es, dass man im Jahr 2018 noch Raucherschutz vor Nichtraucherschutz stellt. Dieselben Abgeordneten, die vor Jahren für das Verbot stimmten, stimmen jetzt dagegen, da es der FPÖ so wichtig ist. Tolle Partei, die Volkspartei ist für das Volk da!

Sehen sie sich diese Rede von Matthias Strolz an. Diese 10 Minuten zahlen sich aus:<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">

<div class="wp-block-embed__wrapper">
</div><figcaption>Man beachte die vielen Zwischenrufe der ÖVP/FPÖ</figcaption></figure> 

## 4. Einschub Sachpolitik und FPÖ

Wenn sich irgendetwas in den Schwanz beißt, dann ist es sinnvolle Politik und FPÖ in einem Satz. Schon vor über 10 Jahren, hat der derzeitige Bundespräsident Alexander Van der Bellen, die Unfähigkeit von Strache bewiesen.<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-4-3 wp-has-aspect-ratio">

<div class="wp-block-embed__wrapper">
</div></figure> 

## 5. 140 auf der Autostrada

Experten empfehlen eine Senkung der Höchstgeschwindigkeit in Österreich auf 100 km/h auf Autobahnen und 80 auf Landstraßen. Was schlägt Norbert Hofer vor? Noch schneller auf Autobahnen fahren und Verzögerung von wichtigen Schienenprojekte in Österreich.

Hauptsache, eine sinnlose Waldviertelautobahn wird einem Ausbau der Franz-Josefs-Bahn gleichgestellt, was eine Beleidigung aller Pendler ist, welche jeden Tag zwischen Gmünd und Wien 2h40 pro Strecke im Zug verlieren.

Auch Krems ist betroffen, die dringende Elektrifizierung zwischen Herzogenburg und Krems ist für das Projekt S-Bahn St.Pölten dringend notwendig und wurde jetzt auf 2025 verschoben. Tolle Leistung Norbert!

## 6. Fazit

Es gibt noch so viel mehr Themen, welche diese Regierung behandelt hat, ich kann aber nicht jedes einzelne davon durchkauen. Denken Sie nur an die folgenden Themen, rein sachlich bezogen:

  * Überwachungspaket
      * Registrierungspflicht für SIM-Karten (Laut Experten sinnlos)
      * Kennzeichenerkennung bei Autobahnen (Laut ASFINAG derzeit garnicht möglich)
      * Bundestrojaner (der Staat darf seine Bürger hacken)
      * **De-facto Abschaffung des Briefgeheimnisses**
      * [IMSI-Catcher][2]
  * Steuerreform
      * grundsätzlich gut, Experten bezweifeln Finanzierung
  * Integration
      * Mittel wurden gekürzt!
      * Deutschförderklassen (Sogar der Bildungsminister findet, dass es sinnlos ist)
  * Kickl&#8217;sche Ausdünstungen
      * Polizeipferde
      * Ausreisezentrum (siehe Irmgard Griss weiter oben)
      * Hetze gegen Ausländer, Islam und Zuwanderer
  * Österreich-App
      * viele Bugs, unfertig
  * Familienbeihilfe
      * **Die Indexierung je nach Herkunftsland ist eindeutig illegal, sie wurde trotzdem beschlossen**. (Gleichbehandlungsgrundsatz der EU)
      * Familienbonus deshalb, da man damit Kinder, die im Ausland wohnen benachteiligt -> Nur Steuererleichterung und bringt Personen mit wenig Einkommen fast nichts
  * Soziales
      * Reform der Mindestsicherung zu Sozialhilfe
          * Noch größere Unterschiede je nach Bundesland
          * Keine Entschärfung für Arme
          * Einsparungspotenzial = 0 => reine Symbolik
  * Sportminister
      * eigentlich nix

In den 1,5 Jahren ist viel passiert, die meiste Zeit jedoch mehr Symbol- als Sachpolitik.

<blockquote class="wp-block-quote">
  <p>
    Die meisten Gesetze wurden mit den Ängsten der Bevölkerung begründet und im Eilverfahren beschlossen.
  </p>
</blockquote>

 [1]: https://diepresse.com/home/innenpolitik/5475976/150-EuroMonat_HartingerKlein-und-die-FakeNewsSchleudern
 [2]: https://de.wikipedia.org/wiki/IMSI-Catcher