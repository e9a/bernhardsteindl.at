---
title: 'Rant: Bloadware auf billigen PCs'
author: Christoph Scheidl
type: post
date: 2018-12-13T12:36:54+00:00
url: /rant-bloadware-auf-billigen-pcs/
categories:
  - Rants

---
Heute hatte ich wieder 2 Rechner vor mir, die einfach nur mit total schwachsinnigen Müll voll waren. Die Hälfte der installierten Programme waren vom Hersteller unnötigerweise aufgespielt worden, um den PC unnötig auszubremsen. Das braucht doch keiner!