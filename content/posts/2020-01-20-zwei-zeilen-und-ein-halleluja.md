---
title: Zwei Zeilen und ein Halleluja
author: Bernhard Steindl
type: post
date: 2020-01-20T19:08:40+00:00
url: /zwei-zeilen-und-ein-halleluja/
categories:
  - Allgemein

---
Manchmal is wiakle zum Giftn. Zum Beispiel, wenn einem die Nextcloud seit dem Update auf 17 mal wieder ein bisserl herumspinnt.<figure class="wp-block-image size-large">

<img loading="lazy" width="1024" height="280" src="https://blog.e9a.at/wp-content/uploads/2020/01/image-1024x280.png" alt="" class="wp-image-673" srcset="https://blog.e9a.at/wp-content/uploads/2020/01/image-1024x280.png 1024w, https://blog.e9a.at/wp-content/uploads/2020/01/image-300x82.png 300w, https://blog.e9a.at/wp-content/uploads/2020/01/image-768x210.png 768w, https://blog.e9a.at/wp-content/uploads/2020/01/image-1536x420.png 1536w, https://blog.e9a.at/wp-content/uploads/2020/01/image-2048x560.png 2048w" sizes="(max-width: 1024px) 100vw, 1024px" /> <figcaption>Wenn man analysiert, fällt einem der CSP-Header am Kopf</figcaption></figure> 

Nun, was ist CSP? Hier geht es um die Ausführung von JavaScript, welches von einem Webbrowser ausgeführt werden soll und eine ziemlich große Angriffsfläche bietet. Daher hat man das mittels eines experimentiellen Header &#8222;hinpfuscht&#8220;, welcher bis heute kein offizieller Standard ist.

Um das Problem in diesem Zusammenhang zu verstehen, muss man wissen, dass im IPv4-Netz, die Adressen Mangelware sind. Also versucht man hier möglichst mit öffentlichen IP-Adressen zu sparen und versucht mit Krückenlösungen wie NAT, eine neue Technologie (IPv6) möglichst lange hinauszuzögern, damit man seine alten Scheißsysteme (und Gewohnheiten) ja nicht anpassen muss.

Daher haben auch wir Reverse-Proxies im Einsatz, welche hinter einer IP, nehmen wir mal die 77.244.22.11, beliebig viele Websites betreiben können, wobei der Webserver hier nur bei der Domain unterscheidet. Da wir aber trotzdem eine möglichst gute Abkapselung unserer Kunden (und im Privatbereich) Websiten brauchen, schieben wir unsere Nextclouds (und andere Software, bei der es gut ist, den Namespace nicht mit allen anderen zu teilen) in Container (welche laufend mehr Bugs erzeugen, als ich Unterhosen besitze aber nichts ist perfekt).

In diesen Containern läuft dann je nach Lust und Laune nginx oder Apache. Das Problem: Der Einfachheit halber verschlüsseln wir den Datenverkehr zwischen Proxy und Nextcloud-Container nicht und deshalb weiß dieser auch naturgemäß nicht von einer etwaigen Verschlüsselung (nach Außen).

Das bringt uns wieder zurück zu CSP. Nextcloud gibt bei den neueren Versionen mittels PHP automatisch CSP-Header mit, damit die faulen User das nicht selbst konfigurieren müssen. Trotzdem generiert die Nextcloud standardmäßig http statt https-Links, welche aber standardmäßig umgeleitet werden und eigentlich per se kein Risiko darstellen. Leider führt dies aber zu den oben gezeigten CSP-Errors, welche vor dem Laden von HTTP-Inhalten warnt, obwohl nginx diese ja sowieso auf https umgeschrieben hätte.

Ja, die zwei heiligen Zeilen, um der Nextcloud dies mitzuteilen sind nun (NC 17 und 18) diese hier (eingefügt in die config.php):

<pre class="wp-block-preformatted">'overwriteprotocol' =&gt; 'https',<br />'overwritewebroot' =&gt; '/'</pre>

Und dann sind wir eigentlich schon wieder fertig. Wenn man weiß wie.