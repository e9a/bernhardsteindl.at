---
title: Alte VHS-Kassetten digitalisieren
author: Christoph Scheidl
type: post
date: 2021-03-17T15:53:00+00:00
url: /alte-vhs-kassetten-digitalisieren/
classic-editor-remember:
  - block-editor
categories:
  - Debian
  - Hardware

---
Ich habe viele Kindheitsvideos von mir auf VHS zuhause, welche ich gerne digitalisieren möchte. Mit einem günstigen &#8222;Video-Grabber&#8220; und einem alten VHS-Player lässt sich das unter Linux problemlos durchführen.

Mein Videograbber hat einen STK1160-Chip, welcher schon einen Treiber im Linux-Kernel hat. Einfach den Stick anstecken, und das Videogerät wird als v4l2-Gerät erkannt und wird unter `/dev/videoX` gemountet (in meinem Fall als video4). Auch eine Sound-Karte stellt das Gerät zur Vefügung.

Nach zahlreichen Versuchen mit verschiedensten Software-Lösungen (VLC-Player unter Ubuntu ist eine einzige Katastrophe) bin ich schließlich bei OBS-Studio gelandet, welches die einfachste und rundste Möglichkeit ist, Video vom Grabber aufzuzeichnen. Ich kann euch dafür [dieses Video][1] sehr ans Herz legen.

Also einfach als Quelle ein V4L2-Device anlegen, ausprobieren welches Video-Device das richtige ist und schon sieht man am Bildschirm den Videoinhalt. Mit einem Rechtsklick auf die Quelle kann man die Option &#8222;Resize Output&#8220; auswählen, damit hat man gleich die richtige Größe eingestellt. Ich verwende für das Audio-Routing `PulseAudio`, im Tab `Recording` kann man OBS einfach als Quelle den Video-Grabber angeben und fertig. Möchte man in die Audio-Quellen reinhören, öffnet man die `Advanced Audio Propertier` und stellt Audio Monitoring auf `Monitor and Output`. Mit einem Klick auf &#8222;Start Recording&#8220; geht es dann auch schon los.

Weil mein Video-Grabber nur Composite-Video kann und mein VHS-Player Scart, hab ich mir mit 2 Krokodilklemmen kurzerhand selbst einen Adapter gebaut. (Mein VHS-Player hat extra Chinch-Audioausgänge, sonst muss man 6 Krokodilklemmen verwenden um Audio auch vom SCART-Signal zu extrahieren).<figure class="wp-block-embed is-type-rich is-provider-imgur wp-block-embed-imgur">

<div class="wp-block-embed__wrapper">
  <blockquote class="imgur-embed-pub" lang="en" data-id="rKk60eN">
    <a href="https://imgur.com/rKk60eN">View post on imgur.com</a>
  </blockquote>
</div><figcaption>SCART-Pinout</figcaption></figure> 

In meinem Fall habe ich ein SCART-Kabel hergenommen, hier wird Composite Video Input und Output gekreuzt (irgendwo logisch). Hab also beim Kabelende einfach Pin 20 auf den mittleren Chinch-Pin und Pin 18 auf den Ground vom Chinch-Kabel gelegt, und schon war ein Bild im OBS zu sehen.

 [1]: https://www.youtube.com/watch?v=yDVxLVz8PVE