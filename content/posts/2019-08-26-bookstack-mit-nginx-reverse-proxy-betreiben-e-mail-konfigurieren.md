---
title: 'Bookstack mit nginx Reverse-Proxy betreiben & E-Mail konfigurieren'
author: Christoph Scheidl
type: post
date: 2019-08-26T11:41:30+00:00
url: /bookstack-mit-nginx-reverse-proxy-betreiben-e-mail-konfigurieren/
categories:
  - Admin-Tipps
  - Debian
  - nginx

---
Bei [Bookstack][1] handelt es sich um eine Anwendung, die eine Dokumentenverwaltung bereitstellt. Leider ist die Doku sehr spärlich ausgeführt, warum ich hier nun kurz zusammenfasse, wie die Applikation hinter einem nginx Reverse-Proxy betreibbar ist.

Die Applikation selbst habe ich mit dem Installationsscript für Ubuntu-Server installiert.

Um E-Mail erfolgreich zu konfigurieren, muss man die Datei &#8222;/var/www/bookstack/.env&#8220; bearbeiten, damit sie folgende Einträge enthält:

<pre class="wp-block-code"><code># Application key
# Used for encryption where needed.
# Run `php artisan key:generate` to generate a valid key.
APP_KEY=censored

# Application URL
# Remove the hash below and set a URL if using BookStack behind
# a proxy, if using a third-party authentication option.
# This must be the root URL that you want to host BookStack on.
# All URL's in BookStack will be generated using this value.
APP_URL=https://your.domain.net

# Database details
DB_HOST=localhost
DB_DATABASE=bookstack
DB_USERNAME=bookstack
DB_PASSWORD=censored

# Mail system to use
# Can be 'smtp', 'mail' or 'sendmail'
MAIL_DRIVER=smtp

MAIL_FROM=test@test.at
MAIL_FROM_NAME=Your Bookstack Server

# SMTP mail options
MAIL_HOST=test.mail.at
MAIL_PORT=465
MAIL_USERNAME=test@test.at
MAIL_PASSWORD=censored
MAIL_ENCRYPTION=ssl


# A full list of options can be found in the '.env.example.complete' file.</code></pre>

Im Reverse-Proxy muss die Config folgendermaßen aussehen:

<pre class="wp-block-code"><code>server {
    listen 80;
    server_name your.domain.net; 
    return 301 https://$server_name$request_uri;
}

# BLOG SITE
server {
 listen 443 ssl http2;
 server_name your.domain.net;

## Source: https://github.com/1activegeek/nginx-config-collection
## READ THE COMMENT ON add_header X-Frame-Options AND add_header Content-Security-Policy IF YOU USE THIS ON A SUBDOMAIN YOU WANT TO IFRAME!

## Certificates from LE container placement
ssl_certificate /etc/letsencrypt/live/your.domain.net/fullchain.pem;
ssl_certificate_key /etc/letsencrypt/live/your.domain.net/privkey.pem;

ssl_ciphers ECDHE-RSA-AES256-GCM-SHA512:DHE-RSA-AES256-GCM-SHA512:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384;
ssl_ecdh_curve secp384r1; # Requires nginx >= 1.1.0
ssl_session_timeout  10m;

## NOTE: The add_header Content-Security-Policy won't work with duckdns since you don't own the root domain. Just buy a domain. It's cheap
## Settings to add strong security profile (A+ on securityheaders.io/ssllabs.com)

add_header Strict-Transport-Security "max-age=63072000; includeSubDomains; preload";
add_header X-Content-Type-Options nosniff;
add_header X-XSS-Protection "1; mode=block";
add_header X-Robots-Tag none; #SET THIS TO index IF YOU WANT GOOGLE TO INDEX YOU SITE!
add_header Referrer-Policy "strict-origin-when-cross-origin";
proxy_cookie_path / "/; HTTPOnly; Secure"; ##NOTE: This may cause issues with unifi. Remove HTTPOnly; or create another ssl config for unifi.
#more_set_headers "Server: Classified";
#more_clear_headers 'X-Powered-By';
 
 client_max_body_size 0; 
 
  
location / {
    proxy_pass http://DOMAIN.OF.YOUR.BOOKSTACK/;
    } 
}</code></pre>

 [1]: http://bookstackapp.com