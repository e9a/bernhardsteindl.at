---
title: miniDV-Kasetten mittels FireWire (i.Link) digitalisieren
author: Christoph Scheidl
type: post
date: 2021-04-10T19:23:29+00:00
url: /minidv-kasetten-mittels-firewire-i-link-digitalisieren/
classic-editor-remember:
  - block-editor
categories:
  - Hardware
  - Projekte

---
Sollen miniDV-Kasetten digitalisiert werden, ist es nicht ratsam, das über einen USB-Grabber zu machen, da das Signal so zuerst von Digital auf Analog, und dann wieder auf Digital umgewandelt wird (im Unterschied zu zB VHS). Bei miniDV handelt es sich nämlich um ein Digitales Videoformat (daher das DV in miniDV). Unsere Kamera hat einen 4-poligen FireWire-Anschluss (von Sony damals i.Link genannt), weshalb ich mich auf die Suche nach einer Möglichkeit begeben habe, diese Schnittstelle zu nutzen und so die miniDV-Kasetten ohne Qualitätsverluste auf den Computer zu übertragen. 

Ich habe mir zunächst eine FireWire-Karte auf Amazon bestellt (siehe [hier][1]). Ich habe eine alte PCI-FireWire-Karte bereits einmal erfolgreich getestet mit dem Camcoder, deshalb wusste ich, dass das ganze unter Linux möglich ist.

Ich habe also die neue FireWire-Karte in meinem Stand-PC eingebaut, das mitgelieferte Kabel angeschlossen, Ubuntu 20.04 installiert, dvgrab installiert, gestartet und siehe da: Die erste Ernüchterung. Es wird keine Kamera gefunden. Also los mit dem Troubleshooting

Zuerst also einmal mit `lspci` nachgesehen, ob die Karte denn auftaucht: Ja, tut sie.  
Als nächstes mittels `ls -lah /dev/fw*` ermittelt, ob eine Kamera auftaucht. Leider nein, nur ein Device mit `fw0` wird erkannt, das ist jedoch nicht meine Kamera.  
`dmesg` zeigt die ganze Zeit folgende Errors (Einträge waren mehrmals vorhanden):

<pre class="wp-block-code"><code>giving up on node ffc0: reading config rom failed: bus reset
rediscovering fw0</code></pre>

Um es kurz zu machen: Alte PCI-Karte probiert, altes Linux (Ubuntu 16.04) probiert, anderes FireWire-Kabel probiert, das ich seperat mitbestellt hatte, Windows probiert, alles zeigte kein Erfolg.

Dann dachte ich mir: Es ist alles wie beim letzten Mal, nur das FireWire-Kabel nicht. Habe also das Kabel gesucht, gefunden, auf dem neuen Rechner mit der neuen Karte unter Kubuntu 20.04 angesteckt, und siehe da, ohne Probleme, Plug&Play, Kamera wird erkannt, dvgrab kann gestartet werden.

Leider weiß ich noch nicht, wo genau die Unterschiede liegen zwischen dem Kabel das funktioniert und denen die nicht funktionieren. Auf dem funktionierenden steht nur oben `IEEE 1394 6P/M 6P/M` und dann ist noch ein mitgelieferter Adapter von 6-Pin auf 4-Pin angesteckt.

Ich werde in den nächsten Tagen ein anderes Kabel bei Amazon bestellen und schauen ob ich damit mehr Erfolg habe. Ich halte den Eintrag hier auf dem laufenden.

UPDATE: Das neue Kabel, das ich bestellt habe ([siehe hier][2]), funktioniert tatsächlich auf Anhieb. Kann ich also weiterempfehlen.

Auf alle Fälle kann nun mittels dvgrab eine Aufnahme angefertigt werden. Ich verwende immer den folgenden Befehl:

<pre class="wp-block-code"><code>dvgrab -autosplit -timestamp -size 0 -rewind -showstatus Name-</code></pre>

Dieser bewirkt, dass automatisch nach Zeitstempel die Aufnahme in Name-TIMESTAMP gesplitet wird. Außerdem wird automatisch das Band komplett zurückgespult. Ich muss bei meiner Kamera dann manuell auf Play drücken, der Rest geht automatisch.

Nachdem das durchgelaufen ist, hat man eine ziemlich große .dv-Datei. Diese wandle ich dann mit folgendem ffmpeg-Befehl in eine kleinere mp4 um:

<pre class="wp-block-code"><code>ffmpeg -i input.dv -vf yadif=0:-1:0,hqdn3d -crf 23 output.mp4</code></pre>

Wenn man mit ffmpeg noch schnell den Anfang oder das Ende wegschneiden will, kann man sich mit folgendem Befehl aushelfen:

<pre class="wp-block-code"><code>ffmpeg -ss 00:00:02.00 -i quelle.mp4 -to 03:32:00.00 -c copy ziel.mp4</code></pre>

Dabei ist mit `-ss` angegeben, wie viel vom Anfang weggeschnitten wird, und `-to` gibt die endgültige Zeit des Videos an (hier muss man also Subtrahieren).

###### Links Und Credit

<p style="font-size:10px">
  <a href="https://debianforum.de/forum/viewtopic.php?t=139647">https://debianforum.de/forum/viewtopic.php?t=139647</a><br /><a href="https://wiki.ubuntuusers.de/FireWire/">https://wiki.ubuntuusers.de/FireWire/</a><br /><a href="https://knowledge.ni.com/KnowledgeArticleDetails?id=kA00Z000000P8hiSAC">https://knowledge.ni.com/KnowledgeArticleDetails?id=kA00Z000000P8hiSAC</a><br /><a href="https://askubuntu.com/questions/956934/on-ubuntu-17-04-how-do-i-get-firewire-ieee1394-working">https://askubuntu.com/questions/956934/on-ubuntu-17-04-how-do-i-get-firewire-ieee1394-working</a><br /><a href="https://bergs.biz/blog/2017/01/26/capture-minidv-tapes-via-firewire/">https://bergs.biz/blog/2017/01/26/capture-minidv-tapes-via-firewire/</a><br /><a href="https://www.galfe.de/video-capture-via-firewire-unter-linux-mit-kino-notebook-mieten/">https://www.galfe.de/video-capture-via-firewire-unter-linux-mit-kino-notebook-mieten/</a><br /><a href="https://blog.devilatwork.de/windows-10-und-eine-minidv-kamera-per-firewire-nutzen/">https://blog.devilatwork.de/windows-10-und-eine-minidv-kamera-per-firewire-nutzen/</a><br /><a href="https://www.studio1productions.com/Articles/Firewire-1.htm">https://www.studio1productions.com/Articles/Firewire-1.htm</a><br /><a href="https://www.slashcam.de/info/video-uebertragung-auf-PC--vom-PanasonicNV-DS8-48871.html">https://www.slashcam.de/info/video-uebertragung-auf-PC&#8211;vom-PanasonicNV-DS8-48871.html</a><br /><a href="https://www.slashcam.de/forum/viewtopic.php?p=103530">https://www.slashcam.de/forum/viewtopic.php?p=103530</a>
</p>

 [1]: https://www.amazon.de/gp/product/B07S9G4XGB
 [2]: https://www.amazon.de/dp/B006OZC0MQ/