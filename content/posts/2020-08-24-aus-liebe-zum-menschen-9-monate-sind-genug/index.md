---
title: "Aus Liebe zum Menschen: 9 Monate sind genug"
author: Bernhard Steindl
type: post
date: 2020-08-24T08:00:12+00:00
url: /aus-liebe-zum-menschen-9-monate-sind-genug/
featured_image: images/headpicture-edited-825x510.jpeg
categories:
  - Allgemein
tags:
  - Rotes Kreuz Krems
  - Zivildienst
---
_Dieser Artikel fasst meinen Zivildienst beim Roten Kreuz Krems zusammen und stellt meine persönliche Meinung dar. Es besteht kein Anspruch auf Neutralität, journalistische Sorgfalt oder andere Leitlinien, die große Medien für sich festlegen_. [FAQs](/faqs-zu-aus-liebe-zum-menschen/)

## 0. Einleitung

Ich kann mich noch gut an meinem ersten Tag im Zivildienst erinnern. 01. Oktober 2019. Etwas surreal wirkt die Situation heute: Lange Warteschlangen in Tulln beim ersten Gesundheitscheck, angeordnet von meiner Zivildienststelle: Rotes Kreuz Krems.

Am Nachmittag fuhr ich als nächstes wieder in die Dienststelle zurück, mit einem Paket voll mit Uniform, Formularen und anderen Dingen. Bei meinem ersten Gespräch mit einem zukünftigen Vorgesetzten sprach er von der großen Verantwortung; und ich solle bitteschön den Tag noch genießen, das werde so schnell nicht mehr vorkommen. Womit er vollkommen Recht hatte.

Ich setzte mich zur Donau, mit dem Gefühl, nun 9 Monate im Dienste des Staates zu stehen. 48-Stunden-Woche. 12 Tage Urlaub. Ein paar Hundert Euro im Monat.

Ich war von Anfang an dagegen, zum Bundesheer zu gehen. Ich bin beinharter Pazifist und lehne jegliche Waffengewalt ab. „Die drei Monate mehr beim Zivildienst werden doch nicht so schlimm sein. **Ich mach ja etwas Sinnvolles.**“ (Ich bin mir ziemlich sicher, dass sich das die meisten auch vor ihrem Dienst dachten.) **Und doch habe ich mich in meinem Leben noch nie so sehr getäuscht.**

## 1. Die Ausbildung

In den ersten Wochen absolviert man die Sanitäterausbildung, welche eine „rudimentäre“ Gesundheitsausbildung darstellt. Ich habe für die anschließende Prüfung **mehr oder weniger nichts gelernt** und habs beim zweiten Mal auch geschafft.

{{< figure align=center src="images/ausbildung.jpg" caption="Auch wenn man während des Kurses schläft, hat man nur selten etwas verpasst, das für den Dienst wirklich wichtig war." >}}

Währenddessen hatte ich durchaus Probleme mit dem Akku meines iPads, 10 Stunden am Tag Zeitung zu lesen war dann doch zu viel und ich musste dann bereits während der Mittagspause wieder aufladen.

Die Ausbildung selbst ist zwar gut gemeint &#8211; doch ist sie viel zu wenig, um in einem Rettungseinsatz wirklich etwas machen zu können. Es gibt zwar weiterführende, dienststellenspezifische Ausbildungen, um am richtigen Rettungsauto mitfahren zu dürfen oder vielleicht sogar als Fahrer mit Blaulicht durch die Stadt zu brausen – diese kann man aber erst gegen Ende des Dienstes machen, damit man zuvor noch möglichst lange **„Taxler“ für Alte und Kranke** spielen darf. Daher kann auch die „einfache“ Fahrerausbildung – der „sicheren Einsatzfahrer“ – bereits recht bald im Laufe des Zivildienstes absolviert werden.

Abgesehen davon war die Ausbildungszeit am Anfang die vielleicht beste Zeit der kompletten 9 Monate: Fixe Arbeitszeiten, freier Samstag und keine dummen Regelungen, wie sie mich später noch erwarteten würden.

## 2. Auf der Dienststelle

Nun, dass man als Zivildiener wohl nicht zu den höherrangigen Mitarbeitern gehört, scheint wohl jedem halbwegs denkenden Menschen klar gewesen zu sein. Dass der Unterschied aber so gewaltig ist, kann sich wohl kein Außenstehender vorstellen.

Im Prinzip gibt es drei Arten von Mitarbeitern:

  * Zivildiener (yaay!)
  * Hauptberufliche
  * Ehrenamtliche Mitarbeiter („Freiwillige“)

Ich beginne mit den **Freiwilligen**: Diese leisten in Ihrer Freizeit Fast-Gratis-Arbeit und fühlen sich dabei wie Batman in Rot. Geschirrspülen, Mist wegräumen oder irgendetwas machen, wozu es einen nicht sonderlich „zaht“ (_„Geh holst uns a Essen vom McDonalds?“_) ist nicht cool.  
Das dürfen dann die machen, die nicht „Nein“ sagen dürfen (bzw. zumindest nicht sollten). Dem Roten Kreuz sind sie aufgrund der vielen Gratis-Arbeitsstunden heilig und deshalb werden hier auch Personen geduldet, die sich schon einiges geleistet haben. Wie immer bestätigen Ausnahmen die Regel.

**Die Hauptberuflichen** haben zuerst einmal meinen Respekt. Wer diese eintönige und langweilige Arbeit freiwillig für diesen Hungerlohn ableistet, macht das wirklich gerne – das merkt man dann auch – oder hat keinen besseren Job gefunden. Leider ist Zweiteres wesentlich öfters vertreten und so muss man sich mit überproportional vielen „Idioten“ herumschlagen.

Das Problem dabei ist, dass zumindest Zivildiener und Hauptberufliche **direkte Konkurrenten** sind. Wir Zivis sind wesentlich billiger, müssen mehr Stunden arbeiten und haben **keinerlei Arbeitnehmerrechte**. Dadurch sind auch während der Coronazeit österreichweit aufgrund der vermehrten Anzahl an [Zivildienern einige Hauptberufliche auf Staatskosten in Kurzarbeit gesetzt worden][1]. Ob das in Krems so war, kann ich nicht beurteilen – alleine die Konkurrenzsituation ist aber laut Zivildienstgesetz verboten, da ein Zivildiener keine Vollzeitkraft ersetzen darf.

Doch ist das in der Realität nicht doch so?

> Das legt das Zivildienstgesetz fest. Es regelt etwa, dass durch die Zuweisung von „Zivis“ keine bestehenden Arbeitsplätze bei Einrichtungen gefährdet sein dürfen. Dass dem nicht überall nachgekommen wird, kritisierte der Bundesrechnungshof bereits im Jahr 2016. In Bericht zum Zivildienst heißt es, dass „der Zivildienst in einigen Bereichen (insbesondere Rettungswesen) in einem Spannungsverhältnis zu der gesetzlich normierten Arbeitsmarktneutralität“ stehe.

Dies führt zu einem gewissen **Spannungsverhältnis** zwischen Mitarbeitern und Zivildienern. Wobei die Spitze des Eisbergs erst erreicht war, nachdem man im März „freiwillige“ Zivildiener besser bezahlte wie die eigenen Mitarbeiter und die Auftragslage knapp über Null war.

## 3. Die Missstände

Ich versuche jetzt – trotz aller Emotionalität, die selbst nach fast zwei Monate nach Beendigung des Dienstes stets noch gegeben ist – halbwegs objektiv zu bleiben und die Missstände korrekt wiederzugeben.

### 3.1 — 30 Minuten geschenkt &#8211; jeden Tag!

Es gibt eine „mündliche Regelung“, die es getreu dem Motto _„Jedes Schriftl is a Giftl“_ niemals auf ein Papier geschafft hat:  
**_Jeder Mitarbeiter hat sich bereits mindestens 15 Minuten vor Dienstbeginn auf der Dienststelle vorzufinden. Manche sind daher bereits 30 – 45 Minuten vorher da.**

Der Grund: Das Auto, in dem man die nächsten 8 Stunden verbringen wird, muss auf Vollständigkeit der Ausrüstung kontrolliert werden (obgleich die vorige diensthabende Mannschaft außerdem etwaigen Verbrauch wieder aufzufüllen hätte).

Nun, für mich war von Anfang an klar, dass ich hierfür nicht meine Freizeit opfern werde. Und wenn ich die Kontrolle schon mache, dann in der Dienstzeit. Für meine rechtmäßige Meinung hatte ich 9 Monate extremen Gegenwind und viele „klärende Gespräche“ und Ermahnungen.

Man wollte partout nicht davon lassen. Und auch alle neuen Zivildiener leisten hier blinden Gehorsam. Heute würde ich nicht mehr darüber diskutieren, sondern dies einfach bei der Bezirkshauptmannschaft anzeigen.

**Bitte checkt das mit der Dienstzeit ab. Ihr müsst in eurer Freizeit nicht arbeiten!**

Die Höhe war dann jener Tag, an dem ich ein Gespräch hatte, weil ich **nicht aus heiterem Himmel 30 Minuten früher in den Dienst gekommen** war und deshalb ein Patient im Auto wartete. Erstens konnte ich dafür nichts und zweitens ist es doch rechtens, pünktlich zu kommen. Die angedrohten rechtlichen Schritte blieben natürlich aus – die Dienststelle schießt sich ja nicht selbst ins Knie.

Ich recherchierte anschließend über die gesetzlichen Grundlagen des Zivildienstes und die Rechtmäßigkeit dieser Anordnung.

> § 4. (1) Die wöchentliche Dienstzeit hat mindestens der Zeit zu entsprechen, die nach den jeweils maßgeblichen Vorschriften für die mit im wesentlichen gleichartigen Dienstleistungen Beschäftigten der Einrichtung (Einsatzstelle) vorgesehen ist, und darf grundsätzlich 45 Stunden nicht überschreiten. Sie ist möglichst gleichmäßig auf die Arbeitstage aufzuteilen.

**„grundsätzlich“ &#8230; Wir hatten grundsätzlich 48 Stunden**

> (3) Im Falle eines Turnusdienstes kann die zulässige maximale Wochendienstzeit nach Abs. 1 bis zum Ausmaß von drei, jene nach Abs. 2 bis zum Ausmaß von zwei Stunden überschritten werden. Die Dienstzeit muß aber innerhalb eines achtwöchigen Zeitraumes so verteilt sein, daß sie im wöchentlichen Durchschnitt die nach Abs. 1 und 2 zulässige maximale Dienstzeit nicht überschreitet.

**Meiner Interpretierung nach heißt das, dass wir im 8-wöchigen Zeitraum auf 45h stehen müssen.**

50 Stunden sind nur erlaubt, wenn „regelmäßig und in erheblichem Umfang Arbeitsbereitschaft fällt“. Das ist bei uns nicht der Fall gewesen, leider ist hier die Verordnung viel zu schwammig.

Natürlich wehrte ich mich gegen diese widersprüchliche, interne Regelung und wollte sie zu Fall bringen. Das schaffte ich zwar nicht, aber ich konnte zumindest ein paar Kollegen dazu bringen, mir es gleichzutun und erst zum vereinbarten Dienstbeginn zu erscheinen. 

### 3.2 Umgang mit Zivildienern

Der nächste schwere Kritikpunkt ist die Tatsache, wie mit Zivildienern in der Dienststelle umgegangen wird. Es soll an anderen Dienststellen besser sein, in Krems war es jedoch zum Fürchten.

Um uns bestmöglichst **von den anderen, „normalen“ Mitarbeitern zu trennen**, gab es ein Zivikammerl – zynisch auch „Aufenthaltsraum Nord“ genannt. Wir durften zwar den Raum mit den „normalen“ Mitarbeitern betreten, doch gab es ein ungeschriebenes Gesetz, dass wir uns „bitte sehr!“ in unser Kammerl zurückziehen sollten. Dies wurde durch strenge Blicke und blöde Kommentare von den Kollegen im „Aufenthaltsraum Süd“ stets unterstrichen.

{{< figure align=center src="images/aufenthaltsraum-nord.jpeg" caption="Hier läuft der Fernseher vor 15 Uhr. Ob der Papa uns da jetzt den Popo aushaut?" >}}

Es ist verboten, den Fernseher vor 15 Uhr aufzudrehen. Keine Ahnung warum. Angeblich da sonst in der Früh keiner den Müll rausträgt. Das gilt aber nur für Zivildiener – die Freiwilligen und Hauptberuflichen dürfen das im anderen Raum natürlich rund um die Uhr.

**Kleinigkeiten**, wie das Nachräumen des Geschirrs und das Müllraustragen **summieren sich** nach 9 Monaten zu einem richtigen Frust zusammen. Zehn Mitarbeiter sitzen blöd vor dem Fernseher, bis einer aufsteht, extra in das „Zivikammerl“ geht und uns sagt, dass wir ihren Müll wegräumen sollen. Was soll die Scheiße?

**Jegliche Kritik** wurde mit _**„Seids froh, dass da sein könnts!“**_ und **_„Früher wars noch schlimmer!“_ abgeschüttelt**. Dass wir den Dienstplan (Wir hatten wechselnde Turnusdienste!) teilweise nicht einmal eine Woche vor dem Dienst hatten, wurde mehrmals als _Versehen_ abgestempelt. Wie soll man eigentlich irgendetwas planen, wenn man nicht einmal weiß, ob man jetzt nächste Woche Samstag oder Sonntag arbeiten muss?

Während des Lockdowns in der **Corona-Krise** brach das Krankentransportgeschäft komplett ein und ich saß nicht selten ganze 8 Stunden auf der Dienststelle blöd („unnötig“) herum. Weil das noch nicht genug war, holte man sich „freiwillige“ Zivildiener, die für viel Geld dann zusätzlich blöd herumsaßen und verlängerte auch noch jene, die eigentlich Ende März heimgehen wollten, bis Juni. Diese durften dann auch noch – mangels vorhandener Aufträge – blöd die Gegend beobachten. Das ist effiziente Personalpolitik!

### 3.3 Der Umgang mit mir

Mir wurde relativ früh klar, dass ich mit dem konservativen Weltbild _„Hände falten, Goschn halten!“_ nichts anfangen kann. Dies und ein paar andere wichtige Gründe hielten mich davon ab, mich in die museale Organisation Bundesheer zu begeben. Dass ich dann ausgerechnet in der schwarzen Hochburg „Rotes Kreuz“ landen würde, ist wohl mehr als nur Ironie.

Nachdem bei der Feuerwehr in Krems alle Plätze im gewünschten Zeitraum vergeben waren und ich keine Lust zu pendeln hatte, fragte ich beim Roten Kreuz Krems an. Im ersten Gespräch schien der Leiter recht nett und sprach von einem anstrengenden Dienst, bei dem vielleicht das ein oder andere Wochenende unter die Räder kommen könnte – ich korrigiere: JEDES EINZELNE WOCHENENDE. Ich hatte keine Ahnung, was mich da erwartete.

Bis hierher wird dieser Weg mit den meisten Zivildienern beim Roten Kreuz gleich sein. Das Marketing der Institution ist genial – [das funktioniert auch mit dem Blutspenden ähnlich gut][2].

Im Jänner 2020 (also Monat 4 des Zwangsdienstes) geisterten Gerüchte herum, **ich solle wohl versetzt werden**. Grund dafür wäre meine ablehnende Haltung zum „früher Kommen“ und vermutlich das „mich auf die Abschussliste setzen“ einiger Opportunisten, welche glaubten, sich durch derartige Schleimerei beim Vorgesetzten Vorteile zu erhaschen.

Das war wohl die mit Abstand schwierigste Zeit für mich. Jeden Tag wurde ich gefragt, wann ich wohl weg wäre, gefolgt von einem Kommentar, **dass mir das schon gehöre** (Man sieht: ein sehr kollegialer Verein). Denn wer sich der Obrigkeit widersetzt, muss mit dem Schlimmsten rechnen. Diese Unterwürfigkeit gegenüber jedem „Hallawachl“, der sich Chef nennen darf oder auch nur auf wichtig tut, ist mein Hauptkritikpunkt an dem ganzen Land. 

Das Letzte, das ich in meinem Leben hergebe, ist das Recht, ein Mensch zu sein. Während dem Zivildienst war ich kein Mensch. **Ich war ein Sklave.** Nur eine Nummer – im Fachjargon auch „Zivildienstzahl“ genannt.

Aufgrund ein paar glücklicher Fügungen bekam ich gegen Jänner mit, dass die Auftragslage in Abenddiensten sehr schwach ist und wandelte den Turnusdienstplan durch geschicktes Manövrieren mit den Dienstplänen, in einen „normalen“, statischen Dienstplan um. Nun hatte ich jeden Tag von 17-1 Uhr Dienst und konnte mir den Rest des Tages für „richtige“ Arbeit freihalten.

Das halte ich meiner Dienststelle wirklich bis heute zu Gute – kann aber die anderen Punkte bei Gott nicht ausgleichen.

Ich behaupte ohne zu Zögern, dass dies mit Abstand **die schlimmste Zeit meines Lebens** war. Nirgendwo war das Arbeitsklima schlechter, die Vorgesetzten unfreundlicher oder die Arbeit so beschissen wie hier.

### 3.4 Feiertagsdialysen

{{< figure align=center src="images/feiertagsdialysen-edited.jpeg" caption="Die Feiertagsdialysenliste — links die Spalte für den Fahrer, rechts für den Sanitäter." >}}

Wie auch jeder Krankheit egal ist, ob es Sonntag oder Montag ist, so ist es auch der Niere egal. Dadurch muss auch an Sonn- und Feiertagen ein gewisses Grundangebot abgedeckt werden. Was ist das Unfaire daran? Die Zuständigkeit fällt ausschließlich auf Zivildiener.

**Wie läuft das ab?**  
Ein paar Wochen vor einem Feiertag legt man Listen auf, bei denen man sich einschreiben kann, wann man Dialyse fährt. Hier wird seitens der Dienststellenleitung gebeten, dass sich das gleichmäßig aufteilt, sodass nicht jeder Zivildiener an jedem Feiertag (wie beispielsweise 25. und 26.12.) arbeiten muss.

Dass das nicht funktioniert, muss wohl jeder geahnt haben. Und hier wird wieder gerne mit „Divide et impera“ regiert, sodass man sich untereinander zerstreitet, anstatt hier endlich mal für Gerechtigkeit kämpft und dies auf alle Mitarbeiter gleichmäßig aufgeteilt wird. Die betroffenen Zivildiener bekommen **nicht einmal einen Wochenendzuschlag** oder Ähnliches – Überstunden werden lediglich 1:1 in Zeitguthaben umgewandelt, unabhängig davon, wann sie geleistet wurden.

**Wie sieht das gesetzlich aus?**  
Die Dienststelle kann die Zivildiener problemlos an einem Feiertag einteilen und muss – das ist der Wahnsinn – dies nicht einmal als Überstunden auszahlen, solange man in dieser Woche unter 48 Stunden ist!

Doch warum lässt uns die Dienststelle das manuell einteilen und macht das nicht von selbst?  
[Das hat den einfachen Grund, dass uns nicht Überstunden angeordnet werden dürfen, die Hauptberufliche nicht machen müssen][3]. Da wir uns diese „selbst“ zuteilen (müssen), sind diese nicht angeordnet. Und somit müssen dank diesem Trick nur Zivildiener an Feiertagen arbeiten.

#### 3.5 Das anstrengende Leben als Fahrer

Jeder hat schon einmal von den komplizierten (aber sinnvollen!) Regelungen für LKW- & Bus-Fahrer gehört. [Mindestruhezeit, erzwungene Pausen und andere Vorschriften gelten][4], um Berufskraftfahrer und alle anderen Teilnehmer im Straßenverkehr möglichst gut zu schützen.

**Preisfrage:** Wie viele dieser Regelungen gelten für Fahrer beim Roten Kreuz?  
  
Richtig gedacht – keine einzige! Man lässt hier 18-jährige Fahranfänger ruhigen Gewissens **12 Stunden (oder mehr) quasi durchfahren**. Natürlich nur aus Liebe zum Menschen – es geht hier natürlich nicht darum, den maximalen Profit aus uns Jungen zu ziehen. 

Möchte jemand mit einem übermüdeten Zivildiener nach einer 12-Stunden-Schicht mitfahren? (Bitte nicht gleich so viele, danke!)

Das ist nicht nur grob fahrlässig, sondern widerspricht auch allen ethischen und moralischen Grundsätzen. Doch die gibt es hier sowieso nicht; die würden den Gewinn schmälern (und die paar kaputten Autos im Jahr zahlt ja sowieso die Versicherung).

Bevor jetzt jemand meint: _„Moment, eine Mannschaft besteht doch eh aus zwei oder drei Sanitätern, die sich abwechseln können“_ – möchte ich nochmal, wie eingangs schon erwähnt, darauf hinweisen, dass für die Fahrer-Berechtigung eine entsprechende interne Ausbildung und Genehmigung erforderlich ist. Die hat nicht jeder und wird erst im Verlauf des Zivildiensts gemacht. Es darf somit leider getrost davon ausgegangen werden, dass ein Zivildiener oft **gar nicht die Möglichkeit hat, den Kollegen fahren lassen** zu dürfen.

Dies und ein paar andere Faktoren brachten mich dazu, am Ende meines Dienstes nicht mehr zu fahren, sondern **daneben zu sitzen und zu schlafen**. Das ist sowieso das Sinnvollste, das man in der Zeit tun kann.

## 4. Ich nenne es Zwangsarbeit

**Definition von [Zwangsarbeit](https://www.dwds.de/wb/Zwangsarbeit)**

{{< figure align=center src="images/zwangsarbeit.png">}}

Nun, manch ein Anhänger der Partei, welcher die unbedingte Erhaltung dieser Sklaverei fordert (ÖVP _\*hust\*_), kommt mir entgegen, dass es doch auch mal „anders“ sein kann, auf der untersten Ebene zu stehen – quasi „richtig lehrreich“ sozusagen.

**Soll ich euch sagen, was ich in 9 Monate dort gelernt habe?** Dass der ganze Verein verlogen ist. Wie man Pensionisten von A nach B bringt. Und das nichts und niemand diese Sklaverei je mehr vor mir rechtfertigen kann.

Nun, was würde man in Österreich als unterste Stufe betrachten? Das ist sehr abhängig von der Weltanschauung. Für mich sind das jene, die zum Beispiel scheinselbstständig als Paketdienstleister, in Schlachthöfen oder anderen schlecht bezahlten Berufen arbeiten. Gut, sowas hatte ich nie. Doch eine wesentliche Freiheit, haben diese Berufe – wie jeder anderer Job auch: Man kann prinzipiell den Hut drauf werfen und heimgehen.

Und am zweiten Tag meines Zivildienstes war es schon scheiße. Und ich wär am liebsten wieder gegangen; hab die Versetzungsliste durchgeschaut, aber nichts Vernünftiges gefunden.

Das ist der Punkt, der sich meiner Meinung nach am schlimmsten auf den Zivildienst auswirkt: Uns fehlt die Möglichkeit zu protestieren, Missstände korrekt zu besprechen und schlimmstenfalls zu gehen.

Die einzige Protestmöglichkeit, außer die illegalen Anordnungen zu ignorieren, war der kurzfristige Krankenstand. 5-Uhr-Dienste zwei Wochen lang? Leider krank. 12-Stunden-Dienst am Sonntag? Leider krank. So makaber das auch klingt: Das war unsere Lösung für Dinge, die wir nicht lösen konnten (oder für ungerecht hielten).

Da das alle machen, hat sich unser <span title="„Bester Innenminister aller Zeiten“" style="border-bottom:1px dotted #AAA">BIMAZ</span> Herbert Kickl eine schöne Regelung ausgedacht:  
**_Jeder, der mehr als 24 Tage krank ist, wird automatisch vom Zivildienst entlassen und muss dann später die restliche Zeit nachholen._  
** Gnädigerweise hat man den Zwangsverlängerten nochmal 24 Tage Krankenstand gegeben, damit aus den 3 unnötigen Extra-Monaten nur noch 2 werden.**

Randnotiz: Ich war übrigens 23 Tage krank.

## 5. Die Organisation Rotes Kreuz

![Fake-Meldung - Zellanalyse: Rotes Kreuz Krems überrumpelt ...][5] <figcaption>noen.at : Bezirksstelle Krems an der Donau</figcaption></figure> 

**„Aus Liebe zum Menschen“** &#8211; der Slogan hört sich gut an. Zumindest für Außenstehende. Für Zivildiener ist er eher verlautbart worden, wenn mal wieder etwas super gelaufen ist.

Vergesst den Slogan. Hier gehts zu wie in jeder Firma auch, nur dass man Zivis als Haussklaven für jegliche Arbeit hat. Stellt euch mal kurz vor, ihr habt in eurer Institution/Unternehmen einen Schani, dem ihr alles anschaffen könnt. Noch mehr als einem Praktikanten. Und im Gegensatz zu einem solchen dazu darf dieser aber nicht kündigen oder gar eine Anweisung ablehnen.

Ein gewisser [Henry Dunant](https://de.wikipedia.org/wiki/Henry_Dunant) wird wie ein Heiliger in der Kirche als Heiland des Roten Kreuzes verehrt. Doch mit barmherzigen Samaritern hat diese Organisation nichts mehr zu tun.

Seit den vielen Corona-Pressekonferenzen fiel die Parteinähe dieser Organisation mehr Personen auf als jemals zuvor. Geld bekommt die Organisation von Spenden, die auf Provisionsbasis wie Zeugen-Jehovas-Vertreter den Pensionisten aufgeschwatzt werden und hauptsächlich vom Staat.

Pro Zivildiener bekommt das [Rote Kreuz 600€/Monat Subvention, sowie 1700€ für die Ausbildung](https://zivildienst.gv.at/202/start.aspx) und noch weitere Förderungen, über die sich andere schon mehr als den Kopf zerbrochen haben. Trotzdem gehen sie regelmäßig an die Medien, um [nach Unterstützern zu betteln][6]. Irgendetwas läuft hier gravierend falsch, wenn man trotz spottbilliger Zivildiener den Apparat nicht finanziert bekommt.

Ich weiß nicht, ob ich hier nicht zu sozialistisch denke, aber so etwas Grundlegendes wie Rettungsdienst oder Krankentransporte sollte der Staat organisieren, nicht eine Organisation, in der sich jeder einzelne für Superman hält.

## 6. Forderungen

  * Abschaffung der unmenschlichen 48-Stunden-Woche
  * Abschaffung der regelmäßigen Wochenenddienste
  * Gleichberechtigung aller drei Berufsgruppen auf der Dienststelle
  * Nachträglich bessere Entlohnung aller Zwangs-verlängerten
  * Abschaffung der pseudo-selbsteingeteilten Feiertagsdialysen
  * Einführung einer verpflichtenden Mittagspause <small>(Richtig gelesen, die gibt es wirklich nicht!)</small>
  * Einführung von verpflichtenden Pausen und einer maximalen Fahrzeit von 6 Stunden pro Schicht

Nun zu den Verantwortlichen in Krems:

Ihr habt maximal dazu beigetragen, mir die 9 Monate so unmenschlich wie möglich zu gestalten. Mit eurer Ignoranz zu offensichtlich illegalen Regelungen, eurem Nichtstun bezüglich der Konflikte der Hauptberuflichen, habt ihr es sowohl uns als auch allen anderen Mitarbeitern schwer gemacht, auch nur eine Stunde hier genießen zu können.

Ihr wisst seit geraumer Zeit, was hier vor sich geht; Listen an unendlichen Forderungen liegen (auch eine von mir) schon lange am Tisch. Was leider fehlt, ist der Druck, endlich etwas zu tun.

Die Verantwortlichen sollen entweder endlich die erforderlichen Maßnahmen ergreifen oder zurücktreten. Das Fass – und mit dieser Aussage bin ich bei Gott nicht alleine – ist in Krems längst übergelaufen – nur hat dies noch niemand öffentlich gesagt.

## 7. Schlusswort

Liebe Kameraden vom Roten Kreuz,

wenn ihr es bis hierher geschafft habt und nicht alles überflogen habt, ist euch gerade eine Liste von Problemen entgegen gebracht worden Es wäre in eurem eigenen Interesse – sowohl aus Image- als auch aus Gründen des internen sozialen Friedens – die Arbeitsbedingungen für Zivildiener und den eigenen hauptamtlichen Mitarbeitern massiv zu verbessern.

Aufgrund der vielen Erschwernisse, der (meiner Meinung nach) illegalen, aber zumindest unfairen Regeln und der enormen Belastung ist der Begriff _Sklaverei_ für die letzten 9 Monate mehr als nur angebracht.

Ihr braucht gar nicht versuchen mich anzurufen. Mir ist das mittlerweile egal. Löst eure Probleme und lasst mich in Ruhe. Dieser Artikel ist mein Werk, um mit diesem schwierigen Kapitel endlich endgültig abschließen zu können.

Jetzt macht bitte euren Job aus demselben Grund, warum ich eigentlich bei euch Zivildienst machen wollte: **„Aus Liebe zum Menschen“**

Mit freundlichen Grüßen  
Bernhard Steindl

Weiterführende Links:

  * [FAQs zu diesem Artikel](/faqs-zu-aus-liebe-zum-menschen/)
  * [Diskussion auf Reddit][7]
  * [Reportage von Addendum (sehr lesenswert!)][8]
  * [Zivildienstserviceagentur (für Verordnungen und Gesetze)][9]

_P.S.: Könnt ihr mir bitte die Bestätigung für den Zivildienst per Post schicken? Ich will die heiligen Hallen des Henry Dunant eigentlich nicht mehr betreten._

 [1]: https://zackzack.at/2020/04/22/rotes-kreuz-schickte-fast-500-mitarbeiter-auf-kurzarbeit/
 [2]: https://www.addendum.org/blutspenden/das-blut-kartell/
 [3]: https://zivildienst.gv.at/403/files/Dienstzeit_VO_1988.pdf
 [4]: https://de.wikipedia.org/wiki/Lenk-_und_Ruhezeiten
 [5]: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fimages05.noen.at%2F440_0008_7702094_kre40rotes_kreuz_bezirksstelle_krems_kr.jpg%2Fteaser-col-8-2019%2F164.773.895&f=1&nofb=1
 [6]: https://www.noen.at/krems/unterstuetzer-gesucht-kremser-retter-mitglieder-sichern-rotkreuz-bestand-krems-wachau-gfoehl-rotes-kreuz-krems-rotkreuz-mitglieder-johann-paul-brunner-152208738
 [7]: https://www.reddit.com/r/Austria/comments/ifmo8s/rotes_kreuz_krems_zivildienst_aus_liebe_zum/
 [8]: https://www.addendum.org/zivildienst/
 [9]: https://zivildienst.gv.at/
