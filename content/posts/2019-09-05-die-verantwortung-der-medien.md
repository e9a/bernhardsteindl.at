---
title: Die Verantwortung der Medien
author: Bernhard Steindl
type: post
date: 2019-09-05T20:04:25+00:00
url: /die-verantwortung-der-medien/
featured_image: https://blog.e9a.at/wp-content/uploads/2019/09/shorty-kreuz-825x510.jpg
categories:
  - Allgemein

---
Heute ist schonwieder etwas passiert. Angeblich wurden die Server der ÖVP gehackt und Daten entnommen und _manipuliert_. Sagt zumindest Sebastian Kurz und ein Sicherheitsunternehmen. Am Dienstag veröffentlichte der Falter eine Excel-Tabelle, welche die &#8222;Doppel&#8220;-Buchhaltung der ÖVP belegt.

Hierbei handelt es sich aber nicht um die Doppelbuchhaltung, welche uns mühsam von unserer damaligen Wirtschaftslehrerin beigebracht wurde (mein Kommentar dazu übrigens: &#8222;Des is a nua dazu do, dass ma unnedig vü Aufwaund fia nix mocht&#8220;), sondern um die _tatsächlichen_ Wahlkampfausgaben von den offiziellen trennt.

### Doppelte Buchhaltung mal anders

<blockquote class="wp-block-quote">
  <p>
    Genau 8.976.781 Euro an Gesamtausgaben für den Wahlkampf ergibt seine Excel-Tabelle – um zwei Millionen Euro mehr als erlaubt. Es gibt aber noch eine zweite Spalte mit dem Namen „Betrag Soll – WK“, also Betrag Wahlkampf. Hier finden sich die gleichen Posten, aber mit der rechtskonformen Endsumme von 6.345.070 Euro. Und die Differenz von mehr als 2,6 Millionen Euro? Die wird kreativ weggebucht in eine dritte Spalte, die den Namen „Betrag Soll – nicht WK“ trägt, also „Nicht-Wahlkampf“.
  </p>
  
  <cite>Die doppelte Buchhaltung der neuen Volkspartei &#8211; Falter 36/19</cite>
</blockquote>

Das ist jetzt prinzipiell nichts neues, hat doch der Rechnungshof nach wie vor keine Einsicht auf die Konten der jeweiligen Parteien. Die können das so machen, es wird ihnen zumindest einige Zeit niemand draufkommen, es sei denn es wird geleakt. Und dazu kam es jetzt auch.

Laut Florian Klenk liegen dem Falter einige Dokumente vor, welche tief in die Interna der ÖVP greifen. Laut Eigenaussagen kommen diese Dokumente von einem Maulwurf, also jemanden, der direkt aus der Partei &#8222;zwitschert&#8220;. Gestern Abend kündigte Florian Klenk weitere Veröffentlichungen für den nächsten Falter, nächsten Dienstag an.

### Die Ibiza-Frage

Nun stellt sich die _Ibiza_-Frage: Dürfen Medien Dokumente/Videos veröffentlichen, auch wenn sie illegalerweise aufgenommen oder entnommen wurden?

Ja, das dürfen sie. Aber mit der Einschränkung, dass nur für die Öffentlichkeit relevante Dinge veröffentlicht werden dürfen. Nacktfotos zum Beispiel eines Politikers sind nicht so öffentlichkeitsrelevant, wie ein Vizekanzler welche gerne Geschäfte mit _schoafen_ Russinen abschließen würde.

Das wurde auch im Fall Ibiza bereits juristisch geklärt und darum wurde auch nur ein kurzer Zusammenschnitt des Videos veröffentlicht. Das andere Material war nicht öffentlichkeitsrelevant und hätte somit zu Problemen geführt.

Kommen wir also zur Frage zurück: Dürfen die Dokumente der ÖVP veröffentlicht werden, unabhängig von ihrer Herkunft? Ja, wenn sie relevant sind. 

Wir haben leider das Problem, dass die Betroffenen zu gerne von den Tatsachen des Materials ablenken und über die Herkunft sprechen. Aber die is größtenteils _wuascht_. Wenn ein amtierender Vizekanzler sich hier korrupt zeigt, ist das relevant, aber vielleicht nicht strafrechtlich bedeutend. Wenn die ÖVP kreative Berechnungsmethoden vorlegt, ist das auch relevant, aber legal.

Die Medien haben die Aufgabe, Missstände aufzudecken und die Bevölkerung zu informieren. Nur so kann Druck aufgebaut werden, der schlussendlich zu Veränderung führt. Wir können hier über den Klimawandel, den Hong-Kong-Protesten oder auch über Zwentendorf sprechen, bei denen die Bevölkerung mit den Medien zumindest versuchte, die Politik zu beeinflussen.

Übrigens ist es nicht nett, wenn man gewisse Medien, wie den Falter nicht mehr zu den Pressekonferenzen der ÖVP einlädt. Die neue Volkspartei hört sich hier eher an wie die alte, verstaubte Raiffeisen-Bauern-Partei, welche sie meiner Meinung nach noch immer und wahrscheinlich auch für immer, ist.

**Die Medien müssen die Verantwortung ernst nehmen.**

Quellen/Empfehlungen  
[Falter &#8211; Die geheime Buchhaltung der Liste Kurz][1] (Paywall)  
[Obermaier, Obermayer &#8211; Die Ibiza-Affäre][2]  
[Zutritt für Falter-Redakteure nicht erwünscht &#8211; Der Standard][3]  
[Geziehlter Angriff auf die ÖVP &#8211; Die Presse][4]  
(Bild) [Die Tagespresse][5]

 [1]: https://www.falter.at/zeitung/20190903/die-geheime-buchhaltung-der-liste-kurz
 [2]: https://www.kiwi-verlag.de/buch/die-ibiza-affaere/978-3-462-05407-1/
 [3]: https://www.derstandard.at/story/2000108268409/falter-journalisten-bei-oevp-hintergrundgespraech-zu-hackerangriff-nicht-erwuenscht
 [4]: https://diepresse.com/home/innenpolitik/nationalratswahl/5684818/Kurz_Sehr-gezielter-HackerAngriff-auf-die-OeVP
 [5]: https://dietagespresse.com/sebastian-kurz-zufrieden-mit-kopftuchverbot-klassenzimmer-endlich-ohne-religioese-symbole/