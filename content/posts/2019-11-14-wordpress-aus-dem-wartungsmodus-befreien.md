---
title: WordPress aus dem Wartungsmodus befreien
author: Christoph Scheidl
type: post
date: 2019-11-14T18:04:15+00:00
url: /wordpress-aus-dem-wartungsmodus-befreien/
categories:
  - Admin-Tipps
  - Wordpress

---
Gerade hatte ich den Fall, das WordPress nach einem Gutenberg-Update im Wartungsmodus festhing. Dies äußert sich dadurch, dass die Website nur mehr anzeigt: &#8222;Wegen Wartungsarbeiten ist diese Website kurzzeitig nicht verfügbar&#8220;

Die Lösung dafür ist denkbar einfach: Einfach aus dem Webserver-Verzeichnis die Datei &#8222;.maintenance&#8220; löschen.

###### Links und Credit

<p style="font-size:12px">
  <a href="https://www.sir-apfelot.de/wordpress-meldung-wartungsarbeiten-22862/">https://www.sir-apfelot.de/wordpress-meldung-wartungsarbeiten-22862/</a>
</p>