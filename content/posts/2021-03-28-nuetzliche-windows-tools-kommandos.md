---
title: Nützliche Windows Tools/Kommandos
author: Christoph Scheidl
type: post
date: 2021-03-28T19:03:57+00:00
url: /nuetzliche-windows-tools-kommandos/
classic-editor-remember:
  - block-editor
categories:
  - Admin-Tipps
  - Windows

---
Hier eine kurze Sammlung von nützlichen Befehlen und Tools für Windows-User und Admins (Tipps in den Kommentaren werden gerne aufgenommen, dieser Artikel wird ständig erweitert):

#### Commands (Win + R)

  * `ncpa.c`pl  
    -> Netzwerkeinstellungen öffnen
  * `appwiz.cpl`  
    -> Software deinstallieren
  * `control`  
    -> Systemsteuerung öffnen
  * `compmgmt.msc`  
    -> Computermanagement öffnen
  * `wf.msc`  
    -> Windows Firewall öffnen
  * `gpedit.msc`  
    -> Gruppenrichtlinien öffnen
  * `regedit`  
    -> Registry öffnen
  * `services.msc`  
    -> Windows Dienste (Services) öffnen
  * `mstsc`  
    -> Remote Desktop Client
  * `systempropertiesadvanced`  
    -> Name ist Programm
  * `useraccountcontrolsettings`  
    -> Name ist wieder Programm
  * `powershell`  
    -> Und nocheinmal Name Programm
  * `cmd`  
    -> Command Prompt
  * `winver`  
    -> Windows Version

###### Links und Credits:

[Chris Titus Tech &#8211; 14 Windows Commands Every User Should Know][1]  
<https://christitus.com/debloat-windows-10-2020/>  
<https://christitus.com/ultimate-windows-setup-guide/>  
<https://christitus.com/windows-10-scripts/>  
<https://christitus.com/ultimate-windows-10-install-iso/>  
<https://christitus.com/windows-10-optimization-guide/>

 [1]: https://youtu.be/10QSpg2sZgk