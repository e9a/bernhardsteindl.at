---
title: 'Typisch Krems: Betonieren, Sudern und Busse verunglimpfen'
author: Bernhard Steindl
type: post
date: 2021-03-07T13:55:01+00:00
url: /typisch-krems-betonieren-sudern-und-busse-verunglimpfen/
classic-editor-remember:
  - block-editor
categories:
  - Politik
  - Rants

---
Nach langer Pause muss ich mich mal wieder zu Wort melden: Was die Kremser ÖVP derzeit aufführt, ist mir nicht mehr wuascht.

Da macht die SPÖ einmal etwas vernünftiges wie eine enorme Ausweitung der Öffentlichen Verkehrsmittel und wieder sudern alle. Krems ist eine Autostadt oder soll das zumindest bleiben, wenn man den Wortmeldungen der "Volkspartei" glauben darf.

{{< figure width="423" height="344" align=center src="/wp-content/uploads/2021/02/grafik.png" title=">Die Prioritätsliste der Stadt Krems in Sachen Mobilität (Stadt Krems Website)" >}}

Das fängt schon damit an, dass auf der ganzen Website der Stadt Krems kein Wort zum Bahnhof Krems und der Anbindung dessen fällt. Dafür wird einem weit und breit erklärt, [wie und wo man in der Stadt parken kann.](https://www.krems.at/leben/mobilitaet/parken) Da braucht man sich dann nicht wundern, wenn die Touristen und ArbeitnehmerInnen dann die ganze Stadt vollparken.

Das soll jetzt kein dummes Autobashing sein, aber eine Stadtregierung, die im Jahr 2021 [noch neue Parkplätze baut](https://www.noen.at/krems/infrastruktur-100-parkplaetze-entstehen-in-krems-krems-print-fridays-for-future-parkplaetze-parkraum-parkraumbewirtschaftung-248445864), darf man wohl in Hinsicht umweltfreundlicher Mobilität nicht ernstnehmen. Traurig genug, dass diese Fläche überhaupt leersteht, aber das kann ja wohl keine Lösung sein.

Nun wieder zurück zum Thema: Der Verkehrsstadtrat dürfte wohl eher zur vernünftigen Seite gehören, indem er dieses Projekt erst durchbrachte. Die Busse fahren nun teilweise im 30-Minuten-Takt durch die Stadt und da fällt dann auf, dass viele Kremserinnen und Kremser schon seit Jahren illegal herumstehen.

Man braucht hier nicht lange herumzuphilosophieren: Verkehrsströme gehen immer den widerstandslosesten Weg. Baut man Straßen/Parkplätze/Autobahnen aus, wird es bequemer mit dem Auto zu fahren. Baut man hingegen die Öffentlichen Verkehrsmittel (gut!) aus, werden diese mehr benutzt. Ditto Fahrradinfrastruktur.

Es gibt bereits Politiker in der Stadt ([sogar von der ÖVP!](https://www.noen.at/krems/interview-hagmann-die-politiker-haben-angst-krems-interview-thomas-hagmann-wirtschaftskammer-220800253)), die natürlich wissen, dass vor allem in der Stadt kein Platz für immer breiter werdende Autos ist. Was ist die Lösung?

## Eine maßgeschneiderte Lösung gibt es nicht!

Es gibt keine einfache Lösung für das Mobilitätsproblem. Wien hat zwar viel Verkehr auf die Öffentlichen Verkehrsmittel umlegen können, dafür hat aber ein milliardenteurer Ausbau über Jahrzehnte sowie viel Kritik in früheren Zeiten schon mehrmals dafür sorgen können, dass dies heute nicht so wäre. Einzelne wenige Unternehmer und Politiker versuchen in Krems seit Jahren, jeglichen Fortschritt zu verhindern.

{{< figure align=center src="/wp-content/uploads/2021/03/vcoe-stadtverkehr-1024x719.png" title="Die Mobilität in Wien verursacht nur die Hälfte der Emissionen/Kopf wie am Land. Quelle: VCÖ" >}}

Dort wo Wien heute ist, muss man in den zentralen Bezirken wie in St. Pölten, Wiener Neustadt als auch Krems erst einmal hinkommen. Hier sind Maßnahmen wie der Öffentliche Verkehr hilfreich, andere wie die Ausweitung des Gewerbegebietes um fast die doppelte Fläche inklusive Waldrodung und neuer S5-Ausfahrt tragen wieder dazu bei, den MIV zu verstärken.

{{< figure width=1024 src="/wp-content/uploads/2021/03/grafik-1024x716.png" title="Der Flächenwidmungsplan weist hier Bauland: Industriegebiet aus. Rechts oben ist eine neue Ausfahrt aus Fahrtrichtung Krems geplant. Quelle: Stadt Krems" >}}

{{< figure src="/wp-content/uploads/2021/03/grafik-1.png" title="Heute sind beim geplanten Erweiterungsgebiet nur Wiesen, Wald und Felder. Satellitenbild: Google" >}}

Klar, das Erweiterungsgebiet ist nicht "Natur pur" und auch kein Naturschutzgebiet, doch bringt eine Erweiterung nur noch mehr Verkehr und Versiegelung. Dies ist nur ein kleines Beispiel, wie hier durch zentrumsferne Firmenansiedelungen Verkehr generiert statt vermieden wird.

## Die Medien spielen mit

Jeder Furz der hiesigen Unternehmer (eigentlich nur der Fleischer Graf und ein Honigladen) plus die Autofahrerlobby in Gestalt der ÖVP und FPÖ versuchen hier, obwohl sie bei den Sitzungen für das Projekt gestimmt haben, eine negative Stimmung für das Projekt aufleben zu lassen.

Dazu zählt auch die NÖN, die als ÖVP-nahe-Zeitung hier immer wieder Öl ins Feuer gießt:

  * [Stadtbus neu: Feiertag für Krems, 18.11.2020](https://www.noen.at/krems/mobilitaet-stadtbus-neu-feiertag-fuer-krems-krems-mobilitaet-stadtbus-krems-oeffentlicher-verkehr-print-233901286)
  * [Startprobleme für Stadtbus in Krems, 23.12.2020](https://www.noen.at/krems/verkehr-startprobleme-fuer-stadtbus-in-krems-krems-stadtbus-krems-print-238896244)
      * Der &#8222;breite&#8220; Bus ist schuld, wenn die Autos illegal parken
  * [Weiter Wirbel um &#8222;breiten&#8220; Kremser Stadtbus, 03.02.2021](https://www.noen.at/krems/debatte-weiter-wirbel-um-breiten-kremser-stadtbus-krems-print-stadtbus-krems-poller-herzogstrasse-247178389)
      * Jetzt entdecken die ÖVP und der Fleischer das Thema für sich, der Bus ist hier bereits seit fast 3 Monaten in Betrieb!
  * [Kremser Stadtbus: Land riet zur kleineren Variante, 11.02.2021](https://www.noen.at/krems/kritik-reisst-nicht-ab-kremser-stadtbus-land-riet-zu-kleinerer-variante-krems-print-stadtbus-krems-poller-248445973)
      * Angeblich riet das Land zu einer kleineren Variante für die Innenstadtlinie &#8211; Meine Frage ist hier warum? Die Fahrbahnen sind überall breit genug!
  * [Stadt Krems: Bus-Linien geprüft und genehmigt, 19.02.2021](https://www.noen.at/krems/debatte-stadt-krems-bus-linien-geprueft-und-genehmigt-krems-stadtbus-krems-poller-print-249548935)
      * Da die Lage zusehends eskaliert, werden im betroffenen Straßenbereich Poller aufgestellt, damit die Autos trotz Verbot nicht illegal parken können.
  * [Lesebrief &#8222;Wie man Gutes schlechtreden kann&#8220;, 23.02.2021](https://www.noen.at/leserbriefe/leserbrief-wie-man-gutes-schlechtreden-kann-krems-stadtbus-krems-250601446)
  * [Externe Hilfe für Kremser Stadtbus, 04.03.2021](https://www.noen.at/krems/verkehr-externe-hilfe-fuer-kremser-stadtbus-krems-print-poller-reinhard-resch-stadtbus-krems-251644408)
      * Nun vielleicht das Happy End? Die Stadtregierung beharrt (Gottseidank!) auf den Öffi-Verkehr und stellt eine Umgestaltung der jeweiligen Straßen in Aussicht (vermutlich Begegnungszonen).

Den Gipfel der Blödheit stellt hier Corona-ist-nicht-so-schlimm-Fernsehsender Servus TV dar, der im folgenden Video das Ganze in die Lächerlichkeit zieht.

[***Link zum Video***](https://cdn.bernhardsteindl.at/Regional/Gro%C3%9Fe%20Busse%20f%C3%BCr%20enge%20Gassen%20in%20Krems-ServusTV.webm)

Folgende Dinge merke ich aus rechtlicher Sicht der gesamten Diskussion an:

  * Bestandsschutz der bestehenden Verkehrswege: Alte Straßen wie diese in der Innenstadt müssen die Mindestbreite nicht erfüllen, das gilt natürlich nur für Neubauten. Sonst müsste man ja auch die halbe Innenstadt abreissen.
  * **§23 StVO , Abs. 1**) Der Lenker hat das Fahrzeug zum Halten oder Parken unter Bedachtnahme auf die beste Ausnützung des vorhandenen Platzes so aufzustellen, daß kein Straßenbenützer gefährdet und kein Lenker eines anderen Fahrzeuges am Vorbeifahren oder am Wegfahren gehindert wird.
      * Wenn der Bus nicht vorbeikommt, gilt an dieser Stelle **automatisch** "Halten- und Parken verboten"
  * **§24 StVO, Abs.1** Das Halten und Parken ist verboten
      * **b)** **auf engen Stellen der Fahrbahn**, im Bereich von Fahrbahnkuppen oder von unübersichtlichen Kurven sowie auf Brücken, in Unterführungen und in Straßentunnels,
      * **o)** wenn Fußgänger, insbesondere auch Personen mit Kinderwagen oder Behinderte mit Rollstuhl, an der Benützung eines Gehsteiges, eines Gehweges oder eines Geh- und Radweges gehindert sind,

Man merke sich, wenn der Bus/Fußgänger/Rollstuhlfahrer nicht am Gehsteig oder auf der Straße vorbeikommt, kann das Auto gar nicht "korrekt abgestellt" worden sein. Das sollte auch eine Lehre sein für Autos in Übergrößen (SUVs, Hausfrauenpanzer, Stadtgeländewagen, Doppeldeckerbusse, ...), welche diese Regelungen auch gerne mit Füßen treten, da ihre Autos breiter sind als die alte Norm und daher nicht in jeden Parkplatz passen.

Das Problem an den oben genannten Regelungen ist, dass sie über Jahrzehnte ignoriert worden sind und hier ein Gewohnheitsrecht eingetreten ist: "Wo Platz ist, kann ich parken". Das stimmt so nicht! **Wo genug Platz für alle anderen Verkehrsteilnehmer auch nach dem Einparken ist, kann ich parken.**

Normalerweise braucht man also den ganzen Schilderwald "Halten und Parken verboten" gar nicht, da es auch ohne Schild illegal wäre. Aber um der Anarchie der Faulheit Herr zu werden, hängt man halt überall Schilder hin.

## Conclusio

Der neue Stadtbus ist ein gutes Projekt, hat aber leider noch immer keine Durchgangslinie und auch keinen 15-Minuten-Takt oder Betrieb am Sonntag. Eine eventuelle Straßenbahn rückt in jahrtausendweite Ferne, wenn man die Anfeindungen an den neuen Stadtbus sieht: Der Bus ist schuld, dass ich nicht mehr (illegal!) parken darf. Gratulation ÖVP, Projekt gelungen!

#### Quellen

[Stadt Krems Flächenwidmung Landersdorf](https://www.krems.at/fileadmin/user_upload/05_Weinzierl_Landersdorf_Krems.pdf)