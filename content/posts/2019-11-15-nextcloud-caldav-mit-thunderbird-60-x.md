---
title: Nextcloud-CalDav mit Thunderbird 60.x
author: Christoph Scheidl
type: post
date: 2019-11-15T07:34:57+00:00
url: /nextcloud-caldav-mit-thunderbird-60-x/
categories:
  - Admin-Tipps

---
Update: Der einfachere Weg ist, auf Thunderbird 67 mit der neueren Lightning-Version upzudaten, welche die untenstehenden Probleme behebt.

Leider gibt es nach wie vor Probleme mit Thunderbird 60 beim Synchronisieren der Nextcloud-Kalender. Diese äußern sich dadurch, das sich der Kalender zwar ohne Probleme hinzufügen lässt, aber ohne irgendeine Fehlermeldung einfach keine EInträge synchronisiert. Es gibt zum Glück einen Workarround:

  * Thunderbird-Einstellungen öffnen
  * Auf Erweitert/Advanced drücken
  * Den Konfigurations-Editor/Config-Editor öffnen
  * Den Parameter &#8222;network.cookie.same-site.enabled&#8220; auf &#8222;false&#8220; setzen.
  * Neu synchronisieren, nun sollte es funktionieren.

###### Links Und Credit

<p style="font-size:12px">
  <a href="https://github.com/nextcloud/server/issues/10134#issuecomment-411315059">https://github.com/nextcloud/server/issues/10134#issuecomment-411315059</a>
</p>