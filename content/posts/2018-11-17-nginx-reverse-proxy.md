---
title: 'NGINX: Reverse-Proxy'
author: Bernhard Steindl
type: post
date: 2018-11-17T13:52:55+00:00
url: /nginx-reverse-proxy/
categories:
  - nginx

---
Hin und wieder kommt man in die Verlegenheit, neue Services auf seinem Server zu installieren und etwas zu herumzuprobieren.

Nginx kann wie haproxy Streams per DNS-Name unterscheiden und danach Traffic umleiten. In diesem Beispiel, wollen wir den gesamten mailcow-Traffic umleiten:

  


```
server {
        listen 443 ssl http2;
        listen [::]:443 ssl http2;
        include https-ls;
        ssl_certificate /etc/letsencrypt/live/mail.e9a.at/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/mail.e9a.at/privkey.pem;

        #root /var/www/;
        #index index.php index.html index.htm;

        server_name mail.e9a.at;

        location / {
          proxy_pass http://127.0.0.1:8080/;
          proxy_set_header Host $http_host;
          proxy_set_header X-Real-IP $remote_addr;
          proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for;
          proxy_set_header X-Forwarded-Proto $scheme;
          client_max_body_size 100m;
        }
}
```