---
title: Rules in Tasmota – Kompressor automatisch nach einer Zeit abschalten
author: Christoph Scheidl
type: post
date: 2021-05-01T10:00:58+00:00
url: /rules-in-tasmota/
classic-editor-remember:
  - block-editor
categories:
  - Hardware
  - Projekte

---
Wir haben zuhause einen Druckluftkompressor, der weiter weg von der Werkstatt steht, damit der Lärm nicht nervt. Der Nachteil davon ist halt, dass zum Ein- und Ausschalten immer zum Kompressor gegangen werden muss, weil kein Schalter damals mit gebaut worden ist. Deshalb will ich dieses Problem nun nachträglich mit einem Shelly 1 mit Tasmota und einem davon angesteuerten Schütz lösen. Das ermöglicht auch neue Anwendungen wie zB das automatische Füllen bei genug Überstrom von der PV oder das automatische Ausschalten in der Nacht (es ist schon des öfteren vorgekommen, dass um 1 in der Nacht der Kompressor angesprungen ist).

Für den Schalter am anderen Ende wird wahrscheinlich in Zukunft ein ESP-01 herhalten, sobald dieser aus-programmiert ist, werde ich hier wieder berichten. Die Idee ist aber, das ganze über die HTTP-Schnittstelle von Tasmota zu realisieren, zB mit einem solchen Befehl: `http://IP.OF.DEV.ICE/cm?cmnd=Backlog%20Power%20on%3B%20RuleTimer1%2010`

Den Shelly für die Schütz-Ansteuerung habe ich einfach mit einer Rule programmiert:

```
Rule1 
  ON Switch1#state=0 DO ENDON 
  ON Switch1#state=2 DO Backlog Power ON; RuleTimer1 1200 ENDON 
  ON Switch1#state=3 DO Backlog Power ON; RuleTimer1 7200 ENDON  
  ON Rules#Timer=1 DO Power1 off ENDON
```
Damit lässt sich mit einem dort angeschlossenen Taster mit einem Tasten der Kompressor für 20 Minuten und mit einem Halten für mehr als 5 Sekunden für 2h einschalten.

###### Links und Credit

- Danke an die Tamota Discord-Community, besonders an Nutzer <em>sfromis</em>, fürs Weiterhelfen
[https://tasmota.github.io/docs/](https://tasmota.github.io/docs/)
