---
title: PHP-Version unter Debian ändern
author: Christoph Scheidl
type: post
date: 2021-04-17T21:16:38+00:00
url: /php-version-unter-debian-aendern/
classic-editor-remember:
  - block-editor
categories:
  - Admin-Tipps

---
Hat man unter Debian mehrere PHP-Versionen gleichzeitig laufen, kann man folgendermaßen die &#8222;Standardversion&#8220; einstellen:

```bash
update-alternatives --set php /usr/bin/php7.4
update-alternatives --set phar /usr/bin/phar7.4
update-alternatives --set phar.phar /usr/bin/phar.phar7.4
update-alternatives --set phpize /usr/bin/phpize7.4
update-alternatives --set php-config /usr/bin/php-config7.4</code></pre>
```

###### Links und Credit

[https://tecadmin.net/switch-between-multiple-php-version-on-debian/](https://tecadmin.net/switch-between-multiple-php-version-on-debian/)