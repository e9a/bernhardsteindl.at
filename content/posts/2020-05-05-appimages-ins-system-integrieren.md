---
title: AppImages ins System integrieren
author: Christoph Scheidl
type: post
date: 2020-05-05T05:29:44+00:00
url: /appimages-ins-system-integrieren/
classic-editor-remember:
  - block-editor
categories:
  - Allgemein

---
AppImages sind in meinen Augen eine tolle Sache: Sie vereinfachen es dem Anbieter sehr, immer die aktuellste Version einer Software ausspielen zu können, ohne auf Repo-Maintainer oder sonstige Instanzen angewiesen zu sein. Was mich an AppImages immer gestört hat ist, dass es keinen guten Support ins System gegeben hat (zB .desktop-File, die sich selbst aktuell hält). Dieses Problem löst das kleine Tool [AppImageLauncher][1], der unter Ubuntu zB einfach aus einer PPA installiert wird und dann automatisch die AppImages ins System integriert. Man erspart sich das obligatorische `chmod a+x whatever.AppImage`, man klickt einfach auf die AppImage drauf, ein Fenster öffnet sich und fragt, ob man diese Software ins System integrieren möchte, und schon ist die Sache erledigt.

UPDATE: Mittlerweile nutze ich noch entspannter den Dienst [AppImaged][2]. Dieser fügt automatisch Desktop-Files etc hinzu, sobald das AppImage im vorher definierten Ordner verschoben wird.

###### Links Und Credit

<p style="font-size:12px">
  <a href="https://github.com/TheAssassin/AppImageLauncher">https://github.com/TheAssassin/AppImageLauncher</a><br /><a href="https://github.com/TheAssassin/AppImageLauncher/wiki/Install-on-Ubuntu-or-Debian">https://github.com/TheAssassin/AppImageLauncher/wiki/Install-on-Ubuntu-or-Debian</a><br /><a href="https://github.com/AppImage/appimaged/">https://github.com/AppImage/appimaged/</a>
</p>

 [1]: https://github.com/TheAssassin/AppImageLauncher
 [2]: https://github.com/AppImage/appimaged/