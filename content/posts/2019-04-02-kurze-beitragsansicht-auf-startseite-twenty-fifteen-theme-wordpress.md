---
title: Kurze Beitragsansicht auf Startseite – Twenty Fifteen Theme WordPress
author: Christoph Scheidl
type: post
date: 2019-04-02T08:11:14+00:00
url: /kurze-beitragsansicht-auf-startseite-twenty-fifteen-theme-wordpress/
classic-editor-remember:
  - block-editor
categories:
  - Wordpress

---
Es hat mich schon länger genervt, das auf der Startseite immer die kompletten Blogposts angezeigt wurden&#8230; Das ging doch sehr zu lasten der Übersicht. Nun habe ich einen Weg gefunden, wie sich das Theme modifizieren lässt, damit nur ein Ausschnitt des Artikels gezeigt wird.

In der content.php-Datei des Themes muss folgender Codeteil ersetzt werden:

<pre class="wp-block-code"><code>&lt;div class="entry-content">
		&lt;?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				__( 'Continue reading %s', 'twentyfifteen' ),
				the_title( '&lt;span class="screen-reader-text">', '&lt;/span>', false )
			) );

			wp_link_pages( array(
				'before'      => '&lt;div class="page-links">&lt;span class="page-links-title">' . __( 'Pages:', 'twentyfifteen' ) . '&lt;/span>',
				'after'       => '&lt;/div>',
				'link_before' => '&lt;span>',
				'link_after'  => '&lt;/span>',
				'pagelink'    => '&lt;span class="screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' &lt;/span>%',
				'separator'   => '&lt;span class="screen-reader-text">, &lt;/span>',
			) );
		?>
	&lt;/div>&lt;!-- .entry-content --></code></pre>

Durch:

<pre class="wp-block-code"><code>&lt;div class="entry-content">
		&lt;?php
		if ( is_single() ) :
			/* translators: %s: Name of current post */
			the_content( sprintf(
				__( 'Continue reading %s', 'twentyfifteen' ),
				the_title( '&lt;span class="screen-reader-text">', '&lt;/span>', false )
			) );

			wp_link_pages( array(
				'before'      => '&lt;div class="page-links">&lt;span class="page-links-title">' . __( 'Pages:', 'twentyfifteen' ) . '&lt;/span>',
				'after'       => '&lt;/div>',
				'link_before' => '&lt;span>',
				'link_after'  => '&lt;/span>',
				'pagelink'    => '&lt;span class="screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' &lt;/span>%',
				'separator'   => '&lt;span class="screen-reader-text">, &lt;/span>',
			) );
			else :

			/* translators: %s: Name of current post */
			the_excerpt( sprintf(
				__( 'Continue reading %s', 'twentyfifteen' ),
				the_title( '&lt;span class="screen-reader-text">', '&lt;/span>', false )
			) );

			wp_link_pages( array(
				'before'      => '&lt;div class="page-links">&lt;span class="page-links-title">' . __( 'Pages:', 'twentyfifteen' ) . '&lt;/span>',
				'after'       => '&lt;/div>',
				'link_before' => '&lt;span>',
				'link_after'  => '&lt;/span>',
				'pagelink'    => '&lt;span class="screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' &lt;/span>%',
				'separator'   => '&lt;span class="screen-reader-text">, &lt;/span>',
			) );
			endif;
		?>
	&lt;/div>&lt;!-- .entry-content --></code></pre>

###### Links Und Credit

<p style="font-size:12px">
  <a href="http://www.matrudev.com/post/content-post-excerpt/">http://www.matrudev.com/post/content-post-excerpt/</a>
</p>