---
title: Von Linux-Terminal auf Nextcloud mittels WebDAV uploaden
author: Christoph Scheidl
type: post
date: 2018-12-20T13:02:03+00:00
url: /von-linux-terminal-auf-nextcloud-mittels-webdav-uploaden/
categories:
  - Debian

---
Um Daten von einem Linux-Server auf eine Nextcloud hochzuladen, kann das Tool curl benutzt werden. Dieser Artikel beschreibt die dafür benötigten Paramter: <https://www.qed42.com/blog/using-curl-commands-webdav>

Ein Beispiel für die Nextcloud würde folgendermaßen aussehen:

<pre class="wp-block-code"><code>curl --user 'username:password' -T filename.log 'https://cloud.nextcloud.xyz/remote.php/webdav/'</code></pre>

###### Links und Credit

<p style="font-size:12px">
  <a href="https://www.qed42.com/blog/using-curl-commands-webdav">https://www.qed42.com/blog/using-curl-commands-webdav</a><br />
</p>