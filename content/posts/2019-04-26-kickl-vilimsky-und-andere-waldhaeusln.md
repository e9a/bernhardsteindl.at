---
title: Kickl, Vilimsky und andere Waldhäusln
author: Bernhard Steindl
type: post
date: 2019-04-26T10:57:31+00:00
url: /kickl-vilimsky-und-andere-waldhaeusln/
categories:
  - Politik
  - Rants

---
Ach, jetzt ist schon wieder etwas passiert. Ein bedauerlicher Einzelfall, [ein kleines Gedichtchen][1] über Ratten oder ein [Plakat der RFJ, welches NS-Propaganda ähnelt][2]. Einzelfall.

Es ist ja wirklich bedauerlich, dass gerade in der Partei, die im rechtsextremen Eck Österreichs fischt, sich soviele &#8222;Nazis&#8220; tummeln. Doch ich fürchte, es reicht. Es erinnert sovieles an die langsame Radikalisierung der 1930er Jahre, nur das Feindbild hat sich verändert. Nun sind es Linke Netzwerke und vorallem die Islamisten, &#8222;Ausleeenda&#8220;, Asylanten und andere Minderheiten, gegen denen gehetzt wird.

Nationalismus basiert auf der Lüge, das ein Volk besser ist als andere. Das 20. Jahrhundert zeigt uns eindrucksvoll, was uns das gebracht hat. Die Geschichte wiederholt sich, wir Menschen lernen nicht daraus. Wir werden sehen, wie es weitergeht. Mehr Überwachung, &#8222;Ausreisezentren&#8220; und einen vollkommen verrückten Innenminister haben wir jetzt schon. Der nächste Schritt nach der jetzigen Ausgrenzung ist die Abgrenzung, ich lasse hier offen, wie diese geschieht.

Ja, wir müssen etwas tun. Zum Beispiel keine rechtsextremen Parteien wählen, aber auch nicht die ÖVP, die diese Entgleisungen erst möglich gemacht hat und teilweise unterstützt. Bei den Europawahlen setzt Kurz neben den altbekannten und ehrwürdigen Karas auch Edtstadler ein, welche absolut unwählbar ist. Auch wenn ich mit der Ideologie nicht übereinstimme, haben die NEOS ein interessantes Konzept vom Vereinigten Europa, welches ich positiv sehe. Die SPÖ hat nichts interessantes vorgelegt und die FPÖ kocht wie immer die alte braune Suppe wieder warm. Informiert euch, wählt keine Idioten in das Parlament!

Noch haben wir ein Parlament und die Möglichkeit, alles zu verändern.

 [1]: https://derstandard.at/2000101884850/Braunauer-FPOe-Vizebuergermeister-tritt-zurueck?ref=rec
 [2]: https://derstandard.at/2000102023835/Wolf-Gut-dass-Herr-Vilimsky-nicht-so-kann-wie-er