---
# Title
title: "MFG - Mit furchtbaren Gsichtern und ohne Tassen im Schrank"
# Enter the author's name
author: Bernhard Steindl
# Customize URI; default: post/filename
#url: /yubikey-fuer-linux-login-als-u2f-2-faktor-verwenden/
# Date
date: 2022-08-31T17:00:00+01:00
# Is this draft?
draft: false
# Show Table of Content?
showToc: true
# Available categories. Please comment out!
categories:
#- Admin-Tipps
#- Allgemein
#- Arch Linux
#- Debian
#- Denkt's noch!
#- Grundlagen
#- Hardware
#- mariadb
#- News
#- nginx
#- Politik
#- Projekte
#- Proxmox
#- Rants
#- Satire
#- Synology
#- Tor
#- Ubiquiti Unifi
#- Windows
#- Wordpress
#- ZFS

---

# MFG

Ich weigere mich, für eine so wissenschaftsfeindliche Corona-Leugner-Gruppe auch nur eine Minute Zeit zu verschwenden. Ganz klare Aussage: Wer die wählt, hat nicht mehr alle Tassen im Schrank, falls dort jemals welche wahren.

Wer sich trotzdem für die *angeblichen* Wahlziele ("Focus" auf Kinder !sic) interessiert und keine Angst vor schiachen Gsichtern hat, [hier lang](https://www.mfg-oe.at/gemeinderatswahl-krems/). Ich habe aber gewarnt.

Ich darf auch anmerken, was passiert, wenn man sich als MFG-Gemeinderat für [irgendetwas sinnvolles oder wissenschaftlich begründbares einsetzt](https://www.krone.at/2566137).

*Falls Sie keine wissenschaftsfeindliche (Idioten-)Protestpartei wählen wollen, die in 5 Jahren genauso wie Team Stronach oder das BZÖ politisch tot ist, hier lang: [Pappkameraden in Krems](/posts/2022-08-25-die-kremser-pappkameraden/)!*