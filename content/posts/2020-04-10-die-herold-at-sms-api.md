---
title: Die HEROLD.at-SMS-API
author: Bernhard Steindl
type: post
date: 2020-04-10T19:00:20+00:00
url: /die-herold-at-sms-api/
categories:
  - Allgemein
tags:
  - herold
  - herold.at
  - sms
  - sms-lücke

---
Man könnte glauben, ich habe den ganzen Tag nichts anderes zu tun, als Bugs bei der Website eines Telefonbuchanbieter zu suchen. Dabei fiel das eher durch Zufall als durch extensives Testen auf.

## Worum geht es?

Herold.at hat eine Funktion, welche das Versenden von Telefonbucheinträgen als SMS erlaubt. Beim Aufrufen dieser Funktion wird ein neues Fenster mit diesem URI-Schema geöffnet:

```
https://www.herold.at/cms/apps/sms/send.php?Content=text
```

Hier selbst wurde zwar ein Google-Captcha geschalten, was aber nicht daran hinderte, eine SMS mit beliebigen Inhalt zu senden:

{{< figure align=center src="/wp-content/uploads/2020/04/grafik.png">}}

Nach dem Klick auf &#8222;SMS senden&#8220; wird dieser Text (zugegeben mit Enkodierungsfehlern, wenn Umlaute vorhanden sind) an die angegebene Rufnummer versendet.

{{< figure align=center src="/wp-content/uploads/2020/04/signal-attachment-2020-04-07-154722.png" caption="Der Empfänger freut sich über eine schöne SMS." >}}

Ein kleiner Test meinerseits bestätigt diese These, der Text kommt als SMS beim Empfänger an.

Da ich vermute, dass das nicht der &#8222;gedachte&#8220; Einsatzzweck dieses Formulars ist, habe ich dies an das Team von Herold.at gemeldet.

Diese API lässt sich natürlich missbrauchen, man könnte hier durch den vertrauenswürdigen Empfänger &#8222;HEROLD&#8220; leichtgläubige Personen zum überweisen von Telefonbuchrechnungen etc. bringen oder auch einfach nur Personen unter falschen Namen mit Nachrichten bombardieren.

## Wie macht man das richtig und baut nicht solche Fehler?

Meiner bescheidenen Meinung nach sollte man dies nicht so implementieren, da aber diese SMS-Funktion nachträglich in das System eingebaut wurde, mussten die Daten (also welcher Text versenden werden soll) irgendwie übertragen werden. Da ich von Außen kaum beurteilen kann, wie das System intern funktioniert, kann ich die Komplexität nicht bewerten.

Der Parameter &#8222;Content&#8220; hätte sich aber auch als POST-Request senden lassen können, so wäre dies nicht so offensichtlich gewesen, aber man hätte diese Funktion trotzdem missbrauchen können.

Vermutlich wäre es besser, wenn diese SMS-Bridge Zugriff auf die Datenbank bekommt und dann eine ID übermittelt, von welcher die Daten dann ausgelesen und versendet werden konnten.

## Wie hat Herold.at reagiert?

Ich sendete am 07.04.2020 eine E-Mail mit fast identischem Inhalt wie dieser Blogpost an office@herold.at. Bis Veröffentlichung dieses Artikels (10.4. 21:00) bekam ich keine Antwort, auch keine Empfangsbestätigung, wodurch ich mich nicht weiter gehindert fühlte, die Lücke öffentlich zu machen.

## Ist die Lücke bereits geschlossen?

Stand 10.4.2020: Nein

## Credits

Versierten Kreisen ist diese Lücke schon länger bekannt, ich selbst wurde darauf von einem Freund hingewiesen. Diese Veröfentlichung ist eine Publikmachung dessen, was viele schon seit einiger Zeit wissen.