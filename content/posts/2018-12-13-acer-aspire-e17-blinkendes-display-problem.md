---
title: Acer Aspire E17 blinkendes Display-Problem
author: Christoph Scheidl
type: post
date: 2018-12-13T12:41:24+00:00
url: /acer-aspire-e17-blinkendes-display-problem/
categories:
  - Hardware

---
Heute kam jemand zu mir mit einem Windows 8.1 Rechner, dessen Display bei der Berührung des Touchpads schwarz wurde und dann erst wieder hell wurde, wenn Input am Touchpad oder an der Tastatur erkannt wurde. Schrecklich, damit zu arbeiten, aber die Lösung war denkbar simpel: Windows Update dürfte hier den Touchpad-Treiber geschossen haben, eine Neuinstallation des Treibers, geladen von der Acer-Seite direkt, hat dem ganzen ein Ende bereitet.

###### Links und Credit

<p style="font-size:12px">
  <a href="http://www.tomsguide.com/answers/id-3269553/acer-laptop-screen-blinking.html">http://www.tomsguide.com/answers/id-3269553/acer-laptop-screen-blinking.html</a><br /><a href="https://www.acer.com/ac/de/AT/content/drivers">https://www.acer.com/ac/de/AT/content/drivers</a><br />
</p>