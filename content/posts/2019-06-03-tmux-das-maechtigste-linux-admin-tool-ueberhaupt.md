---
title: tmux – Das mächtigste Linux-Admin-Tool überhaupt
author: Christoph Scheidl
type: post
date: 2019-06-03T18:43:12+00:00
url: /tmux-das-maechtigste-linux-admin-tool-ueberhaupt/
categories:
  - Allgemein

---
tmux steht für Terminal-Multiplexer. Klingt erstmal nicht sehr spektakulär. Aber dieses Tool erleichtert einem das Admin-Leben ungemein. Bevor ich hier wild herumerkläre, was genau tmux ist, hier ein paar Videos, die das besser erklären:

Erklärung auf Deutsch: <https://youtu.be/OlQMXu4yCGk?t=1403>

Einsteigertutorial auf Englisch: <https://www.youtube.com/playlist?list=PLT98CRl2KxKGiyV1u6wHDV8VwcQdzfuKe>

Dieser Blog-Eintrag dient also nicht zur Erklärung, sondern soll nur meine Konfiguration zeigen. tmux ist nämlich sehr stark anpassbar, hier also zur Inspiration meine .tmux.conf:

Eine nützliche Seite ist (vor allem für die Standard-Shortcuts) [tmuxcheatsheet][1]

Um sich selbst in der .tmux.conf austoben zu können, lohnt sich der Befehl

<pre class="wp-block-code"><code>tmux list-keys</code></pre>

 [1]: http://tmuxcheatsheet.com/