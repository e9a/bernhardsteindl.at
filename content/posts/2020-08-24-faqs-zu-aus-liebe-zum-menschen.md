---
title: FAQs zu „Aus Liebe zum Menschen“
author: Bernhard Steindl
type: post
date: 2020-08-24T11:07:56+00:00
url: /faqs-zu-aus-liebe-zum-menschen/
classic-editor-remember:
  - block-editor
categories:
  - Allgemein
  - Politik
tags:
  - Rotes Kreuz Krems
  - Zivildienst

---
Ich möchte euch folgende Hintergrundinformationen und ein paar Dinge, die in den ursprünglichen Artikel einfach nicht mehr gepasst haben, nicht vorenthalten.

**Ist das einfach ein Rant-Artikel oder hast du einfach nur schlecht geschlafen? Wie kommt man auf die Idee?**

Das ist kein Rant-Artikel über das Rote Kreuz oder der Dienststelle in Krems. Ich konnte in den 9 Monaten nichts zum Besseren wenden, was mich sehr frustrierte. Das ist der Versuch, den Dienst für die Nachfolgenden angenehmer und erträglicher zu machen.

**Bist du dir sicher, dass das &#8222;Früher kommen&#8220; illegal ist?**

Unser Vorgesetzter darf uns zwar prinzipiell die Anweisung geben, dass wir früher kommen sollten, jedoch dürfen wir illegale Anordnungen ignorieren. Wie die Dienstzeitverordnung besagt, dürfen wir im Turnusdienst maximal 48 Stunden/Woche arbeiten. Da die reguläre Dienstzeit bereits 48 Stunden beträgt, ist auch 2 Minuten früher kommen nicht legal.

Eine angedrohte Erhöhung auf 50 Stunden oder mehr ist nur in begründeten Ausnahmefällen und begrenzt möglich.

**Hast du was gegen deine Vorgesetzten?**

Prinzipiell habe ich nichts gegen die (ehemaligen) Vorgesetzten. Durch das permanente Ignorieren unserer Forderungen und Warnungen, dass gewisse Anordnungen illegal sind, haben sie sich jedenfalls nicht mit Ruhm bedeckt.

**Hast du Angst vor einer gerichtlichen Auseinandersetzung?**

Dazu wird es nicht kommen, da die Fakten durch unzählige Zeugen belegt sind und die Vorgesetzten auch wissen, dass manche Dinge maximal rechtliche Grauzone sind. (Deswegen gab es auch nie eine schriftliche Anordnung für das &#8222;Früher kommen&#8220;.)

**Wie sieht ein Arbeitsalltag im Krankentransportdienst aus?**

Man trifft auf der Dienststelle ein und sucht erst einmal den zweiten, mit dem man an dem Tag im Auto sitzt. Wenn dieser Fahrer ist, ist man selbst Sanitäter und umgekehrt. Dann nimmt man die Schlüssel, Pager (Alarmierungsgerät) und die Dokumentations-iPads und marschiert in Richtung Auto. Wenn man Pech hat, kommt man bis zum Ende des Dienstes nicht mehr auf die Dienststelle und man ist 8 (oder 12) Stunden unterwegs.

**Die Kremser können ja nichts dafür, dass die das Gesetz in Tulln/Wien so verpfuschen!**

Das ist auch keine dezidierte Schuldzuweisung sondern eine Aufstellung von Problemen und Forderungen. Zu bestimmen, wer diese wie erfüllt oder verbessert, ist nicht meine Aufgabe. Das erklärte Ziel des Artikels ist die Publikmachung von Missständen, ähnlich wie beim <a href="https://blog.e9a.at/ueberwachungsstaat-kasernstrasse-6/" data-type="post" data-id="207">Artikel zum Bundesschülerheim</a>.

Wesentliche Probleme auf der Dienststelle sind hausgemacht und nicht von externen abhängig. Die gesamte Situation ist einem reichen Land wie Österreich absolut unwürdig. Und bitte nicht wieder mit _„Bei uns war&#8217;s auch scheiße!_“ und _„Jeder muss da durch!“_ kommen. Wir sind dazu da, das Leben aller Menschen besser zu machen. Solche blöden Kommentare sind kontraproduktiv.

**Das was du sagst stimmt zwar, aber wie sollen wir das sonst machen?**

Fast alle Länder der Welt – genauer genommen _alle_ westlichen Länder außer der Schweiz und Österreich – haben den Militärdienst mit gleichzeitigem Ersatz abgeschafft. Es ist einfach nicht mehr zeitgemäß, möglichst viele schlecht ausgebildete Junge als „Kanonenfutter“ in der Armee zu haben.

Alle Branchen haben sich spezialisiert, genauso wie die Heere und Rettungsorganisationen dieser Welt. Menschen zu etwas zu zwingen, das sie nicht wollen, ist kontraproduktiv. Ich plädiere weiterhin für die Abschaffung des gesamten Dienstes.

**Die Freiwilligen spendieren ihre Zeit und du beleidigst sie einfach. Hast du denn gar keinen Respekt?**

Natürlich habe ich Respekt vor Menschen, die ihre Freizeit der Rettung anderer Menschenleben opfern. Das ist toll. Nur ist das kein Freifahrtsschein, die Kollegen auf der Dienststelle schlecht zu behandeln, nur weil sie nicht freiwillig, sondern im Rahmen des Zivildienstes fahren. Gäbe es die Krankentransportleistungen nicht, hätten viele ältere Menschen keine Möglichkeit, zu den Ärzten oder zur Dialyse zu kommen. Das ist mindestens genauso wichtig, wie eine qualifizierte und professionelle Rettungsdienstleistung.

**Du hast da einen Logik-/Rechtschreib-/Recherche-Fehler! Dein Sachverhalt stimmt nicht oder ist gelogen!**

Ich bitte darum, sich bei mir zu melden! Die Kontaktdaten findet ihr im <a href="https://blog.e9a.at/impressum/" data-type="page" data-id="18">Impressum</a>. Dies gilt natürlich auch für jene, die sich im Artikel angegriffen/beleidigt fühlen.

**Hat sich bei dir schon wer gemeldet? Das muss ja Aufsehen erregen!**

Stand 25.08.; 12 Uhr: Bei mir hat sich niemand gemeldet.