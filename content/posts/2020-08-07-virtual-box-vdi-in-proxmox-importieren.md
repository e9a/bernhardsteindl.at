---
title: Virtual-Box .vdi in Proxmox importieren
author: Christoph Scheidl
type: post
date: 2020-08-07T08:09:17+00:00
url: /virtual-box-vdi-in-proxmox-importieren/
classic-editor-remember:
  - block-editor
categories:
  - Allgemein

---
Letztens kam ich in die Situation, von einem alten Server, auf dem VBoxHeadless mit einer Windows-Server-VM lief, auf einen neuen Proxmox-Host zu migrieren. Das stellt sich dabei leichter dar, als gedacht:

  1. Den alten Server herunterfahren
  2. Die VDI-Datei kopieren
  3. Am Proxmox eine neue VM ohne Installationsmedium und mit kompatiblen Netzwerk anlegen
  4. Mit dem Befehl `qm importdisk 102 windows-server.vdi storage_pool` kann nun die VDI in die VM (in diesem Fall mit der ID 102 importiert werden.
  5. Im Proxmox taucht nun eine Unused Disk auf, diese hängen wir nun als SATA0 ein, und die vom Assistenten angelegte löschen wir.
  6. Unter Options legen wir in der Bootreihenfolge noch SATA0 als Boot-Device fest.
  7. Dann können wir schon booten.

###### Links Und Credit

<p style="font-size:12px">
  <a href="https://www.caretech.io/2017/10/17/migrating-virtualbox-vdi-to-proxmox-ve-5/">https://www.caretech.io/2017/10/17/migrating-virtualbox-vdi-to-proxmox-ve-5/</a>
</p>