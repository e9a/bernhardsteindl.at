---
title: Zammad auf Debian mit externem NGINX Reverse-Proxy aufsetzen
author: Christoph Scheidl
type: post
date: 2021-03-29T06:44:24+00:00
url: /zammad-auf-debian-mit-externem-nginx-reverse-proxy-aufsetzen/
classic-editor-remember:
  - block-editor
categories:
  - Admin-Tipps
  - nginx

---
Die Installation ist ziemlich einfach: Elasticsearch installieren, Zammad installieren, Elasticsearch konfigurieren, fertig (siehe Link unten).

Bei mir ist dann noch ein Problem mit HTTPS (Token-Error) aufgetreten, das ließ sich folgendermaßen lösen:

Bei jedem X-Forwarded-Proto statt $scheme https reinschreiben:

<pre class="wp-block-code"><code>server {
        listen 80;
        server_name zammad.example.com;
        return 301 https://$server_name$request_uri;
}
server {
        listen 443 ssl;

        server_name zammad.example.com;

        # SSL
        ssl_certificate /etc/letsencrypt/live/zammad.example.com/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/zammad.example.com/privkey.pem;
        ssl_trusted_certificate /etc/letsencrypt/live/zammad.example.com/fullchain.pem;

    access_log /var/log/nginx/zammad.access.log;
    error_log  /var/log/nginx/zammad.error.log;

    client_max_body_size 50M;

    location ^~ / {
        proxy_http_version 1.1;
        proxy_set_header Host $http_host;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
        proxy_set_header CLIENT_IP $remote_addr;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto https; 
# siehe https://community.zammad.org/t/fresh-install-3-2-x-csrf-token-verification-failed/3080/32v
        proxy_read_timeout 300;
        proxy_pass http://IP.OF.ZAMMAD.MACHINE/;

    }
        include _general.conf;
}
</code></pre>

###### Links und Credit

<https://docs.zammad.org/en/latest/install/debian.html>