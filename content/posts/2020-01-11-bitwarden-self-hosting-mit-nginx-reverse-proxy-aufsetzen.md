---
title: Bitwarden Self-Hosting mit nginx-Reverse-Proxy aufsetzen
author: Christoph Scheidl
type: post
date: 2020-01-11T14:21:19+00:00
url: /bitwarden-self-hosting-mit-nginx-reverse-proxy-aufsetzen/
classic-editor-remember:
  - block-editor
categories:
  - Allgemein

---
Weil Keepass doch ein wenig schwierig zu nutzen ist auf allen Endgeräten, habe ich mich nach einer Alternative umgsehen und bin auf Bitwarden gestoßen. Das Interessante an Bitwarden ist, dass man sich seinen dafür benötigten Server selbst hosten kann. Hier nun ein kleiner Überblick, wie man sich den Server hinter einem nginx Reverse-Proxy installieren kann.

Zu Beginn muss man Docker und Docker-Compose installieren.

Nun muss man sich bei [Bitwarden][1] registrieren, um eine InstallationsID und einen Key zu erhalten.

Nun lässt sich Bitwarden mit einem einfachen Befehl installieren ([siehe hier][2]). Domainnamen gebt ihr euren Zukünftigen an, Zertifikat wollen wir keines registrieren. Nun wird einmal alles notwendige heruntergeladen, danach will er die installation id und den key von vorher. Wenn er euch fragt, ob ihr ein Zertifikat zu verwenden habt, sagt nein. Generieren wollen wir auch keines.

Wenn das durchgelaufen ist, können wir beginnen, die Config-Files anzupassen. 

Beginnen wir mit der `bwdata/config.yml`. Hier müssen wir beim Punkt `real_ips:` (weil wir dann eine nginx Reverse-Proxy verwenden) die IP vom Reverse-Proxy angeben, damit später die echte IP von Nutzern aufgelöst werden kann:

<pre class="wp-block-code"><code>real_ips:
- 192.168.1.1</code></pre>

Als nächstes kümmern wir uns um die `bwdata/env/global.override.env`. Im Falle einer mailcow muss man E-Mail wie folgt konfigurieren:

<pre class="wp-block-code"><code>globalSettings__mail__replyToEmail=DEINE_EMAIL
globalSettings__mail__smtp__host=DEIN_HOST
globalSettings__mail__smtp__port=465
globalSettings__mail__smtp__ssl=true
globalSettings__mail__smtp__username=DEINE_EMAIL
globalSettings__mail__smtp__password=DEIN_PASSWORT
globalSettings__mail__smtp__trustServer=true</code></pre>

Nach jeder Änderung solltet ihr `./bitwarden.sh rebuild`, `./bitwarden.sh updateself` und `./bitwarden.sh update` machen.

Um immer am aktuellen Stand zu sein, empfiehlt es sich, einen täglichen cronjob wie folgt anzulegen:

<pre class="wp-block-code"><code>0 4 * * * /root/bitwarden.sh updateself && /root/bitwarden.sh update</code></pre>

Da Passwörter ja etwas ganz wichtiges sind, solltet ihr auch tägliche Backups davon fahren. Das geht zB mit Borg sehr gut. Dazu einfach den kompletten `bwdata` Ordner sichern, bitwarden legt automatisch jeden Tag um 23:59 einen Dump der Datenbank an, somit muss man sich um Konsistenz keine Gedanken machen.

Nun zum Herzstück des Ganzen, das leider sonst nirgends dokumentiert ist: die Config für einen nginx Reverse-Proxy:

<pre class="wp-block-code"><code>server {
    listen 80;
    listen &#91;::]:80;
    server_name DEINE_DOMAIN;
    return 301 https://$server_name$request_uri;


}

server {
    listen 443 ssl http2; 
    listen &#91;::]:443 ssl http2; # OCSP Stapling fetch OCSP records from URL in ssl_certificate and cache them
    server_name DEINE_DOMAIN; 

    ssl_stapling on;
    ssl_stapling_verify on;
    ssl_certificate /etc/letsencrypt/live/DEINE_DOMAIN/fullchain.pem; 
    ssl_trusted_certificate /etc/letsencrypt/live/DEINE_DOMAIN/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/DEINE_DOMAIN/privkey.pem; 

    resolver 9.9.9.9;
    ssl_ecdh_curve prime256v1;
    #add_header Strict-Transport-Security "max-age=15768000; includeSubDomains; preload;";
    #Server should determine the ciphers, not the client
    ssl_prefer_server_ciphers on;

    # SSL session handling
    ssl_session_timeout 24h;
    ssl_session_cache shared:SSL:50m;
       
    #add_header X-Content-Type-Options nosniff;
    #add_header X-XSS-Protection "1; mode=block";
    #add_header X-Robots-Tag none;
    #add_header X-Download-Options noopen;
    #add_header X-Permitted-Cross-Domain-Policies none;
        
        location / {
            proxy_pass http://IP_DES_BITWARDEN_HOSTS:80;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header X-Forwarded-Server $host;
            proxy_set_header Connection $http_connection;
            client_max_body_size 0;
    }
# Enable gzip but do not remove ETag headers
#    gzip on;
#    gzip_vary on;
#    gzip_comp_level 4;
#    gzip_min_length 256;
#    gzip_proxied expired no-cache no-store private no_last_modified no_etag auth;
#    gzip_types application/atom+xml application/javascript application/json application/ld+json application/manifest+json application/rss+xml application/vnd.geo+json application/vnd.ms-fontobject application/x-font-ttf application/x-web-app-manifest+json application/xhtml+xml application/xml font/opentype image/bmp image/svg+xml image/x-icon text/cache-manifest text/css text/plain text/vcard text/vnd.rim.location.xloc text/vtt text/x-component text/x-cross-domain-policy;

}</code></pre>

Da ich selbst bei nginx-Configs immer nur durch Trial&Error ans Ziel komme, bin ich für Verbesserungsvorschläge gerne offen!

Es gibt anscheinend auch eine Möglichkeit, fail2ban in das Ganze zu integrieren, sobald ich das weiß, reiche ich es natürlich hier nach.

###### Links Und Credit

<p style="font-size:12px">
  <br /><a href="https://help.bitwarden.com/article/updating-on-premise/">https://help.bitwarden.com/article/updating-on-premise/</a><br /><a href="https://github.com/broizter/BitwardenBackup">https://github.com/broizter/BitwardenBackup</a><br /><a href="https://github.com/fail2ban/fail2ban/pull/2567">https://github.com/fail2ban/fail2ban/pull/2567</a><br /><a href="https://help.bitwarden.com/article/install-on-premise/">https://help.bitwarden.com/article/install-on-premise/</a>
</p>

 [1]: https://bitwarden.com/host
 [2]: https://help.bitwarden.com/article/install-on-premise/