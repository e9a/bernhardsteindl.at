---
title: Linux-Login individualisieren
author: Christoph Scheidl
type: post
date: 2020-01-10T08:07:19+00:00
url: /linux-login-individualisieren/
categories:
  - Admin-Tipps
  - Grundlagen

---
Wenn man Linux auch professionell einsetzt, möchte man vielleicht auch sich selbst ein wenig im System bewerben. Unter Linux hat man hier, was das Terminal betrifft, 3 Möglichkeiten.

  1. Pre-Login: issue: unter Debian zB in der Datei `/etc/issue`
  2. Message of the Day: motd: unter Debian zB in der Datei `/etc/motd`, lässt sich hier die &#8222;Nachricht vom Tag&#8220; festlegen, zB ein Logo in ASCII Form
  3. rc-file: zB .bashrc, user-spezifisch, zB um Shell-Scripte aufzurufen

Die `/etc/issue` wird angezeigt, sobald der Boot-Vorgang abgeschlossen ist, bevor das Login auftaucht.

Die `/etc/motd` wird sofort nach dem Einloggen angezeigt.

Die `.bashrc` ist ein Bash-Skript, das nach der `/etc/motd` ausgeführt wird. Wenn zB neofetch am System angezeigt werden soll, kann das einfach in diese Datei eingefügt werden.

Hier gibt es ein nettes Tool, mit den man große ASCII-Schriftzüge (BigFonts) erstellen kann. <http://patorjk.com/software/taag/>

Ich verwende zB die Schriftarten Big oder Slant.

In der `.bashrc` macht sich zB auch ein `neofetch` ganz gut.