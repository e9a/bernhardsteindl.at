---
title: Archer C7 von den Toten zurückholen
author: Christoph Scheidl
type: post
date: 2019-01-13T09:47:41+00:00
url: /archer-c7-von-den-toten-zurueckholen/
categories:
  - Hardware

---
Heute hatten wir einen toten Archer C7 v1.1 vor uns, der nicht einmal mehr per TFTP-Recovery zurückzuholen war. Dies soll einen detaillierten Einblick geben, wie wir das Gerät wieder von den Toten zurückgeholt haben.

## Verbindung zur seriellen Schnittstelle schaffen

Nachdem die integrierte Wiederherstellungsmöglichkeit von TP-Link, einen TFTP-Server aufzusetzen und damit das Image zu flashen, nicht mehr funktioniert hat, mussten wir zu einer anderen Lösung greifen: Der Wiederherstellung mittels serieller Schnittstelle.

Dazu mussten wir das Gerät aufschrauben, und mittels Hilfe aus dem OpenWRT-TableOfHardware-Eintrags für den Archer C7 konnten wir dann den UART-Header auf unserem Board ausmachen. Zuerst versuchten wir es mit einem USB auf RS-232 Adapter von Digitus, aber UART dürfte hier doch ein wenig anders funktionieren, deshalb mussten wir auf diesen USB-UART-TTL-Adapter[^6] zurückgreifen, den wir glücklicherweise zuhause hatten.

Als wir die Verbindung stehen hatten (Pinout siehe hier`[^1]`), konnten wir unter Linux mittels dem Tool &#8222;screen&#8220; ein Terminal mit der BAUD-Rate von 115200 öffnen:

<pre class="wp-block-code"><code>sudo screen /dev/ttyUSB0 115200</code></pre>

Damit hatten wir die Möglichkeit, die Fehlermeldungen des Gerätes auszulesen und dementsprechend darauf zu reagieren

## TFTP-Server aufsetzen

Da wir in diesem Fall mit Arch-Linux arbeiteten, haben wir mittels Pacman das Paket &#8222;tftp-hpa&#8220;[^4] installiert und den TFTP-Server gleich gestartet.

<pre class="wp-block-code"><code>sudo systemctl enable tftpd.service
sudo systemctl start tftpd.service
sudo systemctl status tftpd.service</code></pre>

Nachdem wir wussten, das das funktioniert, haben wir uns auf einem Netzwerk-Interface die Adresse 192.168.1.100 zugewiesen, weil sich von dieser Adresse der Router das Image über TFTP holt. Den Router haben wir mit dem Computer auf dem Port &#8222;LAN 1&#8220; verbunden.

Nun stellten wir uns die Frage, von wo wir das Image herbekommen sollten. Da hat TP-Link gute Arbeit geleistet, jeder Image bekommt man auf deren Produktseite. Also haben wir das passende heruntergeladen, und in den Ordner &#8222;/srv/tftp/&#8220; gepackt, wo der TFTP-Server dieses zum Download anbietet.

## Erster Versuch, das Image zu flashen

In dem OpenWRT-Guide[^1] ist dann eine Befehlskette aufgeschrieben, mittels der man ein Image auf den entweder 8 oder 16MB großen Flash-Chip aufspielen kann. Bei dem Archer C7 v1.1 ist es ein 8MB Chip. Nachdem wir das Flashen mit dem offiziellen Image 5mal versucht hatten und immer wieder die Fehlermeldung

<pre class="wp-block-code"><code>Uncompressing Kernel Image ... Too big uncompressed streamLZMA ERROR 1 - must    RESET board to recover </code></pre>

bekommen haben, mussten wir wieder die Suchmaschine unseres Vertrauens besuchen, um den Fehler zu suchen. 

## Image mit und ohne Bootloader

Nach einigen Foren und Blogposts sind wir schlussendlich auf den Fehler draufgekommen: Die Images auf der Herstellerseite beinhalten den Bootloader. Wenn wir allerdings mittels der Befehlskette des OpenWRT-Eintrags flashen, darf der Bootloader nicht im Image dabei sein. Um das Problem zu beheben, gibt es einen einfachen Trick: Man öffne die .bin-Datei in einem Texteditor, nehme die ersten Quartete bis zu den vielen Nullen (die Nullen exklusive), und sucht nach diesen im Image. Bei uns kam diese Stelle 2mal vor, einmal am Beginn des Files (Dies ist die Einleitung zum Bootloader), und einmal bei Zeile 8225 (dies ist der Beginn des eigentlichen Images). Vor dieser Zeile war alles mit &#8222;FFFF&#8220; ausgefüllt. Alles über den zweiten Header haben wir weggelöscht, und die Datei mit dem Namen &#8222;ArcherC7v1\_tp\_recovery.bin&#8220; abgespeichert. Ob der Dateiname wirklich exakt so lauten muss wissen wir nicht, aber nachdem es so bei uns so geklappt hat, ist es durchaus denkbar, dass es genauso heißen muss.

Nun haben wir wieder die Befehlskette von der Open-WRT-Seite befolgt, und diesmal klappte alles ohne Probleme. Eines sei noch dazu gesagt: Die serielle Console des Routers ist ziemlich intolerant was das Weglöschen von Zeichen betrifft, also bitte tippt jedes Zeichen genau ein, ohne etwas löschen zu müssen, ansonsten kann es zu Fehlern kommen.

## Erfolg  


Nachdem das Gerät diesmal rebootet hat, funktionierte alles auf Anhieb. Man darf sich hier nicht zu sehr erschrecken, die Serielle Schnittstelle liefert so allerlei Fehler, die aber nicht relevant sind.

Nachdem wir unseren LAN-Port wieder auf DHCP gestellt hatten, konnten wir das Web-Interface unter der Standard-Adresse 192.168.0.1 normal aufrufen und uns ohne Probleme einloggen.

Der Router ist wieder unter den Lebenden!

###### Links und Credit

<p style="font-size:12px">
  <code>[^1]:</code><a href="https://openwrt.org/toh/tp-link/archer-c5-c7-wdr7500#recovery_using_serial_connection">https://openwrt.org/toh/tp-link/archer-c5-c7-wdr7500#recovery_using_serial_connection</a><br /><code>[^2]:</code><a href="https://forum.archive.openwrt.org/viewtopic.php?id=44201&p=13">https://forum.archive.openwrt.org/viewtopic.php?id=44201&p=13</a><br /><code>[^3]:</code><a href="https://openwrt.org/toh/hwdata/tp-link/tp-link_archer_c7_ac1750_v1">https://openwrt.org/toh/hwdata/tp-link/tp-link_archer_c7_ac1750_v1</a><br /><code>[^4]:</code><a href="https://wiki.archlinux.org/index.php/TFTP">https://wiki.archlinux.org/index.php/TFTP</a><br /><code>[^5]:</code><a href="https://www.tp-link.com/at/download/Archer-C7_V1.html#Firmware">https://www.tp-link.com/at/download/Archer-C7_V1.html#Firmware</a><br />[^6]: <a href="https://www.amazon.de/dp/B00AFRXKFU/">https://www.amazon.de/dp/B00AFRXKFU/</a><br /><br /><br />
</p>