---
title: Warum Office 365 für Unternehmen keine Lösung ist
author: Christoph Scheidl
type: post
date: 2019-02-08T10:24:16+00:00
url: /warum-office-365-fuer-unternehmen-keine-loesung-ist/
categories:
  - Rants
  - Windows

---
UPDATE: Der Artikel kusiert zwar schon länger, ich komme aber heute erst dazu ihn zu posten&#8230; Ein weiterer Grund, warum man keine Microsoft-Cloud-Produkte nutzen sollte und es nicht DSGVO-Konform ist:

<https://www.heise.de/newsticker/meldung/Untersuchung-Microsoft-Office-sammelt-Daten-und-verstoesst-gegen-die-DSGVO-4224823.html>

Viele Unternehmen versuchen mittlerweile (leider), Ihre IT in die Cloud zu verfrachten. Vor allem in die Cloud US-Amerikanischer Hersteller wie Google, Microsoft oder Dropbox. Warum das aber eigentlich nicht DSGVO-Konform und somit eigentlich schwer illegal ist, zeigt dieser Blog-Eintrag der Firma Nextcloud:<figure class="wp-block-embed-wordpress wp-block-embed is-type-wp-embed is-provider-nextcloud">

<div class="wp-block-embed__wrapper">
  <blockquote class="wp-embedded-content" data-secret="dQVVWMRSC2">
    <a href="https://nextcloud.com/blog/european-datacenter-is-no-solution/">European datacenter is no solution, recent developments show</a>
  </blockquote>
</div><figcaption>European datacenter is no solution, recent developments show &#8211; nextcloud.com</figcaption></figure>