---
title: nsupdate.info DynDNS unter Synology einrichten
author: Christoph Scheidl
type: post
date: 2020-05-07T11:43:02+00:00
url: /nsupdate-info-dyndns-unter-synology-einrichten/
categories:
  - Allgemein

---
Um den nsupdate.info DynDNS-Service unter einer Synology DiskStation verwenden zu können, muss man wie folgt vorgehen:

Zuerst geht man auf _Control Panel_ -> _External Access_ -> _DDNS_.

Nun muss man den Menüpunkt _Customize_ drücken, wo man einen neuen DDNS-Anbieter hinzufügen kann. Dazu einfach im Feld _Service Provider_ einen Namen vergeben, zB _nsupdate.info_, und unter _Query URL_ folgendes eintragen:

<pre class="wp-block-code"><code>https:&#47;&#47;ipv4.nsupdate.info/nic/update</code></pre>

Nun drückt man Save, und auf den Menüpunkt _Add_. Unter _Service Provider_ wählen wir nun unseren erstellten _*nsupdate.info_ aus, bei Hostname geben wir den Hostname an, den wir im Web-Interface von Core-Networks angelegt haben. Unter _Username_ und _Password_ die entsprechend angelegten Zugangsdaten. Mit einem Klick auf _Test Connection_ können wir die Verbindung testen. Dann sollte der Status _Normal_ auftauchen. Wenn ihr nun auf _OK_ klickt, kann es sein, dass der Status _Loading_ steht, davon nicht beirren lassen, einfach das _Control Panel_ schließen und nochmals öffnen.