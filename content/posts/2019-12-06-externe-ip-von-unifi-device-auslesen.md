---
title: Externe IP von Unifi-Device auslesen
author: Christoph Scheidl
type: post
date: 2019-12-06T10:17:33+00:00
url: /externe-ip-von-unifi-device-auslesen/
categories:
  - Admin-Tipps
  - Ubiquiti Unifi

---
Hin und wieder spinnt schon mal das DDNS bei manchen Geräten. Wenn man in diesen Netzen ein Unifi-Gerät auf einem erreichbaren Controller hat, kann man sich aber die externe IP von diesem einfach auslesen. Dazu im Controller auf das Gerät klicken, auf den Reiter &#8222;Config&#8220; (Zahnrad) drücken und ganz unten auf &#8222;Download Device Info&#8220;. Dann erhält man eine JSON, in der man einfach nach &#8222;extip&#8220; suchen kann, und schon hat man seine IP gefunden.

###### Links und Credit

<p style="font-size:12px">
  <a href="https://community.ui.com/questions/Is-there-a-way-to-get-public-IP-address-of-an-AP/3c8138bb-356d-427a-b18a-63bdbe119c0b">https://community.ui.com/questions/Is-there-a-way-to-get-public-IP-address-of-an-AP/3c8138bb-356d-427a-b18a-63bdbe119c0b</a>
</p>