---
# Title
title: "GRÜNE KREMS - Ohne Wahlprogramm keine gute Wahl"
# Enter the author's name
author: Bernhard Steindl
# Customize URI; default: post/filename
#url: /yubikey-fuer-linux-login-als-u2f-2-faktor-verwenden/
# Date
date: 2022-08-31T18:00:00+01:00
# Is this draft?
draft: false
# Show Table of Content?
showToc: true
# Available categories. Please comment out!
categories:
#- Admin-Tipps
#- Allgemein
#- Arch Linux
#- Debian
#- Denkt's noch!
#- Grundlagen
#- Hardware
#- mariadb
#- News
#- nginx
- Politik
#- Projekte
#- Proxmox
#- Rants
#- Satire
#- Synology
#- Tor
#- Ubiquiti Unifi
#- Windows
#- Wordpress
#- ZFS

---

# Die Grünen

*Dies ist ein Artikel der Serie [Pappkameraden in Krems](/posts/2022-08-25-die-kremser-pappkameraden/). Sämtliche Inhalte genügen keinen journalistischen Ansprüchen wie Ausgewogenheit oder DoubleChecks, falls Sie dennoch falsche Informationen finden oder Anmerkungen zu den Artikeln haben, würde ich mich über eine [Rückmeldung](/impressum) sehr freuen.*

Laut aktuellen Pressemittelungen haben sich die Grünen in Krems kürzlich gespalten, von der Landespartei wurde **Markus Schwarz** nach Krems gesetzt und löste den bisherigen Spitzenkandidaten Matthias Unolt ab, welcher zugleich [erbost die Liste *Green Future Krems* gründete](https://www.noen.at/krems/gemeindepolitik-kremser-gruene-brechen-vor-wahl-auseinander-krems-print-gemeinderatswahl-krems-gruene-krems-markus-schwarz-matthias-unolt-326621327). Ein wirkliches Wahlprogramm konnte ich nicht finden, daher sehen wir uns einmal die Wahlplakate an:

{{< figure src="images/gruene1.jpeg" caption="Ich finde, dass dies die Krone des unnötigsten Wahlplakats 2022 verdient. Es sagt mir dass ich Markus Schwarz X-en soll am 04.09.2022. Aber warum soll ich das machen? Ich mach ja nicht etwas, nur weil’s auf einem Plakat steht…!" >}}
{{< figure src="images/gruene2.jpeg" caption="Gut, der Slogan ist schlecht, aber nicht falsch. In Österreich muss man halt im freundlichen Sinne immer die Alkoholiker-Seele ansprechen." >}}
{{< figure src="images/gruene3.jpeg" caption="Ui, zu viele Köche verderben den Brei. Und mit Sarah Wiener möchte ich [eigentlich nichts zu tun haben](https://www.derstandard.at/story/2000106954025/gagenkaiserin-sarah-wiener)." >}}

Auf der [Website](https://noe.gruene.at/gemeinden/krems-an-der-donau-stadt/) lassen sich drei Themen auffinden:
1. Bio.Logisch! Kochen mit Sarah Wiener in Krems
2. Retten wir das Klima für unseren Veltliner!
3. Krems verliert an Boden!

Danach finden sich zwei Zeitungsberichte und ein Foto von Markus Schwarz mit einer *Apple Watch*. Seine E-Mail-Adresse findet sich auch hier. Das war’s. Kein Wahlprogramm. Keine Liste der antretenden Personen. Nichts! Und das soll eine fortschrittliche Partei sein?!

Weitere Forderungen kann ich selbst von einem [Zeitungsartikel](https://www.meinbezirk.at/krems/c-lokales/gruenen-mit-dem-fahrrad-durch-die-stadt_a5554081) abschreiben:

> Ein zeitgemäßes, modernes Radwegkonzept, betonte Schwarz im Gespräch mit Krismer ist eine zentrale Forderung des Grünen Mobilitätsplans für Krems: Mehr aber vor allem bessere Radwege, eine Entflechtung des Rad- und Fußgängerverkehrs, ein Parkleitsystem, das PKWs – insbesondere die von Besuchern – möglichst gar nicht ins Zentrum fahren lässt, ein effizientes Bus-Shuttle mit kleinen E-Bussen sowie die Schaffung von attraktiven und - das vor allem – Diebstahl- und Vandalismus-sicheren Radabstellplätzen sind nur ein Aspekt eines modernen Verkehrssystems, das Schwarz fordert.

Man könnte auch auf Bundesebene dafür sorgen, dass die Polizei Fahrraddiebstähle endlich ernst nimmt. Bis dato wird man nämlich mehr oder weniger auf der Polizeidienststelle ausgelacht und dazu bewogen, gar keine Anzeige zu machen, denn es wird sich sowieso niemand darum kümmern. Man bekommt nicht einmal einen Brief, wenn die *Ermittlungen* eingestellt wurden, wenn sie denn überhaupt begonnen haben.

> „Krems wäre eine ideale Stadt der kurzen Wege - die meisten Shops und Lokale sind in Geh-Distanz. Man muss den Menschen lediglich Lust darauf machen, sich ohne Auto in ein Zentrum zu bewegen, in dem auch im Straßenbild Leben und Lebensfreude statt abgestellten oder Parkplatz suchenden Blechs regiert. Das kann es nur mit starken Grünen in Krems geben“, schließen Krismer und Schwarz ab.

Und warum sehen sie es nicht für notwendig, dazu eine PDF zamzuklatschen, wo all das drinnensteht? Glauben die echt, dass sie so die erforderliche Mehrheit für die Umsetzung ihres *spärlichen* Programms finden? Vielleicht kennt ja jemanden einen Kapazunder der Partei der das weiß?

# Conclusio

Es tut mir Leid, aber wer nicht einmal Zeit hat, die paar Forderungen, die man hat, auf einen Wisch zusammenzuklatschen, der soll sich nicht wundern, wenn er nicht erfolgreich ist. Bis dato fielen die Grünen in Krems nicht damit auf, Kompetenz zu zeigen, wenn ich an ihrem [gescheiterten Baum-Experiment](https://www.noen.at/krems/verwunderung-um-vp-gruen-umweltpolitik-kurios-im-kremser-gemeinderat-krems-baumschutzverordnung-print-gemeinderat-krems-matthias-unolt-276059620) erkennen kann. Eine Partei ohne Wahlprogramm werde ich nicht unterstützen.

*Lesen Sie weitere Artikel der Serie [Pappkameraden in Krems](/posts/2022-08-25-die-kremser-pappkameraden/)!*