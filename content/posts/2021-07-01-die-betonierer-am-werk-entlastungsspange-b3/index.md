---
title: Die Betonierer am Werk – „Entlastungsspange B3“
author: Bernhard Steindl
type: post
date: 2021-07-01T09:57:07+00:00
url: /die-betonierer-am-werk-entlastungsspange-b3/
categories:
  - Allgemein

---
Manchmal ist man schon ein armer Hund &#8211; da lest man sich einmal den neuen Flächenwidmungsplan durch und man stößt wieder auf eine komplett hirnrissige Idee. Das gibt&#8217;s doch gar nicht, dass im Jahr 2021 in Krems noch immer die 60er-Jahre vorherrschen.

Gerade haben wir eine Hitzewelle hinter uns, die uns gezeigt hat, wie sehr sich eine zubetonierte Stadt aufheizen kann &#8211; aber in Krems lernt man bekanntlich nicht aus seinen Fehlern.

## Worum gehts?

{{< figure align=center src="images/image.png" caption="Geplante Spange B3 in Krems" >}}

Offensichtlich ist man bei der Stadt Krems und/oder der Landesregierung unglücklich über die Doppelkreisverkehre in Krems, welche insgesamt einen Verkehr von 18.000 Autos/Tag (Nördlicher Kreisverkehr) und 19.000 Autos/Tag (Südlicher Kreisverkehr) aufweisen. Dazu hat man drei Varianten untersucht, bei denen natürlich die Variante ohne Verkehrsberuhigung als Umsetzungsvariante in Betracht gezogen wurde. Kann man ja sich nicht ausdenken, wenn der Platz nicht mehr schirch wäre, da könnte ja die &#8222;Freie Fahrt für freie Bürger&#8220; beeinträchtigt werden.

Zu Stoßzeiten kommt es hier schon einmal zu Wartezeiten von ein paar Sekündchen, daher will man hier Abhilfe schaffen und eine Spange bauen, welche die Kreisverkehre um prognostizierte 1500 Autos &#8222;entlasten&#8220; wird. Das löst weder ein Stauproblem, noch macht es die Relation schneller, da ja eine neue Ampel geplant wird. Dies bremst natürlich die schwächsten Verkehrsteilnehmer aus und somit wird der Weg für mich als Fußgänger langsamer, während er für die Autofahrer vielleicht schneller, aber wahrscheinlich gleichbleibend wenn nicht sogar langsamer wird.

Rein rechnerisch gesehen kann laut dem Gutachten der Doppelkreisverkehr um etwa 10% entlastet werden, aber nur, wenn die Fußgänger dann von diesen verbannt werden und in eine Brücke oder Unterführung gequetscht werden. Wie in den 70er Jahren versucht man, die &#8222;störenden&#8220; Teilnehmer, welche einem beim Autofahren &#8222;hindern&#8220;, in den Untergrund zu verlagern, während dem Auto der Blick auf die Wachau gegönnt wird.

Es macht überhaupt keinen Sinn, 1250 m² für ein paar Spitzenzeiten zu verbauen, der Zeitverlust für den MIV ist hier sowieso nie mehr als wie eine Minute. Stattdessen ist zu befürchten, dass durch mehr Kapazität noch mehr Verkehr erzeugt wird, was allgemein bekannt ist.

{{< figure align=center src="images/image-2.png" caption="Warum der Yachthafen (links oben ist der Hofer) einen eigenen Anschluss bekommt, wird ein Mysterium bleiben. (Kein besseres Foto verfügbar)" >}}

Weiters wird eine Abfahrt (nur von Richtung Wachau) zur Yachthafenstraße vorgesehen, die ist sowieso komplett sinnlos, aber das ist ein Kriterium für Verkehrspolitik in Krems.

## Alternative Vorschläge

In dem Gutachten wurde gar nicht berücksichtigt, dass man den Verkehr durch öffentliche Verkehrsmittel wesentlich verringern könnte, etwa mit einer Wiederbelebung der Wachaubahn bspw. durch Elektrifizierung und Durchbindung der Kremser REX direkt nach Spitz. Das würde Mut und Innovationskraft vorraussetzen, in Krems betoniert man lieber eine unnötige Spange.

Baut man die Verkehrswege nicht mehr aus, ist irgendwann eine Belastungsgrenze erreicht, bei der auf andere Verkehrswege umgestiegen wird. In Österreich sind 50% aller Autofahrten kürzer als zwei Kilometer, auch hier muss man die Fuß- und Radmobilität fördern statt wieder mehr Straßen für mehr Verkehr zu bauen, was bekanntlich nicht funktioniert.

Mein Vorschlag ist es, den gesamten Bereich außer die B3 zu beruhigen, bei der die Fußgänger diese mit einer Ampel kreuzen können. Der restliche Bereich ist rückzubauen und zu begrünen, was automatisch zu weniger Verkehr führt. Weiters sollte statt der Fußgängerverkehr der Autoverkehr in den Untergrund wandern, das würde auch die permanente Lärmbelastung von 70 dB reduzieren.

## Stellungnahme

Um meine Bedenken auch der Stadt Krems mitteilen zu können, die meinen Blog vermutlich nicht liest (macht ja sonst auch niemand), verfasste ich eine Stellungnahme. Hier stehen auch noch weitere Begründungen für die Ablehnung des Projektes.

[Stellungnahme ansehen](https://cdn.bernhardsteindl.at/Umweltschutz/60-Flaechenwidmungsplan-Stell-BSteindl-5.pdf)

## Links

[Flächenwidmungsplan (Spange ab Seite 29)][1]

[Gutachten][2]

Informationen zur Verkehrswende:

[Landesnaturschutzverband Baden-Würtemberg][2]

[Straßen und Plätze neu denken][3]

[Warum neue Straßen mehr Stau verursachen][4]

 [1]: https://www.krems.at/fileadmin/Dateien/Downloads/Stadtentwicklung/Flaechenwidmung/Erlaeuterungsbericht.pdf
 [2]: https://www.krems.at/fileadmin/Dateien/Downloads/Stadtentwicklung/Flaechenwidmung/Anlagen_zum_Erlaeuterungsbericht.pdf
 [3]: https://www.umweltbundesamt.de/sites/default/files/medien/421/publikationen/180109_uba_broschuere_strassen_und_plaetze_neu_denken.pdf
 [4]: https://de.wikipedia.org/wiki/Braess-Paradoxon
