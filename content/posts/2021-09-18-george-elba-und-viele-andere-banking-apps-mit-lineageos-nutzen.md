---
title: George, ELBA und viele andere Banking-Apps mit LineageOS nutzen
author: Bernhard Steindl
type: post
date: 2021-09-17T22:32:23+00:00
url: /george-elba-und-viele-andere-banking-apps-mit-lineageos-nutzen/
classic-editor-remember:
  - block-editor
categories:
  - Allgemein

---
**Update 18.09.:** Potentiell beleidigende Begriffe wurden entfernt.

Ich mag aus Prinzip kein Stock-Android. Die Updates kommen erst irgendwann, die Entwicklung wird nach Indien und in andere Niedriglohnländer ausgelagert und die spionierenden Google-Play-Services möchte ich gar nicht erst in meine Nähe lassen.

Nun gibt es damit ein kleines Problem, leider benötigen immer mehr Apps das sogenannte &#8222;[SafetyNet][1]&#8222;, welches für Sicherheit unter Android sorgen soll. Wo es Menschen in der Entwicklung gibt, existieren auch Fehler und Backdoors, somit war es bis jetzt ein leichtes, [diese Einschränkungen zu umgehen][2].

Mir persönlich war auch nie klar, warum Banken dieses Sicherheitsfeature benötigen. Wenn ihr Backend gut ist, kann ich selbst mit den umfangreichsten Rootrechten nichts hacken. Das Online-Webbanking hat sogar größeren Funktionsumfang, aber hier kann ich nach belieben in der JS-Konsole rumfuhrwerken. Aber wehe ich will keine Google-Dienste haben und bin mit LineageOS auf einem besseren Sicherheitsstand wie Stock &#8230;

Apropos Google, die lösen halt Sicherheitsprobleme auf ihre Art und Weise. Sie kaufen den [Entwickler von Magisk einfach auf][3], welcher dann aus &#8222;jobtechnischen&#8220; Gründen dann natürlich nicht mehr an dem Tool weiterarbeiten kann. Wirkt wie der [Novomatic-Deal mit der Glawischnig][4], wo man sich auch irgendeinen unnötigen Posten ausgedacht hat. ([Die Tagespresse konnte die Wahrheit nicht toppen][5])

Beinahe 5 Jahre nach der Wahl von Donald Trump kommt nun das erste positive Resultat auch zu mir: [Der Trump-Huawei-Bann][6].

Nun kommen wir der Sache schon näher: Durch dieses Dekret darf Huawei keine Google Play (Services) mehr ausliefern. Das brachte den berühmten Geräte-Hersteller unter Zugzwang und somit wurde die Huawei AppGallery geboren.

Auch die österreichischen Banken (übrigens auch Netflix), welche unter Android nur bei Google Play vertreten waren, mussten in der AppGallery neu releasen. Mit einem Vorteil:

## Ohne Google Play kein SafetyNet

Somit verfügen die Builds im Huawei-Store über keine SafetyNet-Abfrage, da diese unter den Huawei-Handys gar nicht funktionieren würde. Leider gibt es trotzdem ein paar Einschränkungen:

  * (George-only) Die gewünschte App muss direkt von der Huawei AppGallery heruntergeladen werden
  * (George-only) Die AppGallery darf auch nach der Installation nicht entfernt werden, aber alle Prozesse abwürgen & Berechtigungen entfernen ist okay.
  * Die meisten Apps scannen trotzdem nach SuperSU oder Magisk, somit muss zumindest die App unauffindbar gemacht werden (unbenennen des Paketes etc.). Falls das Gerät nicht gerootet ist, kein Problem.

Doch das Endresultat überzeugt: Die Banking-App funktioniert endlich wie gewünscht und so leicht hat man noch [nie knallharte Crypto][7] überlistet. Währenddessen man tausende Android-Benutzer mit SafetyNet-Abfragen nervt, kommt man als vermeintliches Huawei-User einfach ohne irgendwas durch.

## Schritt-für-Schritt-Anleitung

  1. Dafür sorgen, dass Magisk/SuperSU nicht erkannt werden kann (Magisk Hide, unrooten, &#8230;)
  2. [Huawei App Gallery herunterladen][8]
  3. App Gallery ausführen, aber keine Berechtigungen außer die App-Installation freigeben
  4. Gewünschte App suchen:
      1. [George][9] (tested)
      2. [ELBA][9] (shows warning, works)
      3. [BAWAG][9] (not tested)
      4. [Bank Austria][9] (works)
      5. [weitere Banken][10] (oder selbst suchen)
  5. App über die AppGallery installieren
  6. Dem Schorsch eins Auswischen!

Ich habe außer dem George nichts genau durchgetestet, aber hier funktioniert alles inklusive &#8222;Zahlen mit dem Handy&#8220;. Ich freue mich über etwaige Erfahrungsberichte per Mail (würde ich dann hier ergänzen) oder in den Kommentaren drunter.

 [1]: https://developer.android.com/training/safetynet/attestation
 [2]: https://www.didgeridoohan.com/magisk/MagiskHide
 [3]: https://topjohnwu.medium.com/state-of-magisk-2021-fe29fdaee458
 [4]: https://www.novomatic.com/explore-novomatic/presse/pressemitteilungen/glawischnig-wechselt-zu-novomatic
 [5]: https://dietagespresse.com/glawischnig-novomatic/
 [6]: https://www.androidauthority.com/huawei-equipment-ban-987000/
 [7]: https://www.xda-developers.com/bypass-safetynet-hardware-attestation-unlocked-bootloader-magisk-module/
 [8]: https://appgallery.huawei.com/#/app/C27162
 [9]: https://appgallery.huawei.com/#/app/C102556323
 [10]: https://appgallery.huawei.com/#/search/austria%20bank?1631917566582