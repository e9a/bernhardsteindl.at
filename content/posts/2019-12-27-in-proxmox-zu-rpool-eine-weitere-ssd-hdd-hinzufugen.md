---
title: In Proxmox zu rpool eine weitere SSD/HDD hinzufügen
author: Christoph Scheidl
type: post
date: 2019-12-27T08:34:04+00:00
url: /in-proxmox-zu-rpool-eine-weitere-ssd-hdd-hinzufugen/
classic-editor-remember:
  - block-editor
categories:
  - Proxmox
  - ZFS

---
Neulich kam ich in die Situation, dass ich bei einer älteren Proxmox-Installation auf nur eine SSD als Boot-Drive traf. Das ist natürlich sehr schlecht, zumal auf der SSD auch noch VMs und LXCs rannten, die im Falle eines Hardware-Defekts dann kaputt gewesen wären. Mit ZFS ist es jedoch relativ einfach möglich, hier eine weitere SSD hinzuzufügen, damit dann in Zukunft von einem ZFS-mirror gebootet werden kann. Leider ist der genaue Ablauf aber nicht gut dokumentiert seitens Proxmox, deshalb dieser Artikel.

Zu Beginn machen wir am System einmal `zpool status -v`:

<pre class="wp-block-code"><code>  pool: rpool
 state: ONLINE
status: Some supported features are not enabled on the pool. The pool can
        still be used, but some features are unavailable.
action: Enable all features using 'zpool upgrade'. Once this is done,
        the pool may no longer be accessible by software that does not support
        the features. See zpool-features(5) for details.
  scan: none requested
config:

        NAME                                                 STATE     READ WRITE CKSUM
        rpool                                                ONLINE       0     0     0
          mirror-0                                           ONLINE       0     0     0
            sda3                                             ONLINE       0     0     0</code></pre>

Was in diesem Fall zusätzlich schlecht ist: Sollte das System auf die Idee kommen, die Festplatte nicht mehr auf `sde3` zu legen, kann es sein, dass sich der Pool nicht mehr findet. Das wollen wir natürlich nicht, aber eines nach dem anderen.

Zunächst müssen wir eine weitere SSD, im Idealfall mit gleicher Größe einbauen. Dann können wir uns mit dem Befehl `hwinfo --disk` oder mit `fdisk -l` oder mit `lsblk` die neue Platte suchen. Auch in der Proxmox-GUI können wir unter Disks die neue Platte sehen. Sollten wir nur die `/dev/sdx`-Identifikation wissen, dann einfach mit `hwinfo --disk | grep /dev/sda` nachsehen, wie der `by-id` Pfad der Festplatte heißt. Ob man hier die `ata-...` Bezeichung oder die `wnn-...` verwendet, bleibt jedem selbst überlassen, ich verwende für meinen rpool die `ata-` Schreibweise.

Bevor wir uns jedoch nun ins Vergnügen stürzen, müssen wir uns noch die Boot-Sektoren und EFI-Disk kopieren. Das geht mit folgender Befehlsreihe (hier kann man auch die `/dev/sdx` Bezeichner verwenden, weil man das ohnehin nur einmal macht. In diesem Fall ist `/dev/sda` die bestehende SSD und `/dev/sdb` die Neue:

WARNUNG: Das sind gefährliche Befehle. Bitte kontrolliert 3x, am besten im 4-Augen-Prinzip, ob die Platten auch wirklich die richtigen sind. Sollte hier etwas schief gehen, habt ihr euch das Proxmox-System richtig zerfetzt.

<pre class="wp-block-code"><code>sgdisk --replicate=/dev/sdb /dev/sda
sgdisk --randomize-guids /dev/sdb
grub-install /dev/sdb</code></pre>

Was hier passiert ist folgendes:  
Im ersten Befehl wird das Partitionsschema auf die neue SSD repliziert.  
Im zweiten Schritt wird die [GUID][1] mit einem Zufallswert neu gesetzt, damit es hier zu keinem Blödsinn kommt.  
Im dritten Schritt installieren wir GRUB2, den Bootloader von Debian bzw. Proxmox, auf die neue Platte in die zuvor kopierten Partitionen.

ANMERKUNG!

Wie [hier][2] zu sehen, kann es auch sein, dass man anstatt grub-install oder zuätzlich das Tool pve-efiboot-tool verwenden kann/muss/soll? Das muss ich erst evaluieren, eventuell hängt das mit BIOS vs. UEFI-Boot zusammen. Sobald dies getestet ist, werde ich den Artikel dementsprechend anpassen! Das wären die folgenden 2 Befehle: 

<pre class="wp-block-code"><code>pve-efiboot-tool format /dev/sdb2 --force
pve-efiboot-tool init /dev/sdb2</code></pre>

Die offizielle Empfehlung von Proxmox ist anscheinend, grub-install, aber das kann natürlich auch veraltet sein ([siehe hier][3]).

ANMERKUNG ENDE!

Nachdem die Platte nun bootfähig ist, können wir die dritte Partition, auf der nun ZFS die Daten speichern soll, in den ZFS-Pool `rpool` hängen. Hier verwenden wir nun die vorher erklärten `ata-...` Identifier. Wichtig ist hier zu beachten, der Befehl sowohl den Namen des aktuellen Pools haben möchte (hier `rpool`), als auch die aktuelle SSD, auf der der Pool gespeichert ist (in diesem Fall `sda3`). Der Identifier der bestehenden Disk kann hier jedoch nicht irgendwie verwendet werden (also nicht ata-Schreibweisen, wnn-Schreibweise etc.), sondern muss exakt mit der Schreibweise ident sein, die bei `zpool status -v` gezeigt wird. In meinem Fall (siehe oben) ist das nur `sda3`, ohne `/dev` oder ähnlichem davor. Die Platte, die wir hinzufügen wollen, benennen wir aber sehr wohl nach dem `by-id` Schema, damit wir die oben beschriebene Problematik, dass das System die Platte nicht mehr auf /dev/sda legen könnte, nicht haben. Hier ist auch zu beachten, dass die ZFS-Partition nun die Partition 3 ist, weil auf Part1 die Boot-Partition liegt und auf Part2 die EFI-Disk. Das sieht dann in meinem Fall so aus (mit Zensur des Herstellers und der Seriennummer):

<pre class="wp-block-code"><code>zpool attach rpool sda3 /dev/disk/by-id/ata-Herstellergedöns-part3</code></pre>

Mit dem Befehl `zpool status -v 5` könnt ihr nun zusehen, wie das System resilvered (Die 5 gibt dabei die Aktualisierungsrate in Sekunden an).

Bevor das System nicht resilvered ist, solltet ihr es nicht rebooten, sonst könnte es zu Problemen kommen.

Um einen misslungenen Identiefier auszubessern, eignet sich [diese Anleitung][4].

###### Links und Credit

<p style="font-size:12px">
  <a href="https://forum.proxmox.com/threads/different-size-disks-for-zfs-raidz-1-root-file-system.22774/post-208998">https://forum.proxmox.com/threads/different-size-disks-for-zfs-raidz-1-root-file-system.22774/post-208998</a><br /><a href="https://pve.proxmox.com/wiki/ZFS:_Tips_and_Tricks">https://pve.proxmox.com/wiki/ZFS:_Tips_and_Tricks</a><br /><a href="https://pve.proxmox.com/pve-docs/pve-admin-guide.html#sysboot">https://pve.proxmox.com/pve-docs/pve-admin-guide.html#sysboot</a><br /><a href="https://forum.proxmox.com/threads/proxmox-zfs-mirror-install-with-one-disk-initially.53601/">https://forum.proxmox.com/threads/proxmox-zfs-mirror-install-with-one-disk-initially.53601/</a><br /><a href="https://briankoopman.com/moving/">https://briankoopman.com/moving/</a><br /><a href="https://askubuntu.com/questions/27997/which-hard-disk-drive-is-which">https://askubuntu.com/questions/27997/which-hard-disk-drive-is-which</a><br /><a href="https://forum.proxmox.com/threads/zfs-attach-on-boot-drive-is-there-a-right-way.61761/">https://forum.proxmox.com/threads/zfs-attach-on-boot-drive-is-there-a-right-way.61761/</a><br /><a href="https://pve.proxmox.com/wiki/ZFS:_Tips_and_Tricks#Replacing_a_failed_disk_in_the_root_pool">https://pve.proxmox.com/wiki/ZFS:_Tips_and_Tricks#Replacing_a_failed_disk_in_the_root_pool</a><br /><a href="https://plantroon.com/changing-disk-identifiers-in-zpool/">https://plantroon.com/changing-disk-identifiers-in-zpool/</a>
</p>

 [1]: https://de.wikipedia.org/wiki/GUID_Partition_Table
 [2]: https://forum.proxmox.com/threads/proxmox-zfs-mirror-install-with-one-disk-initially.53601/post-261614
 [3]: https://pve.proxmox.com/wiki/ZFS:_Tips_and_Tricks#Replacing_a_failed_disk_in_the_root_pool
 [4]: https://plantroon.com/changing-disk-identifiers-in-zpool/