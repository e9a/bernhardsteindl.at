---
# Title
title: "Das Ende von e9a (2ter Versuch)"
# Enter the author's name
author: Bernhard Steindl
# Don't change
#type: post
# Customize URI; default: post/filename
#url: /yubikey-fuer-linux-login-als-u2f-2-faktor-verwenden/
# Date
date: 2022-01-26T15:00:00+01:00
# Is this draft?
draft: false
# Show Table of Content?
showToc: false
# Available categories. Please comment out!
categories:
#- Admin-Tipps
- Allgemein
#- Arch Linux
#- Debian
#- Denkt's noch!
#- Grundlagen
#- Hardware
#- mariadb
#- News
#- nginx
#- Politik
#- Projekte
#- Proxmox
#- Rants
#- Satire
#- Synology
#- Tor
#- Ubiquiti Unifi
#- Windows
#- Wordpress
#- ZFS
---

Nun ist es vorbei, der e9ablog ist weg und man landet hier auf bernhardsteindl.at. Der Sinn der Sache? Christoph und ich teilen unsere
Blogs auf, derzeit werden alle alten Links von https://e9a.at (Pre-2019) und von https://blog.e9a.at (2019-2021) hier weitergeleitet. Leider hat der automatische
Konverter hier sämtliche Formatierungen ruiniert und daher sind die Texte ohne Quellenangabe und teilweise ohne Bilder.

## Wie geht es weiter?

Hier wird es mehr Analysen zur Tagespolitik und zu Sachthemen geben sowie einzelne IT-relevante Posts. Irgendwann wird auch Christoph seinen Blog einsatzbereit haben
und dann werden seine Links zu seinem Blog weitergeleitet anstatt wir hier.
