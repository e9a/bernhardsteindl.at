---
title: APC USV mit USB ausrüsten
author: Christoph Scheidl
type: post
date: 2018-10-26T14:43:33+00:00
url: /apc-usv-mit-usb-ausruesten/
categories:
  - Hardware

---
Heute ist mir auf einem Flohmarkt eine USV der Marke APC in die Hände gefallen. Leider war kein USB-Adapter von APC dabei, welcher auf der einen Seite einen USB-A Stecker, und auf der anderen Seite einen 10-poligen RJ-50 Stecker hat. Da das Kabel bei Amazon sage und schreibe 40€ kostet und ich keine RJ-50 Stecker zum Kabel selber machen zuhause habe, habe ich einfach nach der Anleitung von Reinhard Weiss ein USB-Kabel abgeschnitten und die 5 Adern auf den Socket der RJ-50 Dose gelötet. Tut was es soll, und 40€ sind gespart.

[Quelle](https://www.reinhardweiss.de/german/backups.htm?sa=X&ved=2ahUKEwjl8PT09KPeAhUBY1AKHTy7AkwQ9QEwAHoECAAQAw")