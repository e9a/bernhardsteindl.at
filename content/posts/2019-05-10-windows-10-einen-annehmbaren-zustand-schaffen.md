---
title: Windows 10 – einen annehmbaren Zustand schaffen
author: Christoph Scheidl
type: post
date: 2019-05-10T12:23:18+00:00
url: /windows-10-einen-annehmbaren-zustand-schaffen/
categories:
  - Windows

---
UPDATE: Der YouTuber &#8222;Chris Titus Tech&#8220;, generell sehr zu empfehlen, wenn es um Windows und Linux Tutorials geht und der schon die ursprünglichen Punkte in diesem Artikel inspiriert hat, hat ein Video hochgeladen, wie sich die Windows-Installation noch mehr bereinigen lässt. Ihr findet es [hier][1], das Video dauert fast eine dreiviertel Stunde, es werden einige interessante Punkte genannt. UPDATE OFF

Microsoft ist schon seit langem ein mühsamer Haufen, um dem ich gerne einen Bogen mache. Nichtsdestotrotz gibt es leider manchmal Situationen, in denen einem Windows nicht erspart bleibt. Hier eine kurze Zusammenfassung, was es bei einer Neuinstallation zu beachten gibt, damit das System in zumindest einem akzeptablen Zustand ist, ohne vollständig überwacht zu werden.

### Versionsauswahl

Nachdem es Microsoft im Moment einfach nicht auf die Reihe bringt, funktionsfähige Feature-Updates zu machen, ist es ratsam, auf die Windows 10 Version 18.03 zu setzen. Bis jetzt wurde es nachher nur mehr schlimmer. Um den nachfolgenden Updates so gut wie möglich aus dem Weg zu gehen, sind folgende Aktionen anzuwenden (deutsche Übersetzung ist nur sinngemäß, nicht wörtilich):

  1. Windows Updates öffnen
  2. Advanced Options / Erweiterte Optionen
  3. Choose when updates are installed / Wann sollen die Updates installiert werden: Hier auf den Eintrag &#8222;Semi-Annual Channel / Halbjahreskanal&#8220; OHNE Targeted/Gezielt Optionen in Klammern
  4. Feature Updates deferred for this many days / Funktionsupdates für so viele Tage verzögern: auf 365 stellen
  5. Quality Updates deferred for this many days / Qualitätupdates für so viele Tage verzögern: auf 30 stellen

Man beachte: Funktionsupdates sind nicht NOTWENDIG! Sicherheitsupdates hingegen schon.

### Windows 10 aktivieren

Ein Tipp den viele vielleicht gar nicht wissen: Windows 10 lässt sich auch mit Windows 7 und Windows 8 Lizenzkeys aktivieren. Das funktioniert zwar nicht immer, aber es ist definitv einen Versuch wert. Um den Lizenzkey einzutragen oder zu ändern, eignet sich für mich am besten der Weg über die Command Line. Folgender Befehl wendet schnell und einfach den Lizenz-Key an:

<pre class="wp-block-code"><code>slmgr /ipk AAAAA-AAAAA-AAAAA-AAAAA-AAAAA</code></pre>

### Windows schnell, leicht und funktionsfähig machen

Nun zu einigen Optimierungen, damit das ganze auch benützbar wird:

  1. [Hier][2] könnt ihr euch ein Skirpt herunterladen, dass Windows 10 bereinigt. Nun öffnet ihr die Powershell als Administrator. 
  2. Befehl eingeben: Set-ExecutionPolicy unrestricted
  3. Nun aus dem Github Ordner Windows10Debloater.ps1 ausführen (Run Once, bestätigen)
  4. Noch nicht rebooten
  5. Settings -> Privacy Settings: Alle auf AUS
  6. Speech auf AUS
  7. Einfach alle Menüs durchgehen und auf AUS stellen wo es geht
  8. Background Apps ausschalten
  9. App diagnostics auf AUS
 10. Power Options: Auf High Performance für Standrechner, bei Laptops je nach Bedarf
 11. Explorer, Rechtsklick auf C-Platte, Option &#8222;Allow Files &#8230;&#8220; unter General ganz unten rausnehmen, auch Subfolders.
 12. FÜR EXPERTEN: Windows Firewall deaktivieren: Run -> wf.msc: Inbound Roules: New Rule, Custom, Next, All Programs, any protocols, any IP, Domain, Private, Public.
 13. NUR FÜR EXPERTEN: Windows Updates -> Advanced Options -> Delivery Optimization: &#8222;Allow downloads from other PCs&#8220; ausschalten, weiters:  
    Run -> services.msc -> Windows Update: Stoppen und Startup Type auf Stop setzen, oder besser: &#8222;Windows Updates Blocker&#8220; von Sordum herunterladen und installieren. ACHTUNG: Diese Optionen sind nur zu empfehlen, wenn ihr euch danach aktiv umhört, ob es irgendwelche schwerwiegende Sicherheitslücken gibt und ihr wisst, was ihr tut.

Es ist außerdem noch eine gute Idee, mit [ShutUp10 von O&O][3] über das System zu gehen, um wirklich die Spionage und Telemetrie abzudrehen.

###### Links Und Credit

<p style="font-size:12px">
  Die Tipps in diesem Beitrag gehen hauptsächlich auf <a href="https://www.youtube.com/user/homergfunk">ChrisTitusTech auf YouTube</a> zurück. Hier die Video-Quellen:<br /><a href="https://youtu.be/N3MH9wizGfw">https://youtu.be/N3MH9wizGfw</a><br /><a href="https://youtu.be/nVy4GAtkh7Q">https://youtu.be/nVy4GAtkh7Q</a><br /><a href="https://youtu.be/ARY3hgJrXbk">https://youtu.be/ARY3hgJrXbk</a><br /><a href="https://www.christitus.com/2018/09/09/debloat-windows-10/">https://www.christitus.com/2018/09/09/debloat-windows-10/</a><br /><a href="https://youtu.be/PYOsevW3KdA">https://youtu.be/PYOsevW3KdA</a>
</p>

 [1]: https://youtu.be/PYOsevW3KdA
 [2]: https://github.com/Sycnex/Windows10Debloater
 [3]: https://www.oo-software.com/en/shutup10