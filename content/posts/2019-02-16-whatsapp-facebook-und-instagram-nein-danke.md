---
title: WhatsApp, Facebook und Instagram? Nein, danke!
author: Bernhard Steindl
type: post
date: 2019-02-16T17:54:00+00:00
url: /whatsapp-facebook-und-instagram-nein-danke/
categories:
  - "Denkt's noch!"

---
Immer wieder werde ich belästigt, mir WhatsApp herunterzuladen. Meistens erntet man Unverständnis, warum man eine &#8222;kostenlose&#8220; App nicht installieren will.

Nun, dazu gibt es unzählige Gründe, hier nur ein **kleiner** Auszug:

## 1. Facebook ist eine Datenkrake

Wir beginnen mit WhatsApp. Es setzt voraus, dass man alle Kontakte zu den Servern hochlädt. Das ist eindeutig illegal, da ich nicht gefragt wurde, ob meine Nummer hochgeladen wurde. Facebook besitzt somit das größte Telefonbuch der Welt. Ob [Otto Wanz][1] auch dieses zerreißen könnte?

WhatsApp wurde im Jahr 2014 von Facebook gekauft. [Folgendes wurde seitens der Geschäftsführung versprochen][2]:

<blockquote class="wp-block-quote">
  <p>
    Here’s what will change for you, our users: nothing. <br /> WhatsApp will remain autonomous and operate independently. You can continue to enjoy the service for a nominal fee. You can continue to use WhatsApp no matter where in the world you are, or what smartphone you’re using. And you can still count on absolutely no ads interrupting your communication. There would have been no partnership between our two companies if we had to compromise on the core principles that will always define our company, our vision and our product.
  </p>
</blockquote>

Man versucht es sich hierbei an der [Salamitaktik][3], [wie ein Beitrag aus 2016 zeigt][4]:

<blockquote class="wp-block-quote">
  <p>
    But by coordinating more with Facebook, we&#8217;ll be able to do things like track basic metrics about how often people use our services and better fight spam on WhatsApp. And by connecting your phone number with Facebook&#8217;s systems, Facebook can offer better friend suggestions and show you more relevant ads if you have an account with them. For example, you might see an ad from a company you already work with, rather than one from someone you&#8217;ve never heard of. You can learn more, including how to control the use of your data, <a href="https://faq.whatsapp.com/general/28030012">here</a>.
  </p>
  
  <cite>Aha, also wir teilen jetzt Daten mit Facebook, damit wir Spam bekämpfen. Ich bin der Meinung dass Facebook die Verbreitung von Spam und Falschnachrichten erst wirklich möglich gemacht hat. Aber ja, solangs ja &#8222;nur&#8220; die Telefonnummern und Statistiken sind &#8230;</cite>
</blockquote>

Nun, wir wissen das WhatsApp Ende-zu-Ende-Verschlüsselung nutzt, initial implementiert von Moxie Marlinspike. Wir können ihm soweit Vertrauen entgegenbringen, was die Verschlüsselung des Inhalts angeht. Doch gerade die Metadaten, also das **Wann**, **Wo**, mit **Wem** und **Warum** sind furchtbar interessant.

Ein kleines Beispiel:

Emma ist beim Frauenarzt. Sie erfährt, dass sie schwanger ist. Durch den Zugriff von der installierten Facebook-App auf Kontakte, Telefonprotokolle, SMS, Kalender und möglicherweise sogar Standort, weiß die Datenkrake natürlich darüber Bescheid, dass sie beim Arzt ist. Nun schreibt Emma mittels WhatsApp ihren Mann an, telefoniert mit ihrer Mutter und ruft ihren Chef an. Hier braucht der Algorithmus nicht wirklich intelligent sein, um zu Wissen was los ist. Nun kann Facebook perfekt zugeschnittene Werbung für den Kauf von Babyartikel oder je nach Einstellung (die Facebook natürlich weiß), Abtreibungskliniken oder Betreuungsangebote vorschlagen.

Das ideale Geschäftsmodell. Die Kombination der Daten macht&#8217;s aus. Facebook besitzt auch Instagram und betreibt damit sogar zwei soziale Netzwerke, um möglichst viele Daten zu sammeln.

Mark Zuckerberg ließ sich die Übernahme von WhatsApp weit über **20 Milliarden US-$** kosten. Das sind für jeden der damaligen User (etwa 1 Milliarde) in etwa 20$.

Schnitt für Schnitt von der Wurst, [pünktlich zum DSGVO-Eintritt wurde das Teilen nun offiziell.][5]

Und schlussendlich wurde die Grenze zwischen den Apps praktisch komplett aufgelöst, die Daten schwimmen nun alle im Facebook-Datenmeer.

Wem das noch nicht Grund genug ist &#8230;

## 2. Facebook ist eine Mafia

[Ein aktueller Beitrag][6], der [Cambridge Analytica-Skandal][7] oder [viele weitere Artikel][8] zeigen: **Facebook scheißt si nix!**  
[Gegen jeden Vorstoß der Behörden][9] gegen die unerlaubte und rechtswidrige Sammeltaktik, wird Berufung eingelegt und somit um Jahre verzögert.

Viele App-Entwickler integrieren die Facebook-APIs, um sich mit Facebook bei den Apps anmelden zu können.

**[JEDER VERNÜNFTIGE ENTWICKLER SOLLTE DIESEN SCHEI% SOFORT ENTFERNEN][10]**

Ja, das meine ich ernst. Jedes Bit wird gesammelt und auf Ewigkeiten gespeichert. Ich nutze kein einziges Produkt von Facebook, also dürften sie nichts von mir besitzen und dürfte auch nichts sammeln. [Es ist ihnen trotzdem egal.][11]

Möglicherweise hat Facebook damit sogar die Wahl von Donald Trump und den immensen Rechtsruck in Europa überhaupt erst möglich gemacht.

## Maßnahmen. Wirklich  


Denkt mal vernünftig nach. Wirklich. Ist das okay? Ist es das Wert? Nein, nicht mal ansatzweise! Nun, was gilt es zu tun?

<div class="wp-block-image">
  <figure class="alignleft is-resized"><img loading="lazy" src="https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fmobilsicher.de%2Fapp%2Fuploads%2F2016%2F09%2FScreenshot_4_WhatsApp_loeschen-450x800.png&f=1" alt="Nase voll? WhatsApp-Konto löschen (Android) - mobilsicher.de" width="231" height="411" /><figcaption>Es ist ganz einfach.</figcaption></figure>
</div>

Es gibt keinen anderen Weg, als diese Kommunikationskanäle zu zerstören. Dazu müsst ihr eure Accounts löschen. Vielleicht schaffen wir es, dass die Installation von Facebook-Apps keine Pflicht für einen Smartphone-User mehr ist

## Propaganda

Bitte sucht euch Alternativen. Um nur einige zu nennen:

  * Signal: Einfach, sicher, Open-Source und gratis. In 5 Minuten installiert und sogar Oma kapiert&#8217;s.
  * Threema: Setzt als eine der wenigen Apps nicht auf die Telefonnummer als Identifier. Dafür nicht Open-Source und auch nicht gratis.
  * XMPP: Dezentral, offen, dafür durch die Standards endlos kompliziert.
  * Matrix: Dezentral und offen, erinnert an Slack. Ideal als Firmen-Messenger verwendbar.

Für die Familie ist Signal ideal. Schnell installiert und als Ersatz-SMS-App eingerichtet, ist es auch für Wenignutzer unauffällig. 

Vergesst nicht, diese Apps gehören weg. Jeder der Angst hat, hier von einem sozialen Kreis augeschlossen zu werden, kann ich berichten das das nicht der Fall ist. Du kannst gerne diesen Artikel als &#8222;Grund&#8220; für den Ausstieg verlinken 🙂

## FAZIT

Leit&#8217;ln, denkts noch! Nichts in der Welt ist gratis, **denn da bist du das Produkt**. Ausnahme: [Signal, das von Spenden lebt][12], übrigens auch von den [WhatsApp-Gründern, die mittlerweile begriffen haben, was sie angerichtet haben.][13]

 [1]: https://www.youtube.com/watch?v=qyK7-Cq6_7s
 [2]: https://blog.whatsapp.com/499/Facebook
 [3]: https://de.wikipedia.org/wiki/Salamitaktik
 [4]: https://blog.whatsapp.com/10000627/Looking-ahead-for-WhatsApp
 [5]: https://www.golem.de/news/trotz-dsgvo-whatsapp-teilt-nun-massenhaft-nutzerdaten-mit-facebook-1805-134528.html
 [6]: https://www.golem.de/news/security-facebooks-sicherheitsdienst-soll-nutzern-hinterherschnueffeln-1902-139420.html
 [7]: https://de.wikipedia.org/wiki/Cambridge_Analytica
 [8]: https://derstandard.at/suche/?query=Facebook
 [9]: https://www.golem.de/news/like-kartellamt-schraenkt-datensammelei-von-facebook-ein-1902-139243.html
 [10]: https://www.kuketz-blog.de/datenschutz-facebooks-unsichtbare-datensammelwut/
 [11]: https://netzpolitik.org/2018/ob-nutzer-oder-nicht-facebook-legt-schattenprofile-ueber-alle-an/
 [12]: https://freedom.press/crowdfunding/signal/
 [13]: https://derstandard.at/2000081053483/Die-zwei-Whatsapp-Gruender-hassten-es-bei-Facebook-zu-arbeiten