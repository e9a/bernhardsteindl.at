---
title: 'Wolfgang Sobotka: Unter Prölls Gnaden'
author: Bernhard Steindl
type: post
date: -001-11-30T00:00:00+00:00
draft: true
url: /?p=448
classic-editor-remember:
  - block-editor
categories:
  - Allgemein

---
Ich schreibe selten direkt über Personen, da es meistens rechtliche Probleme gibt, wenn man die Wahrheit verbreitet. Aber diese Die Abmahnungen der Kanzleien also bitte an bernhard-abmahnungen@e9a.at senden, da werden sie dann professionell in den Papierkorb geschmissen.

<div class="wp-block-image">
  <figure class="aligncenter"><img src="https://www.parlament.gv.at/WWER/PAD_88386/7256051_500.jpg" /></figure>
</div>

Dieses &#8222;freundliche&#8220; Gesicht hat eine interessante Vergangenheit und eine lange Tätigkeit in der Niederösterreichischen Landesregierung hinter sich und es gibt einige Tatsachen, worüber sich die Medien in Niederösterreich nicht rübertrauen.

Erzählen wir mehr über die Geschichte dieses Mannes.

## Ein guter Lehrer

Im Jahr 1982, Sobotka war zu dieser Zeit AHS-Lehrer, schickte er im Rahmen seines Geschichteunterricht Schüler zu den Haushalten in Waidhofen an der Ybbs, um nach dem Wahlverhalten der anstehenden Gemeinderatswahl zu fragen. Sobotka selbst war zu dieser Zeit bereits Gemeinderat der ÖVP und mitten im Wahlkampf. Diese Aktion wurde vom Unterrichtsministerium gestoppt und Gottvater Erwin Pröll holte ihn einige Jahre später in die Niederösterreichische Landesregierung.