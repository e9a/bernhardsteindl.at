---
# Title
title: "FPÖ: Hundezone und Fairness für alle!"
# Enter the author's name
author: Bernhard Steindl
# Customize URI; default: post/filename
#url: /yubikey-fuer-linux-login-als-u2f-2-faktor-verwenden/
# Date
date: 2022-08-26T18:00:00+01:00
# Is this draft?
draft: false
# Show Table of Content?
showToc: true
# Available categories. Please comment out!
categories:
#- Admin-Tipps
#- Allgemein
#- Arch Linux
#- Debian
#- Denkt's noch!
#- Grundlagen
#- Hardware
#- mariadb
#- News
#- nginx
- Politik
#- Projekte
#- Proxmox
#- Rants
#- Satire
#- Synology
#- Tor
#- Ubiquiti Unifi
#- Windows
#- Wordpress
#- ZFS

---

# FPÖ

*Dies ist ein Artikel der Serie [Pappkameraden in Krems](/posts/2022-08-25-die-kremser-pappkameraden/). Sämtliche Inhalte genügen keinen journalistischen Ansprüchen wie Ausgewogenheit oder DoubleChecks, falls Sie dennoch falsche Informationen finden oder Anmerkungen zu den Artikeln haben, würde ich mich über eine [Rückmeldung](/impressum) sehr freuen.*

Ich finde es interessant, dass hier mit der „einzigen“ Wahl geworben wird. Sowas hatten wir auf [österreichischem Gebiet schon einmal](https://de.wikipedia.org/wiki/Anschluss_%C3%96sterreichs#Volksabstimmung). Aber solange Udo Landbauer (der natürlich [nie](https://www.derstandard.at/story/2000101395023/udo-landbauer-folgt-instagram-account-antizion) etwas mit [NS-Gedankengut](https://www.derstandard.at/story/2000072861626/nazi-lieder-bei-burschenschaft-von-fpoe-kandidat-landbauer) zu [tun gehabt](https://www.derstandard.at/story/2000087740696/udo-landbauer-im-niederoesterreichischen-landtag-angelobt) hat) der Landesparteiobmann ist, hat man als Schwarzafrikaner nichts zu befürchten. Oder…?

{{< figure src="images/fpoe1.jpeg" caption="Wenn es keine Wahlen mehr gibt, hat man auch nur eine Wahl. Akzeptieren oder ins Arbeitslager." >}}
{{< figure src="images/fpoe2.jpeg" caption="Es könnte auch eine Anspielung darauf sein, dass sie die Wahlen abschaffen. Dann war es tatsächlich meine einzige Wahl (in Krems)…" >}}
{{< figure src="images/fpoe3.jpeg" caption="Team könnte auch für **T**oll **E**in **A**rier **M**achts stehen" >}}
{{< figure src="images/fpoe4.jpeg" caption="Wenn ich Bürgermeister bin, würde ich den Garten von Frau Rosenkranz zur Hundezone erklären. – Werde ich nun gewählt?" >}}

Natürlich würde ich die FPÖ nicht als neonazistisch einstufen, sondern als rechtsextreme und in der Corona-Politik als wissenschaftsfeindliche Partei, die hin und wieder ein [paar neonazistische Einzelfälle (78 alleine seit 2017!) zu verzeichnen hat.](https://kontrast.at/die-gesammelten-einzelfaelle-der-fpoe/).

Kleiner Auszug:
- EF 77: Der Ternitzer FPÖ-Stadtrat Erwin Scherz postet ein Bild von sich in einem T-Shirt mit dem Reichsadler. Gegenüber der NÖN sagt er, er habe es billig gekauft und „gar nicht so darauf geschaut, was drauf ist!“ Konsequenzen gibt es keine. Das Posting hat Scherz mittlerweile gelöscht. [Quelle](https://www.noen.at/neunkirchen/ternitz-reichsadler-shirt-von-fpoe-stadtrat-schlaegt-hohe-wellen-ternitz-erwin-scherz-reichsadler-facebook-posting-wirbel-fpoe-214558102)
- EF 68: Die FPÖ Niederösterreich setzt ihren Tullner Bezirksparteiobmann Andreas Bors auf die freiheitliche Kandidatenliste für die Nationalratswahl. Im November 2014 wurde ein Foto veröffentlicht, das Bors und zwei andere Männer beim Hitlergruß zeigt. Deshalb war Bors im November 2017 gezwungen, auf ein Bundesratsmandat zu verzichten. [Quelle](https://kurier.at/politik/inland/hitlergruss-foto-fpoe-mann-andreas-bors-tritt-bundesrats-mandat-nicht-an/298.318.660)
- EF 66: Ein Datenleak bei Mailaccounts und Login-Daten von Politikern in Österreich untermauert die Vermutung, dass FPÖ-Klubobmann Johann Gudenus auf Facebook das Passwort "heilheil" verwendet haben soll. Gudenus hatte das zuvor dementiert. "heilheil" erinnert an eine nationalsozialistische Grußformel. [Quelle](https://kontrast.at/gudenus-heiheil-passwort/)
- EF 54: Am 2.2.2019 nennt die Burschenschaft Teutonia Österreich auf Facebook "Ostmark" - ein NS-Begriff für Österreich als Teil von Nazi-Deutschland. Mitglied der Burschenschaft Teutonia sind der FPÖ-Wehrsprecher im Parlament Reinhard Eugen Bösch und ÖBB-Aufsichtsratsvorsitzender Arnold Schiefer. [Quelle](https://derstandard.at/2000097494987/Burschenschaft-Teutonia-schreibt-Ostmark-wenn-sie-Oesterreich-meint)
- [...](https://kontrast.at/die-gesammelten-einzelfaelle-der-fpoe/)

Abgesehen davon konnte ich kein Wahlprogramm finden, daher zitiere ich von [ORF.at](https://noe.orf.at/stories/3167917/):

> Die FPÖ konnte 2017 stark zulegen. Susanne Rosenkranz tritt zum zweiten Mal als Spitzenkandidatin an. Sie sieht ihre Partei als jene, die Fehlentwicklungen aufzeigt. „Parken, Verkehr – wobei man das größer sehen muss, von Radfahren bis Bus, von Fußgängerverkehr bis normaler Autoverkehr. Dann darf man meines Erachtens die einzelnen Gebiete in Krems wie Lerchenfeld und Mitterau nicht vergessen, die eher stiefkindlich behandelt werden.“ Ihr Wahlziel: „Stärker zu werden, weil das ein genereller Wunsch sein muss.“

## Wahlprogramm

Ich bekam schlussendlich doch noch ein *besseres* Wahlprogramm;
leider hatte ich es schon aus Wut aufgrund der ungebetenen „Müllzusendung™️“ ungelesen zerrissen, sodass ich den Flyer erst wieder mühsam zusammenbauen musste:

{{< figure src="images/fpoe_wahlprogramm.jpg" caption="Manchmal packt einen halt die Wut…" >}}

Ich verstehe nicht, warum ich immer diesen parteipolitischen Müll in meinem Postkastl finde, es steht ausdrücklich "Keine unaddressierte Werbung, Parteiwerbung, Gratiszeitungen und Kirchenzeitungen" drauf. Nur die katholische Kirche und die sogenannte „Volks“-Partei halten sich daran, der Rest glaubt wohl, das mein Briefkasten eine Papiertonne ist.

### Bildungspolitik

Im Flyer konnte ich leider dazu nichts finden, aber ich nehme an dass sie sowas wie den Ausbau der Kindergärten etc. unterstützen werden. Klar ist: Kinder haben im Gegensatz zu Pensionisten keine Lobby, daher sind sie prinzipiell eher benachteiligt. 

Apropos: Raum für Senioren zu schaffen klingt nach Altersheim bauen?

### Gemeinderatspolitik

Bemerkenswert ist jedenfalls der Vorfall vor einem Jahr, in der FPÖ-Gemeinderat Werner Friedl nach Ansicht des Mauthausen-Komitees womöglich gegen das Verbotsgesetz verstoßen hat. Für Interessierte: [Zeitungsartikel in der NÖN](https://www.noen.at/krems/krems-freiheitlicher-nach-rede-unter-druck-krems-werner-friedl-reinhard-resch-print-268137683), [Gemeinderatssitzung zum Selbst-Nachhören](https://www.youtube.com/watch?v=rtW1DSS0aZg), ab 45:30 spricht Friedl, vorher spricht Mahrer.

Übrigens wollte auch die ÖVP nicht die [Maria-Grengg](https://de.wikipedia.org/wiki/Maria_Grengg)-Gasse in Margarete-Schörl-Gasse unbenennen!

### Verkehrs & Umweltpolitik

Nachdem "Faires Parken" gefordert wird, nehme ich an dass ein Parkplatz im Stadtgebiet nun soviel wie der Grund + Errichtungskosten + Instandhaltungskosten ausmacht. Ein Quadratmeter in Krems kostet durchschnittlich [6,67€](https://www.wunderwohnen.at/Mietpreise/Nieder%C3%B6sterreich/Krems%20an%20der%20Donau) pro Quadratmeter, wir vernachlässigen nun Errichtungs- und Instandhaltungskosten.

#### Faires Parken nach realer Berechnung

Einfache Rechnung: 2,5m Breite * 3,5m Länge * 6,67 €/qm ergibt ungefähr 59€ pro Monat, also muss ein Jahresticket für Bewohner mindestens €700,- pro Jahr kosten. Ich kann und will nicht verstehen, warum Platz, der mit Blechkisten verstellt ist, die mindestens 90% des Tages nur rumstehen, 10x so billig ist (Parkticket kostet 70€/Jahr), wie wirklicher Wohnraum, der zumindest 50% der Zeit genutzt wird. Natürlich ist das nur vereinfacht berechnet, wenn man (theoretisch) aus jedem Parkplatz am Straßenrand stattdessen Wohnungen bauen würde, würden auch die Mietpreise nachgeben.

Leider sieht das die FPÖ anders und will die Gebühren ausserhalb der blauen Zone abschaffen. Dass die FPÖ schon seit [„Volkswagen“-Zeiten](https://de.wikipedia.org/wiki/KdF-Wagen) nur an Automobilförderung interessiert ist, wundert hoffentlich an dieser Stelle niemanden mehr.

Also klare Wahlempfehlung an jene, die auch 2022 noch glauben, dass das Auto in der Stadt das primäre Fortbewegungsmittel sein sollte.

#### Einschub Ende

Die sonstige Umweltpolitik sieht eigene „Hundezonen“ vor – gut, von mir aus, so lange sie nicht in meinen Garten kacken. Ich vergesse der FPÖ übrigens nicht, dass sie bis vor wenigen Jahren die Klimakrise komplett verleugnet hat und nun angesichts der unbequemen Realität langsam in die Gänge kommen, der uneingeschränkte Autoverkehr wird aber nach wie vor gefördert.

„Ortsbild beibehalten“ & „Dachböden ausbauen“ halte ich persönlich eigentlich für einen Widerspruch. Man sehe sich nur die Dachbodenausbauten in der Wiener Innenstadt an, das verändert das Ortsbild massiv (aber nicht unbedingt ins Negative!). Ob der Ausbau gefördert werden soll, ist die andere Frage; Ich persönlich glaube, dass das der falsche Weg ist und eine Leerstandsabgabe zweckdienlicher wäre.

## Weitere Politik

Die FPÖ plakatiert überall Fairness, dabei sollte doch jedem Wähler klar sein, was Fairness für die FPÖ bedeutet: Systematischer Fremdenhass mit einer Prise Deutschnationalismus hat mindestens 10% Stimmenanteil bei jeder Wahl. Der Zauber besteht nun darin, weitere Prozente zu sammeln wie etwa Protestwähler oder Nichtwähler. Doch man merke sich eines: Die FPÖ war noch immer bereit, billig mit der ÖVP in das Koalitionsbett zu hüpfen. Ob dann der Protestwähler nicht erst recht wieder ÖVP-Politik unterstützt, wäre auf jeden Fall einen Gedanken wert.

Den GEDESAG-Ärger kann ich aufgrund eigener Erfahrungen verstehen, ist mir aber zu Allgemein. Wollen sie Transparenz über die Vergaben schaffen? Dann muss man endlich eine Berichtspflicht einführen, doch die SPÖ und ÖVP verhindern hier gemeinsam Transparenz. Vermutlich kommt die Abneigung aufgrund der sogenannten *Wohnungsschacher* bei denen gutgelegene Wohnungen angeblich irgendwelchen Freunderl zugeschanzt werden. Mir selbst wurde einmal eine gutgelegene Wohnung mit fadenscheinigen Ausreden verwehrt, alleine deshalb befürworte ich größtmögliche Transparenz.

Das sie das Hallenbad nicht bauen wollen, verzeihe ich ihnen sowieso nicht, das kann man nur wollen, wenn man selbst ein Pool im Garten hat. Das Krems dann womöglich ohne Bad dasteht, wird stillschweigend akzeptiert, die Vorgängerpartei [hatte ja immer schon eine Vorliebe für Veranstaltungshallen](https://de.wikipedia.org/wiki/B%C3%BCrgerbr%C3%A4ukeller), welche es in Krems in der Form nicht (mehr) existieren.

# Conclusio

Wer nach [Knittelfeld](https://de.wikipedia.org/wiki/Freiheitliche_Partei_%C3%96sterreichs#Spaltung) und [Ibiza](https://de.wikipedia.org/wiki/Ibiza-Aff%C3%A4re) von rechtsextremen Populisten noch immer nicht genug hat, soll von mir aus die Rosenkranz wählen und *für Gerechtigkeit sorgen*. Das Gerechtigkeit bei der FPÖ primär um die Versorgung der eigenen Spezis mit Posten und Hinabtreten auf die Ärmsten passiert, sei hier aber noch angemerkt. Von mir aus könnt ihr aus Protest daheim bleiben, aber [jede Stimme zählt!](https://de.wikipedia.org/wiki/Landtagswahl_in_K%C3%A4rnten_2013).

*Lesen Sie weitere Artikel der Serie [Pappkameraden in Krems](/posts/2022-08-25-die-kremser-pappkameraden/)!*