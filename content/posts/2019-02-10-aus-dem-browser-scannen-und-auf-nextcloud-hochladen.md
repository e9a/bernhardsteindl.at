---
title: Aus dem Browser scannen und auf Nextcloud hochladen
author: Christoph Scheidl
type: post
date: 2019-02-10T10:22:10+00:00
url: /aus-dem-browser-scannen-und-auf-nextcloud-hochladen/
categories:
  - Debian

---
 

Hier habe ich ein Bash-Script zusammengestellt, dass es ermöglicht, von einem Scanner mittels dem sane-backend-Befehl scanimage ein Dokument zu scannen und dieses dann als PDF auf eine Nextcloud-Instanz mittels WebDAV hochzuladen.

Notwendige Pakete:

`apt install libtiff-tools imagemagick sane-utils` 

Leider ist in imagemagick ein Bug, der es erfordert, dass in der `/etc/ImageMagic-6/policy.xml` die Zeile

<pre class="wp-block-code"><code>&lt;policy domain="coder" rights="none" pattern="PDF" /></code></pre>

durch

<pre class="wp-block-code"><code>&lt;policy domain="coder" rights="read | write" pattern="PDF" /></code></pre>

zu ersetzen. Danach sollte das Script einwandfrei funktionieren. Außerdem könnt ihr in dieser File die memory-limits usw erhöhen oder auskommentieren, damit ihr kein Problem bei größeren Scans bekommt.

###### Links und Credit

<p style="font-size:12px">
  <a href="https://www.masoopy.com/scan-to-pdf-with-command-line-under-gnu-linux/">https://www.masoopy.com/scan-to-pdf-with-command-line-under-gnu-linux</a><br /><a href="https://doc.owncloud.org/server/9.1/user_manual/files/access_webdav.html#accessing-files-using-curl">https://doc.owncloud.org/server/9.1/user_manual/files/access_webdav.html#accessing-files-using-curl</a><br /><a href="https://askubuntu.com/questions/1181762/imagemagickconvert-im6-q16-no-images-defined">https://askubuntu.com/questions/1181762/imagemagickconvert-im6-q16-no-images-defined</a><br /><a href="https://aus.computers.linux.narkive.com/zhy9Wn1O/need-leading-zeroes-in-page-number-from-scanimage-in-this-script">https://aus.computers.linux.narkive.com/zhy9Wn1O/need-leading-zeroes-in-page-number-from-scanimage-in-this-script</a><br /><a href="https://softwareengineering.stackexchange.com/questions/127639/why-do-some-sorting-methods-sort-by-1-10-2-3">https://softwareengineering.stackexchange.com/questions/127639/why-do-some-sorting-methods-sort-by-1-10-2-3</a><br /><a href="https://github.com/ImageMagick/ImageMagick/issues/396">https://github.com/ImageMagick/ImageMagick/issues/396</a><br />
</p>