---
title: Timeout in Nextcloud richtigsetzen (Online-Updater 504 Gateway Timeout)
author: Christoph Scheidl
type: post
date: 2021-04-17T21:15:07+00:00
url: /timeout-in-nextcloud-richtigsetzen-online-updater-504-gateway-timeout/
classic-editor-remember:
  - block-editor
categories:
  - Admin-Tipps

---
Ich hatte schon länger das Problem, dass bei mir der Nextcloud-Online-Updater nicht einwandfrei durchgelaufen ist. Jetzt habe ich das ganze mal gelöst:

In der nginx-config von der Nextcloud direkt habe ich beim php-location-block folgendes eingetragen:

    fastcgi_read_timeout 300;

<s>Damit dauert es 5 Minuten, bevor PHP ein Timeout schießt (da geht sich das Update dann meistens aus).</s> Durch die angegebene Direktive dauert es 5 Minuten, bevor nginx aufhört, auf eine Antwort vom FastCGI-Socket zu warten. (Danke an einen aufmerksamen Leser für die Info!)

Da ich das ganze noch hinter einem weitern nginx-Reverse-Proxy betreibe (nur eine IP, weil Business-Vertrag nicht leistbar), habe ich in dessen Config noch folgendes eingetragen:

    proxy_read_timeout 300s;

Danach ist das Update problemlos durchgelaufen.

###### Links und Credit

[https://distinctplace.com/2017/04/22/nginx-upstream-timed-out/](https://distinctplace.com/2017/04/22/nginx-upstream-timed-out/)
