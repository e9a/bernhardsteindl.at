---
title: Invoice-Ninja selber hosten mit nginx-Reverse-Proxy
author: Christoph Scheidl
type: post
date: 2019-08-26T11:49:52+00:00
url: /invoice-ninja-selber-hosten-mit-nginx-reverse-proxy/
categories:
  - Allgemein

---
[InvoiceNinja][1] ist ein Tool, um als Unternehmen seine Kunden und Rechnungen zu verwalten. Hier kurz die verwendete Konfiguration, um InvoiceNinja hinter einem nginx-Reverse-Proxy selbst zu hosten.

Im ersten Schritt habe ich die Anleitung von InvoiceNinja verwendet, um die Applikation unter Debian aufzusetzen.

InvoiceNinja .env Config:

<pre class="wp-block-code"><code>APP_ENV=production
APP_DEBUG=false
APP_LOCALE=en
APP_URL=https://your.domain.net/
APP_KEY=censored
APP_CIPHER=AES-256-CBC
REQUIRE_HTTPS=true
DB_TYPE=mysql
DB_HOST=localhost
DB_DATABASE=ninja
DB_USERNAME=ninja
DB_PASSWORD=censored
MAIL_DRIVER=smtp
MAIL_PORT=465
MAIL_ENCRYPTION=ssl
MAIL_HOST=test.mail.at
MAIL_USERNAME=test@test.at
MAIL_FROM_NAME='Your Invoice Ninja'
MAIL_FROM_ADDRESS=test@test.at
MAIL_PASSWORD=censored
TRUSTED_PROXIES=YOUR.REVERSE.PROXY.IP
</code></pre>

Die nginx-Config von der InvoiceNinja-Instanz:

<pre class="wp-block-code"><code>server {
    listen 443 ssl;
    listen &#91;::]:443 ssl;
    server_name your.domain.net;

    ssl_certificate /var/www/html/ninja/cert.pem;
    ssl_certificate_key /var/www/html/ninja/key.pem;

    root /var/www/html/ninja/public;

    index index.php;

    charset utf-8;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    access_log  /var/log/nginx/your_ninja_domain.com.access.log;
    error_log   /var/log/nginx/your_ninja_domain.com.error.log;

    sendfile off;

    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php/php7.3-fpm.sock;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_intercept_errors off;
        fastcgi_buffer_size 16k;
        fastcgi_buffers 4 16k;
    }

    location ~ /\.ht {
        deny all;
    }
}</code></pre>

Hier habe ich mit diesem Befehl ein selbst-signiertes Cert generiert:

<pre class="wp-block-code"><code>openssl req -x509 -newkey rsa:4096 -keyout /var/www/html/ninja/key.pem -out /var/www/html/ninja/cert.pem -nodes -days 36500</code></pre>

Und hier die Config des Reverse-nginx-Proxy:

<pre class="wp-block-code"><code>server {
	listen 80;
	listen &#91;::]:80;
	server_name your.domain.net;

	return 301 https://$server_name$request_uri;
	
}
server {
	listen 443;
        listen &#91;::]:443;
	server_name your.domain.net;
	
	ssl_certificate /etc/letsencrypt/live/your.domain.net/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/your.domain.net/privkey.pem;

	location / {
		proxy_pass https://YOUR.INVOICE.NINJA.IP/;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
	        proxy_set_header X-Forwarded-Proto $scheme;
		proxy_set_header Host $http_host;
	}
}</code></pre>

Damit ist die ganze Sache auch schon erledigt und kann über das Web-Interface aufgesetzt werden.

Die Google-Maps API kann [so][2] deaktiviert werden:  
In die `.env` kommt der Befehl `GOOGLE_MAPS_ENABLED=false`

 [1]: https://www.invoiceninja.org/
 [2]: https://invoice-ninja.readthedocs.io/en/latest/configure.html#google-map