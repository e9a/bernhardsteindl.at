---
title: Windows 1809 schießt GRUB + Fix
author: Christoph Scheidl
type: post
date: 2019-01-04T22:27:28+00:00
url: /windows-1809-schiesst-grub-fix/
categories:
  - Windows

---
UPDATE vom 27.12.2019: Es gibt auch zahlreiche andere Methoden, wie dieses Problem gelöst werden kann. Eine ziemlich einfache zeigt ChrisTitusTech [in diesem Video.][1]

Heute hat mein Windows-Dual-Boot-System das Windows-Upgrade auf Windows 1809 durchgeführt. Und als das Update durchgelaufen war, kam beim Starten nur mehr die GRUB-Rescue-Shell. Der Frust über Microsoft war natürlich sehr groß, dennoch musst eine Lösung gefunden werden. Nachdem die ersten Versuche in der Rescue-Shell fehlgeschlagen sind und auch der GRUB-Bootloader von meinem Clonezilla-Stick, den ich gerade dabei hatte, nicht helfen konnte, machte ich mir auf einen anderem Rechner einen Arch-Stick und startete von diesem. 

Folgende Schritte sind nun notwendig:

Arch-Installation mounten:  


<pre class="wp-block-code"><code>mount /dev/nvme0n1p6 /mnt</code></pre>

Das Arch-Rootverzeichnis auf das der Installation setzten:

<pre class="wp-block-code"><code>arch-chroot /mnt /bin/bash</code></pre>

Nun muss die EFI-Partition gemountet werden:  


<pre class="wp-block-code"><code>mkdir esp
mount /dev/nvme0n1p2 esp</code></pre>

Jetzt kann der GRUB-Bootloader installiert werden:

<pre class="wp-block-code"><code>grub-install --target=x86_64-efi --efi-directory=esp --bootloader-id=arch_grub</code></pre>

Nach einem Neustart hat nun alles wieder hingehauen. Eventuell fehlt der Windows-Boot-Manager, hier kann der letzte Blogeintrag (unten verlinkt) weiterhelfen.

###### Links und Credit

<p style="font-size:12px">
  <a href="https://wiki.archlinux.org/index.php/GRUB#Installation_2">https://wiki.archlinux.org/index.php/GRUB#Installation_2</a><br /><a href="https://blog.e9a.at/arch-und-windows-dual-boot/">https://blog.e9a.at/arch-und-windows-dual-boot/</a> <br /><a href="https://youtu.be/r7meKJsjqfY">https://youtu.be/r7meKJsjqfY</a><br /><a href="https://www.youtube.com/watch?v=gEB6JEYZekE">https://www.youtube.com/watch?v=gEB6JEYZekE</a><br />Dank geht raus an 0xEAB, der mir bei der Wieder-Herstellung meines Bootloaders sehr behilflich war, mir mit seinen Kenntnissen über GRUB, UEFI und dem Bootvorgang im Allgemeinen tatkräftig unter die Arme gegriffen und mir damit den Tag gerettet hat.
</p>

<p style="font-size:12px">
  <br />
</p>

 [1]: https://www.youtube.com/watch?v=gEB6JEYZekE