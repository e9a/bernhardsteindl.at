---
title: Komplette MariaDB-Instanz übertragen
author: Christoph Scheidl
type: post
date: 2019-04-01T08:31:25+00:00
url: /komplette-mariadb-instanz-uebertragen/
categories:
  - mariadb

---
Am Wochenende haben wir unser Webhosting, auf dem einige Webseiten liegen, auf einen neuen Server übertragen. Damit das ganze schnell und schmerzlos geht, sind 3 Dinge wichtig:

  * Das Konfigurationsverzeichnis des Webservers (zB /etc/nginx/conf.d)
  * Das Verzeichnis, wo die Websites liegen (zB /var/www)
  * Die Datenbank (zB bei WordPress-Instanzen)

Um alle Datenbanken der maria-db auf einmal sichern zu können, eignet sich der Befehl &#8222;mysqldump&#8220; mit einigen Parametern:

<pre class="wp-block-code"><code>mysqldump -h localhost -u root -p --events --routines --triggers --all-databases --flush-privileges > MySQLData.sql</code></pre>

Die Verzeichnisse und die .sql Datei lassen sich dann zum Beispiel per SFTP auf den neuen Server übertragen.

Um den Dump wieder in die neue Datenbank zu laden (wichtig ist zu beachten, das die MariaDB-Instanzen auf der gleichen Version sind), nimmt man folgenden Befehl:

<pre class="wp-block-code"><code>MySQLData.sql > mysql -u root -p</code></pre>

Wichtig zu beachten: Nimmt man beim ersten Befehl nicht die Option &#8222;&#8211;flush-privileges&#8220;, muss man nach dem Einspielen von Benutzerdaten oder ähnlichem in die Instanz einsteigen und ein &#8222;FLUSH PRIVILEGES&#8220; ausführen, ansonsten funktionieren zB Benutzer nicht. Die Option macht nichts anderes, als in die .sql Datei diesen Befehl hinten anzuführen.

###### Links und Credit

<p style="font-size:12px">
  <a href="https://dba.stackexchange.com/a/35849">https://dba.stackexchange.com/a/35849</a>
</p>