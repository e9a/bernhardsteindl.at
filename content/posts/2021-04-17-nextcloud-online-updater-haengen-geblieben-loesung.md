---
title: Nextcloud-Online-Updater hängen geblieben – Lösung
author: Christoph Scheidl
type: post
date: 2021-04-17T21:09:14+00:00
url: /nextcloud-online-updater-haengen-geblieben-loesung/
classic-editor-remember:
  - block-editor
categories:
  - Admin-Tipps

---
Heute ist mir der Nextcloud Online-Updater hängengeblieben. Dies macht sich dadurch bemerkbar, dass zB vom Webinterface behauptet wird, dass `Step 4 currently in Progress` ist, und er eigentlich herunterladen sollte, aber man keine Netzwerkaktivität feststellen kann.

Das Problem lässt sich ziemlich einfach beheben: Einfach im `data`-Ordner der Nextcloud den Ordner `updater-IRGENDWAS` suchen und diesen löschen. Danach kann man im Webinterface wieder von vorne anfangen

###### Links und Credit

(https://help.nextcloud.com/t/stuck-on-update-process-step-5-is-currently-in-process-please-reload-this-page-later/58371/)