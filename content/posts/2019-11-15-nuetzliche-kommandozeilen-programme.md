---
title: Nützliche Kommandozeilen-Programme
author: Christoph Scheidl
type: post
date: 2019-11-15T05:28:22+00:00
url: /nuetzliche-kommandozeilen-programme/
categories:
  - Admin-Tipps

---
Als Server-Administrator ist man naturgemäß viel in der Kommandozeile unterwegs. Der gute Martin Leyrer vom C3W hat in den letzten Jahren mehrere Vorträge gehalten, in der er nützliche Tools zeigt, die auch mit der UNIX-Philosophie übereinstimmen. Hier sind die Links:

[Easterhegg 2018 &#8211; Moderne Kommandozeilen Werkzeuge][1]

[GPN18 &#8211; Moderne Kommandozeilen Werkzeuge][2]

[GPN19 &#8211; Moderne Kommandozeilen-Werkzeuge (endgültig\_letzte\_Version)][3]



Wer eine eigene Vorstellung zu tmux, dem Terminal-Multiplexer, sehen will, dem empfehle ich dieses Video:

[GPN18 &#8211; tmux &#8211; Warum ein schwarzes Fenster am Bildschirm reicht][4]

###### Links und Credit

<p style="font-size:12px">
  <a href="https://github.com/leyrer">https://github.com/leyrer</a>
</p>

 [1]: https://youtu.be/ApDkyByUHXw
 [2]: https://youtu.be/OlQMXu4yCGk
 [3]: https://youtu.be/8d8-PpcLc24
 [4]: https://youtu.be/JhKXOYepX2E