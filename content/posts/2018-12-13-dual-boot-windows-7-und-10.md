---
title: Dual-Boot Windows 7 und 10
author: Christoph Scheidl
type: post
date: 2018-12-13T12:34:18+00:00
url: /dual-boot-windows-7-und-10/
categories:
  - Windows

---
Neulich habe ich auf eine alte Windows 7 Installtion auf eine 2. Partition Windows 10 installiert. In Windows 10 war auf einmal alles schrecklich langsam. Lösung des Problems: die System-reserviert von Windows 7 löschen, und alles läuft wie geschmiert

###### Links und Credit

<p style="font-size:12px">
  <a href="http://www.tomshardware.co.uk/answers/id-2487165/computer-running-slow-installing-hard-drive-previously.html">http://www.tomshardware.co.uk/answers/id-2487165/computer-running-slow-installing-hard-drive-previously.html</a><br />
</p>