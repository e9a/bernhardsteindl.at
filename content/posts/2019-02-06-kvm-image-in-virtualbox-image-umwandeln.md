---
title: KVM-Image in Virtualbox Image umwandeln
author: Christoph Scheidl
type: post
date: 2019-02-06T11:35:45+00:00
url: /kvm-image-in-virtualbox-image-umwandeln/
categories:
  - Debian
  - Proxmox

---
Hier nur ein kurzer Tipp, wie man .img-Files von KVM in eine Virtualbox-Disk (.vdi) umwandlen kann. Kann manchmal sehr nützlich sein:

<pre class="wp-block-code"><code>qemu-img convert -O vdi original.img original.vdi</code></pre>

Wichtig ist hier nur zu beachten, dass der Befehl mit &#8222;VBoxManage&#8220;, der im Internet kusiert, bei mir keinen Erfolg gebracht hat.