---
title: USG Adoption – Tipps und Tricks
author: Christoph Scheidl
type: post
date: 2018-11-15T19:39:05+00:00
url: /usg-adoption-tipps-und-tricks/
categories:
  - Ubiquiti Unifi

---
Heute habe ich ein USG von Ubiquiti in mein bestehendes Heim-Netzwerk integriert. Diese Seite war dabei äußerst hilfreich:  
<https://help.ubnt.com/hc/en-us/articles/236281367-UniFi-USG-Adoption-How-to-Adopt-a-USG>  
Allerdings gibt es noch einige Tipps, die es zu beachten gibt:

  * Das USG muss eine bestehende WAN-Verbindung haben  
    
  * Das USG muss im selben Netzwerk wie der Controller sein
  * Ihr legt das ganze Netzwerk lahm, also am besten machen, wenn keiner daheim ist

###### Links und Credit

[https://help.ubnt.com/hc/en-us/articles/236281367-UniFi-USG-Adoption-How-to-Adopt-a-USG](https://help.ubnt.com/hc/en-us/articles/236281367-UniFi-USG-Adoption-How-to-Adopt-a-USG)