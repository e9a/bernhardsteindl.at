---
title: Proxmox VM von Anfang an sehen
author: Christoph Scheidl
type: post
date: 2019-12-06T09:42:08+00:00
url: /proxmox-vm-von-anfang-an-sehen/
categories:
  - Admin-Tipps
  - Proxmox

---
Wenn man bei Proxmox eine VM startet, dauert es meist, bis man mittels NoVNC ein Bild bekommt. Um dennoch eventuelle Startoptionen (BIOS etc.) drücken zu können, gibt es einen Trick:

Unter den VM-Optionen gibt es die Option &#8222;`Freece CPU at startup`&#8222;, diese setzt man auf &#8222;`Yes`&#8222;, danach fährt man die VM herunter.

Nun geht man in Shell von Proxmox (am besten in einem anderen Fenster mittels SSH) und gibt folgendes ein:

<pre class="wp-block-code"><code>qm monitor VM_ID</code></pre>

Nun kommt man in die Monitor-Option von QEMU. Anschließend kann man die VM ohne Stress starten, NoVNC öffnen (Hier steht nun &#8222;VM has not initialized graphics&#8220; oder ähnliches), und wenn man sich alles schön angeordnet hat, kann man im Terminal ein &#8222;`c`&#8220; schreiben, und die VM beginnt zu booten.

Sobald man alles erledigt hat, sollte man nicht vergessen, die Option wieder raus-zunehmen, ansonsten wundert man sich das nächste Mal, wenn keine Maschine mehr hochkommt.

###### Links und Credit

<p style="font-size:12px">
  <a href="https://forum.proxmox.com/threads/how-to-enter-safe-mode-in-windows-2003-f8-does-not-work.20031/">https://forum.proxmox.com/threads/how-to-enter-safe-mode-in-windows-2003-f8-does-not-work.20031/</a><br /><a href="https://docs.ansible.com/ansible/latest/modules/proxmox_kvm_module.html">https://docs.ansible.com/ansible/latest/modules/proxmox_kvm_module.html</a><br /><a href="https://en.wikibooks.org/wiki/QEMU/Monitor">https://en.wikibooks.org/wiki/QEMU/Monitor</a><br />
</p>