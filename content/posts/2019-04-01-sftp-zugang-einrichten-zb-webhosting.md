---
title: SFTP-Zugang einrichten (zB Webhosting)
author: Christoph Scheidl
type: post
date: 2019-04-01T10:56:36+00:00
url: /sftp-zugang-einrichten-zb-webhosting/
categories:
  - Debian

---
Will man als Webhoster seinen Kunden einen SFTP-Zugang bieten, lässt sich dieser wie folgt einrichten:

Zuerst wird der User angelegt:

<pre class="wp-block-code"><code>adduser kunde1</code></pre>

Damit der User keinen Zugriff auf die Shell hat, nehmen wir ihm mit folgendem Kommando die Rechte dafür ab:

<pre class="wp-block-code"><code>usermod -s /bin/false kunde1</code></pre>

Nun beschränken wir den Kunden im Dateisystem. Wir nehmen den Ordner &#8222;/var/www/kunde1&#8220; als Beispiel

<pre class="wp-block-code"><code>chown root:root /var/www/kunde1
chmod 755 /var/www/kunde1</code></pre>

Jetzt hätte eigentlich der Webserver keinen Zugriff mehr auf den Ordner, was nicht Sinn der Sache ist&#8230; Ich empfehle, hier nochmals einen Unterordner zu machen, in dem dann der Inhalt der Website liegt. Im Ordner /var/www/kunde1 hat der Kunde selbst keine Rechte.

<pre class="wp-block-code"><code>mkdir /var/www/kunde1/website
chown kunde1:www-data /var/www/kunde1/website</code></pre>

Nun müssen wir in der sshd_config noch ein paar Dinge festlegen:

<pre class="wp-block-code"><code>#Subsystem sftp /usr/lib/openssh/sftp-server
Subsystem sftp internal-sftp
	
Match User kunde1
  # Force the connection to use SFTP and chroot to the required directory.
  ForceCommand internal-sftp
  ChrootDirectory /var/www/kunde1
  # Disable tunneling, authentication agent, TCP and X11 forwarding.
  PermitTunnel no
  AllowAgentForwarding no
  AllowTcpForwarding no
  X11Forwarding no</code></pre>

Nach einem Neustart des sshd-Dienstes, sollte das Login funktionieren. Falls nicht, sind bei den Links unten ein paar common-problems angeführt. 

###### Links Und Credit

<p style="font-size:12px">
  <a href="https://goneuland.de/wordpress/debian-8-jessie-sftp-server-einrichten/">https://goneuland.de/wordpress/debian-8-jessie-sftp-server-einrichten/</a><br /><a href="https://www.server-world.info/en/note?os=Debian_9&p=ssh&f=5">https://www.server-world.info/en/note?os=Debian_9&p=ssh&f=5</a><br /><a href="https://www.tecmint.com/usermod-command-examples/">https://www.tecmint.com/usermod-command-examples/</a><br /><a href="https://serverfault.com/questions/672519/sftp-error-couldnt-read-packet-connection-reset-by-peer">https://serverfault.com/questions/672519/sftp-error-couldnt-read-packet-connection-reset-by-peer</a><br /><a href="https://serverfault.com/questions/584986/bad-ownership-or-modes-for-chroot-directory-component">https://serverfault.com/questions/584986/bad-ownership-or-modes-for-chroot-directory-component</a><br /><a href="https://serverfault.com/questions/611388/need-help-limit-access-to-sftp-user-to-other-users-home-subfolder">https://serverfault.com/questions/611388/need-help-limit-access-to-sftp-user-to-other-users-home-subfolder</a>
</p>