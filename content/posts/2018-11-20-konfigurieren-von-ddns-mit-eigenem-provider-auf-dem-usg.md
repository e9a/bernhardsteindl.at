---
title: Konfigurieren von DDNS mit eigenem Provider auf dem USG
author: Christoph Scheidl
type: post
date: 2018-11-20T18:36:49+00:00
url: /konfigurieren-von-ddns-mit-eigenem-provider-auf-dem-usg/
categories:
  - Ubiquiti Unifi

---
Um sich mit dem USG auch bei dynamischen IP-Adressen verbinden zu können, brauchen wir einen Dynamischen DNS-Provider. Wir verwenden nsupdate.info. Dieser Dienst arbeitet mit dem ddclient. Dieser Client ist auf dem USG vorinstalliert. Lediglich die Konfiugration, die einem der Provider als ganzes ausspuckt, muss man reinkopieren

```vi /etc/ddclient.conf```

Anschließend kopiert man die Konfig hinein, startet den ddclient einmal manuel mittels dem Befehl  


```ddclient```

Und wenn alles klappt, schreibt er einem &#8222;Good&#8220; hin und die IP, die jetzt hochgepusht wurde. Das ist keine Hexerei.

###### Links und Credit

[https://www.nsupdate.info](https://www.nsupdate.info)