---
title: Epson EcoTank-Drucker unter Arch-Linux einrichten
author: Christoph Scheidl
type: post
date: 2018-10-26T15:46:02+00:00
url: /epson-ecotank-et-3600-unter-arch-linux-einrichten/
classic-editor-remember:
  - block-editor
categories:
  - Arch Linux
  - Hardware

---
### Drucken

**Update**: Das AUR-Package [epson-inkjet-printer-escpr2][1] installiert die Treiber für die meisten EcoTank-Drucker mit den jeweiligen Cups-Filtern automatisch.

Mit [yay][2] kann das Paket mit yay -S [epson-inkjet-printer-escpr2][1] installiert werden.

Dies installiert die Treiber u.a. für folgende Geräte:

```
EP-50V Series,EP-879A Series,EP-880A Series,EP-881A Series, ET-3700 Series, ET-3750 Series, ET-4750 Series, ET-8700 Series, ET-M1140 Series, ET-M1180 Series, ET-M2140 Series, ET-M3140 Series, ET-M3170 Series, ET-M3180 Series, EW-M630T Series, EW-M670FT Series, L6160 Series, L6170 Series, L6190 Series, M1180 Series, M2140 Series, M3180 Series, PM-520 Series, PX-M380F, PX-M381FL, PX-M5080F Series, PX-M5081F Series, PX-M680F Series, PX-M7070FX, PX-M7110F, PX-M7110FP, PX-M780F Series, PX-M781F Series, PX-M880FX, PX-M884F, PX-M886FL, PX-S380, PX-S381L, PX-S5010 Series, PX-S5080 Series, PX-S7070X, PX-S7110, PX-S7110P, PX-S880X, PX-S884, WF-2860 Series, WF-3720 Series, WF-3730 Series, WF-4720 Series, WF-4730 Series, WF-4740 Series, WF-7210 Series, WF-7710 Series, WF-7720 Series, WF-C5210 Series, WF-C5290 Series, WF-C5290BA, WF-C529R Series, WF-C529RB, WF-C5710 Series, WF-C5790 Series, WF-C5790BA, WF-C579R Series, WF-C579RB, WF-C8190 Series, WF-C8190B, WF-C8610 Series, WF-C8690 Series, WF-C8690B, WF-C869R Series, WF-M5298 Series, WF-M5299 Series, WF-M5799 Series, XP-15000 Series, XP-5100 Series, XP-6000 Series, XP-6100 Series, XP-8500 Series
```

In den GNOME-Einstellungen konnte ich den Drucker nicht finden, in den Cups-Einstellungen unter http://localhost:631 aber sehr wohl. Hier muss dann nurmehr der richtige Treiber unter dem Hersteller Epson herausgesucht werden.

Auf der nächsten Seite können wir dann noch die Einstellungen für den Drucker setzen:

  * print quality: high  
    
  * Duplex Tumble: 2-side long edge
  * Media-Size: A4 (freie Wahl möglich)

Mit &#8222;Set default options&#8220; bestätigen, und das ganze läuft. Jetzt einfach einen Probe-Druck machen, um zu sehen, ob alles passt.

### Scannen

Für das Scannen benötigt man SANE. Wenn das installiert ist, einfach ein Frontend dafür wählen (zB Simple Scan von Gnome). Unter Preferences sollte der Drucker auftauchen, wann nicht, kann über den Befehl

```scanimage -L```

im Terminal eruiert werden, ob der Scanner auch auftaucht. Bei mir hat das auf Anhieb ohne Treiber funktioniert.

###### Links und Credit

[https://maggblogg.wordpress.com/2015/02/01/epson-workforce-wf-2650dwf-uber-netzwerk-unter-linux-einrichten/](https://maggblogg.wordpress.com/2015/02/01/epson-workforce-wf-2650dwf-uber-netzwerk-unter-linux-einrichten/)

[https://bugzilla.redhat.com/show_bug.cgi?id=1409335#c6](https://bugzilla.redhat.com/show_bug.cgi?id=1409335#c6)

[http://download.ebz.epson.net/dsc/search/01/search/searchModule](>http://download.ebz.epson.net/dsc/search/01/search/searchModule)

 [1]: https://aur.archlinux.org/packages/epson-inkjet-printer-escpr2/
 [2]: https://www.debugpoint.com/2021/01/install-yay-arch/