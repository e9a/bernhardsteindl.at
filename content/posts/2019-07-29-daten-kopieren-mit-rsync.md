---
title: Daten kopieren mit rsync
author: Christoph Scheidl
type: post
date: 2019-07-29T12:26:17+00:00
url: /daten-kopieren-mit-rsync/
categories:
  - Debian

---
Sollen Daten mit Debian kopiert werden, vielleicht auf andere Hosts mit SSH, eignet sich das Programm &#8222;rsync&#8220; dafür sehr gut.

#### Erklärung

<pre class="wp-block-code"><code>rsync -av /var/www/html/ root@1.2.3.4:/var/www/</code></pre>

Dieser Befehl macht folgendes:

  * -v : Verbose-Ausgabe, das heißt, das mehr Information ausgegeben wird
  * -a : Archive-Mode, ist ein Shortcut-Attribut für -rlptgoD, wobei das für folgendes steht:
      * -r : rekursiv, es werden auch Unterverzeichnisse mit-kopiert
      * -l : links, symbolische Links werden erhalten und als solche kopiert
      * -p : permission, die Berechtigungen der Dateien bleiben erhalten
      * -t : times, die Zeit der letzten Bearbeitung usw. bleibt erhalten
      * -g : groups, Gruppen der Dateien bleiben erhalten
      * -o : owner, Besitzer der Dateien bleiben erhalten
      * -D : Ist eine Kurzform für:
          * &#8211;devices : Die Devices-Datei bleibt erhalten
          * &#8211;specials : Die Specials-Datei bleibt erhalten

Was hier noch fehlt für eine 1:1 Kopie (wer das braucht wird es wissen):

  * -H : hard-links, Hard-Links werden beibehalten
  * -A : acls, ACLs werden beibehalten
  * -X : xattrs, XAttrs werden beibehalten

Alle weiteren Parameter erhält man mit:

<pre class="wp-block-code"><code>man rsync

ODER

rsync -h</code></pre>

Mittels dem _root@1.2.3.4_ wird auf einen Remote-Host via SSH kopiert, so kann auch von einem Remote-Host kopiert werden. In beiden Fällen muss eine Authetifizierung von SSH erfolgen, entweder über einen SSH-Key oder über ein Passwort, was abgefragt wird.