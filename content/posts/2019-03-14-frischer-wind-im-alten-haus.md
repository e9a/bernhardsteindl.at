---
title: Frischer Wind im alten Haus
author: Bernhard Steindl
type: post
date: 2019-03-14T14:27:27+00:00
url: /frischer-wind-im-alten-haus/
categories:
  - Politik

---
Ich hätte nicht gedacht, dass mein Artikel über das Bundesschülerheim Krems soviel Aufmerksamkeit erregt. Schlussendlich hat mich der Artikel nach langer Zeit wieder in die Direktion in die Kasernstraße 6 gebracht, gemeinsam mit der Schülervertretung, dem Direktor und den Vertreter(-innen) des Erzieherpersonals.

Ich habe den Artikel montagabends aus folgenden Gründen offline genommen:

  * Falsche Tatsachen: Einige Passagen waren sehr überspitzt formuliert und entsprachen nicht ganz der Wahrheit. Der Artikel bedurfte einer dringenden Bearbeitung.
  * Einladung: Ich bekam die Einladung für ein Gespräch mit dem Direktor
  * Pragmatische Gründe: Falls der Boulevard diese Meldung aufgreift, würde die Thematik unsachlich und vielleicht in einem Shitstorm enden

Ob und wann der Artikel in einer editierten Fassung wieder online gehen wird, kann ich nicht sagen.

## 0. Wind of Change

Ich habe zumindest erwirkt, dass nun viele Sitzungen und Gespräche laufen, um einigen Kritikpunkten den Wind aus den Segeln zu nehmen. Mit dem Elternverein des BSH laufen Gespräche über die Ausgehzeiten, die Schülervertretung bekommt vielleicht ein fixes monatliches Treffen und viele weitere Bausteine befinden sich in Bewegung.

Auch bei den Mädchen gibt es Korrekturen: Ich sprach mit vielen Mädchen, die vor Jahren im Internat waren. Mittlerweile hat sich der &#8222;Erzieherfuhrpark&#8220; verjüngt und die Thematik entschärft. 

Doch leider gibt es immer noch Personal, die noch nicht ganz im 21. Jahrhundert angekommen sind und komische Regelungen aufrechterhalten. Hier ermutige ich die Schüler(-innen), sich beim Direktor zu melden. Dieser zeigt selbst Unverständnis für ewiggestrige Ideen.

## 1. Sensoren

Ganz abhalten lässt sich der Direktor von der Idee mit den Türsensoren nicht, auch wenn meine Kritik (und die von vielen anderen) wohl deutlicher denn je angekommen ist.

Hier wird mit Kanonen auf Spatzen geschossen, niemand bekommt durch diese Maßnahme mehr Sicherheit, sie ist auch mit den jeweiligen Vorfällen unverhältnismäßig.

Ich wünsche mir, dass diese Idee fallengelassen wird, auch die (Mädchen-)Erzieher sind nicht erfreut gewesen, als sie von der Umsetzung erfuhren.

## 2. Vorfälle

Ich wurde dafür kritisiert, die Maden- und Wurmgeschichten wieder veröffentlicht zu haben. Trotzdem passierten diese Vorfälle während meiner Internatszeit und sind Teil meiner Erfahrungen. Zu diesen gehören meiner Meinung nach auch alle Ereignisse dazu.

Weiters gab es Kritik wegen dem Wasservorfall, der an der Infrastruktur der Kremser Wasserwerke lag und das BSH absolut nichts dafür kann. Ich habe von dieser Tatsache bis heute nichts gewusst.

## 3. Ausgehzeiten

Hier zeigt die Direktion Bereitschaft, in Absprache mit den zuständigen Erziehern Ausnahmen der bestehenden Hausordnung zuzulassen. Die bisherigen Grenzen wurden gemeinsam mit dem Elternverein festgelegt und werden jetzt nach meiner Kritik evaluiert.

Man versucht auch das Modell mit den volljährigen Schülern zu überdenken, es ist eine (übliche) Schlüssellösung im Gespräch.

## 4. Essen

Die Küche soll sich seit dem Einstellen von neuen Köchen verbessert haben. Man wird hier nie eine Lösung für alle finden, am Liebsten hätten wir doch alle jeden Tag Schnitzel.

## 5. Kommunikation und Schülervertretung

Mein Eindruck während des Gesprächs:

  * Direktor weiß zu wenig von Problemen des Schülers
  * Schüler weiß nichts von gewissen Vorfällen und aktuellen Begebenheiten im BSH
  * Es fehlt an Feedback zwischen Küche und Schüler

Kurz gesagt: ein &#8222;einfaches&#8220; Kommunikationsproblem

Die geplanten monatlichen Jour-fixes (so nennt man fixe Gespräche, ein Meeting hat man wegen eines spezifischen Problemes), würden hier sicher viel bringen. So kann man sich gegenseitig am aktuellen Stand halten und vielleicht doch etwas bewegen.

## 6. Fazit

Ich bin nur zu diesem Gespräch gegangen, da ich mich von meiner konstruktiven Seite zeigen wollte, zur Kritik gehört auch immer ein Vorschlag, es anders zu machen. Ich hoffe, dass sich die Leitung dies als Vorbild nimmt, manche Themen wirklich umzusetzen.