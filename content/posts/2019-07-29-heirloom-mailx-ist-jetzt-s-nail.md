---
title: heirloom-mailx ist jetzt s-nail
author: Christoph Scheidl
type: post
date: 2019-07-29T12:06:25+00:00
url: /heirloom-mailx-ist-jetzt-s-nail/
categories:
  - Debian
  - News

---
Mit Debian 10 gibt es das Paket heirloom-mailx nicht mehr, dieses heißt nun s-nail. Die Syntax bleibt jedoch komplett gleich. 

Wie man das Programm verwenden kann, haben wir in [diesem][1] Blog-Eintrag bereits gezeigt.

 [1]: https://blog.e9a.at/mails-vom-server-senden/