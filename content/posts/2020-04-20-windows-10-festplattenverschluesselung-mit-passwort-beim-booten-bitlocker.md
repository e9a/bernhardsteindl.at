---
title: Windows 10 – Festplattenverschlüsselung mit Passwort beim Booten – Bitlocker
author: Christoph Scheidl
type: post
date: 2020-04-20T19:25:21+00:00
url: /windows-10-festplattenverschluesselung-mit-passwort-beim-booten-bitlocker/
categories:
  - Admin-Tipps
  - Windows

---
Ich nutze auf meinem Thinkpad (leider) Windows im Dual-Boot mit Linux. Es gibt ja doch Software, die auf Linux nicht laufen und in einer VM einfach nicht möchten/zu wenig Leistung bekommen. Da ich, wenn ich den Käse schon nutzen muss, zumindest alle Board-Mittel zur Steigerung der Sicherheit ausnutzen möchte, habe ich natürlich die Festplattenverschlüsselung mit BitLocker aktiviert. Nachdem ich aber der Windows-Anmeldung als Schutz vor Einbrechern schon gar nicht vertraue, möchte ich ein eigenes Passwort zur Entschlüsselung der Windows-Partition eingeben, bevor Windows überhaupt bootet. Das ist aber nicht so einfach, wie man es vielleicht erwarten sollte.

Zuerst öffnet man das Bitlocker-Menü, entweder über das gute alte &#8222;Control Panel&#8220;, sprich Systemsteuerung, oder einfach über die Windows-Suche. Nun öffnen wir über Win+R die Gruppenrichtlinenverwaltung (und das funktioniert leider nur unter Windows 10 Pro, unter den anderen Editionen muss man das nehme ich an, ohne es getestet zu haben, über die Registry etwas setzen), und hakeln uns hier zu `Computer Configuration -> Administrative Templates -> Windows Components -> Bitlocker Drive Encryption -> Operating System Drives` durch.

Hier müssen wir nun 3 Objekte bearbeiten:

  * `Require additional authentication at startup`: Je nachdem ob man Passwort oder USB-Stick möchte, hier wählen
  * `Enable use of BitLocker authentication requiring preboot keyboard inputs on slates`: Damit man immer das Passwort eingeben muss, auch wenn keine Tastatur vorhanden ist. Ist bei Tablets problematisch, da das ganze keine Onscreen-Tastatur unterstützt.
  * `Allow enhanced PINs at startup`: Damit man zumindest auch alphanummerische Zeichen wählen kann, es ist schon ein Wahnsinn für sich, das das Passwort nicht länger als 20 Zeichen lang sein darf.

MAN BEACHTE! Microsoft ist nicht im Stande, einen Tastatur-Layout-Switcher bei der Abfrage einzubauen. SIE KÖNNEN DAS JA NOCH NICHT EINMAL ERWÄHNEN! Also, das einzige Layout, das verwendet wird, ist en_US&#8230; Wer Sonderzeichen im Passwort hat, die wo anders auf der Tastatur zu finden sind, muss hier umdenken (oder im Windows das Layout umstellen, wenn man das Passwort eingibt). Wer Umlaute im Passwort hat (wovon generell abzuraten ist), kann hier schön sch\***** gehen. Für euch siehts blöde aus.

Nach einem Neustart sehen wir, das es tut, was es soll. Auch wenn mein Vertrauen in den Bitlocker begrenzt ist, ein wenig besser als ganz ohne Verschlüsselung ist es auch.

###### Links und Credit

<p style="font-size:12px">
  <a href="https://www.queensu.ca/its/security/encryption-service/tutorials/windows/windows-81/startup-password">https://www.queensu.ca/its/security/encryption-service/tutorials/windows/windows-81/startup-password</a><br /><a href="https://community.spiceworks.com/topic/2020155-bitlocker-with-a-password-instead-of-pin">https://community.spiceworks.com/topic/2020155-bitlocker-with-a-password-instead-of-pin</a>
</p>