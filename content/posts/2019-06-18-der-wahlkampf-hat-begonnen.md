---
title: Der Wahlkampf hat begonnen
author: Bernhard Steindl
type: post
date: 2019-06-18T09:13:00+00:00
url: /der-wahlkampf-hat-begonnen/
categories:
  - Allgemein

---
Guten Abend, meine Damen und Herren,

in den letzten Wochen zeigte ich meine Abstinenz hier im Blog, hauptsächlich wegen dem Ende meiner schulischen Laufbahn und großen Projekten meines Arbeitgebers.

Ein paar Einzelheiten der letzten Wochen müssen aber sehrwohl noch kommentiert werden, alleine durch die Absurdität dessen.

## Neonazi im EU-Parlament

In jedem vernünftigen und halbwegs demokratischem Land hätte sich der selbsternannte Retter des Abendlandes nach diesem Skandal vor der Öffentlichkeit verstecken müssen. Nicht in Österreich. Nicht hier.

Am Wahlabend habe ich mich schon gefragt, was in diesem Land schiefläuft. Warum sich seit Jahren Politiker aus der Verantwortung entziehen können. Die Demenz der Bevölkerung ist gigantisch.

Zwei Beispiele:

  * Sebastian Kurz: Er präsentiert sich nach 5 Jahren in der Regierung als Newcomer. Die Strategie ging auf, er hat bis heute ausgezeichnete Umfragewerte und kann sich als &#8222;Macher&#8220; präsentieren, wobei die reale Reformgeschwindigkeit sehr zu wünschen übrig lässt.
  * FPÖ Kärnten: Das Bundesland, das vor Niederösterreich (soviel zu: &#8222;Die Roten machen nur Schulden&#8220;) die höchste Verschuldung aller Bundesländer hat, ist mit den Wahlen nach und nach wieder Blauer geworden. Haider rules!

## Die Übergangsregierung

Naja, jetzt ist der Shorty nimmer Bundeskanzler. Ich will mich über das Verhalten vom Ohrwaschlkaktus nicht mehr aufregen, weils sowieso sinnlos ist. Aber das musst mal machen: Zwei Mal die Koalition sprengen und behaupten, die anderen warens und die Bevölkerung ist auch noch so dumm und glaubt das.

## Das Dilemma der SPÖ

Mir tun&#8217;s ja persönlich a bissl leid, die SPÖler. Programm ist nicht schlecht, das Personal, seit der Bundestaxler weg ist auch nicht aber es fehlt hat ein Sebastian Kurz, welcher die sozialen Medien und die Partei gezwungenermaßen vereint. Politik ist das eine, verkaufen das andere. Es ist gar nicht so einfach, das Showbusiness so gut wie da Shorty zu beherrschen und mit leeren Slogans wie &#8222;Es ist Zeit&#8220; einen Wahlkampf zu gewinnen.

Aber gut, eigentlich is eh wuascht, Wien bleibt sowieso Rot und unter 20% fallens nicht.

## Was können wir uns erwarten?

Die einzig relevante Frage ist die, wie das Parlament nach der Wahl aussehen wird. Realistisch gesehen hat das Bubi sich mit der SPÖ und mit der ÖVP verspielt. Wenn der Wahlkampf weiterhin so gut läuft, wird er an die 40% bekommen und kann möglicherweise mit den NEOS koalieren, was uns absolut neoliberale Politik gibt. Not nice.

Wenn die Neos aber zu wenig Sitze im Nationalrat hat, wirds schwierig, denn die Politik der ÖVP widerspricht sich mit der der Grünen in den meisten Teilen.

Servus.