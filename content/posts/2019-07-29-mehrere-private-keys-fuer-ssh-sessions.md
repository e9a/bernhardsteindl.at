---
title: Mehrere Private-Keys für SSH-Sessions
author: Christoph Scheidl
type: post
date: 2019-07-29T12:04:01+00:00
url: /mehrere-private-keys-fuer-ssh-sessions/
categories:
  - Debian

---
Heute habe ich einen Anwendungsfall gehabt, wo es Sinn macht, den Private-Key zu übernehmen: Ich habe einen alten Nextcloud-Server in einen LXC migriert. Weil ich von diesen Debian-Instanzen ein Borg-Backup auf einen entfernten Standort mache, habe ich vom alten Server den Private-Key auf den LXC übernommen. Das funktioniert folgendermaßen:

<pre class="wp-block-code"><code>nano ~/.ssh/config

IdentityFile ~/.ssh/id_rsa  # Private-Key 1
IdentityFile ~/.ssh/id_rsa2 # Private-Key 2</code></pre>

SSH arbeitet diese Einträge von oben nach unten ab, sollte der erste Key nicht authentifizieren, so wird der nächste genommen, bis ans Ende der Liste.