---
title: 'Video-Empfehlung: ZFS-Reihe von Christian Zengel'
author: Christoph Scheidl
type: post
date: 2020-04-13T08:54:33+00:00
url: /video-empfehlung-zfs-reihe-von-christian-zengel/
categories:
  - Allgemein

---
Es ist ja bereits der ein oder andere Artikel über ZFS auf diesem Blog erschienen. Wer sich ein umfassendes Wissen über ZFS aufbauen möchte (was ich jedem sehr ans Herz legen kann), dem kann ich die Videoreihe &#8222;[ZFS Rocks!][1]&#8220; von Christian Zengel sehr empfehlen. Ein Blick lohnt sich sehr!

###### Links und Credit

<p style="font-size:12px">
  <a href="https://www.youtube.com/user/sysopstv">https://www.youtube.com/user/sysopstv</a><br /><a href="https://www.youtube.com/channel/UCC0WuyODhqPuROasZV878Yw">https://www.youtube.com/channel/UCC0WuyODhqPuROasZV878Yw</a>
</p>

 [1]: https://www.youtube.com/channel/UCC0WuyODhqPuROasZV878Yw