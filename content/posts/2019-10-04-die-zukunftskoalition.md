---
title: Die Zukunftskoalition
author: Bernhard Steindl
type: post
date: 2019-10-04T10:38:48+00:00
url: /die-zukunftskoalition/
categories:
  - Allgemein

---
Österreich hat sich wieder der Wahl gestellt und hat sich entschieden. Hört sich jetzt eher nach dem Eppinger (ÖVP-Propagandabeauftragter) an, ist aber tatsächlich so. Nun, was können wir aus der Wahl lernen?

Man kann in einem Video das ganze Land verkaufen, es hat relativ wenig Einfluss auf das Wahlergebnis. Doch wenn der Ex-Parteichef seine eigene Partei mehr oder weniger &#8222;bescheißt&#8220;, hat das tatsächlichen Einfluss. Da könnte einem Norbert Hofer fast leidtun, doch dann fällt einem wieder die NS-Nähe ein und sämtliches Mitleid verpufft durch den Schornstein.

Sämtliche FPÖ-Granden betonen auf das Ziel, nach dem Absacken auf 16 Prozent, in die Opposition zu wandern. Nun gibt es hier Skepsis, da ja das Ziel des &#8222;Ohrwaschlkaktuses&#8220; ja noch immer eine &#8222;Mitte-Rechts&#8220;-Regierung ist. Dass sich die ÖVP in den letzten 2 Jahren schon sehr an die FPÖ parteiprogrammmäßig rangekuschelt hat, lässt sich an der Übereinstimmung von 80% des Programms ablesen. Doch ist es das wert? 

Eine Schwarz-Blaue Koalition zerfetzt es wieder in spätestens 2 Jahren. Ich verwette meine Hand darauf, dass das so kommen würde. Ich schätze SK nicht als dumm ein, deshalb glaube ich nicht dass das kommen wird. 

Sehen wir uns die weiteren Varianten an. Schwarz-Rot ist wahrlich unwahrscheinlich, da das Spitzenpersonal beider Parteien sich mehr als zerstritten haben. So sehr ich den Roten ein Comeback wünsche, das kann nur abseits der Regierung geschehen. Ich glaube das PRW abseits des Gesundheitsministerium für die SPÖ unpassend ist. Hier wäre ein Grobmotoriker wie Michael Häupl passend, welcher als übergewichtiger &#8222;Soze&#8220; die Geschicke der Stadt 2 Jahrzehnte leitete. Das nenne ich einen richtigen SPÖler, welche Erfolge auch verkaufen kann. Einen Häupl konnte kein Skandal erschüttern, keinen Wahlflop oder unglückliche Sager wie die 22-Stunden-Woche vom Sessel schubsen.

Eigentlich auch nicht das Problem von uns, wenn hier eine Partei den leisen, aber sicheren Tod stirbt. Vielleicht gelingt ja ein Reanimationsversuch.

Nun wird von den Medien auch eine weitere Variante ins Spiel gebracht, Schwarz (Türkis)-Grün. Nun, aus sachlichen Gründen ist das wahrlich unwahrscheinlich, widersprechen sich ja die Parteiprogramme der jeweiligen Parteien zu 80%. Die Frage, die man sich stellen muss, ist, ob hier beide zueinander zugehen oder ob SK den leichteren Weg mit der FPÖ geht.

Auf subjektiver Ebene ist es auf jeden Fall erwünscht, das hier endlich etwas passiert. Statt wie Herbert Kickl es sagte, die &#8222;Ausreisezentrum-Tafeln&#8220; wieder montieren zu wollen, sollten eher die 140er-Tafeln auf der A1 abmontiert werden. Wir haben einiges zu tun.

Seit ich mich erinnern kann, wurde die Umweltpolitik in diesem Land gelobt. Die Wasserkraft, keine Atomkraft (ganz ehrlich, das hat mit der Klimadebatte nichts zu tun) und die Nationalparks. Das wir aber seit Jahren keinen weiteren Schritt unternommen haben und stattdessen nur Autobahnen gebaut haben, hat unsere Bilanz nicht besser gemacht. Wir haben unsere Emissionen nicht verringert, sie sind noch imme Stand 1990, wobei Deutschland schon 30% unter ihren damaligen Stand sind.

Ich hoffe, dass dieser Weg eingeschlagen wird, mit der Hoffnung, wirklich etwas zu verändern und nicht nur Pressekonferenzen zu veranstalten.

Servas!