---
title: Windows-Profil aus Domäne entfernen
author: Christoph Scheidl
type: post
date: 2020-01-10T09:09:45+00:00
url: /windows-profil-aus-domaene-entfernen/
categories:
  - Admin-Tipps
  - Windows

---
Immer wieder kommt man als Admin in die Situation, dass man Windows-PCs aus einer Domäne ausbinden möchte, zB, weil ein alter Windows-Server überflüssig geworden ist und die Active-Directory-Funktionen sowieso nie jemand genutzt hat. Um dennoch alle Einstellungen etc. des Domänenprofil zu behalten, kann man das Profil mit einem nützlichen Tool namens `ForensIT User Profile Wizard` auf ein Lokales kopieren. Im übrigen funktioniert das Tool auch in die andere Richtung, das benötige ich aber beinahe nie. Die Möglichkeit ist aber da. 

Die notwendigen Schritte zum Ausbinden lauten wie folgt:

  1. Habe IMMER ein Backup. Auch hier. Das ist keine Ausnahme. Also geh und erstelle eines, wenn du keines hast. Und auch wenn du eines hast, mache ein Aktuelles!
  2. Unter Windows den `lusrmgr.msc` öffnen (als Administrator öffnen)
  3. Unter Benutzer einen neuen (den zukünftigen lokalen Benutzer) erstellen.
  4. Den Benutzernamen merken!
  5. Nun können wird den [ForensIT User Profile Wizard][1] herunterladen. Nicht öffnen, sondern speichern.
  6. Entweder installiert man die .msi, oder man entpackt sie mit 7zip. Eine Installation ist nicht unbedingt notwendig. Zumal der Installer sowieso nur die `Forensit.exe` entpackt. Ein wenig zweckfrei, aber gut.
  7. Nun kann man das Tool ausführen. Erstmal weiter mit `Weiter`
  8. Im 2. Schritt wählt man das Profil aus, welches übertragen werden soll. In den meisten Fällen wohl eines von der Domäne, also zB DOMÄNE\User. Es funktioniert aber genauso umgekehrt, von lokal zur Domäne. Mit den 3 Haken kann man noch festlegen, ob der User danach deaktiviert oder gelöscht werden soll, oder ob Profile ohne Nutzerzuweisung angezeigt werden sollen. Danach auf `Weiter`
  9. Nun wählt man bei ersten Feld seinen Computernamen aus. Unten gibt nun den genauen Benutzernamen ein, den man vorher erstellt hat. Man kann den Nutzer noch als automatisch Einzuloggenden festlegen. Nun wieder auf `Weiter`
 10. Damit führt das Programm nun seine Arbeit aus. Wenn es fertig ist, gibt es einen Neustart.
 11. Nun kann man den PC unter `System` aus der Domäne ausbinden, ihn umbenennen und auch zu einer Workgroup hinzufügen. Das Active-Directory wird damit nicht mehr benötigt.

###### Links und Credit

<p style="font-size:12px">
  <a href="https://www.andysblog.de/windows-computer-aus-der-domaene-entfernen-und-dabei-das-benutzerprofil-behalten">https://www.andysblog.de/windows-computer-aus-der-domaene-entfernen-und-dabei-das-benutzerprofil-behalten</a><br /><a href="https://www.forensit.com/de/Index.html">https://www.forensit.com/de/Index.html</a><br /><a href="https://youtu.be/-l2PRwgq66Y">https://youtu.be/-l2PRwgq66Y</a> (zu sehen ab 47:50, aber ganzes Video empfehlenswert)
</p>

 [1]: https://www.forensit.com/Downloads/Profwiz.msi