---
# Title
title: "Das Totalversagen der „Radfahrhauptstadt“ St. Pölten"
# Enter the author's name
author: Bernhard Steindl
# Customize URI; default: post/filename
#url: /yubikey-fuer-linux-login-als-u2f-2-faktor-verwenden/
# Date
date: 2023-09-23T12:00:00+01:00
# Is this draft?
draft: false
# Show Table of Content?
showToc: true
# Available categories. Please comment out!
categories:
#- Admin-Tipps
#- Allgemein
#- Arch Linux
#- Debian
#- Denkt's noch!
#- Grundlagen
#- Hardware
#- mariadb
#- News
#- nginx
- Politik
#- Projekte
#- Proxmox
- Rants
#- Satire
#- Synology
#- Tor
#- Ubiquiti Unifi
#- Windows
#- Wordpress
#- ZFS

cover:
    image: "images/cover.jpg"
    # can also paste direct link from external site
    # ex. https://i.ibb.co/K0HVPBd/paper-mod-profilemode.png
    alt: "Autozentrierte Politik aus finsteren Zeiten"
    caption: "Autozentrierte Politik aus finsteren Zeiten"
    relative: false # To use relative path for cover image, used in hugo Page-bundles

---

# Einleitung

{{< figure src="images/lebensqualitaet.jpg" caption="St. Pölten bietet wesentlich mehr Lebensqualität, als viele denken" >}}


Seit genau 6 Monaten darf ich mich St. Pöltner nennen. Dies sorgt oft für schadenfrohes Gelächter, meist unbegründet, denn die Stadt bietet viel mehr, als man auf den ersten Blick glauben mag. Es gibt eine schöne Altstadt, Badeseen, Theater und schlussendlich die absolut geniale öffentliche Anbindung. Kein Wunder also, dass die Stadt wächst. Doch kann hier die Infrastruktur mithalten?

## Best of St. Pölten

St. Pölten hat nicht ganz umsonst den Ruf einer Industriestadt, welche grau und menschenfeindlich ist. Es gibt viel Verbesserungspotenzial und offensichtliche Ungerechtigkeiten zwischen den Verkehrsteilnehmer. Hier zeige ich ein paar der schönsten Teile der Stadt her. Währenddessen empfehle ich folgendes Lied als Hintergrundmusik:

{{< youtube nqF9chK05YM >}}

### Mühlbach Ost <-> Fachmarkt Salzer

Glücklicherweise ist die Infrastruktur in der Nähe meiner Wohnhausanlage fantastisch: 3 Supermärkte, ein dm, mehrere Bushaltestellen, Restaurants, etc. alles in Fußgehweite. Nur leider ist die Infrastruktur für zu Fußgehende oder Radfahrer absolut schlecht. Es gibt keinen Zebrastreifen zu den nahen Märkten, es gibt nur einen Trampelpfad über die vielbefahrene Landhauszufahrt B1a.

{{< figure src="images/b1a.jpg" caption="Hier gehen jeden Tag unzählige Menschen ungeschützt einkaufen oder zur Bushaltestelle" >}}

Man sieht hier ganz eindeutig, dass hier immer noch autofokussiert geplant wird. Spätestens ab Baubeginn 2020 muss allen Beteiligten klar gewesen sein, dass es hier eine Lösung brauchen wird. Man hat das gut österreichisch „ausgesessen“ und meldet jetzt, dass `wir zeitnah eine Lösung dafür finden.`

### Mühlbach Ost <-> Stattersdorfer Hauptstraße

{{< figure src="images/fehlender-gehsteig.png" caption="Der rot markierte Bereich zeigt den Bereich ohne Gehsteig. Man geht hier durch Gras/Felder." >}}

Hier könnte man meinen, dass das nur ein Versehen war, dass man nicht einmal den Gehsteig zwischen Staudratgasse/NÖ LGA und Mühlbach Ost fertig ist. Immerhin hat man die Installation einer neuen Ampelanlage inklusive Abbiegestreifen **fristgerecht** zur Eröffnung der Wohnhausanlage fertigstellen können, da wird man ja nicht auf Radfahrer/FußgeherInnen vergessen haben? Das ist übrigens die kürzeste und schnellste Variante in die Innenstadt zu kommen - also keine unbedeutende Strecke.

{{< figure src="images/gschmackige-melonen.jpg" caption="Es gibt zwar gschmackige Melonen, aber keinen Gehsteig hier am Weg in die Stadt. Könnte eine Werbung für ein Bordell sein." >}}

Doch, genau das hat man, denn `Erste Gespräche [...] haben bereits stattgefunden. Eine mit einer Realisierung ist zeitnah nicht zu rechnen.` 100% Versagen und Inkompetenz. Willkommen in St. Pölten.

### Bushaltestelle Staudratgasse

{{< figure src="images/bushaltestellen.png" caption="Die Haltestelle Staudratgasse gibt es zweimal - wo fährt jetzt mein Bus weg?" >}}

Besonderes Schmankerl für Neo-St. Pöltner: Ich will von der Bushaltestelle „Staudratgasse“ wegfahren. Diese gibt es aber zweimal (insgesamt also 4 verschiedene Einstiegsmöglichkeiten). Welche nehme ich? Klassische Antwort: Kommt drauf an! Fährst du mit Bus 100, dann ist die Haltestelle näher an der Landesgesundheitsagentur, wenn mit dem Stadtbus 9 gefahren werden will, muss man zur anderen Haltestelle gehen. Und welche Richtung stadteinwärts oder stadtauswärts ist, ist auch nicht so einfach. Kompliziert? Ja, aber hier hat man wieder diese elendigen Kompetenzstreitigkeiten, denn `Die Haltestelle Staudratgasse der Linie 100 wurde auf Initiative der NÖ Landesgesundheitsagentur vom VOR errichtet. Die Linie 100 gilt als Regionalbus und ist mit dem Taktknoten der ÖBB am Hauptbahnhof abgestimmt.` Daher kann die Stadt nichts machen - oder will es zumindest nicht!

Schön, aber das ist keine Lösung! Sie sollten sich eine Haltestelle teilen und nicht je nach Landes/Stadtkompetenz eigene Haltestellen errichten!


### Kreuzung Wiener Straße x Bruno-Kreisky-Straße x Purkersdorfer Straße

{{< figure src="images/todeszone-traisenplatz.png" caption="Der rote Kreis zeigt die Dehnfuge, bei der schon viele Unfälle passiert sind. Die Linien zeigen den viel zu schmalen Radweg bzw. Risse, in denen Reifen stecken bleiben können" >}}

Im [Generalverkehrsplan 2014](https://www.st-poelten.at/gv-buergerservice/verkehr-mobilitaet-und-reisen/83-buergerservice/verkehr-mobilitaet-und-reisen/16227-generalverkehrskonzept-2014) wurde der Rückbau der Kreuzung festgelegt, sobald die neue Brücke (weiter nördlich, genannt „Willi-Gruber-Brücke“) eröffnet worden ist. Passiert ist bis dato nichts, natürlich will man den Autos keinen Meter Platz wegnehmen, obwohl das Verkehrsaufkommen hier de facto die Größe der Kreuzung nicht mehr rechtfertigt. Auch nicht, dass man mit dem Rad ewig warten muss, damit man über die Kreuzung kommt.

Hier ist das Ungleichgewicht zwischen Radfahrer/Fußgeher sowie Autofahrer besonders schlimm und es wurde schon oft eine Besserung versprochen. Dadurch, dass dies eine Landesstraße ist, kann die Stadt aber problemlos mit dem Finger auf die Landesregierung zeigen und braucht nichts tun.

### Wiener Brücke (B1)

{{< figure src="images/wienerbruecke.jpg" caption="Diesen Platz müssen sich Fußgeher und Radfahrer teilen. Sehen Sie die 3 leeren Autospuren?" >}}

Zwischen Landesregierung und der oben genannten Kreuzung befindet sich eine Traisenbrücke, welche für FußgeherInnen enorme Bedeutung hat. Doch hier steckt die Infrastruktur einfach in den 70er Jahren des letzten Jahrhunderts fest.

Nicht nur, dass hier sich Radfahrende und FußgeherInnen sich auf engsten Raum (mit gefährlichen Lichtmasten) ein paar Meter teilen müssen, währenddessen 3 Autospuren verfügbar sind, nein es gibt auch noch einen Spalt, an den man wunderbar mit dem Rad reinfahren kann und einen schönen Unfall bauen kann.

Gemeldet von mir bereits Mitte 2022, erst im August 2023 wurden gelbe Hinweisschilder aufgestellt, welche vor der tödlichen Gefahr warnen. Ein absolutes Fiasko! Hier zeigt sich die Stadt von Ihrer besten Seite!

### Daniel-Gran-Straße (FH-Fußweg)

St. Pölten hat mittlerweile 4000 Studenten in der Stadt, darauf ist man sich zurecht Stolz! Doch der Weg fast aller Studenten, welche bevorzugt mit dem Zug anreisen, führt über die Daniel-Gran-Straße, an einer sogenannten Bettelampel vorbei.

**Was ist eine Bettelampel?**

Das ist eine Ampel, bei der man als Fußgeher oder Radfahrer einen Knopf drücken muss, damit sie grün wird. Das gefährliche ist hier daran, dass die Wartedauer einfach viel zu lange ist und ich gar nicht weiß, wieviele Züge ich deswegen schon verpasst habe. Daher hat sich für mich und allen MitbürgerInnen eingebürgert, einfach rüberzugehen.

Das führt zu der Situation, dass dies mittlerweile auch Schulkinder machen, sodass sich hier niemand mehr auf die grüne Ampel verlassen kann. Sie hat es sogar auf Google Maps geschafft!

{{< figure src="images/bettelampel.jpg" caption="Aufgrund der langen Wartezeit an der Ampel soll es hier schon zu einschlägigen Flirts gekommen sein." >}}

Die Stadt reagiert so: `Wir haben an dieser Stelle voriges Jahr eine multimodale Zählstelle errichtet [...] Fußgänger:innen und Radfahrer:innen werden in Nord-Süd-Richtung gezählt, der motorisierte Verkehr in Ost-West-Richtung. Die Ergebnisse sind eindeutig und sprechen für Verbesserungsmaßnahmen[...]`

Hier sieht man das Grundproblem! Anstatt die offensichtlichen Probleme anzugehen und diese Ampel einfach abzubauen und einen normalen Radfahrübergang zu bauen, wird alles todanalysiert und hier Steuergeld verbraten! Das ist absolut lächerlich und zeigt auf, was hier schiefläuft.

## Kontakt mit dem Magistrat

Da es nichts bringt, wenn ich mich nur hier aufrege und keinen Kontakt mit der Stadtverwaltung aufnehme, habe ich all diese Punkte bereits vor Monaten der Stadt übermittelt.

Ich sendete am 21. April 2023 eine umfangreiche E-Mail an das Magistrat der Stadt St. Pölten mit der Bitte um Stellungnahme. Ich bekam über 4 Monate keine Antwort bzw. nur automatisierte Rückmeldungen. Daher schrieb ich am 22.08.2023 eine eher *unfreundlichen* Nachfrage:

> Von: Bernhard Steindl <*Mailadresse entfernt*>  
> Gesendet: Dienstag, 22. August 2023 16:14  
> An: rathaus@st-poelten.gv.at; stadtplanung@st-poelten.gv.at  
> Betreff: Re: Versagen der Stadtverwaltung bei Mühlbach Ost  
> Priorität: Hoch
>
> Mittlerweile warte ich seit über 4 (!) Monaten auf Ihre Antwort. Wenn die Antwort nicht den Umfang einer mittellangen Bachelorarbeit umfasst, wäre es langsam an der Zeit für eine Antwort (abgesehen von “in Bearbeitung”…). Sie könnten aber auch gleich zugeben, dass es ihnen völlig egal ist und Sie weiterhin 70er Jahre Verkehrspolitik machen. Ehrlichkeit währt am Längsten.
>
> Nebenbei gratuliere ich zur Errichtung einer neuen (und architektonisch hochwertigen) Hochgarage in bester Lage neben der Traisen. Hier weiß man, dass hier eine Stadt in die Zukunft baut und nicht reaktionäre Autofahrerpolitik betreibt!
>
> Mit freundlichsten Grüßen
>
> Bernhard Steindl

Nun war es plötzlich möglich, mir meine Fragen innerhalb von 24 Stunden zu beantworten. Ein bisserl *granteln* führt in Österreich immer zu zügigeren Arbeiten unserer Beamten.

> Sehr geehrte Damen und Herren,
>
> Seit rund einem Monat bin ich Einwohner von St. Pölten und bin ehrlich gesagt schwer enttäuscht von der “Radhauptstadt St. Pölten”. Ich wohne übrigens am Mühlbach Ost, der neuen Siedlung im Osten der Stadt.
>
> Ich zähle ein paar Punkte auf und bitte um eine Stellungnahme seitens des Rathauses:
>
>> **Bernhard Steindl** *Warum fährt der Bus 2 von Mühlbach Ost am Sonntag nicht? Das Bussystem ist schon kompliziert genug, da muss man nicht noch mehr Schwierigkeiten einbauen, die die Akzeptanz des öffentlichen Verkehres schwächen. Der Schildbürgerstreich ist ja, dass er ja trotzdem fährt, er bleibt einfach nicht stehen.*
>
> **Magistrat** Die Haltestelle Mühlbach Ost ersetzt seit Neuerrichtung die Haltestelle Fachmarkt Salzer in Fahrtrichtung stadteinwärts. Die Haltestelle Fachmarkt Salzer wird bzw. wurde am Sonntag nicht angefahren, da das Fachmarktzentrum an diesem Tag geschlossen ist. Bis zur Fertigstellung des Bauvorhabens Mühlbach Ost bestand an diesem Wochentag nicht der Bedarf diese Haltstelle anzufahren. Diese Information wurde in den IT-Systemen des VOR nicht hinterlegt, weshalb die Haltestelle Mühlbach Ost am Sonntag aktuell noch nicht angefahren wird. Seitens VOR können Fahrplanänderungen – so auch die (Re)Aktivierung einer Haltestelle – nur an bestimmten Tagen im Jahr vorgenommen werden. Mit dem nächsten Fahrplanwechsel am 4.9.2023 (Schulbeginn) werden die Haltestellen Mühlbach Ost stadteinwärts und Fachmarkt Salzer stadtauswärts künftig auch am Sonntag angefahren.
>
>> **Bernhard Steindl** Der Bus 100 soll planmäßig seit 11.04. auch beim Bahnhofplatz wegfahren, er wird sogar an der Tafel angekündigt. Nur wissen die Busfahrer nichts davon, sie fahren einfach durch. Das ist mir allein 3 Mal letzte Woche passiert.
> 
> **Magistrat** Linie 100 ist eine Regionalbuslinie und diese fällt in die Zuständigkeit des VOR. Wir haben diese Information weitergeleitet, können den Ausgang jedoch nicht beeinflussen.
>
>> **Bernhard Steindl** Wenn wir schon bei der Bushaltestelle Staudratgasse sind: Warum in alles in der Welt gibt es diese 2 Mal? Wer denkt sich so einen Blödsinn aus?
>
> **Magistrat** Die Haltestelle Staudratgasse der Linie 100 wurde auf Initiative der NÖ Landesgesundheitsagentur vom VOR errichtet. Die Linie 100 gilt als Regionalbus und ist mit dem Taktknoten der ÖBB am Hauptbahnhof abgestimmt. Die Zielgruppe der Linie 100 ist jener Personenkreis, der mit dem Zug anreist und im Bereich der Haltestelle Staudratgasse arbeitet. Aus diesem Grund verkehrt die Linie 100 an Samstagen, Sonn- und Feiertagen nicht. Die Haltestelle für den LUP besteht seit 2007.
>
>> **Bernhard Steindl** Warum gibt es keinen Zebrastreifen zum Hofer rüber? Es ist hier bereits ein Trampelpfad erkennbar und es gab bereits ein paar gefährliche Situationen, wie ich selbst beobachten konnte. Es kann doch nicht sein, dass der Autoverkehr gegenüber der Bewohner so bevorzugt wird, dass es mutwillig zu Verletzungen kommen kann?
>
> **Magistrat** Da es auf der Südseite der B1a bei der Fa. Ginzinger keinen Gehsteig gibt, kann auch kein Schutzweg errichtet werden. Aktuell planen wir in Abstimmung mit dem Land NÖ (B1a und Sattersdorfer Hauptstraße sind beide Landesstraßen) die Verbindung für Fußgänger:innen und Radfahrer:innen auf der Westseite bis zum Fachmarkt Salzer. Wesentlich wird diese Planung jedoch von dem Umstand beeinflusst, dass der Grünstreifen zwischen Hofer-Parkplatz und Stattersdorfer Hauptstraße nicht zu 100% im Besitz von Stadt oder Land ist. Neben der Abstimmung mit dem/der Grundeigentümer:in erfüllt der Grünstreifen außerdem die Entwässerungsfunktion des Parkplatzes, weshalb er nicht einfach überbaut werden kann/darf. Wir hoffen, dass wir zeitnah eine Lösung dafür finden.
>
>> **Bernhard Steindl** Warum gibt des die Bushaltestelle Mühlbach Ost nur in eine Richtung? Es wäre problemlos Platz für eine zweite gewesen, so macht all das keinen Sinn! Es muss übrigens keine Einbucht geben, für ein paar Sekunden alle 30 Minuten wäre es schon möglich, das AutofahrerInnen kurz warten, wenn der Bus kommt.
>
> **Magistrat** Da es sich bei der B1a um eine der wichtigsten Straßenzüge der Stadt handelt, gibt es seitens des Landes die Vorgabe hier Haltestellen ausschließlich in Form von Busbuchten zu errichten. Für eine Busbucht verfügt die Stadt jedoch nicht über ausreichend Grundfläche. Auch in diesem Fall sind wir als Stadt in Austausch mit dem Land, ob nicht doch eine Randhaltestelle möglich wäre. Im Sinne der einfachen und attraktiven Nutzung des LUP wäre dies jedenfalls anzustreben.
>
>> **Bernhard Steindl** Warum ist der Gehsteig/geplante Radweg zwischen Mühlbach Ost und der Staudratgasse hinauf nicht fertig? Hauptsache der Abbiegestreifen für die Autofahrer und die Bettelampel (welche übrigens bei der Ausfahrt auf der Straße bei Fahrräder nicht Grün wird) geht sich aus, für !grundlegendste! Infrastruktur hat es offensichtlich nicht gereicht.
>
> **Magistrat** Wenn sie die Verbindung entlang der Bruno Kreisky Straße, zwischen Willy Brandt Straße und Stattersdorfer Hauptstraße Bereich Staudratgasse meinen, ist diese in Vorplanung. Dazu bräuchte es eine Grundeinlöse des Grundstücks zwischen Harlander Bach und Bruno Kreisky Straße, welches nicht im Besitz der Stadt ist. Erste Gespräche mit dem Grundeigentümer haben bereits stattgefunden. Eine mit einer Realisierung ist zeitnah nicht zu rechnen.
>
>> **Bernhard Steindl** Warum fährt die Buslinie 2 außerhalb des Vollknotens des Hauptbahnhofes? Das macht die Linie wesentlich unattraktiver als die Linie 9, welche aber wiederum oft zu früh (meist schon um :46 bzw. 16/17) wegfährt, sodass mehrere Fahrgäste dann wieder 30 Minuten warten können. Herzliche Gratulation!
>
> **Magistrat** Aus Effizienzgründen sind die Linien 2, 4 und 8 zu einem Linienbündel zusammengefasst, was bedeutet, dass die Fahrzeiten einer dieser drei Linien immer auf jene der anderen bezogen sind. Seit 2007 bzw. 2017 sind die Abfahrtszeiten im LUP-System nahezu unverändert. Im Gegensatz dazu haben sich die Ankunfts- und Abfahrtszeiten bei der Bahn immer wieder, auch baustellenbedingt, geändert. Aufgrund der Komplexität des LUP-Systems – auch in Zusammenhang mit dem VOR – können die Abfahrtszeiten nicht beliebig angepasst werden. Zudem gibt es an der Haltestelle Hauptbahnhof nur Platz für drei LUP-Busse je Richtung. Würde an den Fahrzeiten einer Linie gedreht werden, hätte dies Auswirkungen auf alle anderen Linien. U.a. könnte dadurch auf der Hauptachse Mühlweg – Promenade – Josefstraße kein 10-Minuten-Takt mehr gewährleistet werden.  
Aktuell befindet sich das LUP-System in Überarbeitung, da bis zum Jahr 2027 das Bussystem neu ausgeschrieben werden muss. Bis dahin können wir bzw. der VOR in diesem relativ starren System keine allzu großen Änderungen vornehmen.
>
>> **Bernhard Steindl** Der Radweg bei der Kreuzung Stattersdorfer Straße/Wiener Straße eine Gemeingefährdung darstellt, aufgrund der großen Löcher und der steilen Rampen, bei denen es einem den Einkauf aus dem Korb hebt. In meinem ganzen Leben habe ich noch nie einen so schlechten Radweg wie zwischen Stattersdorfer/Straße/Wiener Straße bzw. Neugebäudeplatz gesehen. Ich habe gelesen bzw. konnten mir auch mehrere Radfahrer sagen, dass nach der Errichtung der neuen nördlichen Traisenbrücke die alte Brücke zurückgebaut und endlich ein vernünftiger Radweg kommen sollte. Nun ist die neue Brücke bald 10 Jahre offen und sie träumen noch immer irgendwo dahin. Abgesehen von der bekannten “Todeszone” am Traisenplatz, wo die breite Dehnfuge noch immer für Gemeingefährdung sorgt.
>>
>> Jeder wird bald jemanden kennen, den es dort auf die “Pappn” ghaut hat.
>
> **Magistrat** Die Fuß- und Radverkehrsplanung betreffend, hinkt die Stadt leider einige Jahre zurück. Das ist uns bewusst. Seit nun mehr zwei Jahren gibt es ein klares Bekenntnis der Stadt zum Ausbau der Fuß- und Radinfrastruktur. Projekte wie der Promenadenring oder die Ausweitung der Fußgängerzone veranschaulichen diesen Umstand. Aktuell erarbeitet die Stadt die „Leitkonzeption Aktive Mobilität“, in welcher Ausbau- und Sanierungsmaßnahmen der Fuß- und Radinfrastruktur für die nächsten Jahre festgelegt werden. Daneben wurden im Jänner 2023 die Planungen des NÖ Radbasisnetzes für St.Pölten abgeschlossen, in welchem wir über 150! Maßnahmen im Radnetz als notwendig erachtet haben. Für das Jahr 2023 wurden über 2 Mio. Euro für den Ausbau der Radinfrastruktur zur Verfügung gestellt. Noch in diesem Jahr werden über 12 Km neue Radwege, davon 10 Km in die Nachbargemeinden, realisiert. Auch streben wir noch einige Sanierungen an. Der Fokus der nächsten Jahre wird auf dem Ausbau von Hauptrouten liegen, die aktuell im Rahmen der Leitkonzeption erarbeitet werden. Bitte haben Sie Verständnis dafür, dass wir diese Fülle an Maßnahmen nicht innerhalb der nächsten drei Jahre realisieren können.
>
>> **Bernhard Steindl** Nachdem der 100er eigentlich eh immer bei der Staudratgasse stehen bleibt, könnte man ja entweder den 9er oder den 100er permanent über Mühlbach Ost leiten? Die Fahrgäste die ich aussteigen sehe, kommen über den “Gatschweg” meistens in Richtung der neuen Siedlung, in die Staudratgasse verschwindet eine Minderheit.
>
> **Magistrat** Wie ich oben bereits geschrieben haben, kann dies aus mehreren Gründen nicht geschehen: (1) Verkehrt die Linie 100 an Samstagen, Sonn- und Feiertagen nicht, (2) können wir innerhalb der bestehenden Ausschreibung keine Umfassenden Linienänderungen vornehmen und (3) wurde die Linie 100, im Gegensatz zu den LUP-Linien, vom Land bestellt.
>
>> **Bernhard Steindl** Die Bettelampel an der Daniel-Gran-Straße ist so nervig, das mittlerweile der Großteil einfach so rüber geht, das ist natürlich der Gipfel der Verkehrsplanung, wenn man die Fußgeher solange nervt, bis sie sich absichtlich in eine Gefahr begeben. Übrigens hat die Ampel sogar schon einen Google Maps-Eintrag, das muss man auch erstmal schaffen!
>
> **Magistrat** Wir haben an dieser Stelle voriges Jahr eine multimodale Zählstelle errichtet, mit der wir die Verkehrsströme analysieren können. Fußgänger:innen und Radfahrer:innen werden in Nord-Süd-Richtung gezählt, der motorisierte Verkehr in Ost-West-Richtung. Die Ergebnisse sind eindeutig und sprechen für Verbesserungsmaßnahmen zugunsten der Nord-Süd-Richtung. Da es sich bei der Daniel Gran-Straße um einen Abschnitt der Kerntangente Nord und um eine koordinierte Ampelkette (beeinflusst durch den LUP bei der Daniel Gran-Schule) handelt, ist dieses Unterfangen jedoch nicht einfach umzusetzen.
>
> Abschließend möchte ich Sie um Verständnis bitten, dass wir seit einigen Jahren mit einer Fülle an Projekten betraut sind, die es umzusetzen gilt. Gleichzeitig kommen immer mehr Projekte nach, die auch noch irgendwie hineinpassen müssen. Leider ist es (generell den städtischen Verwaltungen) aufgrund begrenzter zeitlicher, personeller und budgetärer Gegebenheiten nicht möglich, alle Wünsche der Bevölkerung morgen umzusetzen. Die Stadt als lebender Organismus unterliegt ständig Veränderungen auf die wir reagieren müssen. Ein Projekt im Zentrum kann sich entsprechend auf andere Stadtteile niederschlagen und umgekehrt. Auf dies und die Bedürfnisse der Bevölkerung – und zwar aller, auch jener mit einer anderen Meinung – müssen wir Rücksicht nehmen und Lösungen entwickeln, mit denen alle leben können.  
> Ich kann Ihnen versprechen, dass ich mich Ihrer Anliegen annehmen und stehe, wie oben geschrieben, auch gerne für einen Lokalaugenschein zur Verfügung.

Man hat sich nach über 4 Monaten also doch noch überzeugen lassen können, mir eine Antwort zukommen zu lassen.

## Weitere Aktionen meinerseits

Ich werde hier natürlich keine Ruhe geben und weiter dafür kämpfen. Daher nehme ich das Gesprächsangebot sowie den Lokalaugenschein gerne an und stehe selbstverständlich konstruktiv zur Verfügung. Was ich nicht mehr hören kann/will ist die ewige Kompetenzstreitigkeit zwischen Stadt und Landesregierung. Nachem wir nun einen besonders autofreundlichen [Landeshauptfrau-Stellvertreter](https://www.noe.gv.at/noe/Landesregierung/Udo_Landbauer_MA.html) Udo („[die siebte Million](https://www.falter.at/zeitung/20180123/wir-schaffen-die-siebte-million)“) Landbauer (mit der Autofreundlichkeit hat die Vorgängerpartei bereits [vor vielen Jahren angefangen](https://de.wikipedia.org/wiki/KdF-Wagen)) sehe ich seitens des Landes schwarz (oder blau. eigentlich eher braun).

Daher ist es umso wichtiger, dass die Bevölkerung in St. Pölten hinter den guten Projekten der Stadt steht (wie etwa der sogenannte „Grüne Loop“), aber sich entschieden gegen Verhinderungs- oder autofreundliche Politik stellt. Nur so kann die Autofokussiertheit langsam aber sicher abnehmen, was auch die Lebensqualität steigern wird. Niemand hält sich gerne an der Wiener Brücke auf, obwohl es hier mit weniger Autos sogar recht schön sein kann. Viele Orte der Stadt verdienen eine Aufwertung, daher muss man der Politik (die teilweise schon die richtigen Impulse setzt) Rückendeckung geben. Daher ist nicht nur Kritik - sondern auch Lob angebracht, wenn etwas gut gelöst wurde. Dazu wird es dann natürlich auch Artikel geben!

# Fazit

Die Stadt St. Pölten bietet vieles - leider auch an Enttäuschungen. Man hat es verabsäumt, die Infrastruktur für Fußgeher- und RadfahrerInnen adäquat an die 1000 neuen Einwohner am Niederösterreich-Ring anzupassen. Man hat Verkehr wieder einmal nur in Autos gedacht - dabei bietet St. Pölten kurze Wege und so viel Lebensqualität. Es tut weh, dass hier ganz einfache Maßnahmen wie ein Gehsteig oder die Entfernung einer Ampel vielen Menschen das Leben erleichtern, aber einfach niemand daran denkt.

Daher ist es auch wichtig, dass die PolitikerInnen auch öfters mit dem Rad oder zu Fuß unterwegs sind - dann würden sie diese offensichtlichen Ungleichgewichte mehr spüren und würden sich nicht nur auf große Prestigeprojekte konzentrieren ([Domplatz](https://www.st-poelten.at/news/18176-baumtroege-spielgeraete-und-mehr-fuer-den-domplatz), [Green Loop](https://www.st-poelten.at/stp25-50/masterplan)), sondern auch die kleinen Maßnahmen beachten.

Mein Appell an die Stadtpolitik: Nehmen Sie meine Punkte ernst und arbeiten Sie daran - manche Dinge kann man mit geringstem finanziellen Aufwand beseitigen - manch andere sind der jahrzehntelangen Ignoranz des Umweltverbundes geschuldet - aber Sie können das ändern!