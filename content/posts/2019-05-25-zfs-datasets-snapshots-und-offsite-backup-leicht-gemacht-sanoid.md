---
title: ZFS Datasets – Snapshots und Offsite-Backup leicht gemacht – Sanoid
author: Christoph Scheidl
type: post
date: 2019-05-25T12:58:00+00:00
url: /zfs-datasets-snapshots-und-offsite-backup-leicht-gemacht-sanoid/
categories:
  - Debian
  - Proxmox
  - ZFS

---
UPDATE vom UPDATE (19.12.2019): Der Fix unten hilft nur bedingt. Ich empfehle, bei cron mit dem Eintrag `MAILTO=""` die E-Mail-Benachrichtigung zu deaktvieren, damit ihr nicht genervt werden. Ich habe das [Issue bei sanoid auf Github][1] im Auge und werde den Artikel aktualiseren, sobald ich mehr weiß.

UPDATE/INFO (18.12.2019): Aktuell gibt es einen Bug, bei dem Sanoid ständig errors von sich gibt, die einem das ganze E-Mail-Postfach füllen (bei Proxmox zumindest). Um dieses Problem zu &#8222;lösen&#8220; (quick and dirty fix), einfach den cron-Befehl 

<pre class="wp-block-code"><code>* 1 * * rm -r /var/run/sanoid_cacheupdate.lock</code></pre>

hinzufügen.

Heute bin ich auf ein kleines, aber feines Tool gestoßen: [Sanoid][2]. Dieses macht es einem einfach, automatische Snapshots eines ZFS-Datasets (zB eine Proxmox-VM auf einem ZFS-Pool) zu erstellen und diese dann auch gleich auf einen Offsite-Server zu sichern.

Das Tool ist gut dokumentiert, allerdings gibt es nicht viele Beispiele, wie genau eine Config aussehen muss. Man findet aber eine beispielhafte sanoid.conf und eine sanoid.defaults.conf, in denen viel erklärt wird.

Die Installation unter Proxmox ist sehr einfach (debian-based), einfach die Anleitung unter INSTALL.md auf GitHub befolgen, in 5 Minuten ist der Fall erledigt. Wichtig ist zu beachten, das sanoid auf dem Hypervisor direkt laufen muss, weil logischerweise ansonsten keine Möglichkeit besteht, die VMs wegzusichern.

Nachdem das ganze installiert ist, kann man unter

<pre class="wp-block-code"><code>/etc/sanoid/sanoid.conf</code></pre>

einfach eine Konfiguration eintragen, in meinem Falle möchte ich zB einen Nextcloud-LXC sichern:

<pre class="wp-block-code"><code>&#91;data0/subvol-100-disk-0]
        use_template = prod


&#91;template_prod]
        frequently = 0
        hourly = 1
        daily = 30
        monthly = 6
        yearly = 7
        autosnap = yes
        autoprune = yes</code></pre>

Hier beispielhaft ein stündlicher Snapshot (falls mir zufällig was blödes passiert während der Wartung, bei Dateischäden hat Nextcloud selbst außerdem eine eigene Versionierung), 30 tägliche Snapshots, 6 monatliche und 7 jährliche. Außerdem soll das ganze automatisch sowohl gesnapshottet werden, als auch gepruned (das heißt, das wenn zB ein neuer stündlicher Snapshot erstellt wird, der letzte gelöscht wird, weil wir ja nur einen stündlichen aufheben wollen).

Nun wird das ganze als Cronjob ausgeführt (in meinem Fall als root, man wird natürlich auch einen eigenen Benutzer machen können). Sanoid muss natürlich dort ausgeführt werden, wo ihr die git-repo hingeforkt habt.

<pre class="wp-block-code"><code>* * * * * TZ=UTC /root/sanoid/sanoid --cron</code></pre>

Eine Minute warten, und siehe da, es funktioniert:

<pre class="wp-block-code"><code>root@host:~# zfs list -rt snap
NAME                                                           USED  AVAIL  REFER  MOUNTPOINT
data0/subvol-100-disk-0@autosnap_2019-05-25_13:54:40_yearly      0B      -  30.1G  -
data0/subvol-100-disk-0@autosnap_2019-05-25_13:54:40_monthly     0B      -  30.1G  -
data0/subvol-100-disk-0@autosnap_2019-05-25_13:54:40_daily       0B      -  30.1G  -
data0/subvol-100-disk-0@autosnap_2019-05-25_12:04:01_hourly   4.19M      -  30.1G  -</code></pre>

Nun können wir mit dem 2. Tool namens syncoid das ganze auch offsite sichern. Dazu brauchen wir auf dem 2. Rechner nur einen ZFS-Pool, und ein paar Tools, die sich ganz einfach (in meinem Fall unter Debian) installieren lassen:

<pre class="wp-block-code"><code>apt install pv mbuffer lzop</code></pre>

Ob die Tools wirklich notwendig sind weiß ich nicht auswendig, aber so funktioniert es bei mir auf jeden Fall.

Syncoid funktioniert über SSH, also am besten einfach eine public-authentication auf den Rechnern einrichten und SSH des Offsite-Rechners auf einen nicht-standardisierten Port legen.

Dann kann das ganze auch schon beginnen, mit einem Befehl:

<pre class="wp-block-code"><code>syncoid data0/subvol-100-disk-0 root@offsite:zpool/dataset --sshport=12345 --source-bwlimit=400k</code></pre>

Man beachte: das Dataset wird von syncoid auf dem Offsite-Rechner angelegt, wenn man das selber macht, kommt eine Fehlermeldung bei syncoid. Mittels &#8211;help kann man noch weitere Optionen sehen, die nützlich sein können (je nach Szenario). Ich habe zum Beispiel noch die Bandbreite limitiert, um die A1-Leitung nicht komplett in Anspruch zu nehmen.

Nun wird das ganze einmal eine Weile in Anspruch nehmen. Aber keine Sorge, sollte das ganze länger dauern als das Cron-Interval, das ihr nun festlegt, wird das 2. mal einfach abgebrochen, solange hier noch übertragen wird. Da im Anschluss nur mehr Änderungen übertragen werden, solltet ihr in Zukunft auch kein Problem haben, das ihr den ganzen Tag die Internetleitung in Anspruch nehmt.

Jetzt noch das ganze als Cronjob anlegen (hier: jeden Tag um 2 in der Früh):

<pre class="wp-block-code"><code>0 2 * * * /root/sanoid/syncoid data0/subvol-100-disk-0 root@offsite:zpool/dataset --sshport=12345 --source-bwlimit=400k</code></pre>

WICHTIG: Bei cronjobs immer den absoluten Pfad zum Programm angeben!

Um zu überprüfen, ob der Bandwidth-Limit funktioniert, empfehle ich euch &#8222;nload&#8220;, einfach über apt installiert, seht ihr genau, welche Bandbreite über eure Netzwerk-Interfaces benutzt werden.

syncoid übertragt alle Snapshot-Zwischenschritte, die ihr am System habt. Das heißt, das ihr auch die stündlichen, täglichen, usw. am anderen System habt. Sollte das nicht erwünscht sein, gibt es dafür eine entsprechende Option (&#8211;no-stream)

Ihr könnt auch als Beispiel im Falle von Proxmox einen Hot-Spare auf einem anderen Ort laufen haben, wo ihr immer diese Snapshots hinkopiert. Sollte euch der Standort abbrennen oder einfach der Server sterben, könnt ihr den anderen nehmen, hinstellen und (sofern ihr auch die Proxmox-notwendigen Konfigurationen etc. über zB ssh-copy übertragen habt) einfach weitermachen, womit ein schnelles weiterarbeiten gewährleistet ist.

 [1]: https://github.com/jimsalterjrs/sanoid/issues/391#issuecomment-567368094
 [2]: https://github.com/jimsalterjrs/sanoid/