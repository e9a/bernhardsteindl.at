---
title: Zugriff auf WAN-seitiges Modem-Interface bei USG
author: Christoph Scheidl
type: post
date: 2018-11-20T12:38:33+00:00
url: /zugriff-auf-wan-seitiges-modem-interface-bei-usg/
categories:
  - Ubiquiti Unifi

---
Beim Erstellen von Netzwerken müssen wir leider oft auf Doppel-Nating zurückgreifen, um hinter dem A1-Router ein USG nutzen zu können (Modemverwendung zB durch A1-TV nicht möglich). Um trotzdem auf das Webinterface des A1-Routers aus dem Netzwerk des USGs zugreifen zu können, müssen wir eine Firewall-Regel hinzufügen (Das geht leider momentan nur in der CLI. Für Vorschläge, wie das ganze im Webinterface des Controllers geht, sind wir gerne offen).  


## Konfiguration am USG

Folgende Konfiguration könnt ihr am USG vornehmen (einloggen über SSH). Diese Änderung ist jedoch nach dem nächsten &#8222;Provisioning&#8220; vom USG wieder weg.  


<pre class="wp-block-code"><code>configure
set service nat rule 5000 type masquerade
set service nat rule 5000 destination address 10.0.0.138
set service nat rule 5000 outbound-interface eth0
commit
save
exit
</code></pre>

## Konfiguration im Unifi Controller

Um die Konfiguration dauerhaft zu haben, müsst ihr folgendes JSON-Objekt in die Datei &#8222;config.gateway.json&#8220; kopieren. Wo genau sich diese Datei findet bzw. wie ihr sie erstellt, steht in diesem Ubiquiti-Artikel: <https://help.ubnt.com/hc/en-us/articles/215458888-UniFi-How-to-further-customize-USG-configuration-with-config-gateway-json>  


```
{
"service": {
        "nat": {
            "rule": {
                "5000": {
                    "destination": {
                        "address": ["10.0.0.138"]
                    },
                    "outbound-interface": ["eth0"],
                    "type": "masquerade"
                }
            }
        }
    }
}
```

Danach klickt im Controller bei USG &#8211; Config auf &#8222;Force-Provisioning&#8220;. Damit werden diese Änderungen auf das USG geladen und sind immer vorhanden.

### Anmerkung:

Bei der Verwendung von OpenVPN müsst ihr beachten, dass ihr zusätzlich zu eurem LAN auch die Route &#8222;10.0.0.0 255.255.255.0&#8220; erlaubt:  


```push "route 10.0.0.0 255.255.255.0"```

###### Links und Credit

[https://help.ubnt.com/hc/en-us/articles/215458888-UniFi-How-to-further-customize-USG-configuration-with-config-gateway-json](https://help.ubnt.com/hc/en-us/articles/215458888-UniFi-How-to-further-customize-USG-configuration-with-config-gateway-json)

[https://owennelson.co.uk/accessing-a-modem-through-a-ubiquiti-usg](https://owennelson.co.uk/accessing-a-modem-through-a-ubiquiti-usg)