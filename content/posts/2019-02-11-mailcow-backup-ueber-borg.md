---
title: mailcow-Backup über Borg
author: Bernhard Steindl
type: post
date: 2019-02-11T21:09:29+00:00
url: /mailcow-backup-ueber-borg/
categories:
  - Debian

---
Andre Peters bietet mit seiner [mailcow][1] eine umfassende Groupware-Lösung an. Nochmals vielen Dank für seinen Beitrag an die Open-Source-Welt!

Ich sichere die meisten unserer Server mittels borg. Damit sichere ich nicht das komplette System, sondern nur relevante Teile. Das sind im Falle der mailcow natürlich nur alle Daten.

Das Backup-Script basiert stark auf dem des mailcow-Urhebers, mit dem Unterschied, dass die Mails nicht ge-&#8222;tar&#8220;-t, sondern einfach nur in ein anderes Verzeichnis kopiert werden, damit das inkrementelle Backup mittels Borg nur die Änderungen eines Tages abbildet.

**UPDATE:** Mit diesem [Commit][2], wurde das Backup standardmäßig rsync-bar und damit auch borg-kompatibel. Mein Workaround wurde dadurch überflüssig

 [1]: https://mailcow.email/
 [2]: https://github.com/mailcow/mailcow-dockerized/commit/60d74e3f17d8527b34975f7f8e7e9a820d0de628