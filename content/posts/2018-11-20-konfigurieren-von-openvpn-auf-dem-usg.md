---
title: Konfigurieren von OpenVPN auf dem USG
author: Christoph Scheidl
type: post
date: 2018-11-20T18:42:57+00:00
url: /konfigurieren-von-openvpn-auf-dem-usg/
categories:
  - Ubiquiti Unifi

---
Hier eine Anleitung, die die Schritte ziemlich gut beschreibt: <https://medium.com/server-guides/how-to-setup-an-openvpn-server-on-a-unifi-usg-e33ea2f6725d>

Lediglich beim Konfigurieren der Client-OVPN-Files gibt es anzumerken, dass die Zertifikate in HTML-Tags gefasst gehören, sprich <ca></ca> für das CA-Zertifikat, <cert></cert> für das client-crt und <key></key> für den client-key.

Wer nicht will, dass der gesamte Traffic über das VPN geleitet wird (zB nur für Wartungsarbeiten), der muss aus dem JSON die Zeile mit der Option &#8222;redirect-gateway def1&#8220; löschen. Auch die IP-Adressen müssen angepasst werden an das zu wartende Netzwerk: &#8222;push-route&#8220; und &#8222;nameserver&#8220; sollen die IPs des zu wartenden Netzwerks sein.

Vergesst nicht, das JSON vorher durch einen Validator zu jagen, ansonsten kann es böse enden und das USG steckt in der &#8222;provisioning-loop&#8220; mit anschließendem Neustart fest.

Falls ihr wie in diesem Artikel (<https://blog.e9a.at/konfigurieren-von-ddns-mit-eigenem-provider-auf-dem-usg/>) gezeigt die &#8222;config.gateway.json&#8220; mit diesem Inhalt bereits habt, könnt ihr euch den Punkt &#8222;service&#8220; von OpenVPN komplett sparen.  


###### Links und Credit

[https://medium.com/server-guides/how-to-setup-an-openvpn-server-on-a-unifi-usg-e33ea2f6725d](https://medium.com/server-guides/how-to-setup-an-openvpn-server-on-a-unifi-usg-e33ea2f6725d215458888-UniFi-How-to-further-customize-USG-configuration-with-config-gateway-json)


[https://help.ubnt.com/hc/en-us/articles/215458888-UniFi-How-to-further-customize-USG-configuration-with-config-gateway-json](https://help.ubnt.com/hc/en-us/articles/215458888-UniFi-How-to-further-customize-USG-configuration-with-config-gateway-json)