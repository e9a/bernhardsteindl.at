---
title: GTK-Window-Buttons nach links in Plasma
author: Christoph Scheidl
type: post
date: 2018-10-26T14:34:10+00:00
url: /gtk-window-buttons-nach-links-in-plasma/
categories:
  - Arch Linux
  - Debian

---
Als User der Plasma-Shell, der aber auch GTK-Anwendungen nutzt, nervt es, wenn Plasma-Anwendungen die Buttons links, und GTK-Anwendungen diese rechts haben. Ein kleiner Ein-Zeiler löst das Problem:

```kwriteconfig5 --file ~/.config/gtk-3.0/settings.ini --group Settings --key "gtk-decoration-layout" "close,maximize,minimize:menu"```

Danach einfach die Applikationen neu starten, und die Buttons sind links.

###### Links und Credit


  Credit gebürt [Zren](https://www.reddit.com/user/Zren) für diesen Post: [https://www.reddit.com/r/kde/comments/8d9zc2/gtk_traffic_buttons_on_left_size](https://www.reddit.com/r/kde/comments/8d9zc2/gtk_traffic_buttons_on_left_size/)