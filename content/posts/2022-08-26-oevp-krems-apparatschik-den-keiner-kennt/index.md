---
# Title
title: "ÖVP Krems - Apparatschik den keiner kennt"
# Enter the author's name
author: Bernhard Steindl
# Customize URI; default: post/filename
#url: /yubikey-fuer-linux-login-als-u2f-2-faktor-verwenden/
# Date
date: 2022-08-31T19:00:00+01:00
# Is this draft?
draft: false
# Show Table of Content?
showToc: true
# Available categories. Please comment out!
categories:
#- Admin-Tipps
#- Allgemein
#- Arch Linux
#- Debian
#- Denkt's noch!
#- Grundlagen
#- Hardware
#- mariadb
#- News
#- nginx
- Politik
#- Projekte
#- Proxmox
#- Rants
#- Satire
#- Synology
#- Tor
#- Ubiquiti Unifi
#- Windows
#- Wordpress
#- ZFS

---

# ÖVP

*Dies ist ein Artikel der Serie [Pappkameraden in Krems](/posts/2022-08-25-die-kremser-pappkameraden/). Sämtliche Inhalte genügen keinen journalistischen Ansprüchen wie Ausgewogenheit oder DoubleChecks, falls Sie dennoch falsche Informationen finden oder Anmerkungen zu den Artikeln haben, würde ich mich über eine [Rückmeldung](/impressum) sehr freuen.*

{{< figure src="images/oevp1.jpeg" caption="Wenn ich an die ÖVP denke, fällt mir auch zuerst *Schwung holen* ein. Nicht Korruption, Betoniererei oder einfach nur Unfähigkeit. Ich hoffe es ist ein Schwung holen für einen Bauchfleck bei der Wahl." >}}
{{< figure src="images/oevp2.jpeg" caption="Währenddessen sich die Ärmsten im Lande vermutlich nur den Urlaub in Balkonien leisten können, hat die ÖVP Krems genug Geld um zumindest alle Plakate zweimal zu bedrucken." >}}
{{< figure src="images/oevp3.jpeg" caption="Und ich wünsche der ÖVP – allumfassend und aus dem Herzen heraus – einen fröhlichen Untergang." >}}

DIe ÖVP finde ich ja am besten: Anstatt wirklich den Wahlkampf später zu starten, wie angekündigt, stellt man Bullshit-Schilder auf, auf denen steht, dass sie erst am 12.8. mit dem Wahlkampf (= Schilder aufstellen?) starten. Als **Bürgermeisterkandidat** haben sie einen Niemand aus dem Landesunternehmen herbeigezaubert – ich muss jetzt wirklich mal nachsehen, wie er heißt: Suche… suche… „Kamleitner“ heißt er. Den Vornamen habe ich schon wieder vergessen, obwohl ich gerade erst zu einem Plakat nachschauen gegangen bin. Ich geh schnell nochmal…

„Florian“ – Florian Kamleitner heißt er. Er arbeitet in einem ÖVP-Versorgungsunternehmen namens *ecoplus*, das unter anderem mit meinem Steuergeld finanziert wird. Wenn ich dann in Zukunft in St. Pölten nur 3 Hausnummern weiter wohne und jeden Tag daran vorbeigehe, vielleicht finde ich dann heraus, was die dort den ganzen Tag machen. Auf der Website steht irgendwas von „Forschung“ und „100 unterstützten Betriebsansiedelungen im Jahr“ aber das hört sich für 85 Mitarbeiter nicht besonders produktiv an; und die 14 Aufsichtsräte (Der [Liederbuch-Udo](https://www.derstandard.at/story/2000072861626/nazi-lieder-bei-burschenschaft-von-fpoe-kandidat-landbauer) ist auch dabei!) finanzieren sich ja auch nicht von selbst…

## Einschub Cartellverband

Wie die KLS Krems richtig anmerkt, war (oder ist) Florian Kamleitner Mitglied der *katholischen Wertegemeinschaft* „Cartellverband“ und wurde in einem Artikel des Standards vor 8 Jahren mehrmals erwähnt. Sein Vulgo-Name war **Slivo**, es kommt vom „Schnapsbrennen“. Ich fasse (bin gerade echt fassungslos) den [Artikel des Standards, in dem Florian Kamleiter als Slivo mehrmals vorkommt](https://www.derstandard.at/story/2000007166981/wer-aus-der-kirche-austritt-der-geht), schnell zusammen.

> Es gibt ein eigenes ÖCV-Gebet, ein Höhepunkt der jährlichen Hauptversammlung ist der Gottesdienst, und unter den Mitgliedern befinden sich einige Geistliche. Das Bekenntnis zur katholischen Kirche ist auch das Prinzip, das bei vielen Verbindungen am ehesten zum Ausschluss führen kann. Oder wie es Rechtswissenschafter Wiesinger ausdrückt: "Wer aus der Kirche austritt, der geht auch hier."

Auweh, wo die katholische Kirche dabei ist, kann das nur schlecht ausgehen.

> Wie so viele verweist er, gefragt nach den ethischen Prinzipien des CV, erst einmal auf die Position der katholischen Kirche. So sei Abtreibung für den Verband offiziell noch immer ein Problem, wie Stöckl erklärt: "Die Position des CV ist da relativ klar: grundsätzlich umfassender Lebensschutz. Mit der Fristenlösung, wie sie jetzt ist, kann man eigentlich nicht zufrieden sein."

Auweh, auweh, auweh…

> Wiesinger als der Älteste in der Runde stellt die Position schließlich klar: "Die offizielle Position ist: Homosexualität ist ein Faktum, aber sie darf nicht ausgelebt werden." Da jeglicher Geschlechtsverkehr außerhalb der Ehe eine Sünde sei und Homosexuelle nicht heiraten dürften, sei das Thema "auch schon wieder in kirchlicher Sicht erledigt".

Anzumerken ist auch, dass der ÖCV eine reine Männer-Studentenverbindung ist; Verbindungen, die Frauen aufnehmen, können *prinzipiell* nicht in den Österreichischen Cartellverband aufgenommen werden.

> Das letzte, im Umgang der Mitglieder miteinander essenziellste Prinzip, ist aber weder die Religion mit ihren unzähligen Verhaltensregeln noch die Universität, Ursprung des gesamten Verbandes, sondern Amicitia – die Lebensfreundschaft. Sie ist der Kitt, der die Mitglieder innerhalb der einzelnen Verbindungen und das ganze Gefüge des Österreichischen Cartellverbands zusammenhält. Sie ist der Vertrauensvorschuss, den die Verbindungsmitglieder untereinander genießen; das Versprechen, einander zu helfen, wenn die Situation sich offenbart und man selbst in der Position dazu ist. Sie ist zu Ende gedacht aber auch das Prinzip, das Freunderlwirtschaft, Bevorzugung und Bildung von Seilschaften ermöglicht.

Wer jetzt noch Lust hat, sollte vielleicht [den ganzen Artikel lesen](https://www.derstandard.at/story/2000007166981/wer-aus-der-kirche-austritt-der-geht). Im Leben würde ich nicht darauf kommen, bei *soetwas* dabei sein zu wollen.

## Ende Cartellverband

Ich frage mich ja, woher das Geld kommt, welches das doppelte Drucken von Wahlplakaten erlaubt – oder die ÖVP lebt jetzt generell nach dem Motto: *Egal wen wir hinstellen (oder auch nicht), unsere 15% hama!* Finde ich persönlich sehr lobenswert, bei der nächsten Wahl sollten sie überhaupt nur mehr einen Sack Erdäpfel (statt Wahlplakaten) hinstellen, das wäre dann wenigstens eine nennenswerte Sozialpolitik, denn die kann man wenigstens fladern und essen. Die schirchen Gfraster von den Schildern helfen mir in der Küche aber maximal beim Speiben.

## Wahlprogramm

Ich konnte leider kein Wahlprogramm finden, daher muss die schlechtgemachte [Seite der Volkspartei](https://stadtkrems.vpnoe.at) herhalten.

Ich dehne jetzt meine Meinung soweit aus und behaupte, dass nur diejenigen ÖVP wählen, welche glauben, eines Tages selbst von dem [System](http://schwarzbuchoevp.at/) profitieren zu können. Aber ihr täuscht euch: Profitiert hat immer nur eine kleine Gruppe, je nach Periode also die Raiffeisen, die Mitglieder des Cartellverband oder bei Kurz sein [eingeschworenes Team](https://kurier.at/politik/inland/wahl/auf-wen-sebastian-kurz-hoert-diese-partie-hat-bei-tuerkis-das-sagen/293.483.134). Noch nie hat bei den ÖVP die bürgerlichen oder kleinunternehmerischen Kreise eine wirkliche Priorität genossen, wirklich wichtig waren immer die Wünsche der [Großspender](https://www.diepresse.com/5334692/kritik-an-12-stunden-arbeitstag-grossspender-haben-ziel-erreicht) - der 12-Stunden-Tag war eine Forderung von Großspender Stefan Pierer, welcher gemeinsam mit [großzügigen Corona-Hilfsgeldzahlungen](https://neuezeit.at/stefan-pierer-vermoegen/) wieder Milliardär wurde.

### Bildungspolitik

Die Bildungspolitik der ÖVP ist schon lange gleich – etwas elitär. Die Gesamtschule wird noch immer abgelehnt, meist aus Angst vor den *Ausländern* in den Städten. Im Detail konnte ich nichts dazu finden.

### Gemeinderatspolitik

Mich irritiert bei den Gemeinderatssitzungen immer mehr, dass die ÖVP meistens gleich mit der FPÖ abstimmt. Konstruktive Kritik kommt selten, teilweise wirken die teilnehmenden Personen eher an substanzloser Kritik interessiert. Selbst für die blödesten Aktionen ist es ihnen nicht zu schade:
- [„Drei Fragen“](https://www.noen.at/krems/oeffentlichkeit-oevp-krems-stellt-drei-fragen-krems-martin-sedelmaier-thomas-hagmann-oevp-krems-print-281307845)
sodass der sonst ruhige Resch auch mal für seine Verhältnisse auszuckt
- [„Novum: Bürgermeister gibt der ÖVP Kontra“](https://www.noen.at/krems/krems-novum-buergermeister-gibt-der-oevp-kontra-krems-politik-reinhard-resch-thomas-hagmann-print-302322200)
und die KLS diese Fragen in ein paar Zeilen zerpflückt:
- [„Offener Brief der KLS an die Kremser ÖVP-Gemeinderatsfraktion“](https://noe.kpoe.at/offener-brief-der-kls-an-die-kremser-oevp-gemeinderatsfraktion/)

Hauptsache dagegen sein und dagegen wettern. Total nutzlose Fundamentalopposition.

Hiermit ist meiner Meinung nach alles gesagt.

### Verkehrs & Umweltpolitik

Die ÖVP hat die derzeitige Lage in Krems zu 90% mitverursacht, durch unvorsichtige Umwidmungen in den Nachkriegsjahren dürfen Grundstückseigentümer praktisch alles machen. Der Gewerbepark ist eine klassische ÖVP-Idee; sowas würden die auch heute noch bauen [siehe Hagenbrunn](https://www.google.com/maps/@48.3425782,16.4644278,417m/data=!3m1!1e3).

Wasser predigen – Wein saufen…, mehr fällt mir bei Blogartikel wie [diesem](https://stadtkrems.vpnoe.at/archiv/krems-darf-nicht-zubetoniert-werden-wann-handeln-sie-herr-resch-141039/) nicht ein.

Und wenn dann doch noch wer so blöd ist und die ÖVP die Klima-Ambitionen abnimmt, darf jene*r gerne daran erinnert werden, das erst [heute die ÖVP mal wieder Klimaschutz im Parlament aktiv blockiert.](https://orf.at/stories/3281906/)

Nur einmal im Klartext: **Wer die ÖVP wählt, verhindert wirksamen Klima & Umweltschutz und ist somit mitverantwortlich für die Klimakrise und all ihren Folgen.** Dieses „Hab ich nicht gewusst“ oder „Die anderen machen auch nix“ lasse ich nicht durchgehen – jeder hat es vollumfassend gewusst. Man hat sich nur selbst angelogen, um die eigene Faulheit zu kaschieren.

### Finanz & Investitionspolitik

In einem Satz zusammengefasst: **Gewinne privatisieren, Verluste verstaatlichen** - am Eindruckvollsten lässt sich das aktuell mit den Gewinnen [mancher Unternehmen (Achtung Ö24)](https://www.oe24.at/coronavirus/zu-hohe-corona-foerderung-brachte-unternehmen-gewinne/507074488) zeigen. In Krems gibt es dazu wenige Beispiele, da hier sowohl die Kontrolle im Gemeinderat besser funktioniert, als auch wesentlich weniger Geld zu verteilen war als wie in der Bundesregierung oder in der Landesregierung.

Ich merke in diesem Zusammenhang auch an, dass das Land Niederösterreich systematisch SPÖ-Gemeinde bestraft, [in dem sie weniger Geld bekommen als ÖVP-Gemeinden](https://www.addendum.org/niederoesterreich/bedarfszuweisungen/). Für mich ist das immer eine Art Androhung, die die ÖVP NÖ in den Raum stellt: Werdet ihr Rot/Grün/Gelb, gibts weniger Geld und auch damit weniger Investitionen. Aus diesem Grund wurde auch seitens der Landesgesundheitsagentur zwecks des Krankenhauses noch nichts gemacht, Aushungern des politischen Gegners nennt man das.

# Conclusio

Mittlerweile hat die ÖVP alle Plakate ausgetauscht und plakatiert mit *Neuer Schwung für Krems* oder mit *Kompetenz*. Da muss ich aufpassen, dass ich mich vor lachen nicht verschlucke, denn wenn die ÖVP für etwas bekannt ist, dann für *frischen Wind* anstatt hemmungsloser Korruption und Freunderlwirtschaft. Man braucht sich nur die [Liste der ÖVP](https://www.krems.at/fileadmin/Dateien/Wahlen/Liste_2_-_Volkspartei_Krems_-_Team_Florian_Kamleitner__OEVP_.pdf) ansehen und man erkennt, dass wieder nur die alte Partie mit dabei ist. Neuer Lack - Rostige (bis löchrige) Schüssel, wer darauf hineinfällt?

Wenigstens führt die ÖVP dieses Jahr einen Feldstudie durch, ob man mit einen politisch unbekannten ÖVP-Apparatschik eine Kommunalwahl gewinnen kann. Meine Spekulation und Hoffnung: Nein.

*Lesen Sie weitere Artikel der Serie [Pappkameraden in Krems](/posts/2022-08-25-die-kremser-pappkameraden/)!*