---
title: 'Sensation: Wünschelrutengeher findet Wasser zwischen Krems und Mautern'
author: Bernhard Steindl
type: post
date: 2019-03-19T13:25:19+00:00
url: /sensation-wasser-zwischen-krems-und-mautern/
featured_image: https://blog.e9a.at/wp-content/uploads/2019/03/proxy.duckduckgo.com_-800x510.jpg
categories:
  - Satire
tags:
  - satire

---
Das Qualitätsmedium &#8222;Heute&#8220; berichtet: Ein Kremser Lehrer wurde Zeuge eines Wunders während dem Heimmarsch von einem Ball. Mit einem Kleiderbügel ausgerüstet, kam er auf die Idee, nach einer &#8222;Wasserader&#8220; zu suchen. Diese obsolete, aus dem Mittelalter stammende Idee, wird heutzutage nur mehr zur Abzocke von gutgläubigen Omas benutzt.

<div class="wp-block-image">
  <figure class="aligncenter"><img src="https://www.mz-web.de/image/8066202/2x1/940/470/258c5b7b62c5d82f36a96791af7bf15e/GE/kleiderbuegel--1237373699812-.jpg" alt="https://www.mz-web.de/image/8066202/2x1/940/470/258c5b7b62c5d82f36a96791af7bf15e/GE/kleiderbuegel--1237373699812-.jpg" /><figcaption>Kleiderbügelverkäufer freuen sich</figcaption></figure>
</div>

Er stolperte um etwa 02:00 Uhr am 17.03.2019 die Mautener Brücke hinauf. Schon nach wenigen Metern stieß der Kleiderbügel durch einen Windstoß nach oben und nach kurzer, keinesfalls nüchternen Beobachtung schien es bestätigt! Es fließt Wasser zwischen Krems und Mautern, über 300 Jahre ist dies niemanden aufgefallen. Mittlerweile gibt es eine ganze Reihe von weiteren Entdeckungen durch die geniale Idee: Auch Wien und Budapest wird von diesem &#8222;Wasser&#8220; durchflossen, manche gaben dem Rinnsal den Namen &#8222;Donau&#8220;. Tausende von Menschen sind mittlerweile unterwegs auf der Suche nach bisher nicht entdeckten Wasseradern. Sogar auf Google Maps ist der Sensationsfund aufgetaucht!

<div class="wp-block-image">
  <figure class="aligncenter"><img loading="lazy" width="1024" height="488" src="https://blog.e9a.at/wp-content/uploads/2019/03/image-1-1024x488.png" alt="" class="wp-image-363" srcset="https://blog.e9a.at/wp-content/uploads/2019/03/image-1-1024x488.png 1024w, https://blog.e9a.at/wp-content/uploads/2019/03/image-1-300x143.png 300w, https://blog.e9a.at/wp-content/uploads/2019/03/image-1-768x366.png 768w, https://blog.e9a.at/wp-content/uploads/2019/03/image-1.png 1673w" sizes="(max-width: 1024px) 100vw, 1024px" /><figcaption> Auf Google Maps wird der neue Fund &#8222;Donau&#8220; benannt </figcaption></figure>
</div>

Neueste Forschungen bestätigen den Fund: Zwischen Krems und Mautern ist tatsächlich ein ziemlich breiter Wasserstrom, der vor vielen Jahren als &#8222;Fluss&#8220; bekannt war. In Windeseile hat die Landesregierung ein neues Forschungszentrum hingebaut, das Landeswünschelrutenzentrum Niederösterreich. [In Großschönau wurde der Wünschelrutenweg Niederösterreich eingerichtet][1], auf den man spielerisch den Umgang mit den Erdstrahlen erlernen kann.

_Anmerkung: Dieser Beitrag ist Satire. Wünschelruten sind Holz- oder Metalläste, die nachweislich und gerichtlich bestätigt nicht funktionieren. Wasseradern gibt es nicht, denn das Grundwasser liegt in Gesteinsschichten und nicht in &#8222;Adern&#8220;. Weitere Informationen gibt es bei_ [_Wikipedia_][2]_, [selbst die staatliche Asfinag][3] hatte Probleme mit Esoterikern_

 [1]: http://www.wuenschelrute.at/
 [2]: https://de.wikipedia.org/wiki/W%C3%BCnschelrute
 [3]: https://derstandard.at/3124638/Die-Pendelprofis-der-Asfinag