---
# Title
title: "KLS Krems - Wir sind keine Kommunisten"
# Enter the author's name
author: Bernhard Steindl
# Customize URI; default: post/filename
#url: /yubikey-fuer-linux-login-als-u2f-2-faktor-verwenden/
# Date
date: 2022-08-31T20:00:00+01:00
# Is this draft?
draft: false
# Show Table of Content?
showToc: true
# Available categories. Please comment out!
categories:
#- Admin-Tipps
#- Allgemein
#- Arch Linux
#- Debian
#- Denkt's noch!
#- Grundlagen
#- Hardware
#- mariadb
#- News
#- nginx
- Politik
#- Projekte
#- Proxmox
#- Rants
#- Satire
#- Synology
#- Tor
#- Ubiquiti Unifi
#- Windows
#- Wordpress
#- ZFS

---

# KLS

*Dies ist ein Artikel der Serie [Pappkameraden in Krems](/posts/2022-08-25-die-kremser-pappkameraden/). Sämtliche Inhalte genügen keinen journalistischen Ansprüchen wie Ausgewogenheit oder DoubleChecks, falls Sie dennoch falsche Informationen finden oder Anmerkungen zu den Artikeln haben, würde ich mich über eine [Rückmeldung](/impressum) sehr freuen.*

{{< figure src="images/kls1.jpeg" caption="Links der Koch vom Schmids und nebenbei Gemeinderat Wolfgang Mahrer" >}}
{{< figure src="images/kls2.jpeg" caption="Gemeindewohnungen bauen: Ob sie das wo durchbringen?" >}}
{{< figure src="images/kls3.jpeg" caption="Die Plakate sind wohl die Einzigen, welche nicht durch Leere oder Lügen glänzen. Abgesehen von nachhaltig, denn das ist wohl keine der antretenden Listen." >}}

Eine jede gute Stadt hat eine kommunistische Liste, aber die KPÖ Krems hat sich ja bekanntlich umbenannt. Das hat übrigens in Deutschland wirklich funktioniert, da gibt es auch eine Partei links der SPD mit dem Namen *Die Linke*. Dass es eine gscheite linke Partei geben muss, ist mir hinreichend bewusst. Die SPÖ hat schon lange nicht mehr mit soziale Politik geglänzt, man blicke nur nach Tirol [(Link: Achtung krone.at!)](https://www.krone.at/2059947)

Nun wieder zurück zu den Tatsachen: Im Prinzip fallen Wolfgang Mahrer (nicht zu verwechseln mit [Harald Mahrer](https://de.wikipedia.org/wiki/Harald_Mahrer)) & co. in der Kremser Politik hauptsächlich durch kleinere populistische Maßnahmen wie das Aufstellen eines Bankomaten und Stellen vieler Anträge im Gemeinderat auf.

Zu meiner Überraschung gibt es eine [Website](https://www.kls-krems.at/) mit [Wahlprogramm](https://www.kls-krems.at/was-wir-wollen/), [Kandidaten](https://www.kls-krems.at/wer-wir-sind/), [Prinzipien](https://www.kls-krems.at/ueber-uns/) und kleinere Blogposts über alle Gemeinderatssitzungen. Vorbildlich, keine andere Partei – außer der SPÖ – hat das ähnlich gut gemacht.

## Wahlprogramm

Das Wahlprogramm findet ihr auf der [KLS-Homepage](https://www.kls-krems.at/was-wir-wollen/) oder [hier](images/KSK_Aug_22_web.pdf).

#### Bildungspolitik

Ein interessanter Punkt ist der Aufbau einer städtischen Kleinstkindbetreuung, in der Regel ist das für Kinder unter 2,5 Jahre. Das ist tatsächlich sinnvoll, denn in dieser nicht allzu kleinen Nische verdienen sich private Anbieter eine goldene Nase und es sorgt (freiwillig!) für einen schnellere Rückkehr der Frauen auf den Arbeitsmarkt.

Weitere Punkte sind nur generisch, immerhin ist die Wirkungskraft der Stadt im Bildungswesen abgesehen von Gebäude & Elementarpädagogik sehr eingeschränkt.

#### Gemeinderatspolitik

Die KLS selbst glänzt immer wieder durch Dringlichkeitsanträge und interessanten Initiativen. Da die SPÖ die KLS aufgrund des Wahlergebnisses von 2017 für die Mehrheiten im Gemeinderat benötigt, hatten sie in der Stadt bis jetzt für ihr Wahlergebnis unproportional viel mitzureden und konnten einige Anträge abändern. Wer sich dafür interessiert kann sich den [Blog der Partei](https://www.kls-krems.at/category/gemeinderat/) durchlesen und mit den jeweiligen [Aufzeichnungen der Sitzungen](https://www.youtube.com/channel/UCL6vtYKsiryjNN7Ma9pA4tQ) vergleichen.

Allgemein lässt sich sagen, dass die Liste - eine gemeinsame Partei ist es ja nicht - im Gemeinderat für die überschaubare Anzahl von 2 Mandataren beeindruckend viel Redezeit und Anträge abliefern. Da wirkt die ÖVP mit ihren 11 Mandataren wie eine Aussenstelle des Thorwestenheimes. Konkrete Zahlen kann ich nicht nennen, da ich online weder Protokolle noch Informationen dazu fand - alle Sitzungen der letzten Periode nachzusehen hätte den Zeitrahmen wohl auch gesprengt.

Die Mandatare der KLS haben keinen Fraktionszwang - das ist auch meiner Meinung nach der einzige Weg! Für was wählt man Mandatare, wenn dann eh der Fraktionsvorsitzende bestimmt, wann die Pfote gehoben wird? Da können sich ja gleich die 5 Hansln ins Hofbräuhaus setzen und das ohne Polit-Theater abstimmen.

Aufhorchen ließ Gemeinderat Mahrer bei der letzten Sitzung, in der er von einem FPÖ-Mandatar Kommunist bezeichnet wurde, mit einem lautstarken Zwischenruf, dass er seit 1991 kein Kommunist mehr sei! Faktisch mag das stimmen, aber wer nimmt schon einen FPÖ-ler ernst, welcher im [Immobiliengeschäft](https://www.firmenabc.at/person/zoehrer-martin_mrawvt) tätig ist? Das ist halt die soziale National-Partei!

#### Verkehrs & Umweltpolitik

Mit der Verkehrspolitik bin ich weitgehend d'accord, nur lehne ich aus wissenschaftlichen Gründen *jeglichen Ausbau von Autoinfrastruktur* ab. Um Verkehr zu verringern muss man Straßen und Parkplätze verringern, nicht das Gegenteil! Man bekämpft Alkoholismus ja auch nicht mit mehr Alkohol sondern mit weniger, aber hier soll es das Heilmittel sein.

Positive Punkte des Parteiprogrammes: 
- Flächendeckender 30er in *dicht bebauten Zonen*
- Mehr Radwege
- Radabstellplätze (auf die Idee kommen die anderen Fraktionen nicht!)
- Gehsteige bauen (Revolutionäre Idee)
- Bahnausbau (Stadt Krems nicht zuständig)
- Stadtbus ausbauen
- Dynamisches Parkleitsystem (Das muss eigentlich nicht dynamisch sein, man müsste den Leuten nur endlich eintrichtern, dass es eigentlich Parkhäuser auch gibt)
- Dauerparkberechtigungen nur mehr für Personen mit Hauptwohnsitz in Krems

Negative Punkte:
- Straßenausbau
- Parkplätze billiger machen (Dauerparkkarten, weniger Gebühren)

### Finanz & Investitionspolitik

Ein wichtiger Punkt laut dem Programm ist leistbares Wohnen und der Neubau von Gemeindewohnungen. Zuerst habe ich mich gewundert - die Stadt Krems soll (wieder) Wohnungen bauen? Dazu hat sie ja die GEDESAG, die dies ja seit vielen Jahren erledigt und zu 99% der Stadt gehört. Eine eigene Bauabteilung zu gründen, wäre wohl Geldverschwendung, daher wird im nächsten Satz angemerkt: Die *GEDESAG* soll das machen.

Gut, unterschreibe ich gerne, vorher müsste man die Geschäfte und Vergaben der GEDESAG [kontrollieren](https://www.noen.at/krems/krems-abfuhr-fuer-opposition-keine-gedesag-kontrolle-krems-gedesag-gemeinderat-krems-164793246). War übrigens ein Antrag der KLS, ihnen ist das Problem wohl bewusst, niedergeschmettert von *SPÖ* und *ÖVP* die wohl irgendetwas zu verbergen haben. Mehr spekuliere ich hier nicht, aber das man hier die Finger drinnen hat, wird in Österreich niemanden wundern.

Die generelle Erhöhung der Richtwertmieten auszusetzen sehe ich gemischten Auges, sind doch die Erhaltungskosten sowohl für private als auch für gemeinnützige und städtische Wohnungen auch gestiegen. Wenn nach 20 Jahren renoviert werden muss und es ist kein Geld im Topf, muss [die öffentliche Hand ordentlich nachhelfen](https://www.report.at/bau-immo/11912-millionen-fuer-den-wohnbau). Hier muss natürlich Balance gehalten werden, denn nicht jeder, welcher in einer GEDESAG-Wohnung wohnt, ist arm. Falls sich jemand die Miete nicht mehr leisten kann, muss es umfangreiche Hilfsangebote geben, vor allem von den Gesellschaften und der Stadt direkt. Es ist nicht Aufgabe einer Partei wie [der KPÖ Graz](https://www.kpoe-graz.at/mieternotruf.phtml), solche Dienste anzubieten. 

Das ist natürlich nur der Fall, wenn eine Partei oder ein paar Personen davon nicht direkt von Wohnungsspekulation und schlechten Verträgen profitieren. Das gibt es leider auch ...

### Weitere Punkte

Einmalig ist auch der Einsatz für ältere Mitbürger, etwa durch Engagement für neue Bankomaten oder dafür zu sorgen, dass ältere Menschen, welche keinen Internetzugriff haben, nicht benachteiligt werden. (Beispielsweise kostet in Wien ein Parkpickerl offline erworben wesentlich mehr als wie online).

# Conclusio

Eine ordentlich linke Partei also? Kommt darauf an. Wie immer empfehle ich, die Wahlprogramme der Parteien und die bisherigen Tätigkeiten in der Stadt durchzulesen. Die KLS fällt hier nicht schlecht auf, das kann aber auch an der kleinen Fraktionsgröße liegen. Auf jeden Fall negativ zu bewerten ist die geringe Frauenquote der Mandatare (derzeit 0%), auf Platz 3 der Liste wäre aber schon eine Frau. Am Ende des Tages fehlt aber auch der Wille zu regieren (klassische Oppositions & Kontrollpartei) im Gegensatz zur KPÖ Graz, welche letztes Jahr dort 28,84% der Stimmen bekam und seitdem die Stadtregierung anführt.

*Lesen Sie weitere Artikel der Serie [Pappkameraden in Krems](/posts/2022-08-25-die-kremser-pappkameraden/)!*