---
# Title
title: "ThinkPad T480e - mit Classic Keyboard und 4K-Bildschirm"
# Enter the author's name
author: Bernhard Steindl
# Customize URI; default: post/filename
# url: /yubikey-fuer-linux-login-als-u2f-2-faktor-verwenden/
# Date
date: 2022-02-12T19:30:00+01:00
# Is this draft?
draft: false
# Show Table of Content?
showToc: true
# Available categories. Please comment out!
categories:
#- Admin-Tipps
#- Allgemein
#- Arch Linux
#- Debian
#- Denkt's noch!
#- Grundlagen
- Hardware
#- mariadb
#- News
#- nginx
#- Politik
#- Projekte
#- Proxmox
#- Rants
#- Satire
#- Synology
#- Tor
#- Ubiquiti Unifi
#- Windows
#- Wordpress
#- ZFS

cover:
    image: "images/front.jpg"
    # can also paste direct link from external site
    # ex. https://i.ibb.co/K0HVPBd/paper-mod-profilemode.png
    alt: "T480e Front"
    caption: "T480 in seiner vollen Pracht"
    relative: false # To use relative path for cover image, used in hugo Page-bundles

---

# Der Werdegang

Als hauptberuflicher IT-ler bekommt man des Öfteren museumsreife Hardware unter die Hände. IBM T60, R61, auch das 400er sind sehr alt und mit dem Core 2 Duo-Prozessor kaum benutzbar. Am besten
schnitt noch das X201 Tablet ab, leider war bei keinem Exemplar der Stift dabei, die Geräte waren aber noch benutzbar. Leider ist sowohl das Display als auch die CPU sogar für meine Tasks nicht
zu gebrauchen.

Doch ein Detail fällt auf: Die Eingabegeräte.

Das sogenannte ThinkPad-Classic-Keyboard ist mehr wie eine Tastatur, es ist vermutlich die beste Notebook-Tastatur die jemals gebaut wurde. Zeitnah mit dem Ende der Classic-ThinkPads 2013 wurden
auch die Tastaturen durch eine modernere Chiclet-Variante ausgetauscht. Das war natürlich noch alles vor meiner Zeit, ich hatte bereits als erstes Notebook ein Sony VAIO, welches aber ein unglaublich
furchtbares Gerät war.

Aus irgendeinen alten Lager hatte ich bereits ein T60 gefunden, dessen Tastatur mir ins Auge sprang. Als mir dann mein Dell XPS 15 mal wieder einging (Akku exploding, ist glaube ich heute noch so), musste ich zwei Wochen mit dem alten Ding durchkommen. Hört sich schlimm an, ist es aber nicht.

Die schlimmsten Punkte waren folgende:
- Das furchtbare Display (1024xirgendwas)
- realtiv wenig RAM (2017 war 4GB schon recht wenig)
- Prozessor ist abgesehen von Virtualisierung recht brauchbar

Natürlich dapackte dieses Ding kein Gnome, sondern XFCE musste herhalten. Der erste Eindruck war aber: Für ein 15 Jahre altes Notebook begeistert einem dieses Ding. Somit waren zwei Sachen klar: 1. Der Dell muss weg, 2. Das nächste Notebook wird ein ThinkPad.

Blöderweise hatten sie damals schon die alten Keyboards versenkt und darum musste ich zu einem modernen greifen. Diese sind auch um Jahrzehnte besser, als alles, das die Konkurrenz baut. Somit wurde es ein X1 Carbon, das aber ein Montagsgerät war. Nach 3 getauschten Mainboards reichte es mir und nachdem auch die Garantie zu Ende ging, wurde dieses verkauft und ein neues angeschafft, mittlerweile ein T14 AMD.

# Die Idee

Das T14 ist wirklich ein grandioses Notebook: Tolle Tastatur, gutes Display, relativ leicht und sehr viel Leistung. Doch leider gab es zwei nervige Punkte, die sich bis heute nicht gelöst haben:
1. Der eingebaute Akku ist mit 50Wh relativ klein und der Ryzen zwar schnell, aber auch Energiehungrig.
2. Ich brauche die Rechenleistung einfach nicht. Nachdem meine Arbeit hauptsächlich darin besteht, in Texteditoren und Shells herumzuackern, brauche ich die Leistung einfach nicht. Minecraft läuft eh auf jeden Rechner.

Somit plante ich bereits sein Monaten, den Traum wahrwerden zu lassen und mir ein (relativ) aktuelles Notebook mit der alten Tastatur bauen zu können.

# Lenovo's verkacken

Mit dem Abgang des ThinkPad-Chefdesigners David Hill wurde die 25-Jahr-Anniversary-Edition veröffentlicht. Dies bekam durchwegs [vernichtende Kritiken](https://www.youtube.com/watch?v=UxQGhqF60zE), und das zurecht! Ein paar Monate später wurde das T480 auf der selben Hardwarebasis, aber mit i7-Quadcore veröffentlicht. Abseits vom Preis, der total absurd war (siehe [Geizhals](https://geizhals.at/lenovo-thinkpad-25-anniversary-edition-20k70000ge-a1707467.html)) und ein T470 mit besserer Tastatur war.

Ab dem T490 wurde die Hardwarebasis des T480s verwendet und somit vielen eine Menge toller Features losgelassen:
- Lenovo PowerBridge (Ein interner und ein externer Akku)
- Steckbarer RAM (nur mehr ein RAM-Riegel steckbar, der andere Teil fix verlötet)
- Steckbares Keyboard nurmehr bei der Hauptserie
- Im Gehäuse verbauter LAN-Port anstatt anständig verbautem (in meinem T14 ging dieser ein)

Somit sind Notebooks ab dem T490 nicht mehr mit dem ThinkPad 25 kompatibel und können auch die Tastatur dessen nicht mehr verwenden.

# und was die Community draus machte

Nun ist die Community um die ThinkPad-Reihe riesig. Das ist deshalb so, dass vor allem die T400-430-Reihe in unglaublicher Stückzahl produziert wurden und auch noch heute brauchbare Notebooks sind. Einige Modder haben sogar in alte X210 und T60 mit aktueller CPU und Thunderbolt ausgestattet! Zwar aufgrund der Kleinserien recht teuer, aber für Liebhaber zählt nicht der Preis sondern die Exklusivität ;-)

Somit haben findige Modder daran gearbeitet, die T25-Tastatur in ein T480 umzubauen. Das funktioniert erstaunlich einfach und zuverlässig:
- [Beispiel 1](https://thinkpad-forum.de/threads/das-thinkpad-25-2-der-thinkpad-25-frankenpad-umbau-versuchsbericht.220152/) (Deutsches ThinkPad-Forum)
- [Beispiel 2](https://www.reddit.com/r/thinkpad/comments/ewtm36/perfect_t480_modification_with_thinkpad_25th_7row/) (Reddit)
- [Beispiel 3](https://www.xyte.ch/mods/t25-frankenpad/) (Modder aus Singapur -> Gibt sogar fertige Kits zu kaufen: [Shop](https://www.xyte.ch/shop/t25-frankenpad-kit/))
- [Beispiel 4](https://kitsunyan.github.io/blog/frankenpad-story.html) (GitHub)

# Mein Projekt

{{< figure align=center src="images/front.jpg" caption="Das T480 in seiner vollen Pracht." >}}

Sowas möchte ich auch! Daher plante ich schon länger, mir ein Custom T480 zusammenzubauen. Ich hatte bereits folgende Komponenten beinander:
- T470 als Ersatzteillager
- T480 auf willhaben um 480€ zusammengekauft
- T25-Teile von Modder zusammengekauft

als ich ein bereits fertig umgebautes am Marktplatz um wenig Geld fand. Somit fand bei mir kein Umbau statt, das T480 und T470 wurde bei den Bekannten verteilt und somit einem sinnvollen Zweck zugeführt. Folgende Ausstattung hat mein T25 Frankenpad derzeit:
- i7-8550
- 32GB DDR4-RAM
- 4K-Display vom X1 Carbon (500 Nits), frisst viel Strom (bis zu 8 Watt) aber das ist es wert
- Intel AX210 (statt der usrprünglich verbauten 8265 WLAN-Karte)
- Fibocom L380-EB (statt der ursprünglich verbauten 850-GL)
- 1TB SSD vom T14 (Samsung Evo Plus)
- Neuer interner Akku 24Wh, 1x 72Wh extern + 1x 42Wh extern

Leider hat es auch einen Grund, warum das fertig umgebaute Ding relativ günstig war: Auf der Seite sieht man Bearbeitungsspuren, ein paar Spalten sind nicht ganz zu (ganz ehrlich, jedes Mal wenn ich ein T480 öffne, brechen mir ein paar Klippser ab ...). Aber mir micht das ehrlich gesagt nix.

## Fotos

{{< figure align=center src="images/keyboard.jpg" caption="Tastatur von oben, ungeputzt" >}}
{{< figure align=center src="images/side.jpg" caption="Hier hat der Vorbesitzer ein paar Schäden hinterlassen - man kann's ihn nicht übelnehmen, man muss schon ziemlich herumfuhrwerken wenn man das alte Keyboard einbauen möchte" >}}

# Zukunftspläne

Natürlich, die CPU ist bereits etwas älter und wird nicht ewig halten. Es gibt für mich zwei "Exit"-Strategien:

1. Jemand baut irgendwann ein Custom-Mainboard mit aktueller CPU für das T480, ist ja heute schon eines der beliebtesten Notebooks
2. Die 30th Anniversary Edition hat auch die 7-reihige Tastatur, aber darauf gibt es derzeit keine Hinweise

Bis dahin werde ich mich mit Ersatzteile eindecken, die externen Akkus sind jetzt schon quasi nicht mehr zu bekommen ...