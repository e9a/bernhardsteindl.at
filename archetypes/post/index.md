---
# Title
title: "000 Example"
# Enter the author's name
author: Bernhard Steindl
# Customize URI; default: post/filename
url: /yubikey-fuer-linux-login-als-u2f-2-faktor-verwenden/
# Date
date: 2022-01-02T18:21:11+01:00
# Is this draft?
draft: true
# Show Table of Content?
showToc: true
# Available categories. Please comment out!
categories:
#- Admin-Tipps
#- Allgemein
#- Arch Linux
#- Debian
#- Denkt's noch!
#- Grundlagen
#- Hardware
#- mariadb
#- News
#- nginx
#- Politik
#- Projekte
#- Proxmox
#- Rants
#- Satire
#- Synology
#- Tor
#- Ubiquiti Unifi
#- Windows
#- Wordpress
#- ZFS

---

# For syntax tips, look here: https://hugoloveit.com/basic-markdown-syntax/ and for syntax highlighting https://gohugo.io/content-management/syntax-highlighting/
