baseURL: https://bernhardsteindl.at
languageCode: de-at
title: Bernhard Steindl's Blog
paginate: 5
theme: ["stoalPaperMod"]

enableRobotsTXT: true
buildDrafts: false
buildFuture: false
buildExpired: false

#googleAnalytics: off

minify:
  disableXML: true
  minifyOutput: true

services:
  twitter:
    disableInlineCSS: true
    enableDNT: true
    simple: true
  youtube:
    privacyEnhanced: true

params:
  env: production # to enable google analytics, opengraph, twitter-cards and schema.
  title: Bernhard Steindl's Blog
  description: "Website von Bernhard Steindl"
  keywords: [Blog, Portfolio]
  author: Bernhard Steindl
  # author: ["Me", "You"] # multiple authors
  #images: ["<link or path of image for opengraph, twitter-cards>"]
  DateFormat: "January 2, 2006"
  defaultTheme: auto # dark, light
  disableThemeToggle: false

  ShowReadingTime: true
  ShowShareButtons: false
  ShowPostNavLinks: true
  ShowBreadCrumbs: true
  ShowCodeCopyButtons: true
  disableSpecial1stPost: false
  disableScrollToTop: false
  comments: false
  hideFooter: true
  hidemeta: false
  hideSummary: false
  showtoc: false
  tocopen: false

  assets:
    # disableHLJS: true # to disable highlight.js
    disableFingerprinting: true
    favicon: "/favicon.ico"
    favicon16x16: "/favicon-16x16.png"
    favicon32x32: "/favicon-32x32.png"
    apple_touch_icon: "/apple-touch-icon.png"
    safari_pinned_tab: "<link / abs url>"

  label:
    text: "bernhardsteindl.at"
    #icon: /apple-touch-icon.png
    #iconHeight: 35

  # profile-mode
  profileMode:
    enabled: false # needs to be explicitly set
    title: ExampleSite
    subtitle: "This is subtitle"
    imageUrl: "<img location>"
    imageWidth: 120
    imageHeight: 120
    imageTitle: my image
    buttons:
      - name: Posts
        url: posts
      - name: Tags
        url: tags

  # home-info mode
  homeInfoParams:
    Title: "Willkommen auf bernhardsteindl.at"
    Content: Politik, Technik und a bissl Verkehr. Schreibt's ma afoch a Mail, wenns a eichan Senf dazua ogem wöts.

  socialIcons:
    - name: email
      url: "mailto:kontakt@e9a.at"
    - name: mastodon
      url: "https://voi.social/@bsteindl"
    - name: twitter
      url: "https://twitter.com/b_steindl"
    - name: instagram
      url: "https://www.instagram.com/bernhard_das_steindl/"
    - name: rss
      url: "index.xml"
    - name: gitlab
      url: "https://gitlab.com/bernhardsteindl"

  cover:
    hidden: false # hide everywhere but not in structured data
    hiddenInList: false # hide on list pages and home
    hiddenInSingle: true # hide on single page
    responsiveImages: true # To enable/disable generation of responsive cover images

  editPost:
    URL: "https://gitlab.com/bernhardsteindl/bernhardsteindl.at/-/tree/master/content/"
    Text: "Änderungen vorschlagen" # edit text
    appendFilePath: true # to append file path to Edit link

  # for search
  # https://fusejs.io/api/options.html
  fuseOpts:
    isCaseSensitive: false
    shouldSort: true
    location: 0
    distance: 1000
    threshold: 0.4
    minMatchCharLength: 0
    keys: ["title", "permalink", "summary", "content"]
menu:
  main:
    - identifier: suche
      name: suche
      url: /search
      weight: 10    
    - identifier: kategorien
      name: kategorien
      url: /categories/
      weight: 20
    - identifier: blog
      name: posts
      url: /posts/
      weight: 30
    - identifier: impressum
      name: impressum
      url: /impressum
      weight: 50
# Read: https://github.com/adityatelange/hugo-PaperMod/wiki/FAQs#using-hugos-syntax-highlighter-chroma
# pygmentsUseClasses: true
# markup:
#     highlight:
#         # anchorLineNos: true
#         codeFences: true
#         guessSyntax: true
#         lineNos: true
#         style: monokai
outputs:
    home:
        - HTML
        - RSS
        - JSON # is necessary
